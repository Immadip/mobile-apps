﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using System.Configuration;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using BusinessEntities.PushNotifications;
using DataAccessLayer.PushNotifications;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Linq;
using System.IO;
using System.Security.Authentication;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web;

namespace BusinessLayer.PushNotifications
{
    public class PushNotificationsBl
    {
        private readonly string _gcmApiKey;
        private readonly string _gcmSendNotificationsUrl;
        private readonly string _apnsSendNotificationsUrl;
        private readonly int _apnsSendNotificationsPort;
        private readonly string _wnsPackageSID;
        private readonly string _wnssApplicationSecrets;


        /// <summary>
        /// This contructor of class PushNotificationsBl
        /// It calls configuration manager for set api key for android platform
        /// and apn send notification url and port for iOS platform. 
        /// </summary> 
        public PushNotificationsBl()
        {
            _gcmApiKey = ConfigurationManager.AppSettings["GcmApiKey"];
            _gcmSendNotificationsUrl = ConfigurationManager.AppSettings["GcmSendNotificationsUrl"];
            _apnsSendNotificationsUrl = ConfigurationManager.AppSettings["ApnsSendNotificationsUrl"];
            _apnsSendNotificationsPort = int.Parse(ConfigurationManager.AppSettings["ApnsSendNotificationsPort"]);
            _wnsPackageSID = ConfigurationManager.AppSettings["PackageSID"];
            _wnssApplicationSecrets = ConfigurationManager.AppSettings["ApplicationSecrets"];
        }

        /// <summary>
        /// This function send notification to specific platform  depends on  device operating system  
        /// </summary>      
        /// <returns>void</returns>
        public void SendNotifications()
        {
            var notifications = GetTechNotifications();
            foreach (var notification in notifications)
            {
                Thread.Sleep(1000);
                switch (notification.DeviceOperatingSystem)
                {
                    case DeviceOperatingSystem.Android:
                        int notificationCount = GetNotificationCountDal.ExecuteSp(notification.DeviceID, notification.UserId);
                        notificationCount++;
                        SetNotificationCountDal.ExecuteSp(notification.DeviceToken, notification.UserId, notificationCount);
                        SendGcmNotification(notification, notificationCount);
                        break;

                    case DeviceOperatingSystem.Ios:
                        //SendApnsNotification(notification);
                        break;

                    case DeviceOperatingSystem.Windows:
                        SendWnsNotification(notification);
                        break;


                    default:
                        throw new ArgumentOutOfRangeException(nameof(notification.DeviceOperatingSystem), "Device Operating System for the Notification is invalid");
                }
            }
        }

        /// <summary>
        /// This function execute store procedure("SP_VDW_GET_PUSHNOTIF_MOB") to send push notification
        /// </summary>      
        /// <returns>void</returns>
        private IEnumerable<NotificationEntity> GetTechNotifications()
        {
            return GetPushNotifications.ExecuteSp();
        }

        /// <summary>
        /// This function GCM(Google Cloud Messaging) for android Operating System to send push notification.
        /// It's gathering all details of notification and sent it to android device
        /// </summary>
        /// <param name="NotificationEntity">operating system name passes as string</param>
        /// <returns>void</returns>
        private async void SendGcmNotification(NotificationEntity notification, int notificationCount)
        {
            var jData = new JObject
            {
                {"UserId", notification.UserId }, {"NotificationId", notification.NotificationId }, {"NotificationTitle", notification.NotificationTitle}, {"NotificationText", notification.NotificationText}, {"NotificationReceivedTime", notification.NotificationReceivedTime}, {"NotificationCount", notificationCount},
            };

            var jGcmPayload = new JObject { { "data", jData } };

            //jGcmPayload.Add("to", "/topics/" + notification.UserId);
            jGcmPayload.Add("to", "/registration_ids/" + notification.DeviceToken);

            var url = new Uri(_gcmSendNotificationsUrl);
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "key=" + _gcmApiKey);
                //client.DefaultRequestHeaders.TryAddWithoutValidation("Sender: id={0}", notification.DeviceToken);

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

                await
                    client.PostAsync(url,
                        new StringContent(jGcmPayload.ToString(), Encoding.Default, "application/json"))
                        .ContinueWith(
                            response => { Console.WriteLine("Message sent to: " + notification.UserId + "."); });

#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            }
        }
        /// <summary>
        /// This function APN(Apple Push Notification) for iOS Operating System to send push notification.
        /// It's gathering all details of notification and sent it to apple iphone device
        /// Developer needs to generate .p12 certificate from mac using  application certificate
        /// Also,ConvertHexStringToByteArray very important for convert hex string to byte array, if it not proper then push notification will no received at apple device
        /// </summary>
        /// <param name="NotificationEntity">operating system name passes as string</param>
        /// <returns>void</returns>
        private async void SendApnsNotification(NotificationEntity notification)
        {
            //load certificate

            int notificationCount = GetNotificationCountDal.ExecuteSp(notification.DeviceID, notification.UserId);
            notificationCount = notificationCount + 1;
            string certificatePath = @"Certificates\VDWCertificates.p12";
            string certificatePassword = "Infi_123";
            X509Certificate2 clientCertificate = new X509Certificate2(certificatePath, certificatePassword);
            X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);

            TcpClient client = new TcpClient(_apnsSendNotificationsUrl, _apnsSendNotificationsPort);
            SslStream sslStream = new SslStream(
                            client.GetStream(),
                            false,
                            new RemoteCertificateValidationCallback(ValidateServerCertificate),
                            null
            );

            try
            {
                sslStream.AuthenticateAsClient(_apnsSendNotificationsUrl, certificatesCollection, SslProtocols.Tls, true);
            }
            catch (AuthenticationException ex)
            {
                client.Close();
                return;
            }

            // Encode a test message into a byte array.
            MemoryStream memoryStream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memoryStream);

            writer.Write((byte)0);  //The command
            writer.Write((byte)0);  //The first byte of the deviceId length (big-endian first byte)
            writer.Write((byte)32); //The deviceId length (big-endian second byte)


            writer.Write(ConvertHexStringToByteArray(notification.DeviceToken.ToUpper()));


            var aps = new JObject {
                { "alert", notification.NotificationText },
                { "badge", notificationCount }

            };

            var payload = new JObject {
                { "aps", aps }
           };

            var jsonPayload = payload.ToString();

            writer.Write((byte)0); //First byte of payload length; (big-endian first byte)
            writer.Write((byte)jsonPayload.Length);     //payload length (big-endian second byte)

            var b1 = Encoding.UTF8.GetBytes(jsonPayload);
            writer.Write(b1);
            writer.Flush();

            var finalBytes = memoryStream.ToArray();
            sslStream.Write(finalBytes);
            sslStream.Flush();

            // Close the client connection.
            client.Close();

            SetNotificationCountDal.ExecuteSp(notification.DeviceToken, notification.UserId, notificationCount);
        }

        /// <summary>
        /// This function conevert hex string value into byte array
        /// </summary>
        /// <param name="hex">hex passes as string</param>
        /// <returns>byte[]</returns>
        public static byte[] ConvertHexStringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        /// <summary>
        /// This function validated server certificate is valid or not. if SslPolicyErrors is "None" means certificate is valid 
        /// return bool for valid  or invalid of certificate.
        /// </summary>
        /// <param name="hex">hex passes as string</param>
        /// <returns>bool</returns>
        private bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }

        /// <summary>
        /// This function WNS(Windows Notification Service) for windows Operating System to send push notification.
        /// It's gathering all details of notification and sent it to windows device
        /// </summary>
        /// <param name="NotificationEntity">operating system name passes as string</param>
        /// <returns>void</returns>
        private async void SendWnsNotification(NotificationEntity notification)
        {
            int notificationCount = GetNotificationCountDal.ExecuteSp(notification.DeviceID, notification.UserId);
            notificationCount++;
            SetNotificationCountDal.ExecuteSp(notification.DeviceToken, notification.UserId, notificationCount);

            string XmlToastTemplate = @"<?xml version='1.0' encoding='utf-8'?><toast><visual><binding template='ToastGeneric'><text>" + notification.NotificationTitle + "</text><text>" + notification.NotificationText + "</text></binding></visual></toast>";
            PostToWns(_wnssApplicationSecrets, _wnsPackageSID, notification.DeviceToken, XmlToastTemplate, "wns/toast", "text/xml");
        }
        public string PostToWns(string secret, string sid, string uri, string xml, string notificationType, string contentType)
        {
            try
            {
                // You should cache this access token.
                var accessToken = GetAccessToken(secret, sid);

                byte[] contentInBytes = Encoding.UTF8.GetBytes(xml);

                HttpWebRequest request = HttpWebRequest.Create(uri) as HttpWebRequest;
                request.Method = "POST";
                request.Headers.Add("X-WNS-Type", notificationType);
                request.ContentType = contentType;

                request.Headers.Add("Authorization", String.Format("Bearer {0}", accessToken.AccessToken));

                using (Stream requestStream = request.GetRequestStream())
                    requestStream.Write(contentInBytes, 0, contentInBytes.Length);

                using (HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse())
                    return webResponse.StatusCode.ToString();
            }

            catch (WebException webException)
            {
                HttpStatusCode status = ((HttpWebResponse)webException.Response).StatusCode;

                if (status == HttpStatusCode.Unauthorized)
                {
                    // The access token you presented has expired. Get a new one and then try sending
                    // your notification again.

                    // Because your cached access token expires after 24 hours, you can expect to get 
                    // this response from WNS at least once a day.

                    GetAccessToken(secret, sid);

                    // We recommend that you implement a maximum retry policy.
                    return PostToWns(secret, sid, uri, xml, notificationType, contentType);
                }
                else if (status == HttpStatusCode.Gone || status == HttpStatusCode.NotFound)
                {
                    // The channel URI is no longer valid.

                    // Remove this channel from your database to prevent further attempts
                    // to send notifications to it.

                    // The next time that this user launches your app, request a new WNS channel.
                    // Your app should detect that its channel has changed, which should trigger
                    // the app to send the new channel URI to your app server.

                    return "";
                }
                else if (status == HttpStatusCode.NotAcceptable)
                {
                    // This channel is being throttled by WNS.

                    // Implement a retry strategy that exponentially reduces the amount of
                    // notifications being sent in order to prevent being throttled again.

                    // Also, consider the scenarios that are causing your notifications to be throttled. 
                    // You will provide a richer user experience by limiting the notifications you send 
                    // to those that add true value.

                    return "";
                }
                else
                {
                    // WNS responded with a less common error. Log this error to assist in debugging.

                    // You can see a full list of WNS response codes here:
                    // http://msdn.microsoft.com/en-us/library/windows/apps/hh868245.aspx#wnsresponsecodes

                    string[] debugOutput = {
                                       status.ToString(),
                                       webException.Response.Headers["X-WNS-Debug-Trace"],
                                       webException.Response.Headers["X-WNS-Error-Description"],
                                       webException.Response.Headers["X-WNS-Msg-ID"],
                                       webException.Response.Headers["X-WNS-Status"]
                                   };
                    return string.Join(" | ", debugOutput);
                }
            }

            catch (Exception ex)
            {
                return "EXCEPTION: " + ex.Message;
            }
        }
        protected OAuthToken GetAccessToken(string secret, string sid)
        {
            var urlEncodedSecret = HttpUtility.UrlEncode(secret);
            var urlEncodedSid = HttpUtility.UrlEncode(sid);

            var body = String.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&scope=notify.windows.com",
                                     urlEncodedSid,
                                     urlEncodedSecret);

            string response;
            using (var client = new WebClient())
            {
                client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                response = client.UploadString("https://login.live.com/accesstoken.srf", body);
            }
            return GetOAuthTokenFromJson(response);
        }
        private OAuthToken GetOAuthTokenFromJson(string jsonString)
        {
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(jsonString)))
            {
                var ser = new DataContractJsonSerializer(typeof(OAuthToken));
                var oAuthToken = (OAuthToken)ser.ReadObject(ms);
                return oAuthToken;
            }
        }

    }
    [DataContract]
    public class OAuthToken
    {
        [DataMember(Name = "access_token")]
        public string AccessToken { get; set; }
        [DataMember(Name = "token_type")]
        public string TokenType { get; set; }
    }

}

