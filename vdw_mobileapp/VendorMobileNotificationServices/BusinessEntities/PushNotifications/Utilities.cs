﻿namespace BusinessEntities.PushNotifications
{
    public enum DeviceOperatingSystem
    {
        Android = 1,
        Ios = 2,
        Windows = 3
    }
}
