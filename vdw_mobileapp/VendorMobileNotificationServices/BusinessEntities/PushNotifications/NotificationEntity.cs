﻿using System;
namespace BusinessEntities.PushNotifications
{
    public class NotificationEntity
    {
        public string UserId { get; set; }
        public long NotificationId { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationText { get; set; }
        public string NotificationType { get; set; }
        public DateTime NotificationReceivedTime { get; set; }
        public DeviceOperatingSystem DeviceOperatingSystem { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceID { get; set; }
    }
}
