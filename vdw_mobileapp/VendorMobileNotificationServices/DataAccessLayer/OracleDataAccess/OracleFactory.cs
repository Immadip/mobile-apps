﻿using System;
using System.Data;
using System.Data.Common;
using Oracle.DataAccess.Client;
using System.Configuration;

namespace DataAccessLayer.OracleDataAccess
{
   public class OracleFactory : GenericDataAccess
    {
        static OracleDbType GetParamType(ParamType paramType)
        {
            switch (paramType)
            {
                case ParamType.String:
                    return OracleDbType.Varchar2;
                case ParamType.Int16:
                    return OracleDbType.Int16;
                case ParamType.Int32:
                    return OracleDbType.Int32;
                case ParamType.Int64:
                    return OracleDbType.Int64;
                case ParamType.Clob:
                    return OracleDbType.Clob;
                case ParamType.Double:
                    return OracleDbType.Double;
                case ParamType.Date:
                    return OracleDbType.Date;
                case ParamType.Cursor:
                    return OracleDbType.RefCursor;
                case ParamType.Time:
                    return OracleDbType.TimeStamp;
                default:
                    throw new Exception("Invalid DataType");
            }
        }

        public override IDbConnection CreateConnection(string connectionString)
        {
            return (IDbConnection)new OracleConnection(
                ""
                );
        }

        public override IDbConnection CreateConnectionFromConnectionString(string connectionString)
        {
            return (IDbConnection)new OracleConnection(connectionString);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public override IDbCommand GetStoredProcCommand(string Storedprocname)
        {
            //Added using block - DISPOSE
            using (OracleCommand command = new OracleCommand())
            {
                command.CommandText = Storedprocname;
                command.CommandType = CommandType.StoredProcedure;
                return (IDbCommand)command;
            }
        }

        public override void AddInParameter(IDbCommand command, string strParamName, ParamType dbType, object paramValue)
        {
            if (command != null)
            {
                //Added using block - DISPOSE
                using (OracleParameter _oraParam = new OracleParameter(strParamName, OracleFactory.GetParamType(dbType)))
                {
                    _oraParam.Direction = System.Data.ParameterDirection.Input;
                    _oraParam.Value = paramValue;
                    command.Parameters.Add(_oraParam);
                }
            }
        }

        public override void AddOutParameter(IDbCommand command, string strParamName, ParamType dbType)
        {
            if (command != null)
            {
                //Added using block - DISPOSE
                using (OracleParameter _oraParam = new OracleParameter(strParamName, OracleFactory.GetParamType(dbType)))
                {
                    _oraParam.Direction = System.Data.ParameterDirection.Output;
                    command.Parameters.Add(_oraParam);
                }
            }
        }

        public override void AddOutParameter(IDbCommand command, string strParamName, ParamType dbType, int size)
        {
            if (command != null)
            {
                //Added using block - DISPOSE
                using (OracleParameter _oraParam = new OracleParameter(strParamName, OracleFactory.GetParamType(dbType), size))
                {
                    _oraParam.Direction = System.Data.ParameterDirection.Output;
                    command.Parameters.Add(_oraParam);
                }
            }
        }

        public override void AddCursorOutParameter(IDbCommand command, string CursorName)
        {
            if (command != null)
            {
                //Added using block - DISPOSE
                using (OracleParameter _oraParam = new OracleParameter(CursorName, OracleDbType.RefCursor))
                {
                    _oraParam.Direction = System.Data.ParameterDirection.Output;
                    command.Parameters.Add(_oraParam);
                }

            }
        }

        public override IDataReader ExecuteReader(IDbCommand oraCommand)
        {
            IDataReader dataReader = null;
            try
            {
                dataReader = oraCommand.ExecuteReader();
            }
            catch (Exception dbEx) //handle generic exception
            {
                //Logger.LogMessage(string.Empty, LogEventId.None, "OracleFactory ExecuteReader", Component.WNO_DISPATCH, dbEx.Message, Severity.Error);
            }

            return dataReader;
        }
        public override DataSet ExecuteDataSet(IDbCommand oraCommand)
        {
            DataSet ds = new DataSet();
            try
            {
                //Added using block - DISPOSE
                using (OracleDataAdapter da = new OracleDataAdapter((OracleCommand)oraCommand))
                {

                    da.Fill(ds);
                }

            }
            catch (Exception argEx) //handle generic exception
            {
                //Logger.LogMessage(string.Empty, LogEventId.None, "OracleFactory ExecuteReader", Component.WNO_DISPATCH, argEx.Message, Severity.Error);
            }

            return ds;
        }
        public override int ExecuteNonQuery(IDbCommand oraCommand)
        {
            int rowsAffected = 0;
            try
            {
                rowsAffected = oraCommand.ExecuteNonQuery();
            }
            catch (Exception dbEx)  //handle generic exception
            {
                //Logger.LogMessage(string.Empty, LogEventId.None, "OracleFactory ExecuteNonQuery", Component.WNO_DISPATCH, dbEx.Message, Severity.Error);
            }

            return rowsAffected;
        }

        public override string GetParameterValue(IDbCommand command, string strParamName)
        {
            if (command != null)
            {
                OracleCommand _oraComand = (OracleCommand)command;
                return _oraComand.Parameters[strParamName].Value.ToString();

            }

            return string.Empty;
        }


        public static DataSet ExecuteData(string commandText, CommandType commandType, bool isCached, string ConnectSchema, string regionId, params OracleParameter[] commandParameters)
        {
            //create & open an OracleConnection, and dispose of it after we are done.
            using (OracleConnection cn = new OracleConnection(getConnectionBasedOnRegion(ConnectSchema, regionId)))
            {
                cn.Open();

                //call the overload that takes a connection in place of the connection string
                return ExecuteData(cn, commandType, commandText, isCached, commandParameters);
            }
        }
        public static DataSet ExecuteData(OracleConnection connection, CommandType commandType, string commandText, bool isCached, params OracleParameter[] commandParameters)
        {
            //create a command and prepare it for execution
            using (OracleCommand cmd = new OracleCommand())
            {
                PrepareCommand(cmd, connection, (OracleTransaction)null, commandType, commandText, commandParameters);
                if (isCached)
                {
                    cmd.AddToStatementCache = true;
                }
                //create the DataAdapter & DataSet
                OracleDataAdapter da = new OracleDataAdapter(cmd);
                DataSet ds = new DataSet();

                //fill the DataSet using default values for DataTable names, etc.
                da.Fill(ds);

                // cmd.Dispose();
                da.Dispose();

                //return the dataset
                return ds;
            }
        }
        private static void PrepareCommand(OracleCommand command, OracleConnection connection, OracleTransaction transaction, CommandType commandType, string commandText, OracleParameter[] commandParameters)
        {
            //if the provided connection is not open, we will open it
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }

            //associate the connection with the command
            command.Connection = connection;

            //set the command text (stored procedure name or Oracle statement)
            command.CommandText = commandText;

            //if we were provided a transaction, assign it.
            if (transaction != null)
            {
                //command.Transaction = transaction;
                //    transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);
            }

            //set the command type
            command.CommandType = commandType;

            //attach the command parameters if they are provided
            if (commandParameters != null)
            {
                AttachParameters(command, commandParameters);
            }

            return;
        }
        private static void AttachParameters(OracleCommand command, OracleParameter[] commandParameters)
        {
            foreach (OracleParameter p in commandParameters)
            {
                //check for derived output value with no value assigned
                if ((p.Direction == ParameterDirection.InputOutput) && (p.Value == null))
                {
                    p.Value = DBNull.Value;
                }

                command.Parameters.Add(p);
            }
        }

        public static int ExecuteNonQueryForWBN(CommandType commandType, string commandText, string ConnectSchema, string regionId, params OracleParameter[] commandParameters)
        {
            //create & open an OracleConnection, and dispose of it after we are done.
            using (OracleConnection cn = new OracleConnection(getConnectionBasedOnRegion(ConnectSchema, regionId)))
            {
                cn.Open();

                //call the overload that takes a connection in place of the connection string
                return ExecuteNonQuery(cn, commandType, commandText, commandParameters);
            }
        }
        public static int ExecuteNonQuery(OracleConnection connection, CommandType commandType, string commandText, params OracleParameter[] commandParameters)
        {
            //Code change for COAD Outage
            using (OracleCommand cmd = new OracleCommand())
            {
                PrepareCommand(cmd, connection, (OracleTransaction)null, commandType, commandText, commandParameters);

                //finally, execute the command.
                return cmd.ExecuteNonQuery();
            }
        }
        public static DataSet ExecuteDataset(string commandText, CommandType commandType, bool isCached, string ConnectSchema, string regionId, params OracleParameter[] commandParameters)
        {
            //create & open an OracleConnection, and dispose of it after we are done.
            using (OracleConnection cn = new OracleConnection(getConnectionBasedOnRegion(ConnectSchema, regionId)))
            {
                cn.Open();

                //call the overload that takes a connection in place of the connection string
                return ExecuteDataset(cn, commandType, commandText, isCached, commandParameters);
            }
        }
        public static DataSet ExecuteDataset(OracleConnection connection, CommandType commandType, string commandText, bool isCached, params OracleParameter[] commandParameters)
        {
            //create a command and prepare it for execution
            using (OracleCommand cmd = new OracleCommand())
            {
                PrepareCommand(cmd, connection, (OracleTransaction)null, commandType, commandText, commandParameters);
                if (isCached)
                {
                    cmd.AddToStatementCache = true;
                }
                //create the DataAdapter & DataSet
                OracleDataAdapter da = new OracleDataAdapter(cmd);
                DataSet ds = new DataSet();

                //fill the DataSet using default values for DataTable names, etc.
                da.Fill(ds);

                // cmd.Dispose();
                da.Dispose();

                //return the dataset
                return ds;
            }
        }
        public static string getConnectionBasedOnRegion(string ConnectSchema, string regionCode)
        {
            //if ((ConnectSchema.Equals("WBN") || ConnectSchema.Equals("VFORCE") || ConnectSchema.Equals("CCP")) && (regionCode == "FL" || regionCode == "CA" || regionCode == "TX" || regionCode == "SE" || regionCode == "TX-SW" || regionCode == "CA-NO" || regionCode == "CA-CN" || regionCode == "CA-SE"))
            //    regionCode = "WEST";

            //String envCode = System.Configuration.ConfigurationManager.AppSettings["ENVCODE"].ToString();
            //String VzOTenvCode = System.Configuration.ConfigurationManager.AppSettings["VZOT_ENVCODE"].ToString();
            //String connString = "";
            //regionCode = "_" + regionCode;
            //switch (ConnectSchema.ToUpper())
            //{
            //    case "CCP":
            //        connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnect_WBN_CCP" + envCode + regionCode].ToString();
            //        break;

            //    case "WBN":
            //        connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnect_WBN" + envCode + regionCode].ToString();
            //        break;

            //    case "VFORCE":
            //        connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnect_vForce" + envCode + regionCode].ToString();
            //        break;

            //    case "VFORCE_AUDIT":
            //        connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnect_vForce_Audit" + envCode + regionCode].ToString();
            //        break;

            //    case "VZOT":
            //        connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnect_VZOT" + VzOTenvCode + regionCode].ToString();
            //        break;

            //    case "DISPCON":
            //        connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnect_DISPCON"].ToString();
            //        break;
            //    //DEFAULT DISPCON
            //    default:
            //        connString = System.Configuration.ConfigurationManager.ConnectionStrings["dbConnection"].ToString();
            //        break;
            //}

            //return connString;
            return "";
        }

    }
}
