﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;
namespace DataAccessLayer.PushNotifications
{
    public class GetNotificationCountDal
    {
        private readonly string _deviceId;
        private readonly string _userId;

        public static string GetProcName()
        {
            return "SP_VDW_GET_NOTIFICATIONS_COUNT";
        }

        private GetNotificationCountDal(string deviceId, string userId)
        {
            _deviceId = deviceId;
            _userId = userId;
        }

        public static int ExecuteSp(string deviceId, string userId)
        {
            OracleConnection con;
            con = new OracleConnection();
            con.ConnectionString =
                ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString; ;
            con.Open();

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.CommandText = GetProcName();
            oraCmd.Connection = con;

            GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
            db.AddInParameter(oraCmd, "P_USER_ID", ParamType.String, userId);
            db.AddInParameter(oraCmd, "P_DEVICE_ID", ParamType.String, deviceId);
            db.AddOutParameter(oraCmd, "O_Notification_Count", ParamType.Int32);

            db.ExecuteNonQuery(oraCmd);

            int count = Convert.ToInt32(db.GetParameterValue(oraCmd, "O_Notification_Count"));

            con.Close();
            con.Dispose();

            return count;
        }
    }
}
