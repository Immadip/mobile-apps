﻿using BusinessEntities.PushNotifications;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace DataAccessLayer.PushNotifications
{
    public class GetPushNotifications
    {
        /// <summary>
        /// This function uses to get store procedure name
        /// </summary>
        /// <param >void</param>
        /// <returns>returns list of notifications</returns>
        public static string GetProcName()
        {
            return ConstantsSp.SP_VDW_GET_PUSHNOTIF_MOB;
        }

        /// <summary>
        /// This function uses to execute the store procedure 
        /// to send push notification
        /// </summary>
        /// <param >void</param>
        /// <returns>returns list of notifications</returns>
        public static List<NotificationEntity> ExecuteSp()
        {
            var con = new OracleConnection
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString
            };
            con.Open();

            var oraCmd = new OracleCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = GetProcName(),
                Connection = con
            };

            var db = GenericDataAccess.CreateDataAccess("ODP");

            db.AddCursorOutParameter(oraCmd, "Records_ref");
            var dsDataset = new DataSet();

            using (var daAdapter = new OracleDataAdapter(oraCmd))
            {
                daAdapter.Fill(dsDataset);
            }

            con.Close();
            con.Dispose();

            return ListOfNotificationsResponse(dsDataset.Tables[0]);
        }

        /// <summary>
        /// This function uses for returns list of notifications 
        /// It returns notification entity details lke notification id,title,text,type and 
        /// device token and operating system of specific device
        /// </summary>
        /// <param name="dataTbl">Data Table passes as input parameter</param>
        /// <returns>returns list of notifications</returns>
        public static List<NotificationEntity> ListOfNotificationsResponse(DataTable dataTbl)
        {
            var lstNotificationDetails = new List<NotificationEntity>();

            if (dataTbl != null)
            {
                lstNotificationDetails = dataTbl.Rows.Cast<DataRow>().Select(row => new NotificationEntity()
                {
                    UserId = row["User_Id"].ToString(),
                    NotificationId = Convert.ToInt64(row["Notification_Id"]),
                    NotificationTitle = row["Notification_Title"].ToString(),
                    NotificationText = row["Notification_Text"].ToString(),
                    NotificationType = row["Notification_Type"].ToString(),
                    NotificationReceivedTime = Convert.ToDateTime(row["Notification_Received_Time"]),
                    DeviceOperatingSystem = MapDeviceOperatingSystem(row["Device_Operating_System"].ToString()),
                    DeviceToken = row["Device_Token"].ToString(),
                    DeviceID = row["Device_ID"].ToString()
                }).ToList();
            }

            return lstNotificationDetails;
        }

        /// <summary>
        /// This function uses for Mapping Device Operating System
        /// </summary>
        /// <param name="operatingSystem">operating system name passes as string</param>
        /// <returns>returns Enumaration of DeviceOperatingSystem</returns>
        private static DeviceOperatingSystem MapDeviceOperatingSystem(string operatingSystem)
        {
            operatingSystem = operatingSystem.ToUpper();
            switch(operatingSystem)
            {
                case "ANDROID":
                    return DeviceOperatingSystem.Android;
                case "IOS":
                    return DeviceOperatingSystem.Ios;
                case "WINDOWS":
                    return DeviceOperatingSystem.Windows;
                default:
                    throw new ArgumentOutOfRangeException(nameof(DeviceOperatingSystem), "Device Operating System for the Notification is invalid");
            }
        }
    }
}