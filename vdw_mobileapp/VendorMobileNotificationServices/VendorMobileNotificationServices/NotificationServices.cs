﻿using BusinessLayer.PushNotifications;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace VendorMobileNotificationServices
{
    class NotificationServices
    {
        static void Main()
        {
            try
            {
                var pushNotificationsBl = new PushNotificationsBl();
                while(true)
                {
                    pushNotificationsBl.SendNotifications();
                    Thread.Sleep(5000);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nException Occurred!");
                Console.Error.WriteLine(ex.Message);
                Console.Error.WriteLine(ex.StackTrace);
                //Exception Logging goes here - Things to look into.
            }
        }
    }
}
