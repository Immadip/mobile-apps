﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class RemarksEntity
    {
        public string JobId { get; set; }
        public string VendorId { get; set; }
        public string TechId { get; set; }
        public string Remarks { get; set; }
        public DateTime RemarksTimeStamp { get; set; }
    }
}
