﻿using BusinessEntities;
using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace QuickJobSearchServices.Controllers
{
    public class QuickSearchRestController : ApiController
    {
        [HttpGet]
        public JobEntity GetJobDetails(string jobID, string regionId)
        {
            try
            {
                GetJobDetailsBL blGetJobSummary = new GetJobDetailsBL();
                JobEntity response = blGetJobSummary.GetJobDetails(jobID, regionId);
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public JobEntity GetJobDetailsFromMultipleDB(string jobID, string regionId)
        {
            try
            {
                GetJobDetailsFromMultipleDBBL blGetJobSummary = new GetJobDetailsFromMultipleDBBL();
                JobEntity response = blGetJobSummary.GetJobDetailsFromMultipleDB(jobID, regionId);
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public JobEntity GetJobDeatilsByOrderID(string orderID, string regionId)
        {
            try
            {
                GetJobDeatilsByOrderIDBL blGetJobDeatilsByOrderID = new GetJobDeatilsByOrderIDBL();
                JobEntity response = blGetJobDeatilsByOrderID.GetJobDeatilsByOrderID(orderID, regionId);
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public List<JobEntity> GetJobListByMON(string mon, string regionId)
        {
            try
            {
                GetJobListByMONBL blGetJobListByMON = new GetJobListByMONBL();
                List<JobEntity> response = blGetJobListByMON.GetJobListByMON(mon, regionId);
                return response;
               
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public bool AddMGRRemarks([FromBody]RemarksEntity remarks)
        {
            try
            {
                JobActionsBL jobActionsBl = new JobActionsBL();
                return jobActionsBl.AddMGRRemarks(remarks);
            }
            catch (Exception ex)
            {
                //Exception Logging goes here - Things to look into.
                return false;
            }
        }

        [HttpGet]
        public List<JobEntity> GetJobListByCommitmentDate(DateTime startCommitmentDate, DateTime endCommitmentDate, string regionId)
        {
            try
            {
                GetJobListByCommitmentDateBL blGetJobListByCommitmentDateBL = new GetJobListByCommitmentDateBL();
                List<JobEntity> response = blGetJobListByCommitmentDateBL.GetJobListByCommitmentDate(startCommitmentDate, endCommitmentDate, regionId);
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public string GetJob()
        {
            return "Hello";
        }

    }
}