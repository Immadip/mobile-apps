﻿using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLayer
{
    public class GetJobDetailsBL
    {
        public JobEntity GetJobDetails(string jobID, string regionId)
        {
            try
            {
                return DataAccessLayer.GetJobDetailsDAL.ExecuteSp(jobID, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
