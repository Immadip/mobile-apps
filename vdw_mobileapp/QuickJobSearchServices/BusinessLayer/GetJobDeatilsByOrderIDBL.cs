﻿using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLayer
{
    public class GetJobDeatilsByOrderIDBL
    {
        public JobEntity GetJobDeatilsByOrderID(string orderID, string regionId)
        {
            try
            {
                return DataAccessLayer.GetJobDeatilsByOrderIDDAL.ExecuteSp(orderID, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
