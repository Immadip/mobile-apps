﻿using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLayer
{   
    public class GetJobListByCommitmentDateBL
    {
        public List<JobEntity> GetJobListByCommitmentDate(DateTime startCommitmentDate, DateTime endCommitmentDate, string regionId)
        {
            try
            {
                return DataAccessLayer.GetJobListByCommitmentDateDAL.ExecuteSp(startCommitmentDate, endCommitmentDate, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
