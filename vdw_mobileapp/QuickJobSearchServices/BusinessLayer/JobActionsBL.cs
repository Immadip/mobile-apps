﻿using System;
using BusinessEntities;
using DataAccessLayer;
using Microsoft.SqlServer.Server;

namespace BusinessLayer
{
    public class JobActionsBL
    {
        public bool AddMGRRemarks(RemarksEntity remarksDetails)
        {
            FormatMGRRemarks(remarksDetails);
            return AddMGRRemarksDAL.ExecuteSp(remarksDetails);
        }

        private void FormatMGRRemarks(RemarksEntity remarksDetails)
        {
            remarksDetails.Remarks += " - " + remarksDetails.VendorId + " - " + String.Format("{0:MM/dd/yyyy HH:mm}", remarksDetails.RemarksTimeStamp);
        }
    }
}
