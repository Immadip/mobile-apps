﻿using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLayer
{    
    public class GetJobListByMONBL
    {
        public List<JobEntity> GetJobListByMON(string mon, string regionId)
        {
            try
            {
                return DataAccessLayer.GetJobListByMONDAL.ExecuteSp(mon, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
