﻿using BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLayer
{    
    public class GetJobDetailsFromMultipleDBBL
    {
        public JobEntity GetJobDetailsFromMultipleDB(string jobID, string regionId)
        {
            try
            {
                return DataAccessLayer.GetJobDetailsFromMultipleDBDAL.ExecuteSp(jobID, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
