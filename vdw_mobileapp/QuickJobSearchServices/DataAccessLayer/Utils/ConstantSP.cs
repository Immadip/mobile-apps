﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Utils
{
    public static class ConstantSP
    {
        public static string SP_VDW_GET_JOBLIST_QUICKSEARCH = "SP_VDW_GET_JOBLIST_QUICKSEARCH";
        public static string SP_MGR_JOBDETAILS_BY_ORDER_ID = "SP_MGR_JOBDETAILS_BY_ORDER_ID";
        public static string SP_MGR_GET_JOBLIST_BY_MON = "SP_MGR_GET_JOBLIST_BY_MON";
        public static string SP_VDW_UPDATE_ADD_REMARKS = "SP_VDW_UPDATE_ADD_REMARKS";
        public static string SP_MGR_GET_JOBS_BY_COMITDATE = "SP_MGR_GET_JOBS_BY_COMITDATE";
        public static string SP_VDW_GET_JOBLIST_COUNT = "SP_VDW_GET_JOBLIST_COUNT";
    }
}
