﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;


namespace DataAccessLayer
{
    public class GetJobListByMONDAL
    {
        private readonly string _regionid;
        private readonly string _mon;

        public static string GetProcName()
        {
            return ConstantSP.SP_MGR_GET_JOBLIST_BY_MON;
        }

        private GetJobListByMONDAL(string regionId, string mon)
        {
            _regionid = regionId;
            _mon = mon;
        }

        public bool MapInput(ref GenericDataAccess db, ref IDbCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            db.AddInParameter(command, "p_mon", ParamType.String, _mon);
            db.AddCursorOutParameter(command, "RECORDS_REF");
            return true;
        }

        public static List<JobEntity> ExecuteSp(string mon, string regionId)
        {
            try
            {
                var ds = new DataSet();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetProcName();
                oraCmd.Connection = con;

                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "p_mon", ParamType.String, mon);
                db.AddCursorOutParameter(oraCmd, "Records_ref");

                DataSet dsDataset = new DataSet();

                //Added using block - DISPOSE
                using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
                {
                    daAdapter.Fill(dsDataset);
                }

                con.Close();
                con.Dispose();

                return MapResponse(dsDataset.Tables[0]);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

       

        public static List<JobEntity> MapResponse(DataTable dataTbl)
        {
            List<JobEntity> lstJobSummaryDetails = new List<JobEntity>();
            try
            {

                if (dataTbl != null)
                {
                    lstJobSummaryDetails = dataTbl.Rows.Cast<DataRow>().Select(row => new JobEntity()
                    {
                        Region = dataTbl.Columns.Contains("awasRegion") ? string.IsNullOrEmpty(row["awasRegion"].ToString()) ? string.Empty : row["awasRegion"].ToString() : string.Empty,

                        JobId = row["JOB_ID"].ToString(),
                        DoOrderId = row["DO_ORDER_ID"].ToString(),
                        Mon = row["mon"].ToString(),
                        JobType = row["job_type"].ToString(),
                        JobStatus = row["JOB_STATUS"].ToString(),
                        CommitmentDate = string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(row["COMMIT_DATE_TIME"].ToString())),
                        CustomerName = row["CUSTOMER_NAME"].ToString(),
                        WireCenter = row["Wire_center"].ToString(),
                        FttpCoreType = row["FTTP_CORE_TYPE"].ToString(),
                        TechCode = row["tech_ec"].ToString(),
                        VdwDispatchDateTime = !string.IsNullOrEmpty(row["vdw_dispatch_datetime"].ToString()) ? Convert.ToDateTime(row["vdw_dispatch_datetime"].ToString()) : (DateTime?)null,
                        VdwRemarks = row["VDW_REMARKS"].ToString(),
                        VendorId = row["vdw_vendor_id"].ToString(),
                        CustomerAddress = row["CUSTOMER_ADDRESS"].ToString(),
                        // Finding the PinCode from Address.
                        PinCode = row["CUSTOMER_ADDRESS"].ToString().Split(new char[] { ' ' }).LastOrDefault(),
                        CustomerRemarks = row["CUSTOMER_STATUS_REMARKS"].ToString(),
                        // Formatting for the Reach Numbers.
                        AltReachNumber = row["ALT_CBR_NUMBER"].ToString()?.Insert(3, "-")?.Insert(7, "-") ?? string.Empty

                    }).ToList();
                }

            }
            catch (Exception ex)
            {

            }

            return lstJobSummaryDetails;
        }
    }
}
