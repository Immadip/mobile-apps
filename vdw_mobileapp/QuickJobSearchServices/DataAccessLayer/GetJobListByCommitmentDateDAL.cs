﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;

namespace DataAccessLayer
{
    public class GetJobListByCommitmentDateDAL
    {
        private readonly string _regionid;
        private readonly DateTime _startCommitmentDate;
        private readonly DateTime _endCommitmentDate;
        

        public static string GetProcName()
        {
            return ConstantSP.SP_MGR_GET_JOBS_BY_COMITDATE;
        }

        private GetJobListByCommitmentDateDAL( DateTime startCommitmentDate, DateTime endCommitmentDate, string regionId)
        {
            _regionid = regionId;
            _startCommitmentDate = startCommitmentDate;
            _endCommitmentDate = endCommitmentDate;
        }

        public bool MapInput(ref GenericDataAccess db, ref IDbCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            db.AddInParameter(command, "p_Start_COMMIT_DATE_TIME", ParamType.DateTime, _startCommitmentDate);
            db.AddInParameter(command, "p_End_COMMIT_DATE_TIME", ParamType.DateTime, _endCommitmentDate);
            db.AddCursorOutParameter(command, "RECORDS_REF");
            return true;
        }

        public static List<JobEntity> ExecuteSp(DateTime startCommitmentDate, DateTime endCommitmentDate, string regionId)
        {
            try
            {
                var ds = new DataSet();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetProcName();
                oraCmd.Connection = con;

                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "p_Start_COMMIT_DATE_TIME", ParamType.Date, startCommitmentDate);
                db.AddInParameter(oraCmd, "p_End_COMMIT_DATE_TIME", ParamType.Date, endCommitmentDate);
                db.AddCursorOutParameter(oraCmd, "Records_ref");

                DataSet dsDataset = new DataSet();

                //Added using block - DISPOSE
                using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
                {
                    daAdapter.Fill(dsDataset);
                }

                con.Close();
                con.Dispose();

                return MapResponse(dsDataset.Tables[0]);
            }
            catch (Exception ex)
            {

            }
            return null;
        }



        public static List<JobEntity> MapResponse(DataTable dataTbl)
        {
            List<JobEntity> lstJobSummaryDetails = new List<JobEntity>();
            try
            {

                if (dataTbl != null)
                {
                    lstJobSummaryDetails = dataTbl.Rows.Cast<DataRow>().Select(row => new JobEntity()
                    {
                        Region = dataTbl.Columns.Contains("awasRegion") ? string.IsNullOrEmpty(row["awasRegion"].ToString()) ? string.Empty : row["awasRegion"].ToString() : string.Empty,

                        JobId = row["JOB_ID"].ToString(),
                        DoOrderId = row["DO_ORDER_ID"].ToString(),
                        Mon = row["mon"].ToString(),
                        JobType = row["job_type"].ToString(),
                        JobStatus = row["JOB_STATUS"].ToString(),
                        CommitmentDate = string.Format("{0:MM/dd/yyyy HH:mm}", Convert.ToDateTime(row["COMMIT_DATE_TIME"].ToString())),
                        CustomerName = row["CUSTOMER_NAME"].ToString(),
                        WireCenter = row["Wire_center"].ToString(),
                        FttpCoreType = row["FTTP_CORE_TYPE"].ToString(),
                        TechCode = row["tech_ec"].ToString(),
                        VdwDispatchDateTime = !string.IsNullOrEmpty(row["vdw_dispatch_datetime"].ToString()) ? Convert.ToDateTime(row["vdw_dispatch_datetime"].ToString()) : (DateTime?)null,
                        VdwRemarks = row["VDW_REMARKS"].ToString(),
                        VendorId = row["vdw_vendor_id"].ToString(),
                        CustomerAddress = row["CUSTOMER_ADDRESS"].ToString(),
                        // Finding the PinCode from Address.
                        PinCode = row["CUSTOMER_ADDRESS"].ToString().Split(new char[] { ' ' }).LastOrDefault(),
                        CustomerRemarks = row["CUSTOMER_STATUS_REMARKS"].ToString(),
                        // Formatting for the Reach Numbers.
                        //AltReachNumber = row["ALT_CBR_NUMBER"].ToString()?.Insert(3, "-")?.Insert(7, "-") ?? string.Empty

                    }).ToList();
                }

            }
            catch (Exception ex)
            {

            }

            return lstJobSummaryDetails;
        }
    }
}
