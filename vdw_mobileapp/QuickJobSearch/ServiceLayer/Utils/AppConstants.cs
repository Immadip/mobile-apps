﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Utils
{
    public class AppConstants
    {
        public static CookieCollection cookieCollections;
        public static bool IsOffeshoreUser = false;// for IsOffeshoreUser true then it will not ask for sso challenge else it will ask for "onshore"
        public static string SessionName = "SMSESSION";
        public static string SessionLogOff = "LOGGEDOFF";
        public static string VzUrl = "https://cofeetechsda-c2.ebiz.verizon.com/cofeetechsdasit/VendorMobileServices/";
        public static string VzIndexUrl = "https://cofeetechsda-c2.ebiz.verizon.com/cofeetechsda/sdna/index.jsp";

    }
}
