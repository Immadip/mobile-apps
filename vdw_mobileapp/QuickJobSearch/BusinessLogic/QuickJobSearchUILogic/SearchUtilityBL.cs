﻿using BusinessEntities.Models;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLogic.QuickJobSearchUILogic
{
    public class SearchUtilityBL
    {

        public static async Task<JobEntity> GetJobDetails(string jobID, string regionId)
        {
            try
            {
                return await ServiceProxy.GetJobDetails(jobID, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<JobEntity> GetJobDetailsFromMultipleDB(string jobID, string regionId)
        {
            try
            {
                return await ServiceProxy.GetJobDetailsFromMultipleDB(jobID, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<JobEntity> GetJobDetailsByOrderID(string orderID, string regionId)
        {
            try
            {
                return await ServiceProxy.GetJobDeatilsByOrderID(orderID, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<JobEntity> GetJobDetailsBytTechID(string techID, string regionId)
        {
            try
            {
                return await ServiceProxy.GetJobDetails(techID, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<List<JobEntity>> GetJobDetailsByMON(string MON, string regionId)
        {
            try
            {
                return await ServiceProxy.GetJobListByMON(MON, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static async Task<bool> AddMGRRemarks(RemarksEntity remarksEntity)
        {
            return await ServiceProxy.AddMGRRemarks(remarksEntity);
        }

        public static async Task<List<JobEntity>> GetJobListByCommitmentDate(DateTime startCommitmentDate, DateTime endCommitmentDate, string regionId)
        {
            try
            {
                return await ServiceProxy.GetJobListByCommitmentDate(startCommitmentDate, endCommitmentDate, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<List<JobEntity>> GetJobSummaryList(string regionId, string vendorId)
        {
            try
            {
                return await ServiceProxy.GetJobSummaryList(regionId, vendorId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}