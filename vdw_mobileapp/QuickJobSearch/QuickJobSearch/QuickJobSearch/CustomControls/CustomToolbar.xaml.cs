﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace QuickJobSearch.CustomControls
{
    public partial class CustomToolbar : ContentView
    {
        private static int NotificationCount;
        public CustomToolbar()
        {
            InitializeComponent();
            BindNotificationCount();
            SetToolbarStyleBasedOnInternetAvailability();
        }

        // To show/hide the Refresh Icon in case there is no Pull-To-Refresh
        public void ShowRefreshIcon()
        {
            refreshJobsIconContainer.IsVisible = true;
        }

        public void HideRefreshIcon()
        {
            refreshJobsIconContainer.IsVisible = false;
        }

        public Image GetRefreshJobsIcon()
        {
            return imgRefreshJobs;
        }

        // To set the Notification Icon Badge Count from anywhere.
        public void SetNotificationIconBadgeCount(int notificationCount)
        {
            NotificationCount = notificationCount;

            // Only the UI Thread can do changes to the View. So using BeginInvokeOnMainThread().
            Device.BeginInvokeOnMainThread(() =>
            {
                BindNotificationCount();
            });
        }

        public void IncrementNotificationIconBadgeCount(int incrementByValue)
        {
            SetNotificationIconBadgeCount(NotificationCount + incrementByValue);
        }

        public void Refresh()
        {
            // Only the UI Thread can do changes to the View. So using BeginInvokeOnMainThread().
            Device.BeginInvokeOnMainThread(() =>
            {
                BindNotificationCount();
                SetToolbarStyleBasedOnInternetAvailability();
            });
        }

        private void BindNotificationCount()
        {
            lblNotificationCount.IsVisible = NotificationCount != 0;
            lblNotificationCount.Text = NotificationCount.ToString();
        }

        public void SetToolbarStyleBasedOnInternetAvailability()
        {
            // Only the UI Thread can do changes to the View. So using BeginInvokeOnMainThread().
            Device.BeginInvokeOnMainThread(() =>
            {
                slToolbar.Style = App.IsInternet ? (Style)Application.Current.Resources["primaryNavigationBar"] : (Style)Application.Current.Resources["primaryNavigationBarNoInternet"];
            });
        }

        protected async void OnLogoutIconTapped(object sender, EventArgs e)
        {
            //Find the Parent Page (because only a Page can Display an Alert)
            var parentPage = Parent;
            while (!(parentPage is Page))
            {
                parentPage = parentPage.Parent;
            }
            var isConfirmed = await ((Page)parentPage).DisplayAlert("Log out", "Are you sure that you want to logout?", "Log out", "Cancel");
            if (!isConfirmed) return;

            if (Device.OS == TargetPlatform.Windows)
            {
                var page = Navigation.NavigationStack.First();
                while (page.Navigation.NavigationStack.Count > 1)
                {
                    await page.Navigation.PopAsync(true);
                }
            }
            else
            {
                await Navigation.PopToRootAsync(true);
            }
        }

        protected void OnNotificationIconTapped(object sender, EventArgs e)
        {          
            Navigation.PopAsync();                    
            //Do not go to the Notifications page if you're already at this page
            //if (Navigation.NavigationStack[Navigation.NavigationStack.Count - 1].ToString() != "QuickJobSearch.Job_Summary.Notifications")
            //{
            //    //Navigation.PushAsync(new QuickJobSearch.Views.NotificationsPage(), true);
            //}
        }
    }
}
