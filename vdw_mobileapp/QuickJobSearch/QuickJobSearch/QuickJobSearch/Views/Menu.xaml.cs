﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace QuickJobSearch.Views
{
    public partial  class Menu : ContentPage
    {
        public ObservableCollection<ScreenModel> veggies = new ObservableCollection<ScreenModel>();
        public ListView ListView { get { return LvMenuItems; } }
        public Menu()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            veggies.Add(new ScreenModel { Name = "Job Search", TargetType = typeof(JobSearch) });
            veggies.Add(new ScreenModel { Name = "Settings", TargetType = typeof(SettingsPage) });
            veggies.Add(new ScreenModel { Name = "Charts", TargetType = typeof(ChartDetailPage) });
            veggies.Add(new ScreenModel { Name = "Logout", TargetType = typeof(LoginPage) });
            LvMenuItems.ItemsSource = veggies;
        }

        private async void NotificationIcon_Clicked(object sender, EventArgs e)
        {
            var parentPage = Parent;
            while (!(parentPage is Page))
            {
                parentPage = parentPage.Parent;
            }
            var page = Navigation.NavigationStack.First();
            while (page.Navigation.NavigationStack.Count > 1)
            {
                await page.Navigation.PopAsync(true);
            }
            //  Navigation.PopAsync();
        }

        private void LogoutIcon_Clicked(object sender, EventArgs e)
        {

        }
        protected async void Logout_Clicked(object sender, EventArgs e)
        {
            try
            {
                //Find the Parent Page (because only a Page can Display an Alert)
                var parentPage = Parent;
                while (!(parentPage is Page))
                {
                    parentPage = parentPage.Parent;
                }
                var isConfirmed = await ((Page)parentPage).DisplayAlert("Log out", "Are you sure that you want to logout?", "Log out", "Cancel");
                if (!isConfirmed) return;

                if (Device.OS == TargetPlatform.Windows)
                {
                    var page = Navigation.NavigationStack.First();
                    while (page.Navigation.NavigationStack.Count > 1)
                    {
                        await page.Navigation.PopAsync(true);
                    }
                }
                else
                {
                    //Poprootasync will take me to the first page(Home Page) in navigation stack and then pushing the loginpage as second in stack.
                    await Navigation.PopAsync(true);
                    await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}
