﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.QuickJobSearchUILogic;
using BusinessEntities.Models;
using Xamarin.Forms;
using BusinessLogic.QuickJobSearchUILogic;


namespace QuickJobSearch.Views
{
    public partial class JobDetailsWithCommentPage : ContentPage
    {
        JobEntity jobEntity;
        public JobDetailsWithCommentPage(JobEntity jobEntity)
        {
            InitializeComponent();
            BindFieldValues(jobEntity);
            NavigationPage.SetHasNavigationBar(this, false);
            #region Soft Back Button Handling
            var backIconTap = new TapGestureRecognizer();
            backIconTap.Tapped += (s, e) =>
            {
                Navigation.PopAsync();
            };
            imgBackIcon.GestureRecognizers.Add(backIconTap);
            lblBackIcon.GestureRecognizers.Add(backIconTap);
            #endregion Soft Back Button Handling
        }

        private void BindFieldValues(JobEntity _job)
        {
            jobEntity = _job;
            lblJobId.Text = _job.JobId;
            lblMon.Text = _job.Mon;
            lblJobType.Text = _job.JobType;
            lblJobStatus.Text = _job.JobStatus;
            lblVendorId.Text = _job.VendorId;
            lblTechId.Text = "VTE01";
            lblVdwDispatchDateTime.Text = _job.VdwDispatchDateTime != null ? _job.VdwDispatchDateTime.Value.ToString("MM/dd/yyyy HH:mm") : "";
            lblCommitmentDateTime.Text = _job.CommitmentDate.ToString();
            lblCustomerName.Text = _job.CustomerName;
            lblCustomerNumber.Text = _job.AltReachNumber;
            lblCustomerAddress.Text = _job.CustomerAddress;
            txtManagerRemark.Text = _job.VdwRemarks + App.CurrentRemark;
        }

        protected async void OnTechIdClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Views.TechDetailPage());
        }

        protected void OnCustomerReachNumberTapped(object sender, EventArgs e)
        {
            var reachNumber = lblCustomerNumber.Text;
            Device.OpenUri(new Uri(string.Format("tel:{0}", reachNumber)));
        }
        protected async void OnAddRemarkButtonClicked(object sender, EventArgs e)
        {          
            Navigation.PushAsync(new Views.AddRemarkPage(jobEntity));            
        }
    }
}
