﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.QuickJobSearchUILogic;
using BusinessEntities.Models;
using Xamarin.Forms;
using BusinessLogic.QuickJobSearchUILogic;

namespace QuickJobSearch.Views
{
    public partial class AddRemarkPage : ContentPage
    {
        JobEntity _job;
        public AddRemarkPage(JobEntity job)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            #region Soft Back Button Handling
            var backIconTap = new TapGestureRecognizer();
            backIconTap.Tapped += (s, e) =>
            {
                Navigation.PopAsync();
            };
            imgBackIcon.GestureRecognizers.Add(backIconTap);
            lblBackIcon.GestureRecognizers.Add(backIconTap);
            #endregion Soft Back Button Handling
            _job = job;
        }
        protected async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            var remarksEntity = new RemarksEntity
            {
                JobId = _job.JobId,
                VendorId = "MGR01",//_job.VendorId,
                Remarks = txtNewRemarks.Text,
                RemarksTimeStamp = DateTime.Now
            };
            if(txtNewRemarks.Text.Length > 0)
                App.CurrentRemark = "MGR01 :" +  txtNewRemarks.Text + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            bool isVdwRemarksAdded = await SearchUtilityBL.AddMGRRemarks(remarksEntity);

            await DisplayAlert("Success", "Your Remarks was added Successfully", "OK");

            await Navigation.PopAsync();

        }

        protected async void OnCancelButtonClicked(object sender, EventArgs e)
        {
            App.CurrentRemark = "ss";
            //Console.WriteLine(App.CurrentRemark);
            await Navigation.PopAsync();
        }
    }
}
