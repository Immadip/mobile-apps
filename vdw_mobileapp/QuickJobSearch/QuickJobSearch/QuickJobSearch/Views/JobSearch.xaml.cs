﻿using BusinessEntities.Models;
using BusinessLogic.QuickJobSearchUILogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace QuickJobSearch.Views
{
    public partial class JobSearch : ContentPage
    {
        public JobSearch()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            DropTypePicker.Items.Add("MON");
            DropTypePicker.Items.Add("Job Id");
            DropTypePicker.Items.Add("Tech Id");
            DropTypePicker.Items.Add("Order Id");
            DropTypePicker.Items.Add("Commitment Date");
            DropTypePicker.Items.Add("Multiple DB");
            DropTypePicker.SelectedIndexChanged += OnPickerSelectedIndexChanged;
        }
        private void OnPickerSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            int value = DropTypePicker.SelectedIndex;
            switch (value)
            {
                case 0:
                    jobIDField.Placeholder = "Enter MON";
                    jobIDField.Text = "MA65XBVA09878";
                    CommitmentDateLayout.IsVisible = false;
                    jobIDField.IsVisible = true; 
                    break;
                case 1:
                    jobIDField.Placeholder = "Enter Job ID";
                    jobIDField.Text = "XBVASRI110";
                    CommitmentDateLayout.IsVisible = false;
                    jobIDField.IsVisible = true;
                    break;
                case 2:
                    jobIDField.Placeholder = "Enter Tech ID";
                    jobIDField.Text = "VTE01";
                    CommitmentDateLayout.IsVisible = false;
                    jobIDField.IsVisible = true;
                    break;
                case 3:
                    jobIDField.Placeholder = "Enter Order ID";
                    jobIDField.Text = "1121787";
                    CommitmentDateLayout.IsVisible = false;
                    jobIDField.IsVisible = true;
                    break;
                case 4:
                    jobIDField.Placeholder = "Enter Start and End Date";
                    CommitmentStartDatePicker.Date = Convert.ToDateTime("01/01/2016");                    
                    CommitmentEndDatePicker.Date = Convert.ToDateTime("01/07/2016");

                    jobIDField.IsVisible = false;
                    CommitmentDateLayout.IsVisible = true;
                    break;
                case 5:
                    jobIDField.Placeholder = "Enter Job ID";
                    jobIDField.Text = "XBVASRI110";
                    CommitmentDateLayout.IsVisible = false;
                    jobIDField.IsVisible = true;
                    break;
            }
        }
        protected void OnSearchButtonClicked(object sender, EventArgs e)
        {
            int value = DropTypePicker.SelectedIndex;
            switch (value)
            {
                case 0://for get job list according to mon
                        App.MON = jobIDField.Text;
                        GetJobListByMon();
                        break;
                   
                case 1://for get job details by job id
                    
                        if (string.IsNullOrEmpty(jobIDField.Text))
                        {
                            DisplayAlert("Invalid jobId", "Please enter again", "Ok");
                            return;
                        }

                        GetJobDetails(jobIDField.Text);
                        break;
                case 2://for get job details by order id

                    if (string.IsNullOrEmpty(jobIDField.Text))
                    {
                        DisplayAlert("Invalid TechId", "Please enter again", "Ok");
                        return;
                    }

                    GetJobDetailsByTechID(jobIDField.Text);
                    break;
                case 3://for get job details by tech id

                    if (string.IsNullOrEmpty(jobIDField.Text))
                    {
                        DisplayAlert("Invalid OrderId", "Please enter again", "Ok");
                        return;
                    }

                    GetJobDetailsByOrderId(jobIDField.Text);
                    break;
                case 4://for get jobs by commitment date

                    if (string.IsNullOrEmpty(CommitmentStartDatePicker.Date.ToString()))
                    {
                        DisplayAlert("Invalid Date", "Please enter again", "Ok");
                        return;
                    }
                    
                    GetJobsByCommitmentDate(CommitmentStartDatePicker.Date.ToString("MM/dd/yyyy"), CommitmentEndDatePicker.Date.ToString("MM/dd/yyyy"));
                    break;

                case 5://for get job details by job id from multiple DB

                    if (string.IsNullOrEmpty(jobIDField.Text))
                    {
                        DisplayAlert("Invalid jobId", "Please enter again", "Ok");
                        return;
                    }

                    GetJobDetailsFromMultipleDB(jobIDField.Text);
                    break;                    
            }
        }

        public async void GetJobDetails(string jobID)
        {
            JobEntity jobEntity = await SearchUtilityBL.GetJobDetails(jobID, App.LoggedInUserRegion);

            if (jobEntity == null)
            {
                await DisplayAlert("No Job Details Found", "Please Try again", "Ok");
                return;
            }

            await Navigation.PushAsync(new Views.JobDetails(jobEntity));
        }

        public async void GetJobDetailsFromMultipleDB(string jobID)
        {
            JobEntity jobEntity = await SearchUtilityBL.GetJobDetailsFromMultipleDB(jobID, App.LoggedInUserRegion);

            if (jobEntity == null)
            {
                await DisplayAlert("No Job Details Found", "Please Try again", "Ok");
                return;
            }

            await Navigation.PushAsync(new Views.JobDetails(jobEntity));
        }

        public async void GetJobDetailsByOrderId(string orderID)
        {
            JobEntity jobEntity = await SearchUtilityBL.GetJobDetailsByOrderID(orderID, App.LoggedInUserRegion);

            if (jobEntity == null)
            {
                await DisplayAlert("No Job Details Found", "Please Try again", "Ok");
                return;
            }

            await Navigation.PushAsync(new Views.JobDetails(jobEntity));
        }

        public async void GetJobsByCommitmentDate(string commitmentStartDatePicker,string commitmentEndDatePicker)
        {
            await Navigation.PushAsync(new Views.JobListByCommitmentDatePage(commitmentStartDatePicker, commitmentEndDatePicker));
        }        

        public async void GetJobDetailsByTechID(string techID)
        {
            //JobEntity jobEntity = await SearchUtilityBL.GetJobDetails(techID, App.LoggedInUserRegion);


            if (jobIDField.Text.ToUpper() == "VTE01") {
                await Navigation.PushAsync(new Views.TechDetailPage());
            }
            else
            {
                await DisplayAlert("Please enter valid techID", "Please Try again", "Ok");
                return;
            }
            
        }


        public async void GetJobListByMon()
        {
            await Navigation.PushAsync(new Views.JobListingPage());
        }
    }
}
