﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Xamarin.Forms;
using System.Diagnostics;
using System.Net;
using Plugin.Fingerprint.Abstractions;
using System.Threading;

namespace QuickJobSearch.Views
{
    public partial class LoginPage : ContentPage
    {
        public string Name { get; set; }
        private bool isLoading = false;
        private CancellationTokenSource _cancel;
        public bool IsLoading
        {
            get
            {
                return isLoading;
            }
            set
            {
                if (isLoading != value)
                {
                    isLoading = value;
                    RaisePropertyChanged("IsLoading");//PropertyChanged(this, new PropertyChangedEventArgs(nameof(IsLoading)));
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        public  LoginPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            if (ServiceLayer.Utils.AppConstants.IsOffeshoreUser == false)
            {
                loaderStackLayout.IsVisible = true;
                IsLoading = true;

                mainStackLayout.IsVisible = false;
                webViewStackLayout.IsVisible = false;
                webviewBrowser.Source = ServiceLayer.Utils.AppConstants.VzUrl;

                webviewBrowser.Navigated += webviewBrowser_Navigated;
                webviewBrowser.Navigating += webviewBrowser_Navigating;
            }
            else
            {
                IsLoading = false;
                loaderStackLayout.IsVisible = false;
                mainStackLayout.IsVisible = true;
                webViewStackLayout.IsVisible = false;
                usernameField.Focus();
                fingerPrintAuth();

            }
            BindingContext = this;
        }

        async void fingerPrintAuth()
        {
            _cancel = new CancellationTokenSource();
            AuthenticationRequestConfiguration authRequestConfig = new AuthenticationRequestConfiguration("Use Finger print  \n\n\n\n\t Authenticate to login to your manager application");
            authRequestConfig.UseDialog = true;
            var result = await Plugin.Fingerprint.CrossFingerprint.Current.AuthenticateAsync(authRequestConfig, _cancel.Token);//("Authenticate to login to your manager application", _cancel.Token);
            await SetResultAsync(result);
        }

        private async Task SetResultAsync(FingerprintAuthenticationResult result)
        {
            if (result.Authenticated)
            {
                await Application.Current.MainPage.Navigation.PopModalAsync();
                //await Navigation.PushAsync(new Home());
            }
            else
            {
              //  lblStatus.Text = $"{result.Status}: {result.ErrorMessage}";
            }
        }

        private void webviewBrowser_Navigating(object sender, CustomControls.CookieNavigationEventArgs args)
        {
            Debug.WriteLine("Navigating to: {0}", args.Url);
        }

        private void webviewBrowser_Navigated(object sender, CustomControls.CookieNavigatedEventArgs args)
        {
            Debug.WriteLine("Finished navigation to: {0}, Cookies: {1}", args.Url, args.Cookies.Count);
            ServiceLayer.Utils.AppConstants.cookieCollections = args.Cookies;
            foreach (Cookie item in ServiceLayer.Utils.AppConstants.cookieCollections)
            {
                if (item.Name == App.SessionName && item.Value != App.SessionLogOff)
                {
                    App.isAuthenticatedUser = true;
                    break;
                }
            }

            if (!App.isAuthenticatedUser)
            {
                webViewStackLayout.IsVisible = true;
                loaderStackLayout.IsVisible = false;
                IsLoading = false;
            }
            else
            {
                mainStackLayout.IsVisible = true;
                webViewStackLayout.IsVisible = false;
                loaderStackLayout.IsVisible = false;
                IsLoading = false;
                fingerPrintAuth(); 
                usernameField.Focus();
                BindingContext = this;
            }
        }

        private bool IsValidUser()
        {
            //Authentication should be done - Things to look into.
            if ((usernameField.Text.ToUpper().Equals("VTE01") || usernameField.Text.ToUpper().Equals("NEPVC")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public async void login()
        {
            try
            {
                App.IsUserLoggedIn = true;
                //App.LoggedInUserId = usernameField.Text.ToUpper();
                App.LoggedInUserRegion = "CP";

                await Application.Current.MainPage.Navigation.PopModalAsync();

            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        #region Button Click Events
        protected void OnLoginButtonClicked(object sender, EventArgs e)
        {
            if (true)
                login();
            else
                DisplayAlert("Alert", "Wrong User ID", "Ok", "Cancel");
        }

        protected void OnPasswordFieldCompleted(object sender, EventArgs e)
        {
            loginButton.Focus();
        }

        protected void OnUsernameFieldCompleted(object sender, EventArgs e)
        {
            passwordField.Focus();
        }

        protected void OnForgotPasswordClicked(object sender, EventArgs e)
        {
            DisplayAlert("Not Implemented", "This feature is not Implemented", "OK");
        }

        #endregion

    }
}
