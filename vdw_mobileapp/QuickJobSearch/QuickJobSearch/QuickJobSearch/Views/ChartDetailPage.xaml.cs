﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Xamarin.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuickJobSearch.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChartDetailPage : ContentPage
    {
        public MyViewModel vm { get; set; }
        public BarViewModel bvm { get; set; }

        public AreaViewModel avm { get; set; }
        public ChartDetailPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            vm = new MyViewModel();

            BindingContext = vm;



            /* Page[] persons =
             {
                 new PieEx { Title = "Pie", BindingContext = vSampleData },
                 new AreaSeriesEx { Title = "Area", BindingContext = vSampleData },
                 new BarEx { Title = "Bar", BindingContext = vSampleData },
                 new StackedBar { Title = "Stack-Bar", BindingContext = vSampleData }
             };
            this.MyCarouselView.ItemsSource = persons;

            /* var vSampleData = new ChartData();
             var vTabMain = new TabbedPage();
             vTabMain.Children.Add(new PieEx { Title = "Pie", BindingContext = vSampleData });
             vTabMain.Children.Add(new AreaSeriesEx { Title = "Area", BindingContext = vSampleData });
             vTabMain.Children.Add(new BarEx { Title = "Bar", BindingContext = vSampleData });
             vTabMain.Children.Add(new StackedBar { Title = "Stack-Bar", BindingContext = vSampleData });*/
        }
        protected void OnStep1TabClicked(object sender, EventArgs e)
        {
            //If Previous Tab is Step 4, then capture the Screenshot of the Map
            vm = new MyViewModel();

            BindingContext = vm;
            ShowStep1();
        }

        protected void OnStep2TabClicked(object sender, EventArgs e)
        {
            //If Previous Tab is Step 4, then capture the Screenshot of the Map
            ShowStep2();
            bvm = new BarViewModel(false);

            BindingContext = bvm;
        }

        protected async void OnStep3TabClicked(object sender, EventArgs e)
        {
            avm = new AreaViewModel();

            BindingContext = avm;

            ShowStep3();

        }

        protected void OnStep4TabClicked(object sender, EventArgs e)
        {
            //If Previous Tab is Step 4, then capture the Screenshot of the Map

            ShowStep4();
            bvm = new BarViewModel(true);

            BindingContext = bvm;
        }


        protected void ShowStep1()
        {
            //step1Container.IsVisible = btnNext.IsVisible = tabNavigationButtonsContainer.IsVisible = true;
            //step2Container.IsVisible = step3Container.IsVisible = step4Container.IsVisible = step5Container.IsVisible = btnPrevious.IsVisible = false;

            highlightStep1.Style = (Style)Application.Current.Resources["completeJobTabHighlighted"];
            highlightStep2.Style = highlightStep3.Style =
                highlightStep4.Style = (Style)Application.Current.Resources["completeJobTabNonHighlighted"];

            tabStep1.Style = (Style)Application.Current.Resources["completeJobTabSelected"];
            tabStep2.Style = tabStep3.Style =
                tabStep4.Style = (Style)Application.Current.Resources["completeJobTabNonSelected"];
        }

        protected void ShowStep2()
        {
            //step2Container.IsVisible = btnPrevious.IsVisible = btnNext.IsVisible = tabNavigationButtonsContainer.IsVisible = true;
            //step1Container.IsVisible = step3Container.IsVisible = step4Container.IsVisible = step5Container.IsVisible = false;

            highlightStep2.Style = (Style)Application.Current.Resources["completeJobTabHighlighted"];
            highlightStep1.Style = highlightStep3.Style =
                highlightStep4.Style = (Style)Application.Current.Resources["completeJobTabNonHighlighted"];

            tabStep2.Style = (Style)Application.Current.Resources["completeJobTabSelected"];
            tabStep1.Style = tabStep3.Style =
                tabStep4.Style = (Style)Application.Current.Resources["completeJobTabNonSelected"];
        }

        protected void ShowStep3()
        {

            //step3Container.IsVisible = btnPrevious.IsVisible = btnNext.IsVisible = tabNavigationButtonsContainer.IsVisible = true;
            //step1Container.IsVisible = step2Container.IsVisible = step4Container.IsVisible = step5Container.IsVisible = false;

            highlightStep3.Style = (Style)Application.Current.Resources["completeJobTabHighlighted"];
            highlightStep1.Style = highlightStep2.Style =
                highlightStep4.Style = (Style)Application.Current.Resources["completeJobTabNonHighlighted"];

            tabStep3.Style = (Style)Application.Current.Resources["completeJobTabSelected"];
            tabStep1.Style = tabStep2.Style =
                tabStep4.Style = (Style)Application.Current.Resources["completeJobTabNonSelected"];
        }

        protected void ShowStep4()
        {
            //step4Container.IsVisible = btnPrevious.IsVisible = btnNext.IsVisible = true;
            //step1Container.IsVisible = step2Container.IsVisible = step3Container.IsVisible = step5Container.IsVisible = tabNavigationButtonsContainer.IsVisible = false;

            highlightStep4.Style = (Style)Application.Current.Resources["completeJobTabHighlighted"];
            highlightStep1.Style = highlightStep2.Style =
                highlightStep3.Style = (Style)Application.Current.Resources["completeJobTabNonHighlighted"];

            tabStep4.Style = (Style)Application.Current.Resources["completeJobTabSelected"];
            tabStep1.Style = tabStep2.Style =
                tabStep3.Style = (Style)Application.Current.Resources["completeJobTabNonSelected"];


        }
        public class MyViewModel
        {
            public PlotModel MyModel { get; set; }

            public MyViewModel()
            {

                PieSeries pieSeries = new PieSeries();
                pieSeries.Slices.Add(new PieSlice("Africa", 1030) { IsExploded = false, Fill = OxyColors.PaleVioletRed });
                pieSeries.Slices.Add(new PieSlice("Americas", 929) { IsExploded = true });
                pieSeries.Slices.Add(new PieSlice("Asia", 4157) { IsExploded = true });
                pieSeries.Slices.Add(new PieSlice("Europe", 739) { IsExploded = true });
                pieSeries.Slices.Add(new PieSlice("Oceania", 350) { IsExploded = true });

                MyModel = new PlotModel();
                MyModel.Series.Add(pieSeries);
            }
        }
        public class BarViewModel
        {
            public PlotModel MyModel { get; set; }

            public BarViewModel(bool isStacked)
            {
                var s1 = new BarSeries { Title = "Series 1", IsStacked = isStacked, StrokeColor = OxyColors.Black, StrokeThickness = 1 };
                s1.Items.Add(new BarItem { Value = 25 });
                s1.Items.Add(new BarItem { Value = 137 });
                s1.Items.Add(new BarItem { Value = 18 });
                s1.Items.Add(new BarItem { Value = 40 });

                var s2 = new BarSeries { Title = "Series 2", IsStacked = isStacked, StrokeColor = OxyColors.Black, StrokeThickness = 1 };
                s2.Items.Add(new BarItem { Value = 12 });
                s2.Items.Add(new BarItem { Value = 14 });
                s2.Items.Add(new BarItem { Value = 120 });
                s2.Items.Add(new BarItem { Value = 26 });

                var categoryAxis = new CategoryAxis { Position = CategoryAxisPosition() };
                categoryAxis.Labels.Add("Category A");
                categoryAxis.Labels.Add("Category B");
                categoryAxis.Labels.Add("Category C");
                categoryAxis.Labels.Add("Category D");
                var valueAxis = new LinearAxis { Position = ValueAxisPosition(), MinimumPadding = 0, MaximumPadding = 0.06, AbsoluteMinimum = 0 };

                MyModel = new PlotModel();
                MyModel.Series.Add(s1);
                MyModel.Series.Add(s2);
                MyModel.Axes.Add(categoryAxis);
                MyModel.Axes.Add(valueAxis);


            }

            private AxisPosition CategoryAxisPosition()
            {
                if (typeof(BarSeries) == typeof(ColumnSeries))
                {
                    return AxisPosition.Bottom;
                }

                return AxisPosition.Left;
            }

            private AxisPosition ValueAxisPosition()
            {
                if (typeof(BarSeries) == typeof(ColumnSeries))
                {
                    return AxisPosition.Left;
                }

                return AxisPosition.Bottom;
            }
        }
        public class AreaViewModel
        {
            public PlotModel MyModel { get; set; }

            public AreaViewModel()
            {
                //var plotModel1 = new PlotModel { Title = "Area Series with crossing lines" };
                var areaSeries1 = new AreaSeries();
                areaSeries1.Points.Add(new DataPoint(0, 50));
                areaSeries1.Points.Add(new DataPoint(10, 140));
                areaSeries1.Points.Add(new DataPoint(20, 60));
                areaSeries1.Points2.Add(new DataPoint(0, 60));
                areaSeries1.Points2.Add(new DataPoint(5, 80));
                areaSeries1.Points2.Add(new DataPoint(20, 70));
                MyModel = new PlotModel();
                MyModel.Series.Add(areaSeries1);

            }
        }
    }
   
}
