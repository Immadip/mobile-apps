﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace QuickJobSearch.Views
{
    public partial class Home : MasterDetailPage
    {
        Menu menu;
        public Home()
        {
            InitializeComponent();
           // NavigationPage.SetHasNavigationBar(this, false);
            menu = new Menu();
            //this.Master = new Menu();
            Master = menu;
            this.Detail = new NavigationPage(new JobSearch())
            {
                BarBackgroundColor = Color.FromHex("CD040B"),
                BarTextColor = Color.Yellow,
                Padding = 0
            };
            menu.ListView.ItemSelected += OnMenuItemSelected;          
        }

        public void setIsPresented()
        {
            if (this.IsPresented)
                this.IsPresented = false;
            else
                this.IsPresented = true;
        }

        protected async void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            var item = e.SelectedItem as ScreenModel;
            if (item != null) {
                if (item.Name == "Logout")
                {
                    await Navigation.PopToRootAsync(true);
                    await Navigation.PushModalAsync(new NavigationPage(new LoginPage()));
                    return;
                }
                Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                IsPresented = false;
            }
        }
    }
}
