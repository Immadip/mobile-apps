﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace QuickJobSearch.Views
{
    public partial class TechDetailPage : ContentPage
    {
        public TechDetailPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            #region Soft Back Button Handling
            var backIconTap = new TapGestureRecognizer();
            backIconTap.Tapped += (s, e) =>
            {
                Navigation.PopAsync();
            };
            imgBackIcon.GestureRecognizers.Add(backIconTap);
            lblBackIcon.GestureRecognizers.Add(backIconTap);
            #endregion Soft Back Button Handling
        }
       
    }
}
