﻿using BusinessEntities;
using BusinessEntities.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ComponentModel;
using Xamarin.Forms;
using BusinessLogic;
using BusinessLogic.QuickJobSearchUILogic;
using DevExpress.Mobile.DataGrid.Theme;
using DevExpress.Mobile.DataGrid;

namespace QuickJobSearch.Views
{
    public partial class JobListingPage : ContentPage
    {
        private List<JobEntity> assignedJobs;
        private List<JobEntity> completedJobs;
        private string currentJobStatusToBeDisplayed;
        private bool _isRowEven;

        public JobListingPage()
        {
            InitializeComponent();
            currentJobStatusToBeDisplayed = VdwConstants.JobCompletedStatus;
            GetJobs();
            NavigationPage.SetHasNavigationBar(this, false);
            var backIconTap = new TapGestureRecognizer();
            backIconTap.Tapped += (s, e) =>
            {
                Navigation.PopAsync();
            };
            imgBackIcon.GestureRecognizers.Add(backIconTap);
            lblBackIcon.GestureRecognizers.Add(backIconTap);
            App.CurrentRemark = "";
            lvJobSummary.RowTap += LvJobSummaryRowTap;

            //theme
            ThemeManager.ThemeName = Themes.Light;

            // Header customization.
            ThemeManager.Theme.HeaderCustomizer.BackgroundColor = Color.FromHex("F9B295");//Color.FromRgb(187, 228, 208);
            ThemeFontAttributes myFont = new ThemeFontAttributes("HelveticaNeueLTStd-Cn",
                                        ThemeFontAttributes.FontSizeFromNamedSize(NamedSize.Medium),
                                        FontAttributes.None, Color.White);
            ThemeManager.Theme.HeaderCustomizer.Font = myFont;
        }

        private void LvJobSummaryRowTap(object sender, DevExpress.Mobile.DataGrid.RowTapEventArgs e)
        {

            var selectedJob = (JobEntity)lvJobSummary.SelectedDataObject;//SelectedItem
                                                                         // ((Grid)lvJobSummary).SelectedDataObject = null;//SelectedItem
            Navigation.PushAsync(new Views.JobDetailsWithCommentPage(selectedJob));

             //Unselect the Item in the ListView.
             lvJobSummary.SelectedDataObject = null;
        }

        public async void GetJobs()
        {
            var jobs = await SearchUtilityBL.GetJobDetailsByMON(App.MON, App.LoggedInUserRegion);

            if (jobs == null) return;
            completedJobs = jobs;
            // Initialize MapPage with the Jobs. The MapPage will plot all these Jobs.
            lvJobSummary.ItemsSource = completedJobs; 
        }
      
    }
}
