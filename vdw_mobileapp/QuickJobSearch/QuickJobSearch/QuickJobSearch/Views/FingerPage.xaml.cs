﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Plugin.Fingerprint.Abstractions;
using Xamarin.Forms;


namespace QuickJobSearch.Views
{
    public partial class FingerPage : ContentPage
    {
        private CancellationTokenSource _cancel;
        public FingerPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }    

        private async void OnAuthenticate(object sender, EventArgs e)
        {
            _cancel = swAutoCancel.IsToggled ? new CancellationTokenSource(TimeSpan.FromSeconds(10)) : new CancellationTokenSource();
            lblStatus.Text = "";
            var result = await Plugin.Fingerprint.CrossFingerprint.Current.AuthenticateAsync("Prove you have fingers!", _cancel.Token);

            await SetResultAsync(result);
        }

        private async void OnLoginClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new LoginPage());           
        }

        private async Task SetResultAsync(FingerprintAuthenticationResult result)
        {
            if (result.Authenticated)
            {
                await Application.Current.MainPage.Navigation.PopModalAsync();
                //await Navigation.PushAsync(new Home());
            }
            else
            {
                lblStatus.Text = $"{result.Status}: {result.ErrorMessage}";
            }
        }
    }
}
