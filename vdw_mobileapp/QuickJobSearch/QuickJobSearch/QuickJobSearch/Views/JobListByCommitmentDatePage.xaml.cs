﻿using BusinessEntities;
using BusinessEntities.Models;
using BusinessLogic.QuickJobSearchUILogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DevExpress.Mobile.DataGrid;
using DevExpress.Mobile.DataGrid.Theme;
using DevExpress.Mobile.Core;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace QuickJobSearch.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class JobListByCommitmentDatePage : ContentPage
    {

        internal List<JobEntity> completedJobs;
        private string currentJobStatusToBeDisplayed;
        private string _commitmentStartDatePicker;
        private string _commitmentEndDatePicker;

        //Load more variables
        
        public JobListByCommitmentDatePage(string commitmentStartDatePicker, string commitmentEndDatePicker)
        {
            InitializeComponent();
            currentJobStatusToBeDisplayed = VdwConstants.JobCompletedStatus;
            _commitmentStartDatePicker = commitmentStartDatePicker;
            _commitmentEndDatePicker = commitmentEndDatePicker;
            GetJobs();
            NavigationPage.SetHasNavigationBar(this, false);
            var backIconTap = new TapGestureRecognizer();
            backIconTap.Tapped += (s, e) =>
            {
                Navigation.PopAsync();
            };
            imgBackIcon.GestureRecognizers.Add(backIconTap);
            lblBackIcon.GestureRecognizers.Add(backIconTap);
            //filter
            var filterIconTap = new TapGestureRecognizer();
            bool isShow = false;
            filterIconTap.Tapped += (s, e) =>
            {
                if (isShow == false)
                {
                    lvJobSummary.FilterPanelVisibility = VisibilityState.Always;
                    lvJobSummary.AutoFilterPanelVisibility = true;
                    lvJobSummary.Redraw(true);
                    isShow = true;
                }
                else
                {
                    lvJobSummary.FilterPanelVisibility = VisibilityState.Never;
                    lvJobSummary.AutoFilterPanelVisibility = false;
                    lvJobSummary.Redraw(true);
                    isShow = false;
                }
            };
            imgFilterIcon.GestureRecognizers.Add(filterIconTap);
            App.CurrentRemark = "";
            //Row tap event
           // lvJobSummary.RowTap += LvJobSummaryRowTap;
            lvJobSummary.SwipeButtonClick += OnSwipeButtonClick;
            lvJobSummary.SwipeButtonShowing += OnSwipeButtonShowing;

           
            //theme
            ThemeManager.ThemeName = Themes.Light;

            // Header customization.
            ThemeManager.Theme.HeaderCustomizer.BackgroundColor = Color.FromHex("F9B295");//Color.FromRgb(187, 228, 208);
            ThemeFontAttributes myFont = new ThemeFontAttributes("HelveticaNeueLTStd-Cn",
                                        ThemeFontAttributes.FontSizeFromNamedSize(NamedSize.Medium),
                                        FontAttributes.None, Color.White);
            ThemeManager.Theme.HeaderCustomizer.Font = myFont;

        }

        public async void GetJobs()
        {
            try
            {
                var jobs = await SearchUtilityBL.GetJobListByCommitmentDate(Convert.ToDateTime(_commitmentStartDatePicker), Convert.ToDateTime(_commitmentEndDatePicker), App.LoggedInUserRegion);

                if (jobs == null) return;
                completedJobs = jobs;
                // Initialize MapPage with the Jobs. The MapPage will plot all these Jobs.
                lvJobSummary.ItemsSource = completedJobs; 
            }
            catch (Exception ex)
            {
            }

        }

        

        void OnSwipeButtonShowing(object sender, SwipeButtonShowingEventArgs e)
        {
            if ((e.ButtonInfo.ButtonName == "LeftButton"))
            {
                e.IsVisible = true;
                e.ButtonInfo.AutoCloseOnTap = true;
            }
        }

        void OnSwipeButtonClick(object sender, SwipeButtonEventArgs e)
        {
            if (e.ButtonInfo.ButtonName == "LeftButton")
            {
                showJobDetails();
            }
        }

        private void LvJobSummaryRowTap(object sender, DevExpress.Mobile.DataGrid.RowTapEventArgs e)
        {
            showJobDetails();
        }

        void showJobDetails()
        {
            var selectedJob = (JobEntity)lvJobSummary.SelectedDataObject;//SelectedItem
            
            // ((Grid)lvJobSummary).SelectedDataObject = null;//SelectedItem
            if(selectedJob !=  null)
            Navigation.PushAsync(new Views.JobDetails(selectedJob));

            //Unselect the Item in the ListView.
            //lvJobSummary.SelectedDataObject = null;
        }
        
       
    }
        
}
