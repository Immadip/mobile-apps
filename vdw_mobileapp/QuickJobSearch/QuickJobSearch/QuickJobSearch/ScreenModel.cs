﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickJobSearch
{
    public class ScreenModel
    {
        public string Name { get; set; }
        public Type TargetType { get;  set; }
    }
}
