﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace QuickJobSearch.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            var windowSize = Window.Current.Bounds;
            QuickJobSearch.App.ScreenWidth = (int)windowSize.Width;
            QuickJobSearch.App.ScreenHeight = (int)windowSize.Height;

            LoadApplication(new QuickJobSearch.App());
        }
    }
}
