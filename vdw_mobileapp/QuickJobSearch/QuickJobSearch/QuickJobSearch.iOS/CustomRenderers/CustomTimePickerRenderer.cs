using QuickJobSearch.CustomControls;
using QuickJobSearch.iOS.CustomRenderers;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomTimePicker), typeof(CustomTimePickerRenderer))]
namespace QuickJobSearch.iOS.CustomRenderers
{
    public class CustomTimePickerRenderer : TimePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<TimePicker> e)
        {
            base.OnElementChanged(e);

            var timePicker = (CustomTimePicker)Element;

            if (timePicker != null)
            {
                SetFont(timePicker);
            }
        }

        private void SetFont(CustomTimePicker customDatePicker)
        {
            Control.Font = UIFont.FromName(customDatePicker.FontFamily, customDatePicker.FontSize);
        }
    }
}