using QuickJobSearch.CustomControls;
using QuickJobSearch.iOS.CustomRenderers;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace QuickJobSearch.iOS.CustomRenderers
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            var picker = (CustomPicker)Element;

            if (picker != null)
            {
                SetFont(picker);
            }
        }

        private void SetFont(CustomPicker customDatePicker)
        {
            Control.Font = UIFont.FromName(customDatePicker.FontFamily, customDatePicker.FontSize);
        }
    }
}