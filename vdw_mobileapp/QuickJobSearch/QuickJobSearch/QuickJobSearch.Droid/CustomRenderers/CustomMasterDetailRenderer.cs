using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android.AppCompat;
using QuickJobSearch.Droid.CustomRenderers;

[assembly: ExportRenderer(typeof(MasterDetailPage), typeof(CustomMasterDetailRenderer))]
namespace QuickJobSearch.Droid.CustomRenderers
{
    public class CustomMasterDetailRenderer : MasterDetailPageRenderer
    {
        protected override void OnElementChanged(VisualElement oldElement, VisualElement newElement)
        {
            base.OnElementChanged(oldElement, newElement);

            var detailContainer = GetChildAt(0);
            var masterContainer = GetChildAt(1);

            // Use reflection to modify MasterDetailContainer's (internal class) TopPadding
            var topPadding = detailContainer.GetType().GetProperty("TopPadding");
            topPadding.SetValue(detailContainer, 0);
            topPadding.SetValue(masterContainer, 0);
        }
    }
}