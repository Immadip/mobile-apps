﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities.VDWEntities
{
    public class DeviceEntity
    {
        public string DeviceId { get; set; }
        public string UserId { get; set; }
        public string MobileOS { get; set; }
        public string DeviceToken { get; set; }
    }
}
