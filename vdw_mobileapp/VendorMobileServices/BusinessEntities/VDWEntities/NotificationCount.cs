﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities.VDWEntities
{
    public class NotificationCount
    {
        public int NoofNotification { get; set; }
        public string DeviceToken { get; set; }
        public string UserId { get; set; }
    }
}
