﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities.VDWEntities
{
    public class LoggingEntity
    {
        public string TechId { get; set; }
        public string VendorId { get; set; }
        public string LogMessage { get; set; }
        public string LogDescription { get; set; }
        public DateTime LogCreatedDate { get; set; }
    }
}
