﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.VDWEntities
{
    public class NotificationEntity
    {
        public int NotificationId { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationText { get; set; }
        public string NotificationType { get; set; }
        public DateTime NotificationReceivedTime { get; set; }
        public string UserId { get; set; }
    }
}
