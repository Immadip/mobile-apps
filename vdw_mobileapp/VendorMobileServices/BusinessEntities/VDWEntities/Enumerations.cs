﻿namespace BusinessEntities.VDWEntities
{
    public enum UserRole
    {
        Vendor = 1,
        Technician = 2
    }
}
