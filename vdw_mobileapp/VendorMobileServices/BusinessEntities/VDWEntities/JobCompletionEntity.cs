﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities.VDWEntities
{
    public class JobCompletionEntity
    {
        public string JobId { get; set; }
        public string Mon { get; set; }
        public string DoOrderId { get; set; }
        public string TechId { get; set; }
        public DateTime? VdwDispatchDateTime { get; set; }
        public string CommitmentDate { get; set; }
        public string DispatchTime { get; set; }
        public string CableLocateNum { get; set; }
        public string CusstatRemarks { get; set; }
        public string CompletionRemarks { get; set; }
        public string ArrivalDate { get; set; }
        public string ArrivalTime { get; set; }
        public string ClearDate { get; set; }
        public string ClearTime { get; set; }
        public string EnggRemarks { get; set; }
        public string ContactName { get; set; }
        public string Contact { get; set; }
        public string NoofBores { get; set; }
        public string BoreTypeNumber { get; set; }
        public string BoreTypeLength { get; set; }
        public string BoreType { get; set; }
        public string TotBoreLength { get; set; }
        public string DirectBuriedLength { get; set; }
        public string PullthroughLength { get; set; }
        public string DropConduitLength { get; set; }
        public string PreCutLength { get; set; }
        public string DropType { get; set; }
        public string JeoPardyCode { get; set; }
        public string JeopardyDate { get; set; }
        public string JeopardyTime { get; set; }
        public string DidCompletejob { get; set; }
        public string DidTerminateDrop { get; set; }
        public string ReferToEngg { get; set; }
        public string TripCharge { get; set; }
        public string DidplaceanONT { get; set; }
        public string ONTLocationRemarks { get; set; }
        public string ONTLocInOut { get; set; }
        public string MapPolygonJson { get; set; }
        public List<ImageEntity> Images { get; set; }

        public override int GetHashCode()
        {
            return DoOrderId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == this)
            {
                return true;
            }
            if (!(obj is JobCompletionEntity)) {
                return false;
            }
            var other = (JobCompletionEntity)obj;
            return this.DoOrderId.Equals(other.DoOrderId);
        }
    }

    public class DropType
    {
        public string FiberType { get; set; }
    }

    public class JeopardyCode
    {
        public string JwmCode { get; set; }
    }

    public class Category
    {
        public int CategoryID { get; set; }

        public string CategoryDescription { get; set; }
    }
}
