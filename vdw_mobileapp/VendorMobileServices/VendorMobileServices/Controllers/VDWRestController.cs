﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using VendorMobileServices;
using BusinessLayer.VDWLogic;
using BusinessEntities.VDWEntities;
using Newtonsoft.Json;


namespace VendorMobileServices.Controllers
{
    public class VDWRestController : ApiController
    {
        //Dependencies to be injected - Things to look into.
        [HttpGet]
        public bool GetCheckConnection()
        {
            return true;
        }

        [HttpGet]
        public bool GetValidateUser(string vendorId, string password)
        {
            try
            {
                ValidateUserBLL blGetValidateUser = new ValidateUserBLL();
                return blGetValidateUser.GetValidateUser(vendorId, password);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpGet]
        public List<JobEntity> GetJobSummary(string vendorId, string regionId)
        {
            try
            {
                GetJobSummaryBLL blGetJobSummary = new GetJobSummaryBLL();
                List<JobEntity> response = blGetJobSummary.GetJobSummaryList(regionId, vendorId);
                return response;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
        
        [HttpGet]
        public List<JobEntity> GetAssignJobList(string vendorId, string regionId)
        {
            try
            {
                GetJobSummaryBLL blGetJobSummary = new GetJobSummaryBLL();
                List<JobEntity> response = blGetJobSummary.GetAssignJobList(regionId, vendorId);
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public List<JobEntity> GetJobSummaryByLastModifiedDate(string vendorId, DateTime lastModifiedDate)
        {
            try
            {
                GetJobSummaryBLL blGetJobSummary = new GetJobSummaryBLL();
                List<JobEntity> response = blGetJobSummary.GetJobSummaryListByLastModifiedDate(vendorId, lastModifiedDate);
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public string GetJobStatus(string doOrderId)
        {
            try
            {
                GetJobSummaryBLL blGetJobSummary = new GetJobSummaryBLL();
                string response = blGetJobSummary.GetJobStatus(doOrderId);
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public int GetNotificationCount(string deviceID,string userID)
        {
            try
            {
                NotificationsBLL blNotifications = new NotificationsBLL();
                int response = blNotifications.GetNotificationCount(deviceID, userID);
                return response;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        
        [HttpPost]
        public void SetNotificationCount([FromBody] NotificationCount notificationcount)
        {
            try
            {
                NotificationsBLL blNotifications = new NotificationsBLL();
                blNotifications.SetNotificationCount(notificationcount.DeviceToken, notificationcount.UserId, notificationcount.NoofNotification);
                
            }
            catch (Exception ex)
            {
               
            }
        }

        public List<DropType> GetDropTypes()
        {
            try
            {
                GetAllDropTypesBLL blGetDropTypes = new GetAllDropTypesBLL();
                List<DropType> response = blGetDropTypes.GetDropTypes();
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<JeopardyCode> GetJeopardyCodes()
        {
            try
            {
                GetAllGeopardyCodesBLL blGetgeocodes = new GetAllGeopardyCodesBLL();
                List<JeopardyCode> response = blGetgeocodes.GetJeopardyCodes();
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Category> GetCategories()
        {
            try
            {
                CompleteJobBLL blCategories = new CompleteJobBLL();
                List<Category> response = blCategories.GetCategories();
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public bool CompleteJob([FromBody] JobCompletionEntity completejobinfo)
        {
            try
            {
                CompleteJobBLL blCompleteJobBLL = new CompleteJobBLL();
                bool response = blCompleteJobBLL.CompleteJob(completejobinfo);
                if(response)
                {
                    var imageUploadBll = new ImageUploadBLL();
                    response = UploadImage(completejobinfo.Images);
                }
                
                return response;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        public bool AssignJob([FromBody] JobEntity assignJob)
        {
            try
            {
                GetJobSummaryBLL blJobSummaryBLL = new GetJobSummaryBLL();
                bool response = blJobSummaryBLL.AssignJob(assignJob);
                return response;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        public bool InsertLoggingInfo([FromBody] LoggingEntity loggingEntity)
        {
            try
            {
                LoggingInfoBLL blLoggingInfoBLL = new LoggingInfoBLL();
                bool response = blLoggingInfoBLL.InsertLoggingInfo(loggingEntity);
                return response;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [HttpPost]
        public bool DeviceInfo([FromBody] DeviceEntity deviceEntity)
        {
            try
            {
                DeviceInfoBLL objDeviceInfoBLL = new DeviceInfoBLL();
                bool response = objDeviceInfoBLL.InsertDeviceInfo(deviceEntity);
                return response;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UploadImage(List<ImageEntity> imagesToUpload)
        {

            bool response = true;
            try
            {
                var imageUploadBll = new ImageUploadBLL();

                foreach(ImageEntity imgObject in imagesToUpload)
                {
                    response = imageUploadBll.UploadImage(imgObject);
                }
                return response;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<GlobalFlagEntity> GetGlobalFlags()
        {
            try
            {
                var commonBll = new CommonBLL();
                List <GlobalFlagEntity> response = commonBll.GetGlobalFlags();
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public List<NotificationEntity> GetNotificationsForUser(string userId)
        {
            try
            {
                NotificationsBLL blNotifications = new NotificationsBLL();
                return blNotifications.GetNotificationsForUser(userId);
            }
            catch (Exception ex)
            {
                //Exception Logging goes here - Things to look into.
                return null;
            }
        }

        [HttpGet]
        public List<JobCompletionEntity> GetJobsCompletionInfo(string userId, UserRole userRole, string regionId)
        {
            try
            {
                var completeJobBll = new CompleteJobBLL();
                return completeJobBll.GetJobsCompletionInfo(userId, userRole, regionId);
            }
            catch (Exception ex)
            {
                //Exception Logging goes here - Things to look into.
                return null;
            }
        }

        [HttpGet]
        public bool MarkNotificationsAsRead(string userId)
        {
            try
            {
                NotificationsBLL blNotifications = new NotificationsBLL();
                return blNotifications.MarkNotificationsAsRead(userId);
            }
            catch (Exception ex)
            {
                //Exception Logging goes here - Things to look into.
                return false;
            }
        }

        [HttpPost]
        public bool AddVdwRemarks([FromBody]RemarksEntity remarks)
        {
            try
            {
                JobActionsBLL jobActionsBll = new JobActionsBLL();
                return jobActionsBll.AddVdwRemarks(remarks);
            }
            catch (Exception ex)
            {
                //Exception Logging goes here - Things to look into.
                return false;
            }
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}