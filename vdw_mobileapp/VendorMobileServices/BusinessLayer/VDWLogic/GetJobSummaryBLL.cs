﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BusinessEntities.VDWEntities;

namespace BusinessLayer.VDWLogic
{
    public class GetJobSummaryBLL
    {
        public List<JobEntity> GetJobSummaryList(string regionId, string techId)
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.GetJobSummaryDal.ExecuteSp(regionId, techId);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public List<JobEntity> GetAssignJobList(string regionId, string vendorId)
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.GetJobAssignDal.ExecuteSp(regionId, vendorId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool AssignJob(JobEntity assignJob)
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.GetJobAssignDal.AssignJob(assignJob.JobId, assignJob.TechCode);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
       
        public List<JobEntity> GetJobSummaryList(string regionId, string vendorId,DateTime lastSyncDate)
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.GetJobSummaryDal.ExecuteSp(regionId, vendorId, lastSyncDate);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<JobEntity> GetJobSummaryListByLastModifiedDate(string vendorId, DateTime lastModifiedDate)
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.GetJobSummaryByLastModfiedDateDal.ExecuteSp(vendorId, lastModifiedDate);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public string GetJobStatus(string doOrderId)
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.GetJobStatusDal.ExecuteSp(doOrderId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
