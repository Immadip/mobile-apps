﻿using BusinessEntities.VDWEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.VDWLogic
{
    public class CommonBLL
    {
        public List<GlobalFlagEntity> GetGlobalFlags()
        {
            return DataAccessLayer.VDWDataAccess.ReadGlobalFlagsDal.ExecuteSP();
        }
    }
}
