﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities.VDWEntities;

namespace BusinessLayer.VDWLogic
{
   public class GetAllGeopardyCodesBLL
    {
        public List<JeopardyCode> GetJeopardyCodes()
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.GetAllGeopardyCodeDAL.ExecuteSp();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
