﻿using BusinessEntities.VDWEntities;
using System.Configuration;
using System.IO;
using DataAccessLayer.VDWDataAccess;

namespace BusinessLayer.VDWLogic
{
    public class ImageUploadBLL
    {
        public bool UploadImage(ImageEntity image)
        {
            return SaveImageDetailsDal.ExecuteSp(image);
        }
    }
}
