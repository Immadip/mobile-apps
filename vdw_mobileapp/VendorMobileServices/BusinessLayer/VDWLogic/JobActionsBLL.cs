﻿using System;
using BusinessEntities.VDWEntities;
using DataAccessLayer.VDWDataAccess;
using Microsoft.SqlServer.Server;

namespace BusinessLayer.VDWLogic
{
    public class JobActionsBLL
    {
        public bool AddVdwRemarks(RemarksEntity remarksDetails)
        {
            FormatVdwRemarks(remarksDetails);
            return AddVdwRemarksDal.ExecuteSp(remarksDetails);
        }

        private void FormatVdwRemarks(RemarksEntity remarksDetails)
        {
            remarksDetails.Remarks += " - " + remarksDetails.VendorId + " - " + String.Format("{0:MM/dd/yyyy HH:mm}", remarksDetails.RemarksTimeStamp);
        }
    }
}
