﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BusinessEntities.VDWEntities;

namespace BusinessLayer.VDWLogic
{
    public class LoggingInfoBLL
    {
        public bool InsertLoggingInfo(LoggingEntity loggingEntity)
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.SaveLoggingInfoDal.ExecuteSp(loggingEntity);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
