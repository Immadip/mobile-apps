﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.VDWEntities;

namespace BusinessLayer.VDWLogic
{
    public class NotificationsBLL
    {
        public List<NotificationEntity> GetNotificationsForUser(string userId)
        {
            return DataAccessLayer.VDWDataAccess.GetNotificationsForUserDal.ExecuteSp(userId);
        }

        public bool MarkNotificationsAsRead(string userId)
        {
            return DataAccessLayer.VDWDataAccess.MarkNotificationsAsReadDal.ExecuteSp(userId);
        }

        public int GetNotificationCount(string deviceId,string userId)
        {
            return DataAccessLayer.VDWDataAccess.GetNotificationCountDal.ExecuteSp(deviceId, userId);
        }

        public void SetNotificationCount(string deviceId, string userId, int notificationCount)
        {
            DataAccessLayer.VDWDataAccess.SetNotificationCountDal.ExecuteSp(deviceId, userId, notificationCount);
        }
    }
}
