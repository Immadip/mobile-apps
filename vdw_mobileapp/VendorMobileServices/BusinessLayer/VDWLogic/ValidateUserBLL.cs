﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BusinessLayer.VDWLogic
{
    public class ValidateUserBLL
    {
        public bool GetValidateUser(string vendorId, string password)
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.ValidateUserDal.ExecuteSp(vendorId, password);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
