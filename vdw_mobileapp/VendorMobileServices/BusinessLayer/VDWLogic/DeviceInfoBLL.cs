﻿using BusinessEntities.VDWEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.VDWLogic
{
    public class DeviceInfoBLL
    {
        public bool InsertDeviceInfo(DeviceEntity deviceEntity)
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.DeviceInfoDal.ExecuteSp(deviceEntity);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
