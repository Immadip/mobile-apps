﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities.VDWEntities;

namespace BusinessLayer.VDWLogic
{
    public class CompleteJobBLL
    {
        public bool CompleteJob(BusinessEntities.VDWEntities.JobCompletionEntity completejobinfo)
        {
            try
            {                
                return DataAccessLayer.VDWDataAccess.CompleteJobDal.ExecuteSp(completejobinfo);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Category> GetCategories()
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.CompleteJobDal.ExecuteCategorySp();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<GlobalFlagEntity> GetGlobalFlags()
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.ReadGlobalFlagsDal.ExecuteSP();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<JobCompletionEntity> GetJobsCompletionInfo(string userId, UserRole userRole, string regionId)
        {
            return DataAccessLayer.VDWDataAccess.GetJobsCompletionInfoDal.ExecuteSp(userId, userRole, regionId);
        }
    }
}
