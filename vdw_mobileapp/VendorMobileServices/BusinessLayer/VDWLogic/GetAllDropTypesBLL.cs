﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities.VDWEntities;

namespace BusinessLayer.VDWLogic
{
   public class GetAllDropTypesBLL
   {
        public List<DropType> GetDropTypes()
        {
            try
            {
                return DataAccessLayer.VDWDataAccess.GetAllDropTypesDal.ExecuteSp();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
