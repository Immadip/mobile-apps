﻿using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.VDWDataAccess
{
    public class DeviceInfoDal
    {
        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_INSERT_DEVICEINFO;
        }

        public static bool ExecuteSp(DeviceEntity deviceEntity)
        {
            try
            {
                var con = new OracleConnection
                {
                    ConnectionString = ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString
                };
                con.Open();

                var oraCmd = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = GetProcName(),
                    Connection = con
                };

                var db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "pDevice_Id", ParamType.String, (object)deviceEntity.DeviceId ?? string.Empty);
                db.AddInParameter(oraCmd, "pUser_Id", ParamType.String, (object)deviceEntity.UserId ?? string.Empty);
                db.AddInParameter(oraCmd, "pMobile_OS", ParamType.String, (object)deviceEntity.MobileOS ?? string.Empty);
                db.AddInParameter(oraCmd, "pDevice_Token", ParamType.String, (object)deviceEntity.DeviceToken ?? string.Empty);
                var ireturnvalue = oraCmd.ExecuteNonQuery();

                con.Close();
                con.Dispose();

                if (ireturnvalue != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
