﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;

namespace DataAccessLayer.VDWDataAccess
{
    public class GetJobStatusDal
    {
        private readonly string _doOrderId;

        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_GET_JOB_STATUS;
        }

        private GetJobStatusDal(string doOrderId)
        {
            _doOrderId = doOrderId;
        }

        public bool MapInput(ref GenericDataAccess db, ref IDbCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            db.AddInParameter(command, "p_doOrderId", ParamType.String, _doOrderId);

            db.AddCursorOutParameter(command, "RECORDS_REF");
            return true;
        }

        public static string ExecuteSp(string doOrderId)
        {
            try
            {
                var ds = new DataSet();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetProcName();
                oraCmd.Connection = con;

                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "p_doOrderId", ParamType.String, doOrderId);

                db.AddCursorOutParameter(oraCmd, "Records_ref");

                DataSet dsDataset = new DataSet();
                using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
                {
                    daAdapter.Fill(dsDataset);
                }

                string returnString = string.Empty;
                if (dsDataset.Tables.Count > 0)
                    returnString = dsDataset.Tables[0].Rows[0][0].ToString();

                con.Close();
                con.Dispose();

                return returnString;//return MapResponse(dsDataset.Tables[0]);
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
