﻿using System;
using System.Configuration;
using System.Data;
using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;
using Oracle.DataAccess.Client;

namespace DataAccessLayer.VDWDataAccess
{
    public class AddVdwRemarksDal
    {
        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_UPDATE_ADD_REMARKS;
        }


        public static bool ExecuteSp(RemarksEntity remarksDetails)
        {
            var con = new OracleConnection
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString
            };

            con.Open();

            var oraCmd = new OracleCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = GetProcName(),
                Connection = con
            };

            var db = GenericDataAccess.CreateDataAccess("ODP");

            db.AddInParameter(oraCmd, "P_JOBID", ParamType.String, remarksDetails.JobId);
            db.AddInParameter(oraCmd, "P_VDW_REMARKS", ParamType.String, remarksDetails.Remarks);
            db.AddInParameter(oraCmd, "P_VENDOR", ParamType.Clob, remarksDetails.VendorId);
            db.AddCursorOutParameter(oraCmd, "Records_ref");

            var ireturnvalue = oraCmd.ExecuteNonQuery();

            con.Close();
            con.Dispose();

            return ireturnvalue != 0;
        }
    }
}
