﻿using System.Configuration;
using System.Data;
using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;
using Oracle.DataAccess.Client;
using System;

namespace DataAccessLayer.VDWDataAccess
{
    public class SaveImageDetailsDal
    {
        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_SAVE_IMGDETAILS_MOBILE;
        }

        public static bool ExecuteSp(ImageEntity image)
        {
            var con = new OracleConnection
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString
            };
            con.Open();

            var oraCmd = new OracleCommand
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = GetProcName(),
                Connection = con
            };

            var db = GenericDataAccess.CreateDataAccess("ODP");
            db.AddInParameter(oraCmd, "p_Tech_Id", ParamType.String, (object)image.TechId ?? string.Empty);
            db.AddInParameter(oraCmd, "p_Job_Id", ParamType.String, (object)image.JobId ?? string.Empty);
            db.AddInParameter(oraCmd, "p_image_Category_Id", ParamType.Int32, (object)image.CategoryId ?? string.Empty);
            db.AddInParameter(oraCmd, "p_Comments", ParamType.String, (object)image.Comments ?? string.Empty);
            db.AddInParameter(oraCmd, "p_Latitude", ParamType.String, (object)image.Latitude ?? string.Empty);
            db.AddInParameter(oraCmd, "p_Longitude", ParamType.String, (object)image.Longitude ?? string.Empty);            
            db.AddInParameter(oraCmd, "p_Image_Time_Stamp", ParamType.Time, (object)image.ImageTimeStamp ?? string.Empty);
            db.AddInParameter(oraCmd, "p_Image_Binary", ParamType.Blob, Convert.FromBase64String(image.ImageBinary));
            db.AddInParameter(oraCmd, "p_Image_Thumbnail_Binary", ParamType.Blob, Convert.FromBase64String(image.ImageThumbnailBinary));
            db.AddOutParameter(oraCmd, "p_Result", ParamType.Int32);
            var ireturnvalue = oraCmd.ExecuteNonQuery();

            con.Close();
            con.Dispose();

            return ireturnvalue != 0;
        }
    }
}
