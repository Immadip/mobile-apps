﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;

namespace DataAccessLayer.VDWDataAccess
{
    public class GetJobAssignDal
    {
        private readonly string _regionid;
        private readonly string _vendorId;

        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_GET_JOBASSIGN_MOBILE;
        }

        private GetJobAssignDal(string regionId, string vendorId)
        {
            _regionid = regionId;
            _vendorId = vendorId;
        }

        public static bool AssignJob(string jobId,string techCode)
        {
            try
            {
                var ds = new DataSet();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = ConstantSP.SP_VDW_INSERT_ASSIGN_TECHN;
                oraCmd.Connection = con;

                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "p_TechId", ParamType.String, techCode);
                db.AddInParameter(oraCmd, "p_JobId", ParamType.String, jobId);

                int ireturnvalue = oraCmd.ExecuteNonQuery();

                con.Close();
                con.Dispose();

                if (ireturnvalue != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static List<JobEntity> ExecuteSp(string regionId, string vendorId)
        {
            try
            {
                var ds = new DataSet();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetProcName();
                oraCmd.Connection = con;

                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "p_vendorId", ParamType.String, vendorId);
                db.AddCursorOutParameter(oraCmd, "Records_ref");

                DataSet dsDataset = new DataSet();

                //Added using block - DISPOSE
                using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
                {
                    daAdapter.Fill(dsDataset);
                }

                con.Close();
                con.Dispose();

                return GetJobSummaryDal.MapResponse(dsDataset.Tables[0]);
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
