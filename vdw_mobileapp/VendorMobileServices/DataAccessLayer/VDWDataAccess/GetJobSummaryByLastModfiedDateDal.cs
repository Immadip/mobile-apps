﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;

namespace DataAccessLayer.VDWDataAccess
{
    public class GetJobSummaryByLastModfiedDateDal
    {
        private readonly string _vendorId;
        private readonly DateTime _lastModifiedDate;

        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_GET_JOBLIST_DATE_MOBILE;
        }

        private GetJobSummaryByLastModfiedDateDal(string vendorId, DateTime lastModifiedDate)
        {
            _vendorId = vendorId;
            _lastModifiedDate = lastModifiedDate;
        }

        public bool MapInput(ref GenericDataAccess db, ref IDbCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            db.AddInParameter(command, "pVendorId", ParamType.String, _vendorId);
            db.AddInParameter(command, "pLast_Modify_Time_Stamp", ParamType.DateTime, _lastModifiedDate);
            db.AddCursorOutParameter(command, "RECORDS_REF");
            return true;
        }

        public static List<JobEntity> ExecuteSp(string vendorId, DateTime lastModifiedDate)
        {
            try
            {
                var ds = new DataSet();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetProcName();
                oraCmd.Connection = con;

                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "p_vendorId", ParamType.String, vendorId);
                db.AddInParameter(oraCmd, "p_Last_Modify_Time_Stamp", ParamType.DateTimeOffset, lastModifiedDate);
                db.AddCursorOutParameter(oraCmd, "Records_ref");

                DataSet dsDataset = new DataSet();

                //Added using block - DISPOSE
                using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
                {
                    daAdapter.Fill(dsDataset);
                }

                con.Close();
                con.Dispose();

                return GetJobSummaryDal.MapResponse(dsDataset.Tables[0]);
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
