﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;
using System.Configuration;

namespace DataAccessLayer.VDWDataAccess
{
    public class MarkNotificationsAsReadDal
    {
        private readonly List<string> _notificationIds;

        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_MARK_NOTIFICATIONS_READ;
        }

        private MarkNotificationsAsReadDal(List<string> notificationIds)
        {
            _notificationIds = notificationIds;
        }

        public bool MapInput(ref GenericDataAccess db, ref IDbCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            var commaSeparatedNotificationIds = _notificationIds.Aggregate("", (current, notificationId) => current + (notificationId + ",")).TrimEnd(',');
            db.AddInParameter(command, "p_NotificationIds", ParamType.String, commaSeparatedNotificationIds);
            return true;
        }

        public static bool ExecuteSp(string userId)
        {
            try
            {
                var ds = new DataSet();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetProcName();
                oraCmd.Connection = con;

                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "p_User_Id", ParamType.String, userId);
                int ireturnvalue = oraCmd.ExecuteNonQuery();

                con.Close();
                con.Dispose();

                if (ireturnvalue != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
