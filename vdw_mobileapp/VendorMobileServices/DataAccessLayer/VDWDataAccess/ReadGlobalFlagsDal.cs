﻿using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.VDWDataAccess
{
    public class ReadGlobalFlagsDal
    {
        private static string GetProcName()
        {
            return ConstantSP.SP_VDW_GET_GLOBALPARAM_MOBILE;
        }

        public static List<GlobalFlagEntity> ExecuteSP()
        {
            var dt = new DataTable();
            OracleConnection con;
            con = new OracleConnection();
            con.ConnectionString =
                ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
            con.Open();

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.CommandText = GetProcName();
            oraCmd.Connection = con;


            GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
            db.AddCursorOutParameter(oraCmd, "Records_ref");

            //Added using block - DISPOSE
            using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
            {
                daAdapter.Fill(dt);
            }

            con.Close();
            con.Dispose();
            return MapGlobalFlagsResponse(dt);
        }

        public static List<GlobalFlagEntity> MapGlobalFlagsResponse(DataTable dataTbl)
        {
            List<GlobalFlagEntity> globalFlags = new List<GlobalFlagEntity>();
            if (dataTbl != null)
            {
                globalFlags = dataTbl.Rows.Cast<DataRow>().Select(row => new GlobalFlagEntity()
                {
                    Key = row["NAME"].ToString(),
                    Value = row["VALUE"].ToString(),

                }).ToList();
            }
            return globalFlags;
        }
    }
}
