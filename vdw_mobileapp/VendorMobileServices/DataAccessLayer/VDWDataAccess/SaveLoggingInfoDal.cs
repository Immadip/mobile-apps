﻿using System.Configuration;
using System.Data;
using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;
using Oracle.DataAccess.Client;
using System;

namespace DataAccessLayer.VDWDataAccess
{
    public class SaveLoggingInfoDal
    {
        public static string GetProcName()
        {
            return ConstantSP.SP_INSERT_VDW_LOGGING;
        }

        public static bool ExecuteSp(LoggingEntity loggingEntity)
        {
            try
            {
                var con = new OracleConnection
                {
                    ConnectionString = ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString
                };
                con.Open();

                var oraCmd = new OracleCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = GetProcName(),
                    Connection = con
                };

                var db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "pTech_Id", ParamType.String, (object)loggingEntity.TechId ?? string.Empty);
                db.AddInParameter(oraCmd, "pVendor_Id", ParamType.String, (object)loggingEntity.VendorId ?? string.Empty);
                db.AddInParameter(oraCmd, "pLog_Message", ParamType.String, (object)loggingEntity.LogMessage ?? string.Empty);
                db.AddInParameter(oraCmd, "pLog_Description", ParamType.String, (object)loggingEntity.LogDescription ?? string.Empty);
                db.AddInParameter(oraCmd, "pLog_Created_Date_Time", ParamType.Time, (object)loggingEntity.LogCreatedDate ?? string.Empty);
                db.AddCursorOutParameter(oraCmd, "Records_ref");
                var ireturnvalue = oraCmd.ExecuteNonQuery();

                con.Close();
                con.Dispose();

                if (ireturnvalue != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }            
        }
    }
}
