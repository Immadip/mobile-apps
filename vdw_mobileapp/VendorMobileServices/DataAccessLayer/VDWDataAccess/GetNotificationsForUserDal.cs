﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;

namespace DataAccessLayer.VDWDataAccess
{
    public class GetNotificationsForUserDal
    {
        private readonly string _userId;

        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_GET_NOTIFICATIONS_USER;
        }

        private GetNotificationsForUserDal(string userId)
        {
            _userId = userId;
        }

        public bool MapInput(ref GenericDataAccess db, ref IDbCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            db.AddInParameter(command, "p_User_Id", ParamType.String, _userId);
            db.AddCursorOutParameter(command, "Records_ref");
            return true;
        }

        public static List<NotificationEntity> ExecuteSp(string userId)
        {
            var dsNotifications = new DataSet();

            OracleConnection con;
            con = new OracleConnection();
            con.ConnectionString =
                ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;;
            con.Open();

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.CommandText = GetProcName();
            oraCmd.Connection = con;

            GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
            db.AddInParameter(oraCmd, "p_User_Id", ParamType.String, userId);
            db.AddCursorOutParameter(oraCmd, "Records_ref");

            using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
            {
                daAdapter.Fill(dsNotifications);
            }
            con.Close();
            con.Dispose();
            if(dsNotifications != null && dsNotifications.Tables.Count > 0)
                return MapResponse(dsNotifications.Tables[0]);
            return null;
        }

        public static List<NotificationEntity> MapResponse(DataTable dtNotifications)
        {
            List<NotificationEntity> notifications = new List<NotificationEntity>();
            if (dtNotifications != null)
            {
                notifications = dtNotifications.Rows.Cast<DataRow>().Select(row => new NotificationEntity()
                {
                    UserId = row["User_Id"].ToString(),
                    NotificationId = Convert.ToInt32(row["Notification_Id"]),
                    NotificationTitle = row["Notification_Title"].ToString(),
                    NotificationText = row["Notification_Text"].ToString(),
                    NotificationType = row["Notification_Type"].ToString(),
                    NotificationReceivedTime = Convert.ToDateTime(row["Notification_Received_Time"]),
                }).ToList();
            }
            return notifications;
        }
    }
}
