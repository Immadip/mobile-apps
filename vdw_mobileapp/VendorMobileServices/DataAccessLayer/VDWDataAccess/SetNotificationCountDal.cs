﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;

namespace DataAccessLayer.VDWDataAccess
{
    public class SetNotificationCountDal
    {
        private readonly string _deviceId;
        private readonly string _userId;
        private readonly int _notificationCount;

        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_SET_NOTIFICATIONS_COUNT;
        }

        private SetNotificationCountDal(string deviceId,string userId, int notificationCount)
        {
            _deviceId = deviceId;
            _userId = userId;
            _notificationCount = notificationCount;
        }

        public static void ExecuteSp(string deviceId, string userId, int notificationCount)
        {
            var dsNotifications = new DataSet();

            OracleConnection con;
            con = new OracleConnection();
            con.ConnectionString =
                ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString; ;
            con.Open();

            OracleCommand oraCmd = new OracleCommand();
            oraCmd.CommandType = CommandType.StoredProcedure;
            oraCmd.CommandText = GetProcName();
            oraCmd.Connection = con;

            GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
            db.AddInParameter(oraCmd, "P_USER_ID", ParamType.String, userId);
            db.AddInParameter(oraCmd, "P_DEVICE_ID", ParamType.String, deviceId);
            db.AddOutParameter(oraCmd, "P_Notification_Count", ParamType.Int32, notificationCount);

            db.ExecuteNonQuery(oraCmd);

            con.Close();
            con.Dispose();
           
       
        }
    }
}
