﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;

namespace DataAccessLayer.VDWDataAccess
{
    public class CompleteJobDal
    {
        private readonly string _jobId;
        private readonly string _comments;

        public static string GetProcName()
        {            
            return ConstantSP.SP_VDW_JOB_COMPLETION;
        }

        public static string GetCategoryProcName()
        {            
            return ConstantSP.SP_VDW_GET_CATEGORIES_MOBILE;
        }


        private CompleteJobDal(string jobId, string comments)
        {
            _jobId = jobId;
            _comments = comments;
        }

        public bool MapInput(ref GenericDataAccess db, ref IDbCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            db.AddInParameter(command, "pJobId", ParamType.String, _jobId);
            db.AddInParameter(command, "pComments", ParamType.String, _comments);
            db.AddCursorOutParameter(command, "RECORDS_REF");
            return true;
        }

        public static bool ExecuteSp(BusinessEntities.VDWEntities.JobCompletionEntity completejobinfo)
        {
            try
            {
                var ds = new DataSet();
                
                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;;
                con.Open();
                
                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetProcName();
                oraCmd.Connection = con;                               
                
                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");

                //db.AddInParameter(oraCmd, "p_ORDERID", ParamType.String, (object)completejobinfo.OrderId ?? string.Empty);
                db.AddInParameter(oraCmd, "P_JOBID", ParamType.String, (object)completejobinfo.JobId ?? string.Empty);
                db.AddInParameter(oraCmd, "P_CLEAR_DATE", ParamType.String, (object)completejobinfo.ClearDate ?? string.Empty);
                db.AddInParameter(oraCmd, "P_CLEAR_TIME", ParamType.String, (object)completejobinfo.ClearTime ?? string.Empty);
                db.AddInParameter(oraCmd, "P_CMPREMARKS", ParamType.String, (object)completejobinfo.CompletionRemarks ?? string.Empty);
                db.AddInParameter(oraCmd, "P_ARRIVAL_DATE", ParamType.String, (object)completejobinfo.ArrivalDate ?? string.Empty);
                db.AddInParameter(oraCmd, "P_ARRIVAL_TIME", ParamType.String, (object)completejobinfo.ArrivalTime ?? string.Empty);
                db.AddInParameter(oraCmd, "P_JWM_DATE", ParamType.String, (object)completejobinfo.JeopardyDate ?? string.Empty);
                db.AddInParameter(oraCmd, "P_JWM_TIME", ParamType.String, (object)completejobinfo.JeopardyTime ?? string.Empty);
                db.AddInParameter(oraCmd, "P_JWM_CODE", ParamType.String, (object)completejobinfo.JeoPardyCode ?? string.Empty);
                db.AddInParameter(oraCmd, "P_REFER_TO_ENGINEERING", ParamType.String, (object)completejobinfo.ReferToEngg ?? string.Empty);
                db.AddInParameter(oraCmd, "P_ENGINEERING_REMARKS", ParamType.String, (object)completejobinfo.EnggRemarks ?? string.Empty);
                db.AddInParameter(oraCmd, "P_CONTACT_NAME", ParamType.String, (object)completejobinfo.ContactName ?? string.Empty);
                db.AddInParameter(oraCmd, "P_CONTACT_NUMBER", ParamType.String, (object)completejobinfo.Contact ?? string.Empty);
                db.AddInParameter(oraCmd, "P_PRECUT_DROP_LENGTH", ParamType.String, (object)completejobinfo.PreCutLength ?? string.Empty);
                db.AddInParameter(oraCmd, "P_ONT_PLACED", ParamType.String, (object)completejobinfo.DidplaceanONT ?? string.Empty);
                db.AddInParameter(oraCmd, "P_DROP_TERMINATED", ParamType.String, (object)completejobinfo.DidTerminateDrop ?? string.Empty);
                db.AddInParameter(oraCmd, "P_DROP_TYPE", ParamType.String, (object)completejobinfo.DropType ?? string.Empty);
                db.AddInParameter(oraCmd, "P_DROP_LENGTH", ParamType.String, (object)completejobinfo.DropConduitLength ?? string.Empty);
                db.AddInParameter(oraCmd, "P_NUM_BORES", ParamType.String, (object)completejobinfo.NoofBores ?? string.Empty);
                db.AddInParameter(oraCmd, "P_TOTAL_BORE_LENGTH", ParamType.String, (object)completejobinfo.TotBoreLength ?? string.Empty);                
                db.AddInParameter(oraCmd, "P_ONT_LOCATION", ParamType.String, (object)completejobinfo.ONTLocInOut ?? string.Empty);
                db.AddInParameter(oraCmd, "P_ONT_LOCATION_REMARKS", ParamType.String, (object)completejobinfo.ONTLocationRemarks ?? string.Empty);                
                db.AddInParameter(oraCmd, "P_CABLE_LOCATE", ParamType.String, (object)completejobinfo.CableLocateNum ?? string.Empty);
                db.AddInParameter(oraCmd, "P_BORE_TYPE", ParamType.String, (object)completejobinfo.BoreType ?? string.Empty);                
                db.AddInParameter(oraCmd, "P_BORE_TYPE_NUMBER", ParamType.String, (object)completejobinfo.BoreTypeNumber ?? string.Empty);                
                db.AddInParameter(oraCmd, "P_BORE_TYPE_LENGTH", ParamType.String, (object)completejobinfo.BoreTypeLength ?? string.Empty);
                db.AddInParameter(oraCmd, "P_TRIP_CHARGE", ParamType.String, (object)completejobinfo.TripCharge ?? string.Empty);
                db.AddInParameter(oraCmd, "P_PULL_THROUGH", ParamType.String, (object)completejobinfo.PullthroughLength ?? string.Empty);
                db.AddInParameter(oraCmd, "P_MAP_POLYGON_JSON", ParamType.Clob, (object)completejobinfo.MapPolygonJson ?? string.Empty);
                db.AddCursorOutParameter(oraCmd, "Records_ref");

                int ireturnvalue = oraCmd.ExecuteNonQuery();

                con.Close();
                con.Dispose();

                if (ireturnvalue != 0)
                    return true;
                else
                    return false;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static List<Category> ExecuteCategorySp()
        {
            try
            {
                var dt = new DataTable();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetCategoryProcName();
                oraCmd.Connection = con;


                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddCursorOutParameter(oraCmd, "Records_ref");

                //Added using block - DISPOSE
                using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
                {
                    daAdapter.Fill(dt);
                }

                con.Close();
                con.Dispose();
                return MapResponse(dt);


            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public static List<Category> MapResponse(DataTable dataTbl)
        {
            List<Category> lstcategories = new List<Category>();
            try
            {

                if (dataTbl != null)
                {
                    lstcategories = dataTbl.Rows.Cast<DataRow>().Select(row => new Category()
                    {
                        CategoryDescription = row["CATOGORY_DISCRIPTION"].ToString(),
                        CategoryID = Convert.ToInt32(row["CATOGORY_ID"].ToString())

                    }).ToList();
                }

            }
            catch (Exception ex)
            {

            }

            return lstcategories;
        }

        
    }
}
