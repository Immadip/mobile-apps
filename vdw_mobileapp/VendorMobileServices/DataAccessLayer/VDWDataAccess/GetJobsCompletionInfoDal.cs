﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;


namespace DataAccessLayer.VDWDataAccess
{
    public class GetJobsCompletionInfoDal
    {
        private readonly string _regionId;
        private readonly string _userId;
        private readonly UserRole _userRole;

        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_GET_COMPLETION;
        }

        private GetJobsCompletionInfoDal(string userId, UserRole userRole, string regionId)
        {
            _regionId = regionId;
            _userId = userId;
            _userRole = userRole;
        }

        public bool MapInput(ref GenericDataAccess db, ref IDbCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            db.AddInParameter(command, "p_userId", ParamType.String, _userId);

            var vendorOrTech = _userRole == UserRole.Vendor ? "VENDOR" : "TECH";
            db.AddInParameter(command, "p_vendorOrTech", ParamType.String, vendorOrTech);

            db.AddCursorOutParameter(command, "RECORDS_REF");
            return true;
        }

        public static List<JobCompletionEntity> ExecuteSp(string userId, UserRole userRole, string regionId)
        {
            try
            {
                var ds = new DataSet();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetProcName();
                oraCmd.Connection = con;

                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddInParameter(oraCmd, "p_userId", ParamType.String, userId);

                var vendorOrTech = userRole == UserRole.Vendor ? "VENDOR" : "TECH";
                db.AddInParameter(oraCmd, "p_vendorOrTech", ParamType.String, vendorOrTech);

                db.AddCursorOutParameter(oraCmd, "RECORDS_REF");

                DataSet dsDataset = new DataSet();

                //Added using block - DISPOSE
                using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
                {
                    daAdapter.Fill(dsDataset);
                }

                con.Close();
                con.Dispose();

                return MapResponse(dsDataset.Tables[0]);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public static List<JobCompletionEntity> MapResponse(DataTable dataTbl)
        {
            var jobsCompletionInfo = new List<JobCompletionEntity>();
            try
            {
                if (dataTbl != null)
                {
                    jobsCompletionInfo.AddRange(dataTbl.Rows.Cast<DataRow>().Select(row => new JobCompletionEntity()
                    {
                        DoOrderId = row["DO_ORDER_ID"].ToString(),
                        JobId = row["JOB_ID"].ToString(),
                        Mon = row["MON"].ToString(),
                        ClearDate = row["CLEAR_DATE"].ToString(),
                        ClearTime = row["CLEAR_TIME"].ToString(),
                        CompletionRemarks = row["CMPREMARKS"].ToString(),
                        ArrivalDate = row["ARRIVAL_DATE"].ToString(),
                        ArrivalTime = row["ARRIVAL_TIME"].ToString(),
                        JeopardyDate = row["JWM_DATE"].ToString(),
                        JeopardyTime = row["JWM_TIME"].ToString(),
                        JeoPardyCode = row["JWM_CODE"].ToString(),
                        ReferToEngg = row["REFER_TO_ENGINEERING"].ToString(),
                        EnggRemarks = row["ENGINEERING_REMARKS"].ToString(),
                        ContactName = row["CONTACT_NAME"].ToString(),
                        Contact = row["CONTACT_NUMBER"].ToString(),
                        PreCutLength = row["PRECUT_DROP_LENGTH"].ToString(),
                        DidplaceanONT = row["ONT_PLACED"].ToString(),
                        DidTerminateDrop = row["DROP_TERMINATED"].ToString(),
                        DropType = row["DROP_TYPE"].ToString(),
                        DropConduitLength = row["DROP_LENGTH"].ToString(),
                        NoofBores = row["NUM_BORES"].ToString(),
                        TotBoreLength = row["TOTAL_BORE_LENGTH"].ToString(),
                        ONTLocInOut = row["ONT_LOCATION"].ToString(),
                        ONTLocationRemarks = row["ONT_LOCATION_REMARKS"].ToString(),
                        CableLocateNum = row["CABLE_LOCATE"].ToString(),
                        BoreType = row["BORE_TYPE"].ToString(),
                        BoreTypeNumber = row["BORE_TYPE_NUMBER"].ToString(),
                        BoreTypeLength = row["BORE_TYPE_LENGTH"].ToString(),
                        TripCharge = row["TRIP_CHARGE"].ToString(),
                        PullthroughLength = row["PULL_THROUGH_LENGTH"].ToString(),
                        MapPolygonJson = row["MAP_POLYGON_JSON"].ToString(),
                    }).Distinct());

                    var images = new List<ImageEntity>();
                    images.AddRange(dataTbl.Rows.Cast<DataRow>().Select(row => new ImageEntity()
                    {
                        DoOrderId = row["DO_ORDER_ID"].ToString(),
                        JobId = row["JOB_ID"].ToString(),
                        Mon = row["MON"].ToString(),
                        TechId = row["TECH_ID"].ToString(),
                        CategoryId = int.Parse(row["IMAGE_CATEGORY_ID"].ToString()),
                        Comments = row["COMMENTS"].ToString(),
                        Latitude = Convert.ToDouble(row["LATITUDE"].ToString()),
                        Longitude = Convert.ToDouble(row["LONGITUDE"].ToString()),
                        ImageTimeStamp = Convert.ToDateTime(row["IMAGE_TIME_STAMP"].ToString()),
                        ImageBinary = Convert.ToBase64String((byte[])row["IMAGE_BINARY"]),
                        ImageThumbnailBinary = Convert.ToBase64String((byte[])row["IMAGE_THUMBNAIL_BINARY"]),
                    }));

                    foreach(var jobCompInfo in jobsCompletionInfo)
                    {
                        jobCompInfo.Images = images.Where(i => i.DoOrderId == jobCompInfo.DoOrderId).ToList();
                    }
                }

            }
            catch (Exception ex)
            {

            }

            return jobsCompletionInfo;
        }
    }
}
