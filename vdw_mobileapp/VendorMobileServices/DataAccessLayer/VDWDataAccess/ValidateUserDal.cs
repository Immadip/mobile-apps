﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;

namespace DataAccessLayer.VDWDataAccess
{
    public class ValidateUserDal
    {
        private readonly string _vendorId;
        private readonly string _password;

        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_VALIDATE_USER_MOBILE;
        }

        private ValidateUserDal(string vendorId, string password)
        {
            _vendorId = vendorId;
            _password = password;
        }

        public bool MapInput(ref GenericDataAccess db, ref IDbCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            db.AddInParameter(command, "pVendorId", ParamType.String, _vendorId);
            db.AddInParameter(command, "pPassword", ParamType.String, _password);
            db.AddCursorOutParameter(command, "pResult");
            return true;
        }

        public static bool ExecuteSp(string vendorId, string password)
        {
            try
            {
                return true;

                #region Uncomment after the SP is ready
                //OracleConnection con;
                //con = new OracleConnection();
                //con.ConnectionString =
                //    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;;
                //con.Open();
                
                //OracleCommand oraCmd = new OracleCommand();
                //oraCmd.CommandType = CommandType.StoredProcedure;
                //oraCmd.CommandText = GetProcName();
                //oraCmd.Connection = con;
                
                //GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                //db.AddInParameter(oraCmd, "p_vendorId", ParamType.String, vendorId);
                //db.AddInParameter(oraCmd, "p_password", ParamType.String, password);
                //db.AddOutParameter(oraCmd, "p_result", ParamType.String);
                
                //oraCmd.ExecuteNonQuery();

                //con.Close();
                //con.Dispose();

                //if (oraCmd.Parameters["@p_result"].Value == "Y")
                //    return true;
                //else
                //    return false;

                #endregion
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
