﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Oracle.DataAccess.Client;
using System.Data;
using BusinessEntities.VDWEntities;
using DataAccessLayer.OracleDataAccess;
using DataAccessLayer.Utils;


namespace DataAccessLayer.VDWDataAccess
{
   public class GetAllGeopardyCodeDAL
    {
        public static string GetProcName()
        {
            return ConstantSP.SP_VDW_GET_JEOPARDYCODES;
        }

        public static List<JeopardyCode> ExecuteSp()
        {
            try
            {
                var dt = new DataTable();

                OracleConnection con;
                con = new OracleConnection();
                con.ConnectionString =
                    ConfigurationManager.ConnectionStrings["WBN_DEV_CP"].ConnectionString;
                con.Open();

                OracleCommand oraCmd = new OracleCommand();
                oraCmd.CommandType = CommandType.StoredProcedure;
                oraCmd.CommandText = GetProcName();
                oraCmd.Connection = con;


                GenericDataAccess db = GenericDataAccess.CreateDataAccess("ODP");
                db.AddCursorOutParameter(oraCmd, "Records_ref");

                //Added using block - DISPOSE
                using (OracleDataAdapter daAdapter = new OracleDataAdapter((OracleCommand)oraCmd))
                {
                    daAdapter.Fill(dt);
                }

                con.Close();
                con.Dispose();
                return MapResponse(dt);


            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public static List<JeopardyCode> MapResponse(DataTable dataTbl)
        {
            List<JeopardyCode> lstcodes = new List<JeopardyCode>();
            try
            {

                if (dataTbl != null)
                {
                    lstcodes = dataTbl.Rows.Cast<DataRow>().Select(row => new JeopardyCode()
                    {                 

                        JwmCode = row["JWMCODE"].ToString(),

                    }).ToList();
                }

            }
            catch (Exception ex)
            {

            }

            return lstcodes;
        }

    }
}
