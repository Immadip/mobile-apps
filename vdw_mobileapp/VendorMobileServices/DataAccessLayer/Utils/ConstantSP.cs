﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Utils
{
    public static class ConstantSP
    {
        public static string SP_VDW_GET_JOBLIST_MOBILE = "SP_VDW_GET_JOBLIST_MOBILE";
        public static string SP_VDW_GET_JOBLIST_DATE_MOBILE = "SP_VDW_GET_JOBLIST_DATE_MOBILE";
        public static string SP_VDW_GET_COMPLETION = "SP_VDW_GET_COMPLETION";
        public static string SP_VDW_GET_JOB_STATUS = "SP_VDW_GET_JOB_STATUS";
        public static string SP_VDW_GET_JOBDETAILS_MOBILE = "SP_VDW_GET_JOBDETAILS_MOBILE";
        public static string SP_VDW_VALIDATE_USER_MOBILE = "SP_VDW_VALIDATE_USER_MOBILE";
        //public static string SP_VDW_COMPLETE_JOB_MOBILE = "SP_VDW_COMPLETE_JOB_MOBILE";
        public static string SP_VDW_GET_NOTIFICATIONS_USER = "SP_VDW_GET_NOTIFICATIONS_USER";
        public static string SP_VDW_GET_NOTIFICATIONS_COUNT = "SP_VDW_GET_NOTIFICATIONS_COUNT";
        public static string SP_VDW_SET_NOTIFICATIONS_COUNT = "SP_VDW_SET_NOTIFICATIONS_COUNT";
        public static string SP_VDW_MARK_NOTIFICATIONS_READ = "SP_VDW_MARK_NOTIFICATIONS_READ";
        public static string SP_VDW_JOB_COMPLETION = "SP_VDW_JOB_COMPLETION_TEST"; //"SP_VDW_JOB_COMPLETION";
        public static string SP_VDW_GET_DROPTYPES_MOBILE = "SP_VDW_GET_DROPTYPES_MOBILE";
        public static string SP_VDW_GET_JEOPARDYCODES = "SP_VDW_GET_JEOPARDYMISSCODE";
        public static string SP_VDW_GET_CATEGORIES_MOBILE = "SP_VDW_GET_CATEGORIES_MOBILE";
        public static string SP_VDW_SAVE_IMGDETAILS_MOBILE = "SP_VDW_SAVE_IMGDETAILS_MOBILE";
        public static string SP_VDW_GET_GLOBALPARAM_MOBILE = "SP_VDW_GET_GLOBALPARAM_MOBILE";
        public static string SP_VDW_UPDATE_ADD_REMARKS = "SP_VDW_UPDATE_ADD_REMARKS";
        public static string SP_VDW_GET_JOBASSIGN_MOBILE = "SP_VDW_GET_JOBASSIGN_MOBILE";
        public static string SP_VDW_INSERT_ASSIGN_TECHN = "SP_VDW_INSERT_ASSIGN_TECHN";
        public static string SP_INSERT_VDW_LOGGING = "SP_INSERT_VDW_LOGGING";
        public static string SP_VDW_INSERT_DEVICEINFO = "SP_VDW_INSERT_DEVICEINFO";

        public static string SP_VDW_GET_JOBLIST_QUICKSEARCH = "SP_VDW_GET_JOBLIST_QUICKSEARCH";
        public static string SP_MGR_JOBDETAILS_BY_ORDER_ID = "SP_MGR_JOBDETAILS_BY_ORDER_ID";
        public static string SP_MGR_GET_JOBLIST_BY_MON = "SP_MGR_GET_JOBLIST_BY_MON";
        //public static string SP_VDW_UPDATE_ADD_REMARKS = "SP_VDW_UPDATE_ADD_REMARKS";
        public static string SP_MGR_GET_JOBS_BY_COMITDATE = "SP_MGR_GET_JOBS_BY_COMITDATE";
    }
}
