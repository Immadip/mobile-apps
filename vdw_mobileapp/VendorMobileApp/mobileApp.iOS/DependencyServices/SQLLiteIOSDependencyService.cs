﻿using mobileApp.iOS.DependencyServices;
using System; using System.Collections.Generic; using System.IO; using System.Linq; using System.Text; using mobileApp.SqlLiteDataAccess;
using SQLite; using Xamarin.Forms;
 using Environment = System.Environment; using mobileApp.DependencyServices;

[assembly: Dependency(typeof(SQLLiteIOSDependencyService))] namespace mobileApp.iOS.DependencyServices { 	public class SQLLiteIOSDependencyService : ISQLiteDependencyService    	{
		public SQLLiteIOSDependencyService()
		{ } 		public SQLite.SQLiteConnection GetConnection() 		{ 			const string sqliteFilename = "com.verizon.vdwmobile.db3"; 			string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder 			string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder 			var path = Path.Combine(libraryPath, sqliteFilename);


			if (!File.Exists(path))
			{
				File.Create(path);
			} 			// Create the connection 			var conn = new SQLite.SQLiteConnection(path); 			// Return the database connection 			return conn; 		} 	} } 