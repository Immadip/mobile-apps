﻿using System;
using mobileApp.DependencyServices;
using mobileApp.iOS;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(NotificationDependencyService))]
namespace mobileApp.iOS
{
    public class NotificationDependencyService : INotificationDependencyService
    {
        private static int _currentBadgeCount;

        public void IncrementLauncherIconBadgeCount(int incrementByValue)
        {
            SetLauncherIconBadgeCount(_currentBadgeCount + incrementByValue);
        }

        public void RegisterForPushNotifications()
        {
            throw new NotImplementedException();
        }

        public void SetLauncherIconBadgeCount(int notificationCount)
        {
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = notificationCount;
        }
    }
}
