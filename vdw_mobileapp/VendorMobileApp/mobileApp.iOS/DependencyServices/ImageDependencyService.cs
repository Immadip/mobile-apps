﻿using System;
using System.IO;
using CoreGraphics;
using Foundation;
using mobileApp.DependencyServices;
using mobileApp.iOS.DependencyServices;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(ImageDependencyService))]
namespace mobileApp.iOS.DependencyServices
{
	public class ImageDependencyService : IImageDependencyService
	{
		public byte[] ResizeImage(byte[] sourceImageBytes, string fileFormat, float maxWidth, float maxHeight)
		{
			var stream = new MemoryStream();
			if (sourceImageBytes != null)
			{
                var data = NSData.FromArray(sourceImageBytes);
                using (var sourceImage = UIImage.LoadFromData(data))
				{
					var sourceSize = sourceImage.Size;
					var maxResizeFactor = Math.Min(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);


					var width = maxResizeFactor * sourceSize.Width;
					var height = maxResizeFactor * sourceSize.Height;

					UIGraphics.BeginImageContextWithOptions(new CGSize((float)width, (float)height), true, 1.0f);
					//  UIGraphics.GetCurrentContext().RotateCTM(90 / Math.PI);
					sourceImage.Draw(new CGRect(0, 0, (float)width, (float)height));

					var resultImage = UIGraphics.GetImageFromCurrentImageContext();
					UIGraphics.EndImageContext();


					if (fileFormat.Contains("png"))
					{
						resultImage.AsPNG().AsStream().CopyTo(stream);//targetFile
					}
					else
					{
						resultImage.AsJPEG().AsStream().CopyTo(stream);
					}

				}
			}
			return stream.ToArray();
		}
	}
}
