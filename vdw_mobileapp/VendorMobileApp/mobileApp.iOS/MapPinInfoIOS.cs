﻿using System;
using MapKit;
using UIKit;
using System.Drawing;
using CoreGraphics;

namespace mobileApp.iOS
{
    public class MapPinInfoIOS : MKPinAnnotationView
    {
        public string Id { get; set; }

        public string Url { get; set; }

        //public string Line { get; set; }

        public MapPinInfoIOS(IMKAnnotation annotation, string id)
            : base(annotation, id)
        {
        }
    }
}

