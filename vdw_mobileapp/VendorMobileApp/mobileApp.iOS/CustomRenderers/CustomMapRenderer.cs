﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using mobileApp.Views;
using mobileApp.iOS.CustomRenderers;
using MapKit;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;
using BusinessEntities.Models;

[assembly: ExportRenderer(typeof(mobileApp.Views.JobSummaryMapPage), typeof(CustomMapRenderer))]
namespace mobileApp.iOS.CustomRenderers
{
    public class CustomMapRenderer : MapRenderer
    {
        UIView customPinView;
        List<MapPinEntity> customPins;

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                var nativeMap = Control as MKMapView;
                nativeMap.GetViewForAnnotation = null;
                nativeMap.CalloutAccessoryControlTapped -= OnCalloutAccessoryControlTapped;
                nativeMap.DidSelectAnnotationView -= OnDidSelectAnnotationView;
                nativeMap.DidDeselectAnnotationView -= OnDidDeselectAnnotationView;
            }

            if (e.NewElement != null)
            {
                var formsMap = (JobSummaryMapPage)e.NewElement;
                var nativeMap = Control as MKMapView;
                customPins = formsMap.CustomPins;

                nativeMap.GetViewForAnnotation = GetViewForAnnotation;
                nativeMap.CalloutAccessoryControlTapped += OnCalloutAccessoryControlTapped;
                nativeMap.DidSelectAnnotationView += OnDidSelectAnnotationView;
                nativeMap.DidDeselectAnnotationView += OnDidDeselectAnnotationView;
            }
        }

        MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView annotationView = null;

            if (annotation is MKUserLocation)
                return null;

            var anno = annotation as MKPointAnnotation;
            var customPin = GetCustomPin(anno);
            if (customPin == null)
            {
                throw new Exception("Custom pin not found");
            }

            annotationView = mapView.DequeueReusableAnnotation(customPin.Id);
            if (annotationView == null)
            {
                annotationView = new MapPinInfoIOS(annotation, customPin.Id);
                annotationView.CalloutOffset = new CGPoint(0, 0);
                annotationView.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);


                /*var view = new UIView();
                var label = new UILabel();
                label.LineBreakMode = UILineBreakMode.WordWrap;
                label.Lines = 0;
                //label.Text = ("Please \n work");
                view.AddSubview(label);
                annotationView.DetailCalloutAccessoryView = label;*/


                ((MapPinInfoIOS)annotationView).Id = customPin.Id;
                ((MapPinInfoIOS)annotationView).Url = customPin.Url;
            }
            annotationView.CanShowCallout = true;

            return annotationView;
        }

        void OnCalloutAccessoryControlTapped(object sender, MKMapViewAccessoryTappedEventArgs e)
        {
            var customView = e.View as MapPinInfoIOS;
            if (!string.IsNullOrWhiteSpace(customView.Url))
            {
                UIApplication.SharedApplication.OpenUrl(new Foundation.NSUrl(customView.Url));
            }
        }

        void OnDidSelectAnnotationView(object sender, MKAnnotationViewEventArgs e)
        {
            var customView = e.View as MapPinInfoIOS;
            customPinView = new UIView();


            var anno = e.View.Annotation;
            var customPin = GetCustomPin((MKPointAnnotation)anno);
            if (customPin == null)
            {
                throw new Exception("Custom pin not found");
            }

            var label = new UILabel();
            label.LineBreakMode = UILineBreakMode.WordWrap;
            label.Lines = 0;
            label.Text = customPin.Name + "\n" + customPin.Number + "\n" + customPin.Addr + "\n" + customPin.Type;



            customPinView.AddSubview(label);
            e.View.DetailCalloutAccessoryView = label;

            /*
            if (customView.Id == "Verizon")
            {
                customPinView.Frame = new CGRect(0, 0, 200, 84);
                var image = new UIImageView(new CGRect(0, 0, 200, 84));
                image.Image = UIImage.FromFile("verizon.png");
                customPinView.AddSubview(image);
                customPinView.Center = new CGPoint(0, -(e.View.Frame.Height + 75));
                e.View.AddSubview(customPinView);
            }*/
        }

        void OnDidDeselectAnnotationView(object sender, MKAnnotationViewEventArgs e)
        {
            if (!e.View.Selected)
            {
                customPinView.RemoveFromSuperview();
                customPinView.Dispose();
                customPinView = null;
            }
        }

        MapPinEntity GetCustomPin(MKPointAnnotation annotation)
        {
            var position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);
            foreach (var pin in customPins)
            {
                if (pin.Pin.Position == position)
                {
                    return pin;
                }
            }
            return null;
        }
    }
}