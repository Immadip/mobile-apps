﻿using mobileApp.CustomControls;
using mobileApp.iOS.CustomRenderers;
using System;
using System.Collections.Generic;
using System.Text;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomDatePicker), typeof(CustomDatePickerRenderer))]
namespace mobileApp.iOS.CustomRenderers
{
    public class CustomDatePickerRenderer : DatePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            CustomDatePicker datePicker = (CustomDatePicker)Element;

            if (datePicker != null)
            {
                SetFont(datePicker);
            }
        }



        private void SetFont(CustomDatePicker customDatePicker)
        {
            Control.Font = UIFont.FromName(customDatePicker.FontFamily, customDatePicker.FontSize);
        }
    }
}
