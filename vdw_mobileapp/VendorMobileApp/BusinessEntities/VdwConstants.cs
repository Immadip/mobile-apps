﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities
{
    public class VdwConstants
    {
        public static string JobPendingStatus = "PLD";
        public static string JobAssignedStatus = "PRE";
        public static string JobCompletedStatus = "CMP";

        public static int ImageCategoryIdForMapScreenshot = 5;
        public static string ImageCommentForMapScreenshot = "Screenshot of the Polygons in the Map";
    }
}
