﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace BusinessEntities.Models
{
    public class ImagesEntity
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Mon { get; set; }             
        public string DoOrderId { get; set; }
        public string JobId { get; set; }
        public string TechId { get; set; }
        public int CategoryId { get; set; }
        public string Comments { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime ImageTimeStamp { get; set; }
        public string ImageBinary { get; set; }
        public string ImageThumbnailBinary { get; set; }
        public string LastModifiedDate { get; set; }
        public string CreatedDate { get; set; }
        public string LastSyncedDate { get; set; }
        public bool SyncRequired { get; set; }

    }

}
