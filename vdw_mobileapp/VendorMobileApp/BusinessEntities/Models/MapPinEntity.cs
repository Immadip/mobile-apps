﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using Xamarin.Forms;

namespace BusinessEntities.Models
{
    public class MapPinEntity
    {
        public Pin Pin { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Number { get; set; }
        public string Addr { get; set; }
        public string Url { get; set; }
    }
}
