﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace BusinessEntities.Models
{
    public class JobCompletionEntity
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string JobId { get; set; }
        public string Mon { get; set; }
        public string DoOrderId { get; set; }
        public string CircuitId { get; set; }
        public string TechId { get; set; }
        public DateTime? VdwDispatchDateTime { get; set; }
        public string CommitmentDate { get; set; }
        public string CableLocateNum { get; set; }
        public string CusstatRemarks { get; set; }
        public string CompletionRemarks { get; set; }
        public string ArrivalDate { get; set; }
        public string ArrivalTime { get; set; }
        public string ClearDate { get; set; }
        public string ClearTime { get; set; }
        public string EnggRemarks { get; set; }
        public string ContactName { get; set; }
        public string Contact { get; set; }
        public string NoofBores { get; set; }
        public string BoreTypeNumber { get; set; }
        public string BoreTypeLength { get; set; }
        public string BoreType { get; set; }
        public string TotBoreLength { get; set; }
        public string DirectBuriedLength { get; set; }
        public string PullthroughLength { get; set; }
        public string DropConduitLength { get; set; }
        public string PreCutLength { get; set; }
        public string DropType { get; set; }
        public string JeoPardyCode { get; set; }
        public string JeopardyDate { get; set; }
        public string JeopardyTime { get; set; }
        public string DidCompletejob { get; set; }
        public string DidTerminateDrop { get; set; }
        public string ReferToEngg { get; set; }
        public string TripCharge { get; set; }
        public string DidplaceanONT { get; set; }
        public string ONTLocationRemarks { get; set; }
        public string ONTLocInOut { get; set; }
        public string MapPolygonJson { get; set; }
        public string LastSyncedDate { get; set; }
        public string LastModifiedDate { get; set; }
        public bool SyncRequired { get; set; }

        [Ignore]
        public List<ImagesEntity> Images { get; set; }
    }
}
