﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace BusinessEntities.Models
{
    public class NotificationEntity
    {
        [PrimaryKey]
        public int NotificationId { get; set; }
        public string UserId { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationText { get; set; }
        public string NotificationType { get; set; }
        public DateTime NotificationReceivedTime { get; set; }
        public bool HasRead { get; set; }
    }
}
