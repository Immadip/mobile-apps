﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace BusinessEntities.Models
{
    public class CategoryWithImagesEntity
    {
        public string CategoryDescription { get; set; }
        public int CategoryId { get; set; }
        public List<ImageDetails> Images { get; set; }
    }
	public class ImageDetails
	{
		public string Comment { get; set; }
		public Image Image{ get;set;}
	}

}
