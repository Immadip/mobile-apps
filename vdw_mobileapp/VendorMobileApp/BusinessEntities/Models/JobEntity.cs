﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using Xamarin.Forms;

namespace BusinessEntities.Models
{
    public class JobEntity
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Mon { get; set; }
        public string JobId { get; set; }
        public string DoOrderId { get; set; }
        public string CustomerName { get; set; }
        public string JobType { get; set; }
        public string JobStatus { get; set; }
        public string AltReachNumber { get; set; }
        public string CommitmentDate { get; set; }
        public string CircuitId { get; set; }
        public string CKL { get; set; }
        public string TechCode { get; set; }
        public string CustomerAddress { get; set; }
        public string PinCode { get; set; }
        public string CustomerRemarks { get; set; }
        public string VdwRemarks { get; set; }
        public string CustomerCBR { get; set; }
        public string ApptWindow { get; set; }
        public string OriginalDueDate { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string IMFlag { get; set; }
        public string FttpCoreType { get; set; }
        public string GarageId { get; set; }
        public string GarageName { get; set; }
        public string ReachNumber { get; set; }
        public string WireCenter { get; set; }
        public string DropType { get; set; }
        public string Region { get; set; }
        public string Center { get; set; }
        public string City { get; set; }
        public string TechName { get; set; }
        public DateTime? VdwDispatchDateTime { get; set; }
        public string VendorId { get; set; }
        public string VdwWireCenterGroup { get; set; }
        public string Cancellation { get; set; }
        public string ChangeHost { get; set; }
        public bool IsSelected { get; set; }
        public bool IsNonCompletedJob { get; set; }
        public string LastSyncedDate { get; set; }
        public string LastModifiedDate { get; set; }
        public bool SyncRequired { get; set; }
        public bool AddressCenter
        {
            get
            {
                return (Device.OS == TargetPlatform.Windows);
            }
        }

        public bool AddressNewLine
        {
            get
            {
                return (Device.OS != TargetPlatform.Windows);
            }
        }
    }
}
