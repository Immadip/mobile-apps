﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities.Models
{
    public class DropTypeEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string FiberType { get; set; }
        public bool ActiveStatus { get; set; }
    }
}
