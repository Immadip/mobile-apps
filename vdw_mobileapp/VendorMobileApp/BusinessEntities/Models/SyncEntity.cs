﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace BusinessEntities.Models
{
    public class SyncEntity
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsSyncRequired { get; set; }
        public string LastSyncDatetime { get; set; }
    }
}
