﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessEntities.Models
{
    public class JeopardyCodeEntity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string JwmCode { get; set; }
        public bool ActiveStatus { get; set; }
    }
}
