﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace BusinessEntities.Models
{
    public class LoggingEntity
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string TechId { get; set; }
        public string VendorId { get; set; }
        public string LogMessage { get; set; }
        public string LogDescription { get; set; }
        public string LogCreatedDate { get; set; }
        public string LastSyncedDate { get; set; }
        public string LastModifiedDate { get; set; }
        public bool SyncRequired { get; set; }
    }
}
