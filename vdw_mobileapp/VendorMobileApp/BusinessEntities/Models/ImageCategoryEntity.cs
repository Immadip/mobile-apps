﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace BusinessEntities.Models
{
    public class ImageCategoryEntity
    {
        public int CategoryId { get; set; }
        public string CategoryDescription { get; set; }
        public bool ActiveStatus { get; set; }
    }
}
