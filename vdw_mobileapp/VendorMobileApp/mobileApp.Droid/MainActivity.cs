﻿using System;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.OS;
//using System.IO.
//using Xamarin.Android;
using Xamarin.Forms;
using Xamarin.Forms.Build;
using Xamarin.Forms.Platform.Android;
using XLabs.Platform.Services.Geolocation;
using XLabs.Platform.Services.Media;
using XLabs.Platform.Device;
using XLabs.Ioc;
using XLabs.Forms.Mvvm;
using XLabs.Forms;
using Android.Content;
using Android.Telephony;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace mobileApp.Droid
{
    [Activity(Label = "VDW Mobile", Icon = "@drawable/vdwlogo", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Unspecified, Theme = "@style/CustomTheme")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        public static Context Context;
        protected override void OnCreate(Bundle bundle)
        {
            try
            {
                Context = this;

                base.OnCreate(bundle);
                global::Xamarin.Forms.Forms.Init(this, bundle);
                global::Xamarin.FormsMaps.Init(this, bundle);

                App.ScreenWidth = (int)(Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density);
                App.ScreenHeight = (int)(Resources.DisplayMetrics.HeightPixels / Resources.DisplayMetrics.Density);

                // Set the IMEI Value in the Global variable, so that it can be used while registering the Device in our DB
                var telephonyManager = (TelephonyManager)GetSystemService(TelephonyService);
                App.Imei = telephonyManager.DeviceId;

                var resolverContainer = new SimpleContainer();
                resolverContainer
                    .Register<IDevice>(t => AndroidDevice.CurrentDevice)
                    .Register<IMediaPicker, MediaPicker>()
                    .Register<IGeolocator, Geolocator>();
                if (!Resolver.IsSet)
                {
                    Resolver.SetResolver(resolverContainer.GetResolver());
                }
                System.Net.ServicePointManager.ServerCertificateValidationCallback += (o, certificate, chain, errors) => true;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
                //Window.DecorView.SetBackgroundColor(Android.Graphics.Color.White);
                LoadApplication(new App());
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool isOk = true;
            // If there are errors in the certificate chain, look at each error to determine the cause.
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                for (int i = 0; i < chain.ChainStatus.Length; i++)
                {
                    if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                        chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                        chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                        chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                        bool chainIsValid = chain.Build((X509Certificate2)certificate);
                        if (!chainIsValid)
                        {
                            isOk = false;
                        }
                    }
                }
            }
            return isOk;
        }
    }
}

