package md5d0dc0ce8f60b2a14c61d0333551488dd;


public class GcmInstanceIdListenerService
	extends com.google.android.gms.iid.InstanceIDListenerService
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onTokenRefresh:()V:GetOnTokenRefreshHandler\n" +
			"";
		mono.android.Runtime.register ("mobileApp.Droid.Notifications.GcmInstanceIdListenerService, mobileApp.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", GcmInstanceIdListenerService.class, __md_methods);
	}


	public GcmInstanceIdListenerService () throws java.lang.Throwable
	{
		super ();
		if (getClass () == GcmInstanceIdListenerService.class)
			mono.android.TypeManager.Activate ("mobileApp.Droid.Notifications.GcmInstanceIdListenerService, mobileApp.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onTokenRefresh ()
	{
		n_onTokenRefresh ();
	}

	private native void n_onTokenRefresh ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
