package md5d0dc0ce8f60b2a14c61d0333551488dd;


public class GcmRegistrationIntentService
	extends mono.android.app.IntentService
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onHandleIntent:(Landroid/content/Intent;)V:GetOnHandleIntent_Landroid_content_Intent_Handler\n" +
			"";
		mono.android.Runtime.register ("mobileApp.Droid.Notifications.GcmRegistrationIntentService, mobileApp.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", GcmRegistrationIntentService.class, __md_methods);
	}


	public GcmRegistrationIntentService (java.lang.String p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == GcmRegistrationIntentService.class)
			mono.android.TypeManager.Activate ("mobileApp.Droid.Notifications.GcmRegistrationIntentService, mobileApp.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "System.String, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0 });
	}


	public GcmRegistrationIntentService () throws java.lang.Throwable
	{
		super ();
		if (getClass () == GcmRegistrationIntentService.class)
			mono.android.TypeManager.Activate ("mobileApp.Droid.Notifications.GcmRegistrationIntentService, mobileApp.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onHandleIntent (android.content.Intent p0)
	{
		n_onHandleIntent (p0);
	}

	private native void n_onHandleIntent (android.content.Intent p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
