using Android.App;
using Android.Content;
using Android.Gms.Gcm.Iid;

namespace mobileApp.Droid.Notifications
{
    [Service(Exported = false), IntentFilter(new[] { "com.google.android.gms.iid.InstanceID" })]
    class GcmInstanceIdListenerService : InstanceIDListenerService
    {
        public override void OnTokenRefresh()
        {
            var intent = new Intent(this, typeof(GcmRegistrationIntentService));
            StartService(intent);
        }
    }
}