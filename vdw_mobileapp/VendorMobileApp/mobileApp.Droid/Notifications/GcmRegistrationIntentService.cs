using System;
using Android.App;
using Android.Content;
using Android.Gms.Gcm;
using Android.Gms.Gcm.Iid;
using Android.Util;

namespace mobileApp.Droid.Notifications
{
    [Service(Exported = false)]
    class GcmRegistrationIntentService : IntentService
    {
        static readonly object Locker = new object();
        private readonly string _userId;

        public GcmRegistrationIntentService() : base("GcmRegistrationIntentService")
        {
            _userId = App.LoggedInUserId;
        }

        protected override void OnHandleIntent(Intent intent)
        {
            try
            {
                Log.Info("GcmRegistrationIntentService", "Calling InstanceID.GetToken");
                lock (Locker)
                {
                    var instanceId = InstanceID.GetInstance(this);
                    var token = instanceId.GetToken(ConstantsAndroid.SenderId, GoogleCloudMessaging.InstanceIdScope, null); //Registration

                    Log.Info("GcmRegistrationIntentService", "GCM Registration Token: " + token);
                    SendRegistrationToAppServer(token);
                    Subscribe(token);
                }
            }
            catch (Exception e)
            {
                //Exception Logging goes here - Things to Look into.
                Log.Debug("GcmRegistrationIntentService", "Failed to get a registration token");
            }
        }

        void SendRegistrationToAppServer(string token)
        {
            // We may send this to the WebApi Service to store the token (may be along with TechId, IMEI, etc) in the DB.
            App.deviceToken = token;
        }

        void Subscribe(string token)
        {
            Log.Info("GcmRegistrationIntentService", "Subscribe to Topic: " + _userId);
            var pubSub = GcmPubSub.GetInstance(this);
            pubSub.Subscribe(token, "/topics/" + _userId, null);
            Log.Info("GcmRegistrationIntentService", "Subscribe to Topic: " + _userId + " - Done");
        }
    }
}