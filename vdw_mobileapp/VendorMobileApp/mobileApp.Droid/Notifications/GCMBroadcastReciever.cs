using System;
using Android.App;
using Android.Content;
using Android.OS;


[assembly: Permission(Name = "com.verizon.vendormobileapp.permission.C2D_MESSAGE")]
[assembly: UsesPermission(Name = "android.permission.WAKE_LOCK")]
[assembly: UsesPermission(Name = "com.verizon.vendormobileapp.permission.C2D_MESSAGE")]
[assembly: UsesPermission(Name = "com.google.android.c2dm.permission.RECEIVE")]
[assembly: UsesPermission(Name = "android.permission.GET_ACCOUNTS")]
[assembly: UsesPermission(Name = "android.permission.INTERNET")]
namespace mobileApp.Droid.Notifications
{
    [BroadcastReceiver(Permission = "com.google.android.c2dm.permission.SEND")]
    [IntentFilter(new string[] { "com.google.android.c2dm.intent.RECEIVE" }, Categories = new string[] { "com.verizon.vendormobileapp" })]
    [IntentFilter(new string[] { "com.google.android.c2dm.intent.REGISTRATION" }, Categories = new string[] { "com.verizon.vendormobileapp" })]
    [IntentFilter(new string[] { "com.google.android.gcm.intent.RETRY" }, Categories = new string[] { "com.verizon.vendormobileapp" })]

    public class GCMBroadcastReciever : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent1)
        {
            PowerManager.WakeLock sWakeLock;
            var pm = PowerManager.FromContext(context);
            sWakeLock = pm.NewWakeLock(WakeLockFlags.Partial, "GCM Broadcast Reciever Tag");
            sWakeLock.Acquire();
            //DependencyService.Get<INotificationDependencyService>().IncrementLauncherIconBadgeCount(1);
            //App.CurrentToolbar.IncrementNotificationIconBadgeCount(1);
             int notificationCount = Convert.ToInt32(intent1.Extras.GetString("NotificationCount"));
                
            var entryActivityName = context.PackageManager.GetLaunchIntentForPackage(context.PackageName).Component.ClassName;
            var intent = new Intent(ConstantsAndroid.IntentAction);
            intent.PutExtra(ConstantsAndroid.IntentExtraBadgeCount, notificationCount);
            intent.PutExtra(ConstantsAndroid.IntentExtraPackagename, context.PackageName);
            intent.PutExtra(ConstantsAndroid.IntentExtraActivityName, entryActivityName);
            context.SendBroadcast(intent);
            //handle the notification here        
            sWakeLock.Release();
        }
    }
}