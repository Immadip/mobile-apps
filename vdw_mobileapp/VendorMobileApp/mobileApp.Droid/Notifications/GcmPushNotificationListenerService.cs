using System;
using Android.App;
using Android.Content;
using Android.Gms.Gcm;
using Android.OS;
using Android.Util;
using Xamarin.Forms;
using mobileApp.CustomControls;
using BusinessEntities.Models;
using mobileApp.DependencyServices;
using mobileApp.SqlLiteDataAccess;

namespace mobileApp.Droid.Notifications
{
    [Service(Exported = false), IntentFilter(new[] { "com.google.android.c2dm.intent.RECEIVE" })]
    public class GcmPushNotificationListenerService : GcmListenerService
    {
        public override void OnMessageReceived(string from, Bundle data)
        {
            try
            {
                var notification = new NotificationEntity()
                {
                    NotificationId = Convert.ToInt32(data.GetString("NotificationId")),
                    UserId = data.GetString("UserId"),
                    NotificationTitle = data.GetString("NotificationTitle"),
                    NotificationText = data.GetString("NotificationText"),
                    NotificationReceivedTime = Convert.ToDateTime(data.GetString("NotificationReceivedTime")),
                    HasRead = false
                };

                Log.Debug("GcmPushNotificationListenerService", "From:    " + from);
                Log.Debug("GcmPushNotificationListenerService", "Message: " + notification.NotificationText);

                CreateNotificationOnNotificationTray(notification);
                CreateInAppNotification(notification);
                DependencyService.Get<INotificationDependencyService>().IncrementLauncherIconBadgeCount(1);
                App.CurrentToolbar.IncrementNotificationIconBadgeCount(1);
            }
            catch (Exception ex)
            {
                Log.Error("GcmPushNotificationListenerService", "Error Occurred while Receiving Push Message" + ex.Message);
            }

        }

        private void CreateNotificationOnNotificationTray(NotificationEntity notification)
        {

            var intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

            var notificationBuilder = new Notification.Builder(this)
                .SetSmallIcon(Resource.Drawable.vdwlogo)
                .SetContentTitle(notification.NotificationTitle)
                .SetContentText(notification.NotificationText)
                .SetAutoCancel(true)
                .SetContentIntent(pendingIntent);

            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(notification.NotificationId, notificationBuilder.Build());
        }

        private void CreateInAppNotification(NotificationEntity notification)
        {
            var notificationLocalDataAccess = new NotificationLocalDataAccess();
            notificationLocalDataAccess.SaveNotification(notification);
        }
    }
}