namespace mobileApp.Droid
{
    public class ConstantsAndroid
    {
        public const string SenderId = "423576043037"; // Google API Project Number

        public const string IntentAction = "android.intent.action.BADGE_COUNT_UPDATE";
        public const string IntentExtraBadgeCount = "badge_count";
        public const string IntentExtraPackagename = "badge_count_package_name";
        public const string IntentExtraActivityName = "badge_count_class_name";
    }
}