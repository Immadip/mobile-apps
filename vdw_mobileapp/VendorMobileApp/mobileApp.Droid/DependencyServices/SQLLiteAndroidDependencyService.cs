using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using mobileApp.Droid.DependencyServices;
using SQLite;
using Xamarin.Forms;
using Environment = System.Environment;
using mobileApp.DependencyServices;

[assembly: Dependency(typeof(SQLLiteAndroidDependencyService))]
namespace mobileApp.Droid.DependencyServices
{
    public class SQLLiteAndroidDependencyService : ISQLiteDependencyService
    {
        public SQLiteConnection GetConnection()
        {
            const string sqliteFilename = "com.verizon.vdwmobile.db3";
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
            var path = Path.Combine(documentsPath, sqliteFilename);

            var conn = new SQLiteConnection(path);

            // Return the database connection 
            return conn;
        }
    }
}