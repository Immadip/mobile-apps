using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Common;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using mobileApp.Droid.DependencyServices;
using mobileApp.DependencyServices;
using mobileApp.Droid.Notifications;

[assembly: Xamarin.Forms.Dependency(typeof(NotificationDependencyService))]
namespace mobileApp.Droid.DependencyServices
{
    public class NotificationDependencyService : INotificationDependencyService
    {
        private static int _currentBadgeCount;

        public void IncrementLauncherIconBadgeCount(int incrementByValue)
        {
            SetLauncherIconBadgeCount(_currentBadgeCount + incrementByValue);
        }

        public void RegisterForPushNotifications()
        {
            if (IsPlayServicesAvailable())
            {
                var intent = new Intent(MainActivity.Context, typeof(GcmRegistrationIntentService));
                MainActivity.Context.StartService(intent);
            }
        }

        public void SetLauncherIconBadgeCount(int notificationBadgeCount)
        {
            _currentBadgeCount = notificationBadgeCount;
            var entryActivityName = MainActivity.Context.PackageManager.GetLaunchIntentForPackage(MainActivity.Context.PackageName).Component.ClassName;
            var intent = new Intent(ConstantsAndroid.IntentAction);
            intent.PutExtra(ConstantsAndroid.IntentExtraBadgeCount, notificationBadgeCount);
            intent.PutExtra(ConstantsAndroid.IntentExtraPackagename, MainActivity.Context.PackageName);
            intent.PutExtra(ConstantsAndroid.IntentExtraActivityName, entryActivityName);
            MainActivity.Context.SendBroadcast(intent);
        }


        private bool IsPlayServicesAvailable()
        {
            var resultCode = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(MainActivity.Context);
            if (resultCode == ConnectionResult.Success) return true;    //Google Play Services is available
            if (GoogleApiAvailability.Instance.IsUserResolvableError(resultCode))
            {
                //User need to Install Google Play Services
            }
            else
            {
                //This device is not supported
                //Finish();
            }
            return false;
        }
    }
}