using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using mobileApp.Droid.DependencyServices;
using mobileApp.DependencyServices;

[assembly: Xamarin.Forms.Dependency(typeof(ImageDependencyService))]

namespace mobileApp.Droid.DependencyServices
{
    public class ImageDependencyService : IImageDependencyService
    {
        public byte[] ResizeImage(byte[]  sourceImageBytes, string fileFormat, float maxWidth, float maxHeight)
        {
            var stream = new MemoryStream();
            if (sourceImageBytes != null)
            {
                // First decode with inJustDecodeBounds=true to check dimensions
                var options = new BitmapFactory.Options()
                {
                    InJustDecodeBounds = false,
                    InPurgeable = true,
                };

               

                using (var image = BitmapFactory.DecodeByteArray(sourceImageBytes, 0, sourceImageBytes.Length, options))
                {
                    if (image != null)
                    {
                        var sourceSize = new Size((int)image.GetBitmapInfo().Height, (int)image.GetBitmapInfo().Width);

                        var maxResizeFactor = Math.Min(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);

                        var width = (int)(maxResizeFactor * sourceSize.Width);
                        var height = (int)(maxResizeFactor * sourceSize.Height);

                        using (var bitmapScaled = Bitmap.CreateScaledBitmap(image, height, width, true))
                        {
                            if (fileFormat.Contains("png"))
                                bitmapScaled.Compress(Bitmap.CompressFormat.Png, 100, stream);
                            else
                                bitmapScaled.Compress(Bitmap.CompressFormat.Jpeg, 95, stream);
                            bitmapScaled.Recycle();
                        }

                        image.Recycle();
                    }
                    else
                        Log.Error("ImageServiceDroid", "Image scaling failed" );
                   
                }
            }
            return stream.ToArray();
        }
    }
}