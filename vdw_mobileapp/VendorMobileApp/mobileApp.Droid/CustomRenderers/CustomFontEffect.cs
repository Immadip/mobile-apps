using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Widget;
using Android.Graphics;
using System.ComponentModel;
using mobileApp.Droid.CustomRenderers;
using mobileApp.CustomControls;

[assembly: ResolutionGroupName("Verizon")]
[assembly: ExportEffect(typeof(FontEffect), "FontEffect")]
namespace mobileApp.Droid.CustomRenderers
{
    public class FontEffect : PlatformEffect
    {
        TextView control;
        protected override void OnAttached()
        {
            try
            {
                control = Control as TextView;
                Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, "Fonts/" + CustomFontEffect.GetFontFileName(Element) + ".otf");
                control.Typeface = font;
                control.TextSize = CustomFontEffect.GetFontSize(Element);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot set property on attached control. Error: ", ex.Message);
            }
        }

        protected override void OnDetached()
        {
        }

        protected override void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            if (args.PropertyName == CustomFontEffect.FontFileNameProperty.PropertyName)
            {
                Typeface font = Typeface.CreateFromAsset(Forms.Context.ApplicationContext.Assets, "Fonts/" + CustomFontEffect.GetFontFileName(Element) + ".otf");
                control.Typeface = font;
                control.TextSize = CustomFontEffect.GetFontSize(Element);
            }
        }

    }
}