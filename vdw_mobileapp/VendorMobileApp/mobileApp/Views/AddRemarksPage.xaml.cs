﻿using BusinessEntities.Models;
using BusinessLogic.VDWUILogic;
using System;
using System.Linq;
using Xamarin.Forms;

//This is from Demo 1

namespace mobileApp.Views
{
    public partial class AddRemarksPage : ContentPage
    {
        //This is for Demo 2
        private readonly JobEntity _job;
        public AddRemarksPage(JobEntity job)
        {
            InitializeComponent();
            
            NavigationPage.SetHasNavigationBar(this, false);

            #region Soft Back Button Handling
            var backIconTap = new TapGestureRecognizer();
            backIconTap.Tapped += (s, e) =>
            {
                Navigation.PopAsync();
            };
            imgBackIcon.GestureRecognizers.Add(backIconTap);
            lblBackIcon.GestureRecognizers.Add(backIconTap);
            #endregion Soft Back Button Handling

            _job = job;
            lblOrder.Text = job.DoOrderId;
            lblCircuitId.Text = "11/BURY/A6HD00/VZMA";
            lblJobId.Text = job.JobId;
            txtExistingRemarks.Text = job.VdwRemarks;

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Refresh the Toolbar, in case any change in the Notification count, etc
            toolbar.Refresh();
            App.CurrentToolbar = toolbar;
        }

        protected async void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            var remarksEntity = new RemarksEntity
            {
                JobId = _job.JobId,
                VendorId = _job.VendorId,
                Remarks = txtNewRemarks.Text,
                RemarksTimeStamp = DateTime.Now
            };

            bool isVdwRemarksAdded = await JobActionsBL.AddVdwRemarks(remarksEntity);

            await DisplayAlert("Success", "Your Remarks was added Successfully", "OK");

            await Navigation.PopAsync();
        }

        protected async void OnCancelButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}
