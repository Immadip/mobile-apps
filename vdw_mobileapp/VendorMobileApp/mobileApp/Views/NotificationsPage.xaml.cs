﻿using BusinessLogic;
using mobileApp.CustomControls;
using mobileApp.DependencyServices;
using mobileApp.SqlLiteDataAccess;
using System;
using System.Linq;

using Xamarin.Forms;

namespace mobileApp.Views
{
    public partial class NotificationsPage : ContentPage
    {
        private readonly NotificationLocalDataAccess _notificationLocalDataAccess;
        private bool _isRowEven;
        public NotificationsPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            #region Soft Back Button Handling
            var backIconTap = new TapGestureRecognizer();
            backIconTap.Tapped += (s, e) =>
            {
                Navigation.PopAsync();
            };
            imgBackIcon.GestureRecognizers.Add(backIconTap);
            lblBackIcon.GestureRecognizers.Add(backIconTap);
            #endregion Soft Back Button Handling

            _notificationLocalDataAccess = new NotificationLocalDataAccess();
            FetchAndBindNotifications();

            MarkNotificationsAsRead();
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Refresh the Toolbar, in case any change in the Notification count, etc
            toolbar.Refresh();
            App.CurrentToolbar = toolbar;
        }

        public void FetchAndBindNotifications()
        {
            var notifications = _notificationLocalDataAccess.GetNotificationsByUserId(App.LoggedInUserId).OrderByDescending(notif => notif.NotificationReceivedTime);
            //Mark the notifications as read
            _notificationLocalDataAccess.MarkAllNotificationsAsRead();

            LblNoNotifications.IsVisible = notifications == null || !notifications.Any();
            LvNotifications.ItemsSource = notifications;
            
        }

        protected void OnNotificationCellAppearing(object sender, EventArgs e)
        {
            var viewCell = (ViewCell)sender;
            if (viewCell.View != null)
            {
                viewCell.View.BackgroundColor = (_isRowEven) ? (Color)Application.Current.Resources["evenNotificationCellColor"] : (Color)Application.Current.Resources["oddNotificationCellColor"];
            }

            _isRowEven = !_isRowEven;
        }

        private async void MarkNotificationsAsRead()
        {
            App.CurrentToolbar.SetNotificationIconBadgeCount(0);
            await NotificationBL.SetNotificationCount(App.deviceToken,App.LoggedInUserId,0);
            Device.OnPlatform(iOS: () => DependencyService.Get<INotificationDependencyService>().SetLauncherIconBadgeCount(0));
            Device.OnPlatform(Android: () => DependencyService.Get<INotificationDependencyService>().SetLauncherIconBadgeCount(0));
        }
    }
}
