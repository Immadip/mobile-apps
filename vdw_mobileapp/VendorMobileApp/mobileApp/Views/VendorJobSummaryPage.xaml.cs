﻿using BusinessEntities;
using BusinessEntities.Models;
using BusinessLogic.VDWUILogic;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace mobileApp.Views
{
    public partial class VendorJobSummaryPage : ContentPage, INotifyPropertyChanged
    {
        private List<JobEntity> assignedJobs;
        private List<JobEntity> unassignedJobs;
        private List<JobEntity> completedJobs;
        private string currentJobStatusToBeDisplayed;
        private bool _isRowEven;  

        private bool isLoading = false;
        public bool IsLoading
        {
            get
            {
                return isLoading;
            }
            set
            {
                if (isLoading != value)
                {
                    isLoading = value;
                    RaisePropertyChanged("IsLoading");//PropertyChanged(this, new PropertyChangedEventArgs(nameof(IsLoading)));
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public VendorJobSummaryPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            currentJobStatusToBeDisplayed = VdwConstants.JobPendingStatus;
            GetJobs();

            if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.iOS)
            {
                toolbar.ShowRefreshIcon();
                var imgRefreshJobs = toolbar.GetRefreshJobsIcon();
                var grRefreshJobs = new TapGestureRecognizer();
                grRefreshJobs.Tapped += OnListViewRefreshing;
                imgRefreshJobs.GestureRecognizers.Add(grRefreshJobs);
            }

            loaderStackLayout.IsVisible = false;
            IsLoading = false;
            BindingContext = this;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Refresh the Toolbar, in case any change in the Notification count, etc
            toolbar.Refresh();
            App.CurrentToolbar = toolbar;
        }

        public async void GetJobs()
        {
            var jobArr = await JobSummaryBL.GetJobAssignList(App.LoggedInUserId, "CP");
            //var jobArr = VDWDatabase.getJobListDB();
            if (jobArr == null) return;

            // Initialize MapPage with the Jobs. The MapPage will plot all these Jobs.
            if (jobArr.Any())
            {
                mapView.InitializeMapPage(jobArr);
            }

            assignedJobs = jobArr.Where(j => !string.IsNullOrEmpty(j.TechCode) && j.JobStatus != VdwConstants.JobCompletedStatus).ToList();
            unassignedJobs = jobArr.Where(j => string.IsNullOrEmpty(j.TechCode) && j.JobStatus != VdwConstants.JobCompletedStatus).ToList();
            completedJobs = jobArr.Where(j => j.JobStatus == VdwConstants.JobCompletedStatus).ToList();

            // Completed Jobs does not require the Switches for Selection.
            foreach (var job in completedJobs)
            {
                job.IsNonCompletedJob = false;
            }

            foreach (var job in assignedJobs)
            {
                job.IsNonCompletedJob = true;
            }

            foreach (var job in unassignedJobs)
            {
                job.IsNonCompletedJob = true;
            }

            //multiPage = new SelectMultipleBasePage<JobSummaryTable>(UnassignedJobs) { Title = "Check all that apply" };
            //await Navigation.PushAsync(multiPage);
            UpdateStack();
        }
        public void UpdateStack()
        {
            var oddJobViewStyle = (Style)Application.Current.Resources["oddJobCell"];
            var evenJobViewStyle = (Style)Application.Current.Resources["evenJobCell"];
            if (assignedJobs != null && unassignedJobs != null)
            {
                tabAssigned.Text = string.Format("Assigned ({0})", assignedJobs.Count);
                tabUnAssigned.Text = string.Format("Pending ({0})", unassignedJobs.Count);
                tabCompleted.Text = string.Format("Completed ({0})", completedJobs.Count);
                //Pending
                if (currentJobStatusToBeDisplayed == VdwConstants.JobPendingStatus)
                {
                    //LvJobDetail.ItemsSource = unassignedJobs.OrderBy(j => j.PinCode).ThenBy(j => Convert.ToDateTime(j.CommitmentDate));
                    LvJobDetail.ItemsSource = unassignedJobs.OrderBy(j => j.PinCode).ThenBy(j => DateTime.ParseExact(j.CommitmentDate, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture));
                }
                //Assigned
                else if (currentJobStatusToBeDisplayed == VdwConstants.JobAssignedStatus)
                {
                    LvJobDetail.ItemsSource = assignedJobs.OrderBy(j => j.PinCode).ThenBy(j => DateTime.ParseExact(j.CommitmentDate, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture));
                }
                else if (currentJobStatusToBeDisplayed == VdwConstants.JobCompletedStatus)
                {
                    LvJobDetail.ItemsSource = completedJobs.OrderBy(j => j.PinCode).ThenBy(j => DateTime.ParseExact(j.CommitmentDate, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture));
                }

            }
            btnCancel.IsEnabled = false;
            btnAssigned.IsEnabled = false;
            btnCancel.BackgroundColor = Color.FromHex("cccccc");
            btnAssigned.BackgroundColor = Color.FromHex("cccccc");

        }

        protected void OnUnAssignedButtonClicked(object sender, EventArgs e)
        {
            unAssignedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabHighlighted"];
            assignedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabNonHighlighted"];
            completedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabNonHighlighted"];
            currentJobStatusToBeDisplayed = VdwConstants.JobPendingStatus;
            btnAssigned.Text = "Assign";
            btnAssigned.IsVisible = btnCancel.IsVisible = true;
            UpdateStack();

        }

        protected void OnAssignedButtonClicked(object sender, EventArgs e)
        {
            unAssignedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabNonHighlighted"];
            completedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabNonHighlighted"];
            assignedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabHighlighted"];
            currentJobStatusToBeDisplayed = VdwConstants.JobAssignedStatus;
            btnAssigned.Text = "Un-Assign";
            btnAssigned.IsVisible = btnCancel.IsVisible = true;
            UpdateStack();
        }

        protected void OnCompletedButtonClicked(object sender, EventArgs e)
        {
            unAssignedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabNonHighlighted"];
            assignedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabNonHighlighted"];
            completedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabHighlighted"];
            currentJobStatusToBeDisplayed = VdwConstants.JobCompletedStatus;
            btnAssigned.IsVisible = btnCancel.IsVisible = false;
            UpdateStack();
        }
        protected void onCancelClick(object sender, EventArgs e)
        {
            foreach (JobEntity itemRow in LvJobDetail.ItemsSource)
            {
                itemRow.IsSelected = false;
            }
            UpdateStack();
        }
        protected void OnToggled(object sender, EventArgs e)
        {
            bool CheckedStatus = false;
            foreach (JobEntity itemRow in LvJobDetail.ItemsSource)
            {
                if (itemRow.IsSelected == true)
                {
                    btnCancel.IsEnabled = true;
                    btnAssigned.IsEnabled = true;
                    btnCancel.BackgroundColor = Color.FromHex("CD040B");
                    btnAssigned.BackgroundColor = Color.FromHex("CD040B");
                    CheckedStatus = true;
                    return;
                }
            }
            btnCancel.BackgroundColor = Color.FromHex("cccccc");
            btnAssigned.BackgroundColor = Color.FromHex("cccccc");
            btnCancel.IsEnabled = false;
            btnAssigned.IsEnabled = false;

        }

        protected async void onAssignClick(object sender, EventArgs e)
        {
            btnCancel.IsEnabled = false;
            btnAssigned.IsEnabled = false;
            btnCancel.BackgroundColor = Color.FromHex("cccccc");
            btnAssigned.BackgroundColor = Color.FromHex("cccccc");
            var action = "Cancel";
            var answer = false;
            if (currentJobStatusToBeDisplayed == VdwConstants.JobPendingStatus)
            {
                var page = new VendorPopup();
                App.TechnicianCode = "";
                await Navigation.PushAsync(page);
                var result = await CheckTechcode();
                //action = await DisplayActionSheet("Select a technician", "Cancel", null, "VTE01", "VTE02", "VTE03");
            }
            else
            {
                answer = await DisplayAlert("Alert", "Are you sure, you want to Un-assign the Job?", "Yes", "No");
            }

            foreach (JobEntity itemRow in LvJobDetail.ItemsSource)
            {
                if (itemRow.IsSelected == true)
                {
                    if (currentJobStatusToBeDisplayed == VdwConstants.JobPendingStatus)
                    {

                        if (App.TechnicianCode != "" && App.TechnicianCode != "Cancel")
                        {
                            itemRow.TechCode = App.TechnicianCode;
                            var result = await JobSummaryBL.PutAssignJob(itemRow);

                        }
                        else
                        {
                            btnCancel.IsEnabled = true;
                            btnAssigned.IsEnabled = true;
                            btnCancel.BackgroundColor = Color.FromHex("CD040B");
                            btnAssigned.BackgroundColor = Color.FromHex("CD040B");
                            return;
                        }
                    }
                    else
                    {
                        if (answer)
                        {
                            itemRow.TechCode = string.Empty;
                            var result = await JobSummaryBL.PutAssignJob(itemRow);
                        }
                    }
                }
            }
            GetJobs();
        }

        protected async Task<bool> CheckTechcode()
        {
            while (App.TechnicianCode == "")
            {
                await Task.Delay(50);
            }
            return true;
        }
        protected void OnItemClick(object sender, EventArgs e)
        {
            var LVjobdetail = sender as ListView;
            var selectedJob = (JobEntity)LVjobdetail.SelectedItem;
            ((ListView)LVjobdetail).SelectedItem = null;
            Navigation.PushAsync(new Views.JobDetailsPage(selectedJob));

            // Unselect the Item in the ListView.
            LVjobdetail.SelectedItem = null;
            //Testing the Reverting process - Second Time

        }
        protected void OnJobSummaryListButtonClicked(object sender, EventArgs e)
        {
            jobSummaryMapContainer.IsVisible = false;
            jobSummaryListContainer.IsVisible = true;
            highlightJobSummaryList.Style = (Style)Application.Current.Resources["jobSummaryTabHighlighted"];
            highlightJobSummaryMap.Style = (Style)Application.Current.Resources["jobSummaryTabNonHighlighted"];
        }

        protected void OnJobSummaryMapButtonClicked(object sender, EventArgs e)
        {
            jobSummaryListContainer.IsVisible = false;
            jobSummaryMapContainer.IsVisible = true;
            highlightJobSummaryList.Style = (Style)Application.Current.Resources["jobSummaryTabNonHighlighted"];
            highlightJobSummaryMap.Style = (Style)Application.Current.Resources["jobSummaryTabHighlighted"];

            mapView.RecenterMapView();
        }

        protected void OnListViewCellAppearing(object sender, EventArgs e)
        {
            var viewCell = (ViewCell)sender;
            if (viewCell.View != null)
            {
                viewCell.View.Style = (_isRowEven) ? (Style)Application.Current.Resources["evenJobCell"] : (Style)Application.Current.Resources["oddJobCell"];
            }

            _isRowEven = !_isRowEven;
        }
        protected async void OnListViewRefreshing(object sender, EventArgs e)
        {
            // If it is windows platform, then there is no pull to refresh, we are using refresh button on the toolbar. So we need to show Activity Indicator.
            if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.iOS)
            {
                IsLoading = true;
                loaderStackLayout.IsVisible = true;
            }
                
            bool result = await VdwLocalDataAccess.CheckInternet(App.LoggedInUserId);
            GetJobs();
            
            // Set the IsRefreshing Property of the ListView to false to let it know that refreshing is completed and the Loading Symbol disappears.
            LvJobDetail.IsRefreshing = false;

            if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.iOS)
            {
                IsLoading = false;
                loaderStackLayout.IsVisible = false;
            }
        }
    }
}
