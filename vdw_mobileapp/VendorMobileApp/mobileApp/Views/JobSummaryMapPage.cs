﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using mobileApp.Views;
using mobileApp;
using BusinessLogic;

using Xamarin.Forms;
using System.Diagnostics;
using BusinessLogic.VDWUILogic;
using BusinessEntities.Models;
using BusinessEntities;
using TK.CustomMap;
using System.Net;

namespace mobileApp.Views
{
    public partial class JobSummaryMapPage : Map
    {
        private readonly Geocoder _geoCoder;
        private List<JobEntity> _jobs;

        public List<MapPinEntity> CustomPins;

        public JobSummaryMapPage()
        {
            MapType = MapType.Street;

            _geoCoder = new Geocoder();
            CustomPins = new List<MapPinEntity>();
        }

        public async void InitializeMapPage(List<JobEntity> jobs)
        {
            _jobs = jobs;
            await CreatePins();
            RecenterMapView();
        }

        public void RecenterMapView()
        {
            if (CustomPins.Any())
            {
                var pinPositions = CustomPins.Select((c => c.Pin.Position)).ToArray();

                double maxLat = -85, minLat = 85, maxLon = -180, minLon = 180;

                foreach (var pinPosition in pinPositions)
                {
                    maxLat = pinPosition.Latitude > maxLat ? pinPosition.Latitude : maxLat;
                    maxLon = pinPosition.Longitude > maxLon ? pinPosition.Longitude : maxLon;
                    minLat = pinPosition.Latitude < minLat ? pinPosition.Latitude : minLat;
                    minLon = pinPosition.Longitude < minLon ? pinPosition.Longitude : minLon;
                }

                var center = new Position((maxLat + minLat) / 2, (maxLon + minLon) / 2);
                var distanceInKms = center.DistanceTo(new Position(maxLat, maxLon));

                this.MoveToRegion(MapSpan.FromCenterAndRadius(center, Distance.FromKilometers(distanceInKms)));
                this.Focus();
            }
        }

        private async Task CreatePins()
        {
            try
            {
                if (_jobs == null)
                {
                    Debug.WriteLine("MapPage.CreatePins: MapPage is not initialized. Initialize the MapPage with the Jobs to be Plotted");
                    return;
                }

                // Fire CreatePin() for each job
                var createPinTaskList = _jobs.Select(j => CreatePin(j, 1)).ToList();

                // Wait for all CreatePin() to complete
                await Task.WhenAll(createPinTaskList);
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }

        }
        private async Task CreatePin(JobEntity job, int trialNumber)
        {
            try
            {
                Debug.WriteLine("MapPage.CreatePin: CustomerAddress for {0}: {1}", job.JobId, job.CustomerAddress);
                Debug.WriteLine("MapPage.CreatePin: Creating Pin for " + job.JobId);

                var pinPositions = await _geoCoder.GetPositionsForAddressAsync(job.CustomerAddress);
                var pinPosition = pinPositions.FirstOrDefault();
                if (pinPosition != null)
                {
                    var cpin = new MapPinEntity()
                    {
                        Pin = new Pin
                        {
                            Type = PinType.Place,
                            Position = new Position(pinPosition.Latitude, pinPosition.Longitude),
                            Label = job.JobId,
                            Address = job.CustomerAddress
                        },
                        Id = job.JobId,
                        Name = job.CustomerName,
                        Type = job.JobType,
                        Number = job.AltReachNumber,
                        Addr = job.CustomerAddress,
                        Url = Device.OnPlatform(string.Format("http://maps.apple.com/?q={0}", WebUtility.UrlEncode(job.CustomerAddress)),
                        string.Format("geo:0,0?q={0}", WebUtility.UrlEncode(job.CustomerAddress)), 
                        string.Format("bingmaps:?where={0}", Uri.EscapeDataString(job.CustomerAddress)))
                };

                    // Locking the variable CustomPins in order to prevent other threads inserting into it simultaneously
                    lock(CustomPins)
                    {
                        CustomPins.Add(cpin);
                    }

                    // Locking the variable Pins in order to prevent other threads inserting into it simultaneously
                    lock (this.Pins)
                    {
                        this.Pins.Add(cpin.Pin);
                    }
                }
            }
            catch (Exception e)
            {
                App.InsertInLogTable(e);

                Debug.WriteLine("MapPage.CreatePin: Exception occurred while Trying to get position and create the pin (Trial#{0}). Maximum Trials before ignoring the Pin: 3, Job Info: Job ID: {1}, CustomerName: {2}, CustomerAddress: {3}, Exception Info: Message: {4}, StackTrace:{5}, InnerException:{6}", trialNumber, job.JobId, job.CustomerName, job.CustomerAddress, e.Message, e.StackTrace, e.InnerException);

                //Try again after a second for 2 more times
                if (trialNumber <= 3)
                {
                    await Task.Delay(1000);
                    await CreatePin(job, ++trialNumber);
                }
            }
            return;
        }
    }
}
