﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace mobileApp.Views
{
    public partial class VendorPopup : ContentPage
    {
        private bool _isRowEven;
        private IEnumerable<Technician> tempRecords;
        public VendorPopup()
        {
            InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
            GetTechnicianPicker();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Refresh the Toolbar, in case any change in the Notification count, etc
            toolbar.Refresh();
            App.CurrentToolbar = toolbar;
        }

        private void OnClose(object sender, EventArgs e)
        {
            App.TechnicianCode = "Cancel";
            Navigation.PopAsync();
        }

        private void OnSubmit(object sender, EventArgs e)
        {
            if (LvTechnician.SelectedItem != null)
            {
                Technician objTechnician = (Technician)LvTechnician.SelectedItem;
                App.TechnicianCode = objTechnician.TechnicianCode;
                Navigation.PopAsync();
               
            }
            else
            {
                if (tempRecords.Count() > 0)
                {
                    Technician objTechnician = (Technician)tempRecords.FirstOrDefault();
                    App.TechnicianCode = objTechnician.TechnicianCode;
                    Navigation.PopAsync();
                }
            }
        }
        protected void OnListViewCellAppearing(object sender, EventArgs e)
        {
            var viewCell = (ViewCell)sender;
            if (viewCell.View != null)
            {
                viewCell.View.Style = (_isRowEven) ? (Style)Application.Current.Resources["evenJobCell"] : (Style)Application.Current.Resources["oddJobCell"];
            }

            _isRowEven = !_isRowEven;
        }
        public void GetTechnicianPicker()
        {
            var technician = new Technician[] { new Technician() { TechnicianCode = "VTE01",TechnicianDisplayName="VTE01  -  James, Smith" } , new Technician() { TechnicianCode = "VTE02",TechnicianDisplayName="VTE02  -  John, Smith" }, new Technician() { TechnicianCode = "VTE03",TechnicianDisplayName="VTE03  -  Robert, Garcia" },
            new Technician() { TechnicianCode = "TEC11",TechnicianDisplayName="TEC11  -  Maria, Rodriguez" } , new Technician() { TechnicianCode = "TEC23",TechnicianDisplayName="TEC23  -  Mary, Smith" }, new Technician() { TechnicianCode = "TEC89",TechnicianDisplayName="TEC89  -  Maria, Hernandez" },
            new Technician() { TechnicianCode = "KVT91",TechnicianDisplayName="KVT91  -  Maria, Martinez" } , new Technician() { TechnicianCode = "KVT86",TechnicianDisplayName="KVT86  -  James, Johnson" }, new Technician() { TechnicianCode = "KVT75",TechnicianDisplayName="KVT75  -  William, Smith" },
            new Technician() { TechnicianCode = "RVS44",TechnicianDisplayName="RVS44  -  Robert, Johnson" } , new Technician() { TechnicianCode = "RVS23",TechnicianDisplayName="RVS23  -  Sarah, Smith" }, new Technician() { TechnicianCode = "RVS89",TechnicianDisplayName="RVS89  -  Mike, Jones" } };
            LvTechnician.ItemsSource = technician;
           
        }
        
        private void OnTextChanged(object sender, TextChangedEventArgs e)
        {

            var technician = new Technician[] { new Technician() { TechnicianCode = "VTE01",TechnicianDisplayName="VTE01  -  James, Smith" } , new Technician() { TechnicianCode = "VTE02",TechnicianDisplayName="VTE02  -  John, Smith" }, new Technician() { TechnicianCode = "VTE03",TechnicianDisplayName="VTE03  -  Robert, Garcia" },
            new Technician() { TechnicianCode = "TEC11",TechnicianDisplayName="TEC11  -  Maria, Rodriguez" } , new Technician() { TechnicianCode = "TEC23",TechnicianDisplayName="TEC23  -  Mary, Smith" }, new Technician() { TechnicianCode = "TEC89",TechnicianDisplayName="TEC89  -  Maria, Hernandez" },
            new Technician() { TechnicianCode = "KVT91",TechnicianDisplayName="KVT91  -  Maria, Martinez" } , new Technician() { TechnicianCode = "KVT86",TechnicianDisplayName="KVT86  -  James, Johnson" }, new Technician() { TechnicianCode = "KVT75",TechnicianDisplayName="KVT75  -  William, Smith" },
            new Technician() { TechnicianCode = "RVS44",TechnicianDisplayName="RVS44  -  Robert, Johnson" } , new Technician() { TechnicianCode = "RVS23",TechnicianDisplayName="RVS23  -  Sarah, Smith" }, new Technician() { TechnicianCode = "RVS89",TechnicianDisplayName="RVS89  -  Mike, Jones" } };
            var searchText = e.NewTextValue;
           
            tempRecords = technician.Where(c => c.TechnicianDisplayName.ToUpper().Contains(searchText.ToUpper()));
            LvTechnician.ItemsSource = tempRecords;
          
        }
        private void lv_Technician_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            
            if (e.SelectedItem != null)
            {
                Technician objTechnician = (Technician)LvTechnician.SelectedItem;
                searchBarTech.Text = objTechnician.TechnicianDisplayName;
                GetTechnicianPicker();
            }
        }
    }
    public class Technician
    {
        public string TechnicianCode { get; set; }
        public string TechnicianDisplayName { get; set; }
    }
}
