﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities.Models;
using Xamarin.Forms;

//This is from Demo 1

namespace mobileApp.Views
{
    public partial class JobComplectionImageCmt : ContentPage
    {
        //This is for Demo 2
        public JobComplectionImageCmt(BusinessEntities.Models.ImageDetails image)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
			cambraShotImage.Source = image.Image.Source;
			gridComment.HeightRequest = Device.OnPlatform(515,650,700);

        }
        private void OnClose(object sender, EventArgs e)
        {
            App.TechnicianCode = "Null";
            Navigation.PopAsync();
        }

        private void OnSubmit(object sender, EventArgs e)
        {
            if (txtComments.Text != string.Empty)
                App.TechnicianCode = txtComments.Text;
            else
                App.TechnicianCode = "Null";
			Navigation.PopAsync();			
        }
    }
}
