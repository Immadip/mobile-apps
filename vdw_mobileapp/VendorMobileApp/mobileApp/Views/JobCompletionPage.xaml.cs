﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BusinessLogic;
using BusinessLogic.VDWUILogic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using TK.CustomMap;
using mobileApp.ViewModels;
using System.Diagnostics;
using XLabs.Platform.Services.Media;
using System.IO;
using Newtonsoft.Json;
using System.Linq;
using XLabs.Forms.Controls;
using mobileApp.CustomControls;
using BusinessEntities.Models;
using mobileApp.DependencyServices;
using mobileApp.Helpers;
using BusinessEntities;
using System.Globalization;
using System.Collections.ObjectModel;
//using Java.IO;
using Newtonsoft.Json.Linq;

//This is from Demo 1

namespace mobileApp.Views
{
    //This is for Demo 2
    public partial class JobCompletionPage
    {
        private JobCompletionEntity _completeJobinfo = new JobCompletionEntity();
        private List<DropTypeEntity> _dropTypes = new List<DropTypeEntity>();
        private List<JeopardyCodeEntity> _jeopardyCodes = new List<JeopardyCodeEntity>();
        private List<ImageCategoryEntity> _categories = new List<ImageCategoryEntity>();
        private List<CategoryWithImagesEntity> _categoriesImages = new List<CategoryWithImagesEntity>();
        private List<ImagesEntity> _imagesToUpload = new List<ImagesEntity>();
        private List<string> _imageFilePaths = new List<string>();
        private List<GlobalFlagEntity> _globalParams = new List<GlobalFlagEntity>();
        private string _alertMessage;
        private bool _isFormValid;
        private MapsViewModel mapsViewModel;
        private MultiPicker _multiPicker;
        private bool _isJobAlreadyCompleted;
        private XLabs.Platform.Services.Geolocation.Position loc;

        public JobCompletionPage(JobCompletionEntity completejobinfo, bool IsJobAlreadyCompleted)
        {
            
            try
            {
                InitializeComponent();

                NavigationPage.SetHasNavigationBar(this, false);
                
                #region Soft Back Button Handling
                var backIconTap = new TapGestureRecognizer();
                backIconTap.Tapped += (s, e) =>
                {
                    Navigation.PopAsync();
                };
                imgBackIcon.GestureRecognizers.Add(backIconTap);
                lblBackIcon.GestureRecognizers.Add(backIconTap);
                #endregion Soft Back Button Handling

                _completeJobinfo = completejobinfo;
                _isJobAlreadyCompleted = IsJobAlreadyCompleted;

                BindingContext = mapsViewModel = new MapsViewModel();
                GetDropTypes();
                GetJeopardyCodes();
                GetCategories();
                
                InitializeDateAndTimePickers();

                if (!string.IsNullOrEmpty(completejobinfo.JobId))
                {
                    lblJobId.Text = completejobinfo.JobId;
                }

                lblCircuitId.Text = "11/BURY/A6HD00/VZMA";  // Hardcoded temporarily - Things to Look into.

                if (completejobinfo.VdwDispatchDateTime != null)
                {
                    lblDispatchDateTime.Text = _completeJobinfo.VdwDispatchDateTime != null ? _completeJobinfo.VdwDispatchDateTime.Value.ToString("MM/dd/yyyy HH:mm") : "";
                }

                if (!string.IsNullOrEmpty(completejobinfo.CusstatRemarks))
                {
                    txtCustStatRemarks.Text = completejobinfo.CusstatRemarks;
                }

                mapView.MapType = MapType.Satellite;
                mapView.SetBinding(TKCustomMap.PolylinesProperty, "Lines");
                mapView.SetBinding(TKCustomMap.CustomPinsProperty, "Pins");
                mapView.SetBinding(TKCustomMap.MapRegionProperty, "MapRegion");
                mapView.SetBinding(TKCustomMap.MapFunctionsProperty, "MapFunctions");
                mapView.IsRegionChangeAnimated = true;

                if (_isJobAlreadyCompleted)
                {
                    BindAllCompletionInfoToUi();
                    DisableAllFields(completionInfoContainer);
                    imageUploadInputContainer.IsVisible = false;
                    BtnCancel.IsVisible = BtnSubmit.IsVisible = BtnBoreTypes.IsVisible = BtnClearMapIcons.IsVisible = false;
                }
                else
                {
                    mapView.SetBinding(TKCustomMap.MapLongPressCommandProperty, "MapLongPressCommand");
                    mapView.SetBinding(TKCustomMap.PinDragEndCommandProperty, "DragEndCommand");
                    mapView.SetBinding(TKCustomMap.PinSelectedCommandProperty, "PinSelectedCommand");
                    mapView.SetBinding(TKCustomMap.SelectedPinProperty, "SelectedPin");
                }
				//txtCustStatRemarks.BackgroundColor = Device.OnPlatform(Color.White,Color.FromHex("FFFFFF"),Color.Default);
              
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        private async void OnCustStatRemarksClicked(object sender, EventArgs e)
        {  
            if(((Label)sender).Text.Length > 0)         
            await DisplayAlert("Customer Remarks", ((Label)sender).Text, "OK");
        }

        private void InitializeDateAndTimePickers()
        {
            ArrivalDatePicker.Date = DateTime.Now;
            ArrivalTimePicker.Time = DateTime.Now.TimeOfDay;

            ClearDatePicker.Date = DateTime.Now;
            ClearTimePicker.Time = DateTime.Now.TimeOfDay;

            JeopardyDatePicker.Date = DateTime.Now;
            JeopardyTimePicker.Time = DateTime.Now.TimeOfDay;
        }

        #region Binding Values to UI
        private void BindAllCompletionInfoToUi()
        {
            try
            {
                BindBasicInfoToUi(); //Step 1
                BindDropInfoToUi(); //Step 2
                BindOntInfoToUi(); //Step 5

                #region FillUpCategoriesImages
                // Exclude Screenshot of the Map.
                var ImageEntities = _completeJobinfo.Images.Where(i => i.CategoryId != VdwConstants.ImageCategoryIdForMapScreenshot);

                foreach (var imageEntity in ImageEntities)
                {
					
                    var image = new Image
                    {
                        Source = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(imageEntity.ImageBinary))),
                        Aspect = Aspect.AspectFit,
                        WidthRequest = App.ScreenWidth * 0.75
                    };

					var imgDetails = new ImageDetails
					{
						Image = image,
						Comment = imageEntity.Comments
					};


					// Add the imgObj and category to the CategoryImages List.
					_categoriesImages.Find(
                        catImg => (catImg.CategoryId == imageEntity.CategoryId)).Images.Add(imgDetails);

                }

                // Call the Method which creates the Collapsible Panels for the Images
                BindImagesToUi();
                #endregion FillUpCategoriesImages

                #region Display Map Screenshot
                // Get the Screenshot Image
                var screenShotImageEntity = _completeJobinfo.Images.FirstOrDefault(i => i.CategoryId == VdwConstants.ImageCategoryIdForMapScreenshot);

                mapScreenshotContainer.IsVisible = true;
                mapContainer.IsVisible = false;

                if (screenShotImageEntity != null)
                {
                    // Add the Image stream as the Source to the Step 4 Screenshot Image
                    mapScreenshotImage.Source = ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(screenShotImageEntity.ImageBinary)));
                }
                #endregion Display Map Screenshot

            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        private void BindBasicInfoToUi()
        {
            try
            {
				
				ArrivalDatePicker.Date = Device.OnPlatform(DateTime.ParseExact(_completeJobinfo.ArrivalDate.Replace("/","-"),"MM-dd-yyyy", null),DateTime.ParseExact(_completeJobinfo.ArrivalDate.Replace("-", "/"), "MM/dd/yyyy", CultureInfo.InvariantCulture),DateTime.ParseExact(_completeJobinfo.ArrivalDate.Replace("-", "/"), "MM/dd/yyyy", CultureInfo.InvariantCulture));//Device.OnPlatform("MM-dd-yyyy","MM/dd/yyyy","MM/dd/yyyy") CultureInfo.InvariantCulture
                ArrivalTimePicker.Time = DateTime.ParseExact(_completeJobinfo.ArrivalTime, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;

				ClearDatePicker.Date = Device.OnPlatform(DateTime.ParseExact(_completeJobinfo.ClearDate.Replace("/", "-"), "MM-dd-yyyy", null), DateTime.ParseExact(_completeJobinfo.ClearDate.Replace("-", "/"), "MM/dd/yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(_completeJobinfo.ClearDate.Replace("-", "/"), "MM/dd/yyyy", CultureInfo.InvariantCulture));//ClearDate

                ClearTimePicker.Time = DateTime.ParseExact(_completeJobinfo.ClearTime, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;

                txtCableLocateNum.Text = _completeJobinfo.CableLocateNum;

                if (!string.IsNullOrEmpty(_completeJobinfo.JeoPardyCode)
                    && _completeJobinfo.JeopardyDate != null
                    && _completeJobinfo.JeopardyTime != null)
                {
                    JeopardyPicker.SelectedIndex = JeopardyPicker.Items.IndexOf(JeopardyPicker.Items.FirstOrDefault(i => i == _completeJobinfo.JeoPardyCode));
					JeopardyDatePicker.Date = Device.OnPlatform(DateTime.ParseExact(_completeJobinfo.JeopardyDate.Replace("/", "-"), "MM-dd-yyyy", null), DateTime.ParseExact(_completeJobinfo.JeopardyDate.Replace("-", "/"), "MM/dd/yyyy", CultureInfo.InvariantCulture), DateTime.ParseExact(_completeJobinfo.JeopardyDate.Replace("-", "/"), "MM/dd/yyyy", CultureInfo.InvariantCulture));//JeopardyDate
                    JeopardyTimePicker.Time = DateTime.ParseExact(_completeJobinfo.JeopardyTime, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;
                }
                else
                {
                    JeopardyCodeContainer.IsVisible = JeopardyDateTimeContainer.IsVisible = false;
                }
                DidCompleteJob.IsToggled = _completeJobinfo.DidCompletejob == Constants.Yes;

                // User Entered Completion Remarks is appended with some more information starting with "DWO".
                if (_completeJobinfo.CompletionRemarks != null)
                {
                    txtCompletionRemarks.Text = _completeJobinfo.CompletionRemarks.Remove(_completeJobinfo.CompletionRemarks.IndexOf("DWO"));
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }

        }

        private void BindDropInfoToUi()
        {
            try
            {
                ReferToEngg.IsToggled = _completeJobinfo.ReferToEngg == Constants.Yes;
                if (ReferToEngg.IsToggled)
                {
                    txtEnggRemarks.Text = _completeJobinfo.EnggRemarks;
                    txtContactName.Text = _completeJobinfo.ContactName;
                    txtContact.Text = _completeJobinfo.Contact;
                }
                else
                {
                    DropTypePicker.SelectedIndex = DropTypePicker.Items.IndexOf(DropTypePicker.Items.FirstOrDefault(i => i == _completeJobinfo.DropType));
                    TripCharge.IsToggled = _completeJobinfo.TripCharge == Constants.Yes;
                    if (!TripCharge.IsToggled)
                    {
                        txtNoOfBores.Text = _completeJobinfo.NoofBores;

                        lblSelectedBoreTypes.Text = _completeJobinfo.BoreType;
                        txtTotalBoreLength.Text = _completeJobinfo.TotBoreLength;
                        txtDirectBuriedLength.Text = _completeJobinfo.DirectBuriedLength;
                        txtPullThroughLength.Text = _completeJobinfo.PullthroughLength;
                        txtDropConduitLength.Text = _completeJobinfo.DropConduitLength;
                        txtPreCutLength.Text = _completeJobinfo.PreCutLength;
                    }
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }

        }

        private void BindOntInfoToUi()
        {
            try
            {
                DidTerminateDrop.IsToggled = _completeJobinfo.DidTerminateDrop == Constants.Yes;
                DidPlaceONT.IsToggled = _completeJobinfo.DidplaceanONT == Constants.Yes;

                if (DidPlaceONT.IsToggled)
                {
                    ONTLocInOut.IsToggled = _completeJobinfo.ONTLocInOut == Constants.Input;
                    txtONTLocationRemarks.Text = _completeJobinfo.ONTLocationRemarks;
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        #endregion Binding Values to UI

        private void DisableAllFields(Layout<View> layout)
        {
            foreach (var child in layout.Children)
            {
                if (child is Layout<View>)
                {
                    DisableAllFields((Layout<View>)child);
                }
                // Donot disable the Buttons because the Image Category Collapsible Panels are Buttons
                else if (!(child is Button))
                {
                    child.IsEnabled = false;
                }

                if (child is Entry)
                {
                    ((Entry)child).Placeholder = string.Empty;
                }

                if (child is Picker)
                {
                    ((Picker)child).Title = string.Empty;
                }
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                // Refresh the Toolbar, in case any change in the Notification count, etc
                toolbar.Refresh();
                App.CurrentToolbar = toolbar;

                if (_multiPicker != null)
                {
                    var selectedBoreTypes = _multiPicker.GetSelection();

                    PathCreationRow.IsVisible = PathCreationOutofRow.IsVisible = RoadOrDriveway.IsVisible = BoreTypes.IsVisible = false;
                    lblSelectedBoreTypes.Text = string.Empty;

                    if (selectedBoreTypes != null && selectedBoreTypes.Count > 0)
                    {
                        BoreTypes.IsVisible = true;
                        lblSelectedBoreTypes.Text = string.Join(", ", selectedBoreTypes.Select(b => b.Name));
                        foreach (var a in selectedBoreTypes)
                        {
                            if (a.Name.Equals("Path Creation In ROW"))
                            {
                                PathCreationRow.IsVisible = true;
                                lblPathCreationRow.Text = a.Name;
                            }

                            if (a.Name.Equals("Path Creation Out of ROW"))
                            {
                                PathCreationOutofRow.IsVisible = true;
                                lblPathCreationOutofRow.Text = a.Name;
                            }

                            if (a.Name.Equals("Road/Driveway"))
                            {
                                RoadOrDriveway.IsVisible = true;
                                lblRoadOrDriveway.Text = a.Name;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
            try
            {
                mapView.MapCenter = (Position)Application.Current.Resources["position"];
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }


        }

        protected async void OnAddImageButtonClicked(object sender, EventArgs e)
        {
            if (CategoryPicker.SelectedIndex == -1)
            {
                await DisplayAlert("Invalid Category", "Please Select Category of the Image", "Ok");
                return;
            }

            if (!DeviceHardware.LocationAvailable())
            {
                await DisplayAlert("Location Unavailable", "Please turn ON Location to Upload Image", "Ok");
                return;
            }


            MediaFile media = await DeviceHardware.TakePicture();



            if (media == null) return;
            var imgObject = new Image
            {
                Source = ImageSource.FromStream(() => media.Source),
                Aspect = Aspect.AspectFit,
                WidthRequest = App.ScreenWidth * 0.75
            };

			string comments = string.Empty;


			var imgDetails = new ImageDetails
			{
				Image = imgObject,
				Comment = comments
			};



            App.TechnicianCode = string.Empty;
            var page = new JobComplectionImageCmt(imgDetails);
            await Navigation.PushAsync(page);

            var result = await CheckTechcode();

			if (App.TechnicianCode != "Null")
			{
				imgDetails.Comment = App.TechnicianCode;
			}
            //Add the imgObj and category to the CategoryImages List
            //For now, comparing using Description, but we need to compare using CategoryId - Things to look into.                
            // _categoriesImages.Find(catImg => catImg.CategoryDescription == CategoryPicker.Items[CategoryPicker.SelectedIndex]).Images.Add(imgObject);

            //Need to get comments from the User.
            

            _categoriesImages.Find(
                catImg => catImg.CategoryId == _categories[CategoryPicker.SelectedIndex].CategoryId).Images.Add(imgDetails);

            BindImagesToUi();

            var categoryId = _categories.Find(c => c.CategoryDescription == CategoryPicker.Items[CategoryPicker.SelectedIndex]).CategoryId;

            var imageStream = new MemoryStream();
            media.Source.CopyTo(imageStream);

			var imgEntityObject = CreateImageEntity(imageStream.ToArray(), categoryId, imgDetails.Comment);
            _imagesToUpload.Add(imgEntityObject);
            _imageFilePaths.Add(media.Path);

        }

        protected async Task<bool> CheckTechcode()
        {
            while (App.TechnicianCode == string.Empty)
            {
                await Task.Delay(50);
            }
            return true;
        }

        public ImagesEntity CreateImageEntity(byte[] originalImageBytes, int categoryId, string comments)
        {

            //GetLocation() was throwing some task stopped exception (may be we need to increase the timeout (Right now, it is 500)), so hardcoding for now - Things to look into.
            //var loc = new XLabs.Platform.Services.Geolocation.Position
            //{
            //    Latitude = 8.3526542,
            //    Longitude = 6.48936515
            //};
            var imageBytes = DependencyService.Get<IImageDependencyService>().ResizeImage(originalImageBytes, "jpg", MobileConstants.ImageMaxWidth, MobileConstants.ImageMaxHeight);
            var thumbnailBytes = DependencyService.Get<IImageDependencyService>().ResizeImage(originalImageBytes, "jpg", MobileConstants.ThumbnailImageMaxWidth, MobileConstants.ThumbnailImageMaxHeight);
            var lth = imageBytes.Length;
            var imgObject = new ImagesEntity
            {
                JobId = _completeJobinfo.JobId,
                ImageBinary = Convert.ToBase64String(imageBytes),
                CategoryId = categoryId,
                ImageThumbnailBinary = Convert.ToBase64String(thumbnailBytes),
                ImageTimeStamp = DateTime.UtcNow,
                Latitude = loc == null ? 8.3526542 : loc.Latitude,
                Longitude = loc == null ? 6.48936515 : loc.Longitude,
                TechId = App.LoggedInUserId,
                DoOrderId = _completeJobinfo.DoOrderId,
                Mon = _completeJobinfo.Mon,
                Comments = comments,
            };
            return imgObject;
        }

        public async void CaptureMapScreenshotAndAddToQueue()
        {
            try
            {
                if (_isJobAlreadyCompleted) return;
                // Recenter the Map to show all Pins (Icons) before capturing the Screenshot.
                mapsViewModel.RecenterMapView();

				var screenshotBytes = await mapsViewModel.MapFunctions.GetSnapshot();

                // Delete the Existing Map Screenshot if any.
                _imagesToUpload.RemoveAll(i => i.CategoryId == VdwConstants.ImageCategoryIdForMapScreenshot);
                _imagesToUpload.Add(CreateImageEntity(screenshotBytes, VdwConstants.ImageCategoryIdForMapScreenshot, VdwConstants.ImageCommentForMapScreenshot));
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }

        }

        public bool IsFormValid
        {
            get
            {
                return IsFormValid1;
            }

            set
            {
                IsFormValid1 = value;
            }
        }

        public bool IsFormValid1
        {
            get
            {
                return _isFormValid;
            }

            set
            {
                _isFormValid = value;
            }
        }

        protected async void OnBoreTypesButtonClicked(object sender, EventArgs ea)
        {
            var items = new List<PickerItem>();
            items.Add(new PickerItem { Name = "Road/Driveway" });
            items.Add(new PickerItem { Name = "Path Creation Out of ROW" });
            items.Add(new PickerItem { Name = "Path Creation In ROW" });

            if (_multiPicker == null)
                _multiPicker = new MultiPicker(items);

            await Navigation.PushAsync(_multiPicker, true);
        }

        public void GetDropTypes()
        {
            _dropTypes = VdwLocalDataAccess.GetDropTypesDB();
            List<DropTypeEntity> dropList = _dropTypes;
            if (_dropTypes == null) dropList = new List<DropTypeEntity>();
            foreach (var item in dropList)
            {
                DropTypePicker.Items.Add(item.FiberType);
            }
        }

        public void GetJeopardyCodes()
        {
            _jeopardyCodes = VdwLocalDataAccess.GetGeopardyCodesDB();
            foreach (var item in _jeopardyCodes)
            {
                JeopardyPicker.Items.Add(item.JwmCode);
            }
        }

        public void GetCategories()
        {
            _categories = VdwLocalDataAccess.GetCategoriesDB();

            foreach (var item in _categories)
            {
                CategoryPicker.Items.Add(item.CategoryDescription);

                //Initialise the CategoryImages List
                _categoriesImages.Add(new CategoryWithImagesEntity
                {
                    CategoryDescription = item.CategoryDescription,
                    CategoryId = item.CategoryId,
					Images = new List<ImageDetails>()
                });
            }
        }

        public void BindImagesToUi()
        {
            try
            {
                imagesPreview.Children.Clear();
                foreach (var category in _categoriesImages)
                {
                    var currentCategoryStackLayout = new StackLayout
                    {
                        Orientation = StackOrientation.Vertical
                    };


                    foreach (var image in category.Images)
                    {
						currentCategoryStackLayout.Children.Add(image.Image);
						var currentCategoryLabel = new Label
						{
							Text = image.Comment,
							Style = (Style)Application.Current.Resources["fieldCommentLabel"],

						};
                        if(image.Comment != null)
						currentCategoryStackLayout.Children.Add(currentCategoryLabel);
                    }


					currentCategoryStackLayout.IsVisible = category.Images.Any();

                    var currentCategoryCollapsibleButton = new Button
                    {
                        Text = category.CategoryDescription + " (" + category.Images.Count + ")",
                        Style = currentCategoryStackLayout.IsVisible ? (Style)Application.Current.Resources["imageCategoryPanelActive"]
                                                                    : (Style)Application.Current.Resources["imageCategoryPanelInActive"],
                    };
                    currentCategoryCollapsibleButton.Clicked += (sender, args) =>
                    {
                        currentCategoryStackLayout.IsVisible = !currentCategoryStackLayout.IsVisible;
                        currentCategoryCollapsibleButton.Style = currentCategoryStackLayout.IsVisible ? (Style)Application.Current.Resources["imageCategoryPanelActive"]
                                                                                                        : (Style)Application.Current.Resources["imageCategoryPanelInActive"];
                    };

                    imagesPreview.Children.Add(currentCategoryCollapsibleButton);

                    imagesPreview.Children.Add(currentCategoryStackLayout);
                }

            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        public async Task<int> CompleteTheJob(JobCompletionEntity completejobinfo)
        {
            try
            {
                //Disable the Submit and Cancel Buttons to prevent the user from Submitting it again or Cancelling it after the Data is sent
                BtnSubmit.IsEnabled = false;
                BtnCancel.IsEnabled = false;

                //Add ImageEntitie's list to completejobinfoobject
                completejobinfo.Images = _imagesToUpload;

                completejobinfo.MapPolygonJson = JsonConvert.SerializeObject(mapsViewModel.Pins);

                //bool objJobCompleted = await JobActionsBL.CompleteTheJob(completejobinfo);

                int objJobCompleted = await VdwLocalDataAccess.CompleteTheJob(completejobinfo, _imagesToUpload, mapsViewModel);


                if (objJobCompleted > 0)
                {
                    await DisplayAlert("Alert", "Job Completed Successfully", "OK");
                    // Delete the Images from the Device

                    // There was an Memory Issue in Android due to piling up of photos in the internal folder. So clearing them up once the Job is Completed.
                    /* if(Device.OS == TargetPlatform.Android)
                     {
                         foreach (var imageFilePath in _imageFilePaths)
                         {
                             new File(imageFilePath).Delete();
                         }
                     }*/
                }
                else
                {
                    await DisplayAlert("Oops", "Job is not Completed", "Try again");
                }
                await Navigation.PushAsync(new Views.TechJobSummaryPage());
                Navigation.RemovePage(this);
                return objJobCompleted;

            }

            catch (Exception)
            {
                return 0;
            }

        }

        protected void OnSubmitButtonClicked(object sender, EventArgs e)
        {
            try
            {
                //Assuming the Form to be valid. It will be set as False by one of the Validator Methods if not Valid.
                IsFormValid = true;

                //Validate the controls in the First Screen.
                ValidateStep1();

                if (IsFormValid)
                {
                    if (ReferToEngg.IsToggled)
                    {
                        ValidateReferToEngineeringIsTrue();
                        if (IsFormValid)
                        {
                            BindReferToEngineeringIsTrue();
                        }
                    }
                    else
                    {
                        ValidateReferToEngineeringIsFalse();
                        if (IsFormValid)
                        {
                            BindReferToEngineeringIsFalse();
                        }
                    }
                }

                if (IsFormValid)
                {
                    if (TripCharge.IsToggled == false)
                    {
                        ValidateTripChargeIsFalse();
                        if (IsFormValid)
                        {
                            BindTripChargeIsFalse();
                        }
                    }

                    else if (TripCharge.IsToggled)
                    {
                        if (IsFormValid)
                        {
                            BindTripChargeIsTrue();
                        }
                    }
                }

                //Validate the controls in the Image Upload Screen.
                if (IsFormValid)
                {
                    ValidateStep3();
                }

                if (IsFormValid)
                {
                    //These are fields which should be bound in all scenarios.
                    BindStep1();
                }

                if (IsFormValid)
                {
                    //Validate Business Rules and Submit the Form.
                    ValidateBeforeSubmit(_completeJobinfo);
                }

            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        public void ValidateStep1()
        {
            try
            {
                if (string.IsNullOrEmpty(lblJobId.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Job ID is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    lblJobId.BackgroundColor = Color.Yellow;
                    return;
                }
                lblJobId.BackgroundColor = Color.Default;

                if (string.IsNullOrEmpty(txtCableLocateNum.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Cable Locate Number is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtCableLocateNum.BackgroundColor = Color.Yellow;
                    return;
                }
                txtCableLocateNum.BackgroundColor = Color.Default;
                //Should check whether this is a required field.

                if (!(ClearDatePicker.Date > DateTime.Parse(_completeJobinfo.CommitmentDate)))
                {
                    if (JeopardyPicker.SelectedIndex < 0)
                    {
                        IsFormValid = false;
                        _alertMessage = "Jeopardy Code is Empty";
                        DisplayAlert("Alert", _alertMessage, "ok");
                        JeopardyPicker.BackgroundColor = Color.Yellow;
                        return;
                    }
                    JeopardyPicker.BackgroundColor = Color.Default;


                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        public async void ValidateStep3()
        {
            try
            {
                var imageRequiredValidationFlag = App.GlobalFlags.FirstOrDefault(f => f.Key == Constants.ImageRequiredValidationFlagKey);
                if (imageRequiredValidationFlag != null && imageRequiredValidationFlag.Value == "Y")
                {
                    foreach (var category in _categoriesImages)
                    {
                        if (category.CategoryDescription == "Fiber")
                        {
                            if (category.Images.Count <= 0)
                            {
                                IsFormValid = false;
                                _alertMessage = "Please upload any image for the " + category.CategoryDescription + " category";
                                await DisplayAlert("Alert", _alertMessage, "ok");
                                return;
                            }
                        }
                        else if (category.CategoryDescription == "Hub")
                        {
                            if (category.Images.Count <= 0)
                            {
                                IsFormValid = false;
                                _alertMessage = "Please upload any image for the " + category.CategoryDescription + " category";
                                await DisplayAlert("Alert", _alertMessage, "ok");
                                return;
                            }
                        }

                        else if (category.CategoryDescription == "Home")
                        {
                            if (category.Images.Count <= 0)
                            {
                                IsFormValid = false;
                                _alertMessage = "Please upload any image for the " + category.CategoryDescription + " category";
                                await DisplayAlert("Alert", _alertMessage, "ok");
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        public void ValidateReferToEngineeringIsTrue()
        {
            try
            {
                if (string.IsNullOrEmpty(txtEnggRemarks.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Engineering Remarks is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtEnggRemarks.BackgroundColor = Color.Yellow;
                    return;
                }
                txtEnggRemarks.BackgroundColor = Color.Default;

                if (string.IsNullOrEmpty(txtContactName.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Contact Name is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtContactName.BackgroundColor = Color.Yellow;
                    return;
                }
                txtContactName.BackgroundColor = Color.Default;

                if (string.IsNullOrEmpty(txtContact.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Contact Number is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtContact.BackgroundColor = Color.Yellow;
                    return;
                }
                else if (!Regex.IsMatch(txtContact.Text, "[0-9]{3}-[0-9]{3}-[0-9]{4}"))
                {
                    IsFormValid = false;
                    _alertMessage = "Contact Number should be in ###-###-####";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtContact.BackgroundColor = Color.Yellow;
                    return;
                }
                txtContact.BackgroundColor = Color.Default;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        public void BindReferToEngineeringIsTrue()
        {
            try
            {
                _completeJobinfo.ReferToEngg = Constants.Yes;
                _completeJobinfo.EnggRemarks = txtEnggRemarks.Text;
                _completeJobinfo.ContactName = txtContactName.Text;
                _completeJobinfo.Contact = txtContact.Text;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }

        }

        public void ValidateReferToEngineeringIsFalse()
        {
            try
            {
                if (DropTypePicker.SelectedIndex < 0)
                {
                    IsFormValid = false;
                    _alertMessage = "DropType is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    DropTypePicker.BackgroundColor = Color.Yellow;
                    return;
                }
                DropTypePicker.BackgroundColor = Color.Default;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }

        }

        public void BindReferToEngineeringIsFalse()
        {
            _completeJobinfo.ReferToEngg = Constants.No;
            _completeJobinfo.DropType = DropTypePicker.SelectedIndex >= 0 ? DropTypePicker.Items[DropTypePicker.SelectedIndex] : string.Empty;
        }

        public void ValidateTripChargeIsFalse()
        {
            try
            {
                if (string.IsNullOrEmpty(txtNoOfBores.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Number of Bores is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtNoOfBores.BackgroundColor = Color.Yellow;
                    return;
                }
                txtNoOfBores.BackgroundColor = Color.Default;

                if (string.IsNullOrEmpty(txtNoofBoresPathCreationRow.Text) && string.IsNullOrEmpty(txtNoofBoresPathCreationOutofRow.Text) && string.IsNullOrEmpty(txtNoofBoresRoadOrDriveway.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "No of BoreTypes is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtBoreLengthPathCreationRow.BackgroundColor = Color.Yellow;
                    txtBoreLengthPathCreationOutofRow.BackgroundColor = Color.Yellow;
                    txtBoreLengthRoadOrDriveway.BackgroundColor = Color.Yellow;
                    return;
                }
                txtNoofBoresPathCreationRow.BackgroundColor = Color.Default;
                txtNoofBoresPathCreationOutofRow.BackgroundColor = Color.Default;
                txtNoofBoresRoadOrDriveway.BackgroundColor = Color.Default;

                if (string.IsNullOrEmpty(txtBoreLengthPathCreationRow.Text) && string.IsNullOrEmpty(txtBoreLengthPathCreationOutofRow.Text) && string.IsNullOrEmpty(txtBoreLengthRoadOrDriveway.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Bore Length/Lengths are Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtBoreLengthPathCreationRow.BackgroundColor = Color.Yellow;
                    txtBoreLengthPathCreationOutofRow.BackgroundColor = Color.Yellow;
                    txtBoreLengthRoadOrDriveway.BackgroundColor = Color.Yellow;
                    return;
                }
                txtBoreLengthPathCreationRow.BackgroundColor = Color.Default;
                txtBoreLengthPathCreationOutofRow.BackgroundColor = Color.Default;
                txtBoreLengthRoadOrDriveway.BackgroundColor = Color.Default;


                if (string.IsNullOrEmpty(txtTotalBoreLength.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Total Bore Length is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtTotalBoreLength.BackgroundColor = Color.Yellow;
                    return;
                }
                txtTotalBoreLength.BackgroundColor = Color.Default;

                //Making it invisible because the Value of this field is not recorded anywhere. We Should Check where this field's Value is used. - Things to look into.
                //if (string.IsNullOrEmpty(txtDirectBuriedLength.Text))
                //{
                //    IsFormValid = false;
                //    _alertMessage = "Direct Buried Length is Empty";
                //    DisplayAlert("Alert", _alertMessage, "ok");
                //    txtDirectBuriedLength.BackgroundColor = Color.Yellow;
                //    return;
                //}
                //txtDirectBuriedLength.BackgroundColor = Color.Default;

                if (string.IsNullOrEmpty(txtPullThroughLength.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Pull through Length is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtPullThroughLength.BackgroundColor = Color.Yellow;
                    return;
                }
                txtPullThroughLength.BackgroundColor = Color.Default;

                if (string.IsNullOrEmpty(txtDropConduitLength.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Drop Conduit Length is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtDropConduitLength.BackgroundColor = Color.Yellow;
                    return;
                }
                txtDropConduitLength.BackgroundColor = Color.Default;

                if (string.IsNullOrEmpty(txtPreCutLength.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "Pre Cut Length is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtPreCutLength.BackgroundColor = Color.Yellow;
                    return;
                }
                txtPreCutLength.BackgroundColor = Color.Default;

            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        public void BindTripChargeIsFalse()
        {
            try
            {
                _completeJobinfo.TripCharge = Constants.No;
                _completeJobinfo.NoofBores = txtNoOfBores.Text;
                _completeJobinfo.BoreTypeNumber = (Convert.ToInt32(txtNoofBoresPathCreationRow.Text) + Convert.ToInt32(txtNoofBoresPathCreationOutofRow.Text) + Convert.ToInt32(txtNoofBoresRoadOrDriveway.Text)).ToString();
                _completeJobinfo.BoreTypeLength = (Convert.ToInt32(txtBoreLengthPathCreationRow.Text) + Convert.ToInt32(txtBoreLengthPathCreationOutofRow.Text) + Convert.ToInt32(txtBoreLengthRoadOrDriveway.Text)).ToString();

                if (!string.IsNullOrEmpty(lblSelectedBoreTypes.Text))
                {
                    _completeJobinfo.BoreType = lblSelectedBoreTypes.Text;
                }

                _completeJobinfo.TotBoreLength = txtTotalBoreLength.Text;
                _completeJobinfo.DirectBuriedLength = txtDirectBuriedLength.Text;
                _completeJobinfo.PullthroughLength = txtPullThroughLength.Text;
                _completeJobinfo.DropConduitLength = txtDropConduitLength.Text;
                _completeJobinfo.PreCutLength = txtPreCutLength.Text;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        public void BindTripChargeIsTrue()
        {
            try
            {
                _completeJobinfo.TripCharge = Constants.Yes;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }

        }

        public void BindStep1()
        {
            try
            {
                _completeJobinfo.JobId = lblJobId.Text;

                _completeJobinfo.ArrivalDate = ArrivalDatePicker.Date.ToString("MM/dd/yyyy");

                TimeSpan arrivalTimeSpan = ArrivalTimePicker.Time;
                string arrivalHour = (int)arrivalTimeSpan.TotalHours + arrivalTimeSpan.ToString(@"\:mm");
                DateTime arrTime = Convert.ToDateTime(arrivalHour);
                _completeJobinfo.ArrivalTime = arrTime.ToString("HH:mm");

                _completeJobinfo.ClearDate = ClearDatePicker.Date.ToString("MM/dd/yyyy");

                TimeSpan clearTimeSpan = ClearTimePicker.Time;
                string clearHour = (int)clearTimeSpan.TotalHours + clearTimeSpan.ToString(@"\:mm");
                DateTime clearTime = Convert.ToDateTime(clearHour);
                _completeJobinfo.ClearTime = clearTime.ToString("HH:mm");

                _completeJobinfo.CableLocateNum = txtCableLocateNum.Text;

                if (!(ClearDatePicker.Date > DateTime.Parse(_completeJobinfo.CommitmentDate)))
                {
                    _completeJobinfo.JeoPardyCode = JeopardyPicker.SelectedIndex >= 0 ? JeopardyPicker.Items[JeopardyPicker.SelectedIndex] : string.Empty;

                    _completeJobinfo.JeopardyDate = JeopardyDatePicker.Date.ToString("MM/dd/yyyy");

                    TimeSpan jeopardyTimeSpan = JeopardyTimePicker.Time;
                    string jeopardyHour = (int)jeopardyTimeSpan.TotalHours + jeopardyTimeSpan.ToString(@"\:mm");
                    DateTime jeopardyTime = Convert.ToDateTime(jeopardyHour);
                    _completeJobinfo.JeopardyTime = jeopardyTime.ToString("HH:mm");
                }

                _completeJobinfo.DidCompletejob = DidCompleteJob.IsToggled ? Constants.Yes : Constants.No;

            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }

        }

        public void ValidateBeforeSubmit(JobCompletionEntity completejobinfo)
        {
            try
            {

                if (DidPlaceONT.IsToggled)
                {
                    completejobinfo.DidplaceanONT = Constants.Yes;
                    ValidateOntLocationDetails();
                    if (IsFormValid)
                    {
                        BindOntLocationInfo();
                    }
                    completejobinfo.ONTLocInOut = ONTLocInOut.IsToggled ? Constants.Input : Constants.Output;
                }
                else
                {
                    completejobinfo.DidplaceanONT = Constants.No;
                }

                completejobinfo.DidTerminateDrop = DidTerminateDrop.IsToggled ? Constants.Yes : Constants.No;


                if (!string.IsNullOrEmpty(txtCompletionRemarks.Text))
                {
                    completejobinfo.CompletionRemarks = txtCompletionRemarks.Text + " DWO:" + completejobinfo.VdwDispatchDateTime.Value.ToString("HH:mm") + " " + completejobinfo.VdwDispatchDateTime.Value.ToString("MM/dd/yyyy") + " " + completejobinfo.TechId + " **COMPLETE" +
                        " CMP:TRIP CHARGE:" + completejobinfo.TripCharge + " BY USER:" + completejobinfo.TechId +
                        " CMP:PULL THROUGH LENGTH:" + completejobinfo.PullthroughLength + " BY USER:" + completejobinfo.TechId +
                        " CMP:PRE CUT LENGTH:" + completejobinfo.PreCutLength + " BY USER:" + completejobinfo.TechId +
                        " T- " + completejobinfo.TechId +
                        " CUSTOMER CONTACT:" + completejobinfo.Contact +
                        " #ARRIVAL DATE " + completejobinfo.ArrivalDate + "#" +
                        " #ARRIVAL TIME " + completejobinfo.ArrivalTime + "#" +
                        " DROP LENGTH:" + completejobinfo.DropConduitLength + ";" +
                        " NUM BORES:" + completejobinfo.NoofBores + ";" +
                        " TOTAL BORE LENGTH:" + completejobinfo.TotBoreLength;
                }

                if (IsFormValid)
                {
                    validateBusinessRules(completejobinfo);
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        public void validateBusinessRules(JobCompletionEntity completejobinfo)
        {
            try
            {
                if (!(ClearDatePicker.Date.Date < DateTime.Now.Date || (ClearDatePicker.Date.Date == DateTime.Now.Date && ClearTimePicker.Time <= DateTime.Now.TimeOfDay)))
                {
                    IsFormValid = false;
                    _alertMessage = "Clear Date and Time cannot be later than the Current Date and Time";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    ClearDatePicker.BackgroundColor = Color.Yellow;
                    ClearTimePicker.BackgroundColor = Color.Yellow;
                    return;
                }

                ClearDatePicker.BackgroundColor = Color.Default;
                ClearTimePicker.BackgroundColor = Color.Default;

                if (!(ArrivalDatePicker.Date.Date < DateTime.Now.Date || (ArrivalDatePicker.Date.Date == DateTime.Now.Date && ArrivalTimePicker.Time < DateTime.Now.TimeOfDay)))
                {
                    IsFormValid = false;
                    _alertMessage = "Arrival Date and Time should be before the Current Date and Time";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    ArrivalDatePicker.BackgroundColor = Color.Yellow;
                    ArrivalTimePicker.BackgroundColor = Color.Yellow;
                    return;
                }

                ArrivalDatePicker.BackgroundColor = Color.Default;
                ArrivalTimePicker.BackgroundColor = Color.Default;

                if (!(ClearDatePicker.Date > DateTime.ParseExact(_completeJobinfo.CommitmentDate, "MM/dd/yyyy HH:mm", null)))//DateTime.Parse(_completeJobinfo.CommitmentDate)
                {
                    if (!(JeopardyDatePicker.Date.Date < ClearDatePicker.Date.Date || (JeopardyDatePicker.Date.Date == ClearDatePicker.Date.Date && JeopardyTimePicker.Time < ClearTimePicker.Time)))
                    {
                        IsFormValid = false;
                        _alertMessage = "Jeopardy Date and Time should be before the Clear Date and Time";
                        DisplayAlert("Alert", _alertMessage, "ok");
                        JeopardyDatePicker.BackgroundColor = Color.Yellow;
                        JeopardyTimePicker.BackgroundColor = Color.Yellow;
                        return;
                    }
                    JeopardyDatePicker.BackgroundColor = Color.Default;
                    JeopardyTimePicker.BackgroundColor = Color.Default;

                    if (IsFormValid)
                    {
                        CompleteTheJob(completejobinfo);
                    }

                }
                else
                {
                    if (IsFormValid)
                    {
                        CompleteTheJob(completejobinfo);
                    }
                }

            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        public void ValidateOntLocationDetails()
        {
            try
            {
                if (string.IsNullOrEmpty(txtONTLocationRemarks.Text))
                {
                    IsFormValid = false;
                    _alertMessage = "ONT Location Remarks is Empty";
                    DisplayAlert("Alert", _alertMessage, "ok");
                    txtONTLocationRemarks.BackgroundColor = Color.Yellow;
                    return;
                }
                txtONTLocationRemarks.BackgroundColor = Color.Default;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }


        }

        public void BindOntLocationInfo()
        {
            try
            {
                _completeJobinfo.ONTLocationRemarks = txtONTLocationRemarks.Text;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        protected void RefToEng_Toggled(object sender, ToggledEventArgs e)
        {
            try
            {
                if (e.Value)
                {
                    ReferToEnggYes.IsVisible = true;
                    ReferToEnggNo.IsVisible = false;
                    TripChargeToggle.IsVisible = false;
                    DropTypeLayout.IsVisible = false;
                }
                else
                {
                    ReferToEnggNo.IsVisible = true;
                    ReferToEnggYes.IsVisible = false;
                    TripCharge.IsToggled = true;
                    DropTypeLayout.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        protected void TripCharge_Toggled(object sender, ToggledEventArgs e)
        {
            try
            {
                if (e.Value)
                {
                    TripChargeToggle.IsVisible = false;
                    DropTypeLayout.IsVisible = true;

                }
                else
                {
                    TripChargeToggle.IsVisible = true;
                    DropTypeLayout.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        protected void DidPlaceONT_Toggled(object sender, ToggledEventArgs e)
        {
            try
            {
                if (e.Value)
                {
                    DidPlaceONTYes.IsVisible = true;
                }
                else
                {
                    DidPlaceONTYes.IsVisible = false;
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        protected void CableLocateNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            int _limit = 12;
            var entry = (Entry)sender;

            ((Entry)sender).TextColor = Color.Black;
            BtnSubmit.IsEnabled = true;
            double num;
            if (!string.IsNullOrEmpty(entry.Text) && !double.TryParse(entry.Text, out num))
            {
                ((Entry)sender).TextColor = Color.Red;
                DisplayAlert("Alert", "Enter Only Numbers", "ok");
            }
            else if (entry.Text.Length > _limit)
            {
                entry.Text = entry.Text.Remove(entry.Text.Length - 1);
                DisplayAlert("Alert", "No of Characters Exceeded", "ok");
            }
        }        

        protected void Remarks_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateRemarksLength((Editor)sender);
        }

        protected void EnggRemarks_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateRemarksLength((Editor)sender);
        }

        protected void ONTLocationRemarks_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateRemarksLength((Editor)sender);
        }

        protected void ContactName_TextChanged(object sender, TextChangedEventArgs e)
        {
            int _limit = 20;
            var entry = (Entry)sender;

            ((Entry)sender).TextColor = Color.Black;
            if (entry.Text.Length > _limit)
            {
                entry.Text = entry.Text.Remove(entry.Text.Length - 1);
                DisplayAlert("Alert", "No of Characters Exceeded", "ok");
            }
        }

        protected void NoOfBores_TextChanged(object sender, TextChangedEventArgs e)
        {
            int _limit = 2;
            int num;
            var entry = (Entry)sender;

            ((Entry)sender).TextColor = Color.Black;
            BtnSubmit.IsEnabled = true;
            if (!string.IsNullOrEmpty(entry.Text) && !Int32.TryParse(entry.Text, out num))
            {
                ((Entry)sender).TextColor = Color.Red;
                DisplayAlert("Alert", "Only Enter Numbers", "ok");
            }
            else if (entry.Text.Length > _limit)
            {
                entry.Text = entry.Text.Remove(entry.Text.Length - 1);
                DisplayAlert("Alert", "No of Characters Exceeded", "ok");
            }

        }

        protected void PathCreationRowBores_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void PathCreationRowBoreLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void PathCreationOutofRowBores_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void PathCreationOutofRowBoreLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void RoadOrDrivewayBores_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void RoadOrDrivewayBoreLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void TotalBoreLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void DirectBuriedLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void PullThroughLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void DropOrConduitLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void PreCutLength_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateNumericBehaviourAndLength((Entry)sender);
        }

        protected void ContactNumber_TextChanged(object sender, TextChangedEventArgs e)
        {
            var contactNumberEntry = (Entry)sender;

            var contactNumber = contactNumberEntry.Text;
            if (!string.IsNullOrEmpty(contactNumber))
            {
                //Remove all the Hyphens
                contactNumber = contactNumber.Replace("-", "");

                //Reinsert the Hyphens at the right Positions
                if (contactNumber.Length > 6)
                {
                    contactNumber = contactNumber.Insert(3, "-");
                    contactNumber = contactNumber.Insert(7, "-");
                }

                else if (contactNumber.Length > 3)
                {
                    contactNumber = contactNumber.Insert(3, "-");
                }

                //Maximum number of characters is 12 (10 Numbers + 2 hyphens)
                if (contactNumber.Length > 12)
                {
                    //Remove the characters beyond 12th Position
                    contactNumber = contactNumber.Remove(12);
                    DisplayAlert("Alert", "No of Characters Exceeded", "ok");
                }
            }

            contactNumberEntry.Text = contactNumber;

            BtnSubmit.IsEnabled = true;
        }

        public void ValidateNumericBehaviourAndLength(Entry entry)
        {
            int _limit = 4;
            int num;

            (entry).TextColor = Color.Black;
            BtnSubmit.IsEnabled = true;
            if (!string.IsNullOrEmpty(entry.Text) && !Int32.TryParse(entry.Text, out num))
            {
                (entry).TextColor = Color.Red;
                DisplayAlert("Alert", "Enter Only Numbers", "ok");
            }
            else if (entry.Text.Length > _limit)
            {
                entry.Text = entry.Text.Remove(entry.Text.Length - 1);
                DisplayAlert("Alert", "No of Characters Exceeded", "ok");
            }
        }

        public void ValidateRemarksLength(Editor entry)
        {
            int _limit = 255;

            (entry).TextColor = Color.Black;
            string text = entry.Text;
            BtnSubmit.IsEnabled = true;
            if (text.Length > _limit)
            {
                entry.Text = entry.Text.Remove(entry.Text.Length - 1);
                DisplayAlert("Alert", "No of Characters Exceeded", "ok");
            }
        }

        protected async void OnAbortButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }


        #region Tab Change Events

        protected void OnStep1TabClicked(object sender, EventArgs e)
        {
            //If Previous Tab is Step 4, then capture the Screenshot of the Map
            if (step4Container.IsVisible)
            {
                CaptureMapScreenshotAndAddToQueue();
            }
            ShowStep1();
        }

        protected void OnStep2TabClicked(object sender, EventArgs e)
        {
            //If Previous Tab is Step 4, then capture the Screenshot of the Map
            if (step4Container.IsVisible)
            {
                CaptureMapScreenshotAndAddToQueue();
            }
            ShowStep2();
        }

        protected async void OnStep3TabClicked(object sender, EventArgs e)
        {
            //If Previous Tab is Step 4, then capture the Screenshot of the Map
            if (step4Container.IsVisible)
            {
                CaptureMapScreenshotAndAddToQueue();
            }

            // From Windows desktop, he cannot upload Images.
            if (Device.OS == TargetPlatform.Windows && !_isJobAlreadyCompleted)
            {
                btnUploadImage.IsEnabled = false;
                DisplayAlert("Alert", "Upload Image is not Supported in your Device", "Close");
            }

            ShowStep3();
            if (Device.OS == TargetPlatform.Windows && !_isJobAlreadyCompleted)
                loc = await DeviceHardware.GetLocation();
        }

        protected void OnStep4TabClicked(object sender, EventArgs e)
        {
            //If Previous Tab is Step 4, then capture the Screenshot of the Map
            if (step4Container.IsVisible)
            {
                CaptureMapScreenshotAndAddToQueue();
            }
            ShowStep4();
        }

        protected void OnStep5TabClicked(object sender, EventArgs e)
        {
            //If Previous Tab is Step 4, then capture the Screenshot of the Map
            if (step4Container.IsVisible)
            {
                CaptureMapScreenshotAndAddToQueue();
            }
            ShowStep5();
        }

        protected void OnNextClicked(object sender, EventArgs e)
        {
            if (step1Container.IsVisible)
            {
                ShowStep2();
            }

            else if (step2Container.IsVisible)
            {
                ShowStep3();
            }

            else if (step3Container.IsVisible)
            {
                ShowStep4();
            }

            else if (step4Container.IsVisible)
            {
                //If Previous Tab is Step 4, then capture the Screenshot of the Map
                CaptureMapScreenshotAndAddToQueue();

                ShowStep5();
            }
        }
        protected void OnPreviousClicked(object sender, EventArgs e)
        {
            if (step5Container.IsVisible)
            {
                ShowStep4();
            }

            else if (step4Container.IsVisible)
            {
                //If Previous Tab is Step 4, then capture the Screenshot of the Map
                CaptureMapScreenshotAndAddToQueue();

                ShowStep3();
            }

            else if (step3Container.IsVisible)
            {
                ShowStep2();
            }

            else if (step2Container.IsVisible)
            {
                ShowStep1();
            }
        }

        protected void ShowStep1()
        {
            step1Container.IsVisible = btnNext.IsVisible = tabNavigationButtonsContainer.IsVisible = true;
            step2Container.IsVisible = step3Container.IsVisible = step4Container.IsVisible = step5Container.IsVisible = btnPrevious.IsVisible = false;

            highlightStep1.Style = (Style)Application.Current.Resources["completeJobTabHighlighted"];
            highlightStep2.Style = highlightStep3.Style =
                highlightStep4.Style = highlightStep5.Style = (Style)Application.Current.Resources["completeJobTabNonHighlighted"];

            tabStep1.Style = (Style)Application.Current.Resources["completeJobTabSelected"];
            tabStep2.Style = tabStep3.Style =
                tabStep4.Style = tabStep5.Style = (Style)Application.Current.Resources["completeJobTabNonSelected"];
        }

        protected void ShowStep2()
        {
            step2Container.IsVisible = btnPrevious.IsVisible = btnNext.IsVisible = tabNavigationButtonsContainer.IsVisible = true;
            step1Container.IsVisible = step3Container.IsVisible = step4Container.IsVisible = step5Container.IsVisible = false;

            highlightStep2.Style = (Style)Application.Current.Resources["completeJobTabHighlighted"];
            highlightStep1.Style = highlightStep3.Style =
                highlightStep4.Style = highlightStep5.Style = (Style)Application.Current.Resources["completeJobTabNonHighlighted"];

            tabStep2.Style = (Style)Application.Current.Resources["completeJobTabSelected"];
            tabStep1.Style = tabStep3.Style =
                tabStep4.Style = tabStep5.Style = (Style)Application.Current.Resources["completeJobTabNonSelected"];
        }

        protected void ShowStep3()
        {

            step3Container.IsVisible = btnPrevious.IsVisible = btnNext.IsVisible = tabNavigationButtonsContainer.IsVisible = true;
            step1Container.IsVisible = step2Container.IsVisible = step4Container.IsVisible = step5Container.IsVisible = false;

            highlightStep3.Style = (Style)Application.Current.Resources["completeJobTabHighlighted"];
            highlightStep1.Style = highlightStep2.Style =
                highlightStep4.Style = highlightStep5.Style = (Style)Application.Current.Resources["completeJobTabNonHighlighted"];

            tabStep3.Style = (Style)Application.Current.Resources["completeJobTabSelected"];
            tabStep1.Style = tabStep2.Style =
                tabStep4.Style = tabStep5.Style = (Style)Application.Current.Resources["completeJobTabNonSelected"];
        }

        protected void ShowStep4()
        {
            step4Container.IsVisible = btnPrevious.IsVisible = btnNext.IsVisible = true;
            step1Container.IsVisible = step2Container.IsVisible = step3Container.IsVisible = step5Container.IsVisible = tabNavigationButtonsContainer.IsVisible = false;

            highlightStep4.Style = (Style)Application.Current.Resources["completeJobTabHighlighted"];
            highlightStep1.Style = highlightStep2.Style =
                highlightStep3.Style = highlightStep5.Style = (Style)Application.Current.Resources["completeJobTabNonHighlighted"];

            tabStep4.Style = (Style)Application.Current.Resources["completeJobTabSelected"];
            tabStep1.Style = tabStep2.Style =
                tabStep3.Style = tabStep5.Style = (Style)Application.Current.Resources["completeJobTabNonSelected"];

            //Recenter the Map View in Step 4
            mapsViewModel.RecenterMapView();
        }

        protected void ShowStep5()
        {
            step5Container.IsVisible = btnPrevious.IsVisible = tabNavigationButtonsContainer.IsVisible = true;
            step1Container.IsVisible = step2Container.IsVisible =
                step3Container.IsVisible = step4Container.IsVisible = btnNext.IsVisible = false;

            highlightStep5.Style = (Style)Application.Current.Resources["completeJobTabHighlighted"];
            highlightStep1.Style = highlightStep2.Style =
                highlightStep3.Style = highlightStep4.Style = (Style)Application.Current.Resources["completeJobTabNonHighlighted"];

            tabStep5.Style = (Style)Application.Current.Resources["completeJobTabSelected"];
            tabStep1.Style = tabStep2.Style =
                tabStep3.Style = tabStep4.Style = (Style)Application.Current.Resources["completeJobTabNonSelected"];
        }

        #endregion Tab Change Events

        #region Hardware Back Button handling
        protected override bool OnBackButtonPressed()
        {
            if (!_isJobAlreadyCompleted)
            {
                ConfirmDiscardChanges();
            }
            return true;
        }

        protected async void ConfirmDiscardChanges()
        {
            var isConfirmed = await DisplayAlert("Discard Changes", "Are you sure that you want to leave without Submitting?", "Yes", "No");
            if (isConfirmed)
            {
                await Navigation.PopAsync(true);
            }
        }
        #endregion Hardware Back Button handling
    }
}
