﻿using BusinessLogic.VDWUILogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Connectivity.Plugin;

using Xamarin.Forms;
using mobileApp.DependencyServices;
using BusinessEntities.Models;
using System.ComponentModel;
using Plugin.DeviceInfo;
using System.Net;

namespace mobileApp.Views
{
    public partial class VdwLoginPage : ContentPage, INotifyPropertyChanged
    {
        public string Name { get; set; }
        private bool isLoading = false;
        public bool IsLoading
        {
            get
            {
                return isLoading;
            }
            set
            {
                if (isLoading != value)
                {
                    isLoading = value;
                    RaisePropertyChanged("IsLoading");//PropertyChanged(this, new PropertyChangedEventArgs(nameof(IsLoading)));
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public VdwLoginPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            if (ServiceLayer.Utils.AppConstants.IsOffeshoreUser == false) {
                loaderStackLayout.IsVisible = true;
                IsLoading = true;

                mainStackLayout.IsVisible = false;
                webViewStackLayout.IsVisible = false;
                webviewBrowser.Source = ServiceLayer.Utils.AppConstants.VzUrl;

                webviewBrowser.Navigated += webviewBrowser_Navigated;
                webviewBrowser.Navigating += webviewBrowser_Navigating;
            }
            else {
                IsLoading = false;
                loaderStackLayout.IsVisible = false;
                mainStackLayout.IsVisible = true;
                webViewStackLayout.IsVisible = false;
                usernameField.Focus();
            }
            BindingContext = this;
        }

        private void webviewBrowser_Navigating(object sender, CustomControls.CookieNavigationEventArgs args)
        {
            Debug.WriteLine("Navigating to: {0}", args.Url);
        }

        private void webviewBrowser_Navigated(object sender, CustomControls.CookieNavigatedEventArgs args)
        {
            Debug.WriteLine("Finished navigation to: {0}, Cookies: {1}", args.Url, args.Cookies.Count);
            ServiceLayer.Utils.AppConstants.cookieCollections = args.Cookies;
            foreach (Cookie item in ServiceLayer.Utils.AppConstants.cookieCollections)
            {
                if (item.Name == App.SessionName && item.Value != App.SessionLogOff)
                {
                    App.isAuthenticatedUser = true;
                    break;
                }
            }
            
            if (!App.isAuthenticatedUser)
            {
                webViewStackLayout.IsVisible = true;
                loaderStackLayout.IsVisible = false;
                IsLoading = false;
            }
            else
            {
                mainStackLayout.IsVisible = true;
                webViewStackLayout.IsVisible = false;
                loaderStackLayout.IsVisible = false;
                IsLoading = false;

                usernameField.Focus();
                BindingContext = this;
            }
        }

        private bool IsValidUser()
        {
            //Authentication should be done - Things to look into.
            if ((usernameField.Text.ToUpper().Equals("VTE01") || usernameField.Text.ToUpper().Equals("NEPVC")))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Called when the login button is clicked. Authenticates user with server and presents job summary screen.
        public async void login()
        {
            try
            {
                disableControls();
                App.IsUserLoggedIn = true;
                App.LoggedInUserId = usernameField.Text.ToUpper();
                App.LoggedInUserRegion = "CP";
                App.IsVendor = !IsTech();

                SetUpGlobalFlags();
                Device.OnPlatform(Android: () => DependencyService.Get<INotificationDependencyService>().RegisterForPushNotifications());

                UpdateDeviceInfo();
                DateTime currentDatetime = DateTime.Now;
                //List<JobEntity> _lstJobSummary = await JobSummaryBL.GetJobSummaryList(App.LoggedInUserId, App.LoggedInUserRegion);

                bool result = await VdwLocalDataAccess.CheckInternet(App.LoggedInUserId);
                if (!result)
                {
                    var isConfirmed = await DisplayAlert("Alert", "Currently your network is not available, Do you want to work offline?", "Proceed", "Cancel");
                    if (!isConfirmed)
                    {
                        enableControls();
                        return;
                    }
                }

                if (!App.IsVendor)
                {
                    await Navigation.PushAsync(new Views.TechJobSummaryPage());
                    enableControls();
                    return;
                }
                else
                {
                    await Navigation.PushAsync(new Views.VendorJobSummaryPage());
                    enableControls();
                    return;
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }
        private void enableControls()
        {
            IsLoading = false;
            loaderStackLayout.IsVisible = false;
            mainStackLayout.IsVisible = true;
            usernameField.IsEnabled = true;
            passwordField.IsEnabled = true;
            loginButton.IsEnabled = true;
        }
        private void disableControls()
        {
            IsLoading = true;
            loaderStackLayout.IsVisible = true;
            usernameField.IsEnabled = false;
            passwordField.IsEnabled = false;
            loginButton.IsEnabled = false;           
        }
        private bool IsTech()
        {
            //User Role Check should be done - Things to look into.
            if (usernameField.Text.Equals("VTE01"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private async void SetUpGlobalFlags()
        {
            //Read Global Flags from the DB
            App.GlobalFlags = await CommonBL.GetGlobalFlags();
        }

        private async void UpdateDeviceInfo()
        {
            // Waiting for Registration to Complete
            await Task.Delay(1000);

            DeviceEntity objDeviceInfo = new DeviceEntity();
            objDeviceInfo.DeviceId = Device.OnPlatform(CrossDeviceInfo.Current.Id,App.Imei , CrossDeviceInfo.Current.Id);
            objDeviceInfo.DeviceToken = App.deviceToken;
            objDeviceInfo.MobileOS = Device.OnPlatform("IOS", "ANDROID", "WINDOWS");
            objDeviceInfo.UserId = App.LoggedInUserId;
            await DeviceInfoBL.UpdateDeviceInfo(objDeviceInfo);
        }

        #region Button Click Events
        protected void OnLoginButtonClicked(object sender, EventArgs e)
        {
            if(IsValidUser())
                login();
            else
                DisplayAlert("Alert", "Wrong User ID", "Ok", "Cancel");
        }

        protected void OnPasswordFieldCompleted(object sender, EventArgs e)
        {
            loginButton.Focus();
        }

        protected void OnUsernameFieldCompleted(object sender, EventArgs e)
        {
            passwordField.Focus();
        }

        protected void OnForgotPasswordClicked(object sender, EventArgs e)
        {
            DisplayAlert("Not Implemented", "This feature is not Implemented", "OK");
        }

        #endregion

    }
}