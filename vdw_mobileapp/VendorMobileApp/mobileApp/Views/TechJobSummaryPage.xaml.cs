﻿using BusinessEntities;
using BusinessEntities.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ComponentModel;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace mobileApp.Views
{
    public partial class TechJobSummaryPage : ContentPage, INotifyPropertyChanged
    {
        private List<JobEntity> assignedJobs;
        private List<JobEntity> completedJobs;
        private string currentJobStatusToBeDisplayed;
        private bool _isRowEven;

        private bool isLoading = false;
        JobSummaryMapPage mapView;
        public bool IsLoading
        {
            get
            {
                return isLoading;
            }
            set
            {
                if (isLoading != value)
                {
                    isLoading = value;
                    RaisePropertyChanged("IsLoading");//PropertyChanged(this, new PropertyChangedEventArgs(nameof(IsLoading)));
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }


        public TechJobSummaryPage()
        {
            InitializeComponent();
            mapView = new JobSummaryMapPage();
            NavigationPage.SetHasNavigationBar(this, false);

            currentJobStatusToBeDisplayed = VdwConstants.JobAssignedStatus;
            GetJobs();

            if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.iOS)
            {
                toolbar.ShowRefreshIcon();
                var imgRefreshJobs = toolbar.GetRefreshJobsIcon();
                var grRefreshJobs = new TapGestureRecognizer();
                grRefreshJobs.Tapped += OnListViewRefreshing;
                imgRefreshJobs.GestureRecognizers.Add(grRefreshJobs);
            }

            loaderStackLayout.IsVisible = false;
            IsLoading = false;
            BindingContext = this;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Refresh the Toolbar, in case any change in the Notification count, etc
            toolbar.Refresh();
            App.CurrentToolbar = toolbar;
        }

        public void GetJobs()
        {
            var jobs = VdwLocalDataAccess.getJobListDB();

            if (jobs == null) return;

            assignedJobs = jobs.Where(j => j.JobStatus == VdwConstants.JobAssignedStatus).ToList();
            completedJobs = jobs.Where(j => j.JobStatus == VdwConstants.JobCompletedStatus).ToList();

            // Initialize MapPage with the Jobs. The MapPage will plot all these Jobs.
            if (jobs.Any())
            {
                mapView.InitializeMapPage(assignedJobs);
            }

            UpdateStack();
        }

        public void UpdateStack()
        {
            var oddJobViewStyle = (Style)Application.Current.Resources["oddJobCell"];
            var evenJobViewStyle = (Style)Application.Current.Resources["evenJobCell"];

            tabAssigned.Text = string.Format("Assigned ({0})", assignedJobs.Count());
            tabCompleted.Text = string.Format("Completed ({0})", completedJobs.Count());

            //Assigned
            if (currentJobStatusToBeDisplayed == VdwConstants.JobAssignedStatus)
            {
                lvJobSummary.ItemsSource = assignedJobs.OrderBy(j => j.PinCode).ThenBy(j => DateTime.ParseExact(j.CommitmentDate, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture));
            }
            //Completed
            else if (currentJobStatusToBeDisplayed == VdwConstants.JobCompletedStatus)
            {
                lvJobSummary.ItemsSource = completedJobs.OrderBy(j => j.PinCode).ThenBy(j => DateTime.ParseExact(j.CommitmentDate, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture));
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Logout();
            return true;
        }

        protected async void Logout()
        {
            var isConfirmed = await DisplayAlert("Log out", "Are you sure that you want to logout?", "Log out", "Cancel");
            if (isConfirmed)
            {
                await Navigation.PopToRootAsync(true);
            }
        }

        protected void OnListViewCellAppearing(object sender, EventArgs e)
        {
            var viewCell = (ViewCell)sender;
            if (viewCell.View != null)
            {
                viewCell.View.Style = (_isRowEven) ? (Style)Application.Current.Resources["evenJobCell"] : (Style)Application.Current.Resources["oddJobCell"];
            }

            _isRowEven = !_isRowEven;
        }

        protected async void OnListViewRefreshing(object sender, EventArgs e)
        {
            // If it is windows platform, then there is no pull to refresh, we are using refresh button on the toolbar. So we need to show Activity Indicator.
            if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.iOS)
            {
                IsLoading = true;
                loaderStackLayout.IsVisible = true;
            }

            bool result = await VdwLocalDataAccess.CheckInternet(App.LoggedInUserId);
            GetJobs();

            // Set the IsRefreshing Property of the ListView to false to let it know that refreshing is completed and the Loading Symbol disappears.
            lvJobSummary.IsRefreshing = false;

            if (Device.OS == TargetPlatform.Windows || Device.OS == TargetPlatform.iOS)
            {
                IsLoading = false;
                loaderStackLayout.IsVisible = false;
            }
        }

        protected void OnListViewJobSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }

            var selectedJob = (JobEntity)lvJobSummary.SelectedItem;
            ((ListView)lvJobSummary).SelectedItem = null;
            Navigation.PushAsync(new Views.JobDetailsPage(selectedJob));
            
            // Unselect the Item in the ListView.
            lvJobSummary.SelectedItem = null;
        }

        protected void OnJobSummaryListButtonClicked(object sender, EventArgs e)
        {
            jobSummaryMapContainer.Children.Clear();
            jobSummaryListContainer.IsVisible = true;
            jobSummaryMapContainer.IsVisible = false;
            highlightJobSummaryList.Style = (Style)Application.Current.Resources["jobSummaryTabHighlighted"];
            highlightJobSummaryMap.Style = (Style)Application.Current.Resources["jobSummaryTabNonHighlighted"];
            
        }

        protected void OnJobSummaryMapButtonClicked(object sender, EventArgs e)
        {
            
            mapView.IsVisible = true;
            jobSummaryListContainer.IsVisible = false;
            jobSummaryMapContainer.IsVisible = true;
            highlightJobSummaryList.Style = (Style)Application.Current.Resources["jobSummaryTabNonHighlighted"];
            highlightJobSummaryMap.Style = (Style)Application.Current.Resources["jobSummaryTabHighlighted"];

            mapView.RecenterMapView();
            jobSummaryMapContainer.Children.Add(mapView);
        }

        protected void OnAssignedButtonClicked(object sender, EventArgs e)
        {
            assignedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabHighlighted"];
            completedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabNonHighlighted"];
            currentJobStatusToBeDisplayed = VdwConstants.JobAssignedStatus;
            UpdateStack();
        }

        protected void OnCompletedButtonClicked(object sender, EventArgs e)
        {
            assignedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabNonHighlighted"];
            completedHighLight.Style = (Style)Application.Current.Resources["jobTypeTabHighlighted"];
            currentJobStatusToBeDisplayed = VdwConstants.JobCompletedStatus;
            UpdateStack();
        }

        protected void OnCustomerReachNumberTapped(object sender, EventArgs e)
        {
            if(sender is Label)
            {
                var reachNumber = ((Label)sender).Text;
                Device.OpenUri(new Uri(string.Format("tel:{0}", reachNumber)));
            }
            else if(sender is Image)
            {
                var reachNumber = ((Label)(((StackLayout)(((Image)sender).Parent)).Children[1])).Text;
                Device.OpenUri(new Uri(string.Format("tel:{0}", reachNumber)));
            }
        }
    }
}
