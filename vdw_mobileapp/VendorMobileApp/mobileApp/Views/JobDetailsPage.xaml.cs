﻿using BusinessEntities;
using BusinessEntities.Models;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace mobileApp.Views
{
    public partial class JobDetailsPage : ContentPage
    {
        private readonly JobEntity _job;

        public JobDetailsPage(JobEntity job)
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            #region Soft Back Button Handling
            var backIconTap = new TapGestureRecognizer();
            backIconTap.Tapped += (s, e) =>
            {
                Navigation.PopAsync();
            };
            imgBackIcon.GestureRecognizers.Add(backIconTap);
            lblBackIcon.GestureRecognizers.Add(backIconTap);
            #endregion Soft Back Button Handling

            _job = job;

            BindFieldValues();
            SetCustomerAddressAndPosition();
            btnAddRemarks.IsVisible = _job.JobStatus == VdwConstants.JobAssignedStatus && !App.IsVendor;

            btnCompleteJob.IsVisible = !App.IsVendor || (_job.JobStatus == VdwConstants.JobCompletedStatus);
            btnCompleteJob.Text = (_job.JobStatus == VdwConstants.JobCompletedStatus) ? "Completion Info" : "Complete Job";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Refresh the Toolbar, in case any change in the Notification count, etc
            toolbar.Refresh();
            App.CurrentToolbar = toolbar;
        }

        private void BindFieldValues()
        {
            lblJobId.Text = _job.JobId;
            lblMon.Text = _job.Mon;
            lblJobType.Text = _job.JobType;
            lblJobStatus.Text = _job.JobStatus;
            lblVendorId.Text = _job.VendorId;
            lblVdwDispatchDateTime.Text = _job.VdwDispatchDateTime != null ? _job.VdwDispatchDateTime.Value.ToString("MM/dd/yyyy HH:mm") : "";
            lblCommitmentDateTime.Text = _job.CommitmentDate;
            lblCustomerName.Text = _job.CustomerName;
            lblCustomerNumber.Text = _job.AltReachNumber;
            lblCustomerAddress.Text = _job.CustomerAddress;
        }

        private async void SetCustomerAddressAndPosition()
        {
            try
            {
                if (!Application.Current.Resources.ContainsKey("address"))
                {
                    Application.Current.Resources.Add("address", _job.CustomerAddress);
                }
                else
                {
                    Application.Current.Resources["address"] = _job.CustomerAddress;
                }

                var geoCoder = new Geocoder();
                var positions = await geoCoder.GetPositionsForAddressAsync(_job.CustomerAddress);
                var position = positions.LastOrDefault();

                if (!Application.Current.Resources.ContainsKey("position"))
                {
                    Application.Current.Resources.Add("position", position);
                }
                else
                {
                    Application.Current.Resources["position"] = position;
                }

                Debug.WriteLine("Position Latitude: {0}", position.Latitude);
                Debug.WriteLine("Position Longitude: {0}", position.Longitude);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception occurred while getting Customer Position");
                Debug.WriteLine(e.StackTrace);
            }
        }

        protected void OnAddRemarksButtonClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AddRemarksPage(_job));
        }

        protected void OnCompleteJobButtonClicked(object sender, EventArgs e)
        {
            JobCompletionEntity completeJobInfo;
            if (_job.JobStatus != VdwConstants.JobCompletedStatus)
            {
                completeJobInfo = new JobCompletionEntity
                {
                    DoOrderId = _job.DoOrderId,
                    JobId = _job.JobId,
                    Mon = _job.Mon,
                    CusstatRemarks = _job.CustomerRemarks,
                    VdwDispatchDateTime = _job.VdwDispatchDateTime,
                    TechId = _job.TechName,
                    CommitmentDate = _job.CommitmentDate
                };
            }
            else
            {
                completeJobInfo = VdwLocalDataAccess.GetJobCompletionInfoForJob(_job.JobId) ?? new JobCompletionEntity();
                completeJobInfo.Images = VdwLocalDataAccess.getImageListDB(_job.JobId);
            }

            Navigation.PushAsync(new JobCompletionPage(completeJobInfo, _job.JobStatus == VdwConstants.JobCompletedStatus));
        }

        protected void OnCustomerReachNumberTapped(object sender, EventArgs e)
        {
            var reachNumber = lblCustomerNumber.Text;
            Device.OpenUri(new Uri(string.Format("tel:{0}", reachNumber)));
        }

        protected void OnCustomerAddressTapped(object sender, EventArgs e)
        {
            var address = lblCustomerAddress.Text;

            switch (Device.OS)
            {
                case TargetPlatform.iOS:
                    Device.OpenUri(
                      new Uri(string.Format("http://maps.apple.com/?q={0}", WebUtility.UrlEncode(address))));
                    break;
                case TargetPlatform.Android:
                    Device.OpenUri(
                      new Uri(string.Format("geo:0,0?q={0}", WebUtility.UrlEncode(address))));
                    break;
                case TargetPlatform.Windows:
                case TargetPlatform.WinPhone:
                    Device.OpenUri(
                      new Uri(string.Format("bingmaps:?where={0}", Uri.EscapeDataString(address))));
                    break;
            }
        }
    }
}
