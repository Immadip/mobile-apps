//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace mobileApp.Views {
    using System;
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;
    
    
    public partial class VendorJobSummaryPage : global::Xamarin.Forms.ContentPage {
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::mobileApp.CustomControls.CustomToolbar toolbar;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button tabJobSummaryList;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button tabJobSummaryMap;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button highlightJobSummaryList;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button highlightJobSummaryMap;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.StackLayout jobSummaryListContainer;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button tabUnAssigned;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button tabAssigned;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button tabCompleted;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button unAssignedHighLight;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button assignedHighLight;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button completedHighLight;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ScrollView scrollView;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ListView LvJobDetail;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button btnCancel;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.Button btnAssigned;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.StackLayout jobSummaryMapContainer;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::mobileApp.Views.JobSummaryMapPage mapView;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.RelativeLayout loaderStackLayout;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private global::Xamarin.Forms.ActivityIndicator actIndicator;
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Forms.Build.Tasks.XamlG", "0.0.0.0")]
        private void InitializeComponent() {
            this.LoadFromXaml(typeof(VendorJobSummaryPage));
            toolbar = this.FindByName<global::mobileApp.CustomControls.CustomToolbar>("toolbar");
            tabJobSummaryList = this.FindByName<global::Xamarin.Forms.Button>("tabJobSummaryList");
            tabJobSummaryMap = this.FindByName<global::Xamarin.Forms.Button>("tabJobSummaryMap");
            highlightJobSummaryList = this.FindByName<global::Xamarin.Forms.Button>("highlightJobSummaryList");
            highlightJobSummaryMap = this.FindByName<global::Xamarin.Forms.Button>("highlightJobSummaryMap");
            jobSummaryListContainer = this.FindByName<global::Xamarin.Forms.StackLayout>("jobSummaryListContainer");
            tabUnAssigned = this.FindByName<global::Xamarin.Forms.Button>("tabUnAssigned");
            tabAssigned = this.FindByName<global::Xamarin.Forms.Button>("tabAssigned");
            tabCompleted = this.FindByName<global::Xamarin.Forms.Button>("tabCompleted");
            unAssignedHighLight = this.FindByName<global::Xamarin.Forms.Button>("unAssignedHighLight");
            assignedHighLight = this.FindByName<global::Xamarin.Forms.Button>("assignedHighLight");
            completedHighLight = this.FindByName<global::Xamarin.Forms.Button>("completedHighLight");
            scrollView = this.FindByName<global::Xamarin.Forms.ScrollView>("scrollView");
            LvJobDetail = this.FindByName<global::Xamarin.Forms.ListView>("LvJobDetail");
            btnCancel = this.FindByName<global::Xamarin.Forms.Button>("btnCancel");
            btnAssigned = this.FindByName<global::Xamarin.Forms.Button>("btnAssigned");
            jobSummaryMapContainer = this.FindByName<global::Xamarin.Forms.StackLayout>("jobSummaryMapContainer");
            mapView = this.FindByName<global::mobileApp.Views.JobSummaryMapPage>("mapView");
            loaderStackLayout = this.FindByName<global::Xamarin.Forms.RelativeLayout>("loaderStackLayout");
            actIndicator = this.FindByName<global::Xamarin.Forms.ActivityIndicator>("actIndicator");
        }
    }
}
