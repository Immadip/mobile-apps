﻿using BusinessEntities.Models;
using mobileApp.CustomControls;
using mobileApp.Views;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace mobileApp
{
    public class App : Application
    {
        public static string LoggedInUserId;
        public static string LoggedInUserRegion;
        public static bool IsVendor;
        public static string TechnicianCode;
        public static bool IsUserLoggedIn;
        public static bool IsInternet;
        public static CustomToolbar CurrentToolbar;
        public static List<GlobalFlagEntity> GlobalFlags;
        public static int ScreenWidth;
        public static int ScreenHeight;
        static VdwLocalDataAccess databaseInstance;
        public static string deviceToken;
        public static string Imei;
        public static bool isAuthenticatedUser = false;
        public static string strAuthenticatedToken = "";
        public static string SessionName = "SMSESSION";
        public static string SessionLogOff = "LOGGEDOFF";

        public static App CurrentApp;

        public static object Console { get; private set; }

        public App()
        {
            CurrentApp = this;

            // set up the styles dictionary
            InitializeStyles();

            // Initialize the Custom Toolbar
            CurrentToolbar = new CustomToolbar();

            // The root page of your application    
            MainPage = new NavigationPage(new VdwLoginPage());

            databaseInstance = new VdwLocalDataAccess();//Create DB and tables



        }
        public static int InsertInLogTable(Exception ex)
        {
            LoggingEntity objLoggingEntity = new LoggingEntity();
            if (App.IsVendor)
            {
                objLoggingEntity.TechId = App.LoggedInUserId;
                objLoggingEntity.VendorId = "";
            }
            else
            {
                objLoggingEntity.VendorId = App.LoggedInUserId;
                objLoggingEntity.TechId = "";
            }
            objLoggingEntity.LogMessage = ex.Message;
            objLoggingEntity.LogDescription = ex.StackTrace.ToString();
            DateTime todayDate = DateTime.Now;
            objLoggingEntity.LogCreatedDate = todayDate.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            objLoggingEntity.LastSyncedDate = todayDate.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            objLoggingEntity.LastModifiedDate = todayDate.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            objLoggingEntity.SyncRequired = true;
            return VdwLocalDataAccess.InsertLoggingEntityTable(objLoggingEntity);
        }

        /*
         * Sets up the resource dictionary that sets the global styles for the rest of the application
         */
        void InitializeStyles()
        {

            #region Colors
            var oddNotificationCellColor = Color.FromHex("#DDDDDD");
            var evenNotificationCellColor = Color.FromHex("#FEF0E8");
            #endregion Colors

            #region Buttons
            var smallButton = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Button.FontSizeProperty, Value = 18},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Button.HeightRequestProperty, Value = 40},
                    new Setter {Property = Button.WidthRequestProperty, Value = 120},
                }
            };

            var smallIncreaseWidthButton = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Button.FontSizeProperty, Value = 18},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Button.HeightRequestProperty, Value = 40},
                    new Setter {Property = Button.WidthRequestProperty, Value = 140},
                }
            };

            var samewidthButton = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Button.FontSizeProperty, Value = 18 },
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Button.FontAttributesProperty, Value = FontAttributes.Bold },
                    new Setter {Property = Button.WidthRequestProperty, Value = ScreenWidth * 0.5 },
                    new Setter {Property = Button.HeightRequestProperty, Value = 40 },
                    new Setter {Property = Button.BorderRadiusProperty, Value = 0 }
                }
            };

            var thinSmallButtonCenter = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Button.FontSizeProperty, Value = 12},
                    new Setter {Property = CustomFontEffect.FontSizeProperty, Value = 12},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Button.HeightRequestProperty, Value = 30},
                    new Setter {Property = Button.WidthRequestProperty, Value = 120},
                    new Setter {Property = Button.HorizontalOptionsProperty, Value = LayoutOptions.Center}
                }
            };

            var smallCancelButton = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Button.FontSizeProperty, Value = 18},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("#959595")},
                    new Setter {Property = Button.HeightRequestProperty, Value = 40},
                    new Setter {Property = Button.WidthRequestProperty, Value = 120},
                }
            };

            var mediumButtonCenter = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Button.FontSizeProperty, Value = 18},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Button.HeightRequestProperty, Value = 40},
                    new Setter {Property = Button.WidthRequestProperty, Value = 200},
                    new Setter {Property = Button.HorizontalOptionsProperty, Value = LayoutOptions.Center}
                }
            };

            var imageCategoryPanelActive = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Button.FontSizeProperty, Value = 12},
                    new Setter {Property = Button.HeightRequestProperty, Value = 32},
                    new Setter {Property = Button.WidthRequestProperty, Value = 400},
                    new Setter {Property = Button.HorizontalOptionsProperty, Value = LayoutOptions.Center},
                    new Setter {Property = Button.BorderRadiusProperty, Value = 0},
                }
            };

            var imageCategoryPanelInActive = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("FBC8B3")},
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("444444")},
                    new Setter {Property = Button.FontSizeProperty, Value = 12},
                    new Setter {Property = Button.HeightRequestProperty, Value = 32},
                    new Setter {Property = Button.WidthRequestProperty, Value = 400},
                    new Setter {Property = Button.HorizontalOptionsProperty, Value = LayoutOptions.Center},
                    new Setter {Property = Button.BorderRadiusProperty, Value = 0},
                }
            };

            var halfButtonActive = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Button.FontSizeProperty, Value = 18},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Button.HeightRequestProperty, Value = 40},
                    new Setter {Property = Button.WidthRequestProperty, Value = App.ScreenWidth * 0.475},
                }
            };

            var halfButtonInactive = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Button.FontSizeProperty, Value = 18},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Button.HeightRequestProperty, Value = 40},
                    new Setter {Property = Button.WidthRequestProperty, Value = App.ScreenWidth * 0.475},
                    new Setter {Property = Button.BorderWidthProperty, Value = 2},
                    new Setter {Property = Button.BorderColorProperty, Value = Color.FromHex("CD040B")}
                }
            };

            var tableSection = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property = Button.BackgroundColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Button.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Button.HeightRequestProperty, Value = App.ScreenHeight * 0.075},
                    new Setter {Property = Button.WidthRequestProperty, Value = App.ScreenWidth * 0.95},
                    new Setter {Property = Button.HorizontalOptionsProperty, Value = LayoutOptions.CenterAndExpand},
                    new Setter {Property = Button.FontAttributesProperty, Value = FontAttributes.Bold}
                }
            };
            #endregion Buttons

            #region Pages
            var backgroundColor = new Style(typeof(ContentPage))
            {
                Setters =
                {
                    new Setter {Property = ContentPage.BackgroundColorProperty, Value = Color.FromHex("EAEBE9")}
                }
            };

            var overlayPanel = new Style(typeof(Layout))
            {
                Setters =
                {
                    new Setter {Property = Layout.BackgroundColorProperty, Value = Color.FromHex("FFFFFF")}
                }
            };

            var horizontalSeparator = new Style(typeof(BoxView))
            {
                Setters =
                {
                    new Setter {Property = BoxView.ColorProperty, Value = Color.FromHex("999999")},
                    new Setter {Property = BoxView.HeightRequestProperty, Value = 1}
                }
            };

            var navigationPage = new Style(typeof(NavigationPage))
            {
                Setters =
                {
                    new Setter {Property = NavigationPage.BarBackgroundColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = NavigationPage.BarTextColorProperty, Value = Color.FromHex("FFFFFF")},
                }
            };

            var primaryNavigationBar = new Style(typeof(StackLayout))
            {
                Setters =
                {
                    new Setter {Property = StackLayout.BackgroundColorProperty, Value = Color.FromHex("CD040B")},
                }
            };

            var primaryNavigationBarNoInternet = new Style(typeof(StackLayout))
            {
                Setters =
                {
                    new Setter {Property = StackLayout.BackgroundColorProperty, Value = Color.FromHex("d7dbe2")},
                }
            };

            var primaryNavigationBarText = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Label.FontSizeProperty, Value = 15},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                }
            };

            var secondaryNavigationBar = new Style(typeof(StackLayout))
            {
                Setters =
                {
                    new Setter {Property = StackLayout.BackgroundColorProperty, Value = Color.FromHex("FEF0E8")},
                    new Setter {Property = StackLayout.HeightRequestProperty, Value = 40}
                }
            };

            var secondaryNavigationBarText = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("444444")},
                    new Setter {Property = Label.FontSizeProperty, Value = 20},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Label.FontAttributesProperty, Value = FontAttributes.Bold},
                    new Setter {Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Center}
                }
            };

            #endregion Pages

            #region Images
            var vdwLogoContainer = new Style(typeof(Layout<View>))
            {
                Setters =
                {
                    new Setter {Property = Layout<View>.HeightRequestProperty, Value = App.ScreenHeight / 2.5 },
                    new Setter { Property = Layout<View>.WidthRequestProperty, Value = App.ScreenWidth },
                }
            };

            var vdwLogo = new Style(typeof(Image))
            {
                Setters =
                {
                    new Setter {Property = Image.HeightRequestProperty, Value = App.ScreenHeight / 4 },
                    new Setter { Property = Image.WidthRequestProperty, Value = App.ScreenHeight / 4 },
                }
            };
            #endregion Images

            #region Fields

            var generalLabel = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("333333")},
                    new Setter {Property = Label.FontSizeProperty, Value = 16},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") }
                }
            };

			var noOfBoreLabel = new Style(typeof(Label))
			{
				Setters =
				{
					new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("333333")},
					new Setter {Property = Label.FontSizeProperty, Value = Device.OnPlatform(14,16,16)},
					new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
					new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") }
				}
			};

            var errorLabel = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("CD040B")},
                    new Setter {Property = Label.FontSizeProperty, Value = 16},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") }
                }
            };

            var disclaimerText = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("333333")},
                    new Setter {Property = Label.FontSizeProperty, Value = 16},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Label.VerticalTextAlignmentProperty, Value = TextAlignment.End},
                    new Setter {Property = Label.HorizontalTextAlignmentProperty, Value = TextAlignment.Center}
                }
            };

            var pageTitle = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("FFFFFF")},
                    new Setter {Property = Label.BackgroundColorProperty, Value = Color.FromHex("e74c3c")},
                    new Setter {Property = Label.FontSizeProperty, Value = 17},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Label.FontAttributesProperty, Value = FontAttributes.Bold},
                    new Setter {Property = Label.WidthRequestProperty, Value = ScreenWidth },
                    new Setter {Property = Label.HeightRequestProperty, Value = 35 }
                }
            };

            var fieldLabel = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("444444")},
                    new Setter {Property = Label.FontSizeProperty, Value = Device.OnPlatform(14,15,17)},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Center},
                    new Setter {Property = Label.WidthRequestProperty, Value = ScreenWidth * 0.45}
                }
            };

			var fieldCommentLabel = new Style(typeof(Label))
			{
				Setters =
				{
					new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("444444")},
					new Setter {Property = Label.FontSizeProperty, Value = Device.OnPlatform(14,15,17)},
					new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
					new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
					new Setter {Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Center},
					new Setter {Property = Label.HorizontalTextAlignmentProperty, Value = TextAlignment.Center},
					new Setter {Property = Label.WidthRequestProperty, Value = ScreenWidth * 0.45}
				}
			};

            var fieldValue = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("444444")},
                    new Setter {Property = Label.FontSizeProperty, Value = Device.OnPlatform(14,15,17)},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Label.VerticalOptionsProperty, Value = LayoutOptions.Center},
                    new Setter {Property = Label.WidthRequestProperty, Value = ScreenWidth * 0.45}
                }
            };

            var entryField = new Style(typeof(Entry))
            {
                Setters =
                {
                    new Setter {Property = Entry.TextColorProperty, Value = Color.FromHex("4E4E4E")},
                    new Setter {Property = Entry.FontSizeProperty, Value = Device.OnPlatform(14,15,17)},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Entry.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Entry.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                }
            };

            var loginEntryField = new Style(typeof(Entry))
            {
                Setters =
                {
                    new Setter {Property = Entry.TextColorProperty, Value = Color.FromHex("4E4E4E")},
                    new Setter {Property = Entry.FontSizeProperty, Value = Device.OnPlatform(14,15,17)},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Entry.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Entry.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                    new Setter {Property = Entry.WidthRequestProperty, Value = ScreenWidth / 1.25 }
                }
            };

            var editorField = new Style(typeof(Editor))
            {
                Setters =
                {
                    new Setter {Property = Editor.TextColorProperty, Value = Color.FromHex("4E4E4E")},
                    new Setter {Property = Editor.FontSizeProperty, Value = Device.OnPlatform(14,15,17)},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Editor.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property = Editor.VerticalOptionsProperty, Value = LayoutOptions.FillAndExpand},
                }
            };

            var datePickerField = new Style(typeof(CustomDatePicker))
            {
                Setters =
                {
                    new Setter {Property = CustomDatePicker.FontSizeProperty, Value = Device.OnPlatform(14,15,17)},
                    new Setter {Property = CustomDatePicker.FontFamilyProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = CustomDatePicker.VerticalOptionsProperty, Value = LayoutOptions.Center}
                }
            };

            var timePickerField = new Style(typeof(CustomTimePicker))
            {
                Setters =
                {
                    new Setter {Property = CustomTimePicker.FontSizeProperty, Value = Device.OnPlatform(14,15,17)},
                    new Setter {Property = CustomTimePicker.FontFamilyProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = CustomTimePicker.VerticalOptionsProperty, Value = LayoutOptions.Center}
                }
            };

            var pickerField = new Style(typeof(CustomPicker))
            {
                Setters =
                {
                    new Setter {Property = CustomPicker.FontSizeProperty, Value = Device.OnPlatform(14,15,17)},
                    new Setter {Property = CustomPicker.FontFamilyProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = CustomPicker.VerticalOptionsProperty, Value = LayoutOptions.Center}
                }
            };

            var switchField = new Style(typeof(Switch))
            {
                Setters =
                {
                    new Setter {Property = Switch.HorizontalOptionsProperty, Value = LayoutOptions.End},
                    new Setter {Property = Switch.VerticalOptionsProperty, Value = LayoutOptions.Center}
                }
            };

            var jobViewText = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property=Label.TextColorProperty, Value = Color.FromHex("444444") },
                    new Setter {Property = Label.FontSizeProperty, Value = Device.OnPlatform(14,17,17)},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") }
                }
            };

            var oddNotificationView = new Style(typeof(StackLayout))
            {
                Setters =
                {
                    new Setter {Property=StackLayout.BackgroundColorProperty, Value = Color.FromHex("DDDDDD") },
                }
            };

            var evenNotificationView = new Style(typeof(StackLayout))
            {
                Setters =
                {
                    new Setter {Property=StackLayout.BackgroundColorProperty, Value = Color.FromHex("FEF0E8") },
                    new Setter {Property=StackLayout.HeightRequestProperty, Value=200 }
                }
            };

            var oddJobCell = new Style(typeof(StackLayout))
            {
                Setters =
                {
                    new Setter {Property=StackLayout.BackgroundColorProperty, Value = Color.FromHex("FFFFFF") },
                }
            };

            var evenJobCell = new Style(typeof(StackLayout))
            {
                Setters =
                {
                    new Setter {Property=StackLayout.BackgroundColorProperty, Value = Color.FromHex("FEF0E8") },
                }
            };

            var jobCellText = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property=Label.TextColorProperty, Value = Color.FromHex("444444") },
                    new Setter {Property = Label.FontSizeProperty, Value = Device.OnPlatform(14,17,17)},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") }
                }
            };

            var searchHeaderBackground = new Style(typeof(StackLayout))
            {
                Setters =
                {
                    new Setter {Property=StackLayout.BackgroundColorProperty, Value = Color.FromHex("CD040B") },
                }
            };

            var searchHeaderText = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property=Label.TextColorProperty, Value = Color.FromHex("FFFFFF") },
                    new Setter {Property = Label.FontSizeProperty, Value = Device.OnPlatform(14,17,17)},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") }
                }
            };

            var searchBarText = new Style(typeof(SearchBar))
            {
                Setters =
                {
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = SearchBar.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") }
                }
            };

            var notificationViewText = new Style(typeof(Label))
            {
                Setters =
                {
                    new Setter {Property=Label.TextColorProperty, Value = Color.FromHex("444444") },
                    new Setter {Property = Label.FontSizeProperty, Value = Device.OnPlatform(14,17,17)},
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Label.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") }
                }
            };
            #endregion Fields

            #region Tab Highlighters
            var completeJobTabNonHighlighted = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("F9B295") },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                }
            };

            var completeJobTabHighlighted = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("CD040B") },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                }
            };

            var jobSummaryTabNonHighlighted = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("DDDDDD") },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                }
            };

            var jobSummaryTabHighlighted = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("666666") },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                }
            };

            var jobTypeTabNonHighlighted = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("FBC8B3") },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                }
            };

            var jobTypeTabHighlighted = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("CD040B") },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                }
            };
            #endregion Tab Highlighters

            #region Tabs
            var completeJobTabNonSelected = new Style(typeof(Button))
            {
                Setters =
            {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("FBC8B3") },
                    new Setter {Property=Button.TextColorProperty, Value = Color.FromHex("FFFFFF") },
                    new Setter {Property=Button.FontSizeProperty, Value = 14 },
                    new Setter {Property=Button.FontAttributesProperty, Value = FontAttributes.Bold },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                    }
            };

            var completeJobTabSelected = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("FBC8B3") },
                    new Setter {Property=Button.TextColorProperty, Value = Color.FromHex("444444") },
                    new Setter {Property=Button.FontSizeProperty, Value = 14 },
                    new Setter {Property=Button.FontAttributesProperty, Value = FontAttributes.Bold },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                }
            };

            var jobSummaryTab = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("EEEEEE") },
                    new Setter {Property=Button.TextColorProperty, Value = Color.FromHex("CD040B") },
                    new Setter {Property=Button.FontSizeProperty, Value = 18 },
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property=Button.FontAttributesProperty, Value = FontAttributes.Bold },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                }
            };

            var jobTypeTab = new Style(typeof(Button))
            {
                Setters =
                {
                    new Setter {Property=Button.BackgroundColorProperty, Value = Color.FromHex("FEF0E8") },
                    new Setter {Property=Button.TextColorProperty, Value = Color.FromHex("555555") },
                    new Setter {Property=Button.FontSizeProperty, Value = 18 },
                    new Setter {Property = CustomFontEffect.FontFileNameProperty, Value = Device.OnPlatform("HelveticaNeueLTStd-Cn", "HelveticaNeueLTStd-Cn", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue")},
                    new Setter {Property = Button.FontFamilyProperty, Value=Device.OnPlatform("HelveticaNeueLTStd-Cn", "", @"\Assets\Fonts\HelveticaNeue-LightCond#Helvetica Neue") },
                    new Setter {Property=Button.FontAttributesProperty, Value = FontAttributes.Bold },
                    new Setter {Property=Button.BorderRadiusProperty, Value = 0 }
                }
            };
            #endregion Tabs

            var activityIndicatorColor = new Style(typeof(ActivityIndicator))
            {
                Setters =
                {
					new Setter {Property=ActivityIndicator.ColorProperty, Value = Color.FromHex("CD040B") },
                }
            };

            //this creates a global ResourceDictionary, so Styles can be used in other screens
            Resources = new ResourceDictionary();

            Resources.Add("oddNotificationCellColor", oddNotificationCellColor);
            Resources.Add("evenNotificationCellColor", evenNotificationCellColor);

            Resources.Add("smallButton", smallButton);
            Resources.Add("smallIncreaseWidthButton", smallIncreaseWidthButton);            
            Resources.Add("samewidthButton", samewidthButton);
            Resources.Add("thinSmallButtonCenter", thinSmallButtonCenter);
            Resources.Add("smallCancelButton", smallCancelButton);
            Resources.Add("mediumButtonCenter", mediumButtonCenter);
            Resources.Add("imageCategoryPanelActive", imageCategoryPanelActive);
            Resources.Add("imageCategoryPanelInActive", imageCategoryPanelInActive);
            Resources.Add("halfButtonActive", halfButtonActive);
            Resources.Add("halfButtonInactive", halfButtonInactive);
            Resources.Add("tableSection", tableSection);

            Resources.Add("backgroundColor", backgroundColor);
            Resources.Add("overlayPanel", overlayPanel);
            Resources.Add("horizontalSeparator", horizontalSeparator);
            Resources.Add("navigationPage", navigationPage);
            Resources.Add("primaryNavigationBar", primaryNavigationBar);
            Resources.Add("primaryNavigationBarText", primaryNavigationBarText);
            Resources.Add("primaryNavigationBarNoInternet", primaryNavigationBarNoInternet);
            Resources.Add("secondaryNavigationBar", secondaryNavigationBar);
            Resources.Add("secondaryNavigationBarText", secondaryNavigationBarText);
            Resources.Add("searchBarText", searchBarText);
            Resources.Add("searchHeaderText", searchHeaderText);
            Resources.Add("searchHeaderBackground", searchHeaderBackground);

            Resources.Add("vdwLogoContainer", vdwLogoContainer);
            Resources.Add("vdwLogo", vdwLogo);

            Resources.Add("disclaimerText", disclaimerText);
            Resources.Add("generalLabel", generalLabel);
			Resources.Add("noOfBoreLabel", noOfBoreLabel);
			Resources.Add("errorLabel", errorLabel);
            Resources.Add("pageTitle", pageTitle);
            Resources.Add("fieldLabel", fieldLabel);
            Resources.Add("fieldValue", fieldValue);
			Resources.Add("fieldCommentLabel", fieldCommentLabel);
            Resources.Add("entryField", entryField);
            Resources.Add("loginEntryField", loginEntryField);
            Resources.Add("editorField", editorField);
            Resources.Add("datePickerField", datePickerField);
            Resources.Add("timePickerField", timePickerField);
            Resources.Add("pickerField", pickerField);
            Resources.Add("switchField", switchField);
            Resources.Add("jobViewText", jobViewText);
            Resources.Add("oddJobCell", oddJobCell);
            Resources.Add("evenJobCell", evenJobCell);
            Resources.Add("jobCellText", jobCellText);
            Resources.Add("notificationViewText", notificationViewText);

            Resources.Add("completeJobTabNonHighlighted", completeJobTabNonHighlighted);
            Resources.Add("completeJobTabHighlighted", completeJobTabHighlighted);
            Resources.Add("jobSummaryTabNonHighlighted", jobSummaryTabNonHighlighted);
            Resources.Add("jobSummaryTabHighlighted", jobSummaryTabHighlighted);
            Resources.Add("jobTypeTabNonHighlighted", jobTypeTabNonHighlighted);
            Resources.Add("jobTypeTabHighlighted", jobTypeTabHighlighted);

            Resources.Add("completeJobTabNonSelected", completeJobTabNonSelected);
            Resources.Add("completeJobTabSelected", completeJobTabSelected);
            Resources.Add("jobSummaryTab", jobSummaryTab);
            Resources.Add("jobTypeTab", jobTypeTab);

            Resources.Add("activityIndicatorColor", activityIndicatorColor);
        }
        protected override void OnStart()
        {
            // Handle when your app starts
            //getGPS();

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }


    }
}