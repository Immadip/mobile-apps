﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using mobileApp.Job_Summary;
using mobileApp;
using BusinessEntities;
using BusinessLogic;

using Xamarin.Forms;

namespace mobileApp.Job_Summary
{
    public partial class MapPage : Xamarin.Forms.Maps.Map
    {
        public List<CustomPin> CustomPins = new List<CustomPin>();

        Geocoder geoCoder = new Geocoder();

        private double lat = 0;
        private double lon = 0;

        public MapPage()
        {
            MapType = MapType.Street;
            WidthRequest = App.ScreenWidth;
            HeightRequest = App.ScreenHeight;

            //Create JobSummary objects to pin
            List<JobSummary> jobObjects = new List<JobSummary>();

            var job1 = new JobSummary()
            {
                CustomerName = "Kayla",
                JobID = "A1",
                JobType = "Buried",
                AltReachNumber = "555-555-5555",
                CustomerAddress = "220 Triphammer Rd, Ithaca, NY 14850"
            };

            var job2 = new JobSummary()
            {
                CustomerName = "Nick",
                JobID = "A2",
                JobType = "Underground",
                AltReachNumber = "777-777-7777",
                CustomerAddress = "Balch Hall, Ithaca, NY 14850"
            };

            var job3 = new JobSummary()
            {
                CustomerName = "Jeet",
                JobID = "A3",
                JobType = "Buried",
                AltReachNumber = "555-555-5555",
                CustomerAddress = "407 Eddy St, Ithaca, NY 14850",
            };

            var job4 = new JobSummary()
            {
                CustomerName = "Amanda",
                JobID = "A4",
                JobType = "Underground",
                AltReachNumber = "777-777-7777",
                CustomerAddress = "4 Forest Park Lane, Ithaca NY 14853",
            };

            var job5 = new JobSummary()
            {
                CustomerName = "Andy",
                JobID = "A5",
                JobType = "Buried",
                AltReachNumber = "555-555-5555",
                CustomerAddress = "Duffield Hall, Ithaca, NY 14850",
            };

            var job6 = new JobSummary()
            {
                CustomerName = "Shanaaz",
                JobID = "A6",
                JobType = "Underground",
                AltReachNumber = "777-777-7777",
                CustomerAddress = "6-44 Ho Plaza, Ithaca, NY 14850",
            };

            var job7 = new JobSummary()
            {
                CustomerName = "Harini",
                JobID = "A7",
                JobType = "Buried",
                AltReachNumber = "555-555-5555",
                CustomerAddress = "311 Eddy St, Ithaca, NY 14850",
            };

            jobObjects.Add(job1);
            jobObjects.Add(job2);
            jobObjects.Add(job3);
            jobObjects.Add(job4);
            jobObjects.Add(job5);
            jobObjects.Add(job6);
            jobObjects.Add(job7);

            //Create pins for each JobSummary
            int i = 0;
            foreach (JobSummary jsumm in jobObjects)
            {
                createPins(jsumm, i);
                i++;
            }
        }

        public async void createPins(JobSummary job, int y)
        {
            var tempCoord = await geoCoder.GetPositionsForAddressAsync(job.CustomerAddress);

            foreach (var x in tempCoord)
            {
                lat = x.Latitude;
                lon = x.Longitude;

                CustomPin cpin = new CustomPin()
                {
                    Pin = new Pin
                    {
                        Type = PinType.Place,
                        Position = new Position(lat, lon),
                        Label = job.JobID,
                        Address = job.CustomerAddress
                    },
                    Id = job.JobID,
                    Name = job.CustomerName,
                    Type = job.JobType,
                    Number = job.AltReachNumber,
                    Addr = job.CustomerAddress
                };

                CustomPins.Add(cpin);
                this.Pins.Add(cpin.Pin);


                if (y == 0)
                {
                    this.MoveToRegion(MapSpan.FromCenterAndRadius(
                      new Position(lat, lon), Distance.FromMiles(3)));
                }
            }

            return;
        }

    }
}
