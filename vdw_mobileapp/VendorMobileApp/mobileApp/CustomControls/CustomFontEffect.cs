﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace mobileApp.CustomControls
{
    public  class CustomFontEffect : DatePicker
    {
        public static readonly BindableProperty FontSizeProperty =
           BindableProperty.Create("FontSize", typeof(float), typeof(CustomFontEffect), 15f);

        public float FontSize
        {
            get { return (float)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }


        public static readonly BindableProperty FontFileNameProperty = BindableProperty.CreateAttached("FontFileName", typeof(string), typeof(CustomFontEffect), "", propertyChanged: OnFileNameChanged);
        //public static readonly BindableProperty FontSizeProperty = BindableProperty.CreateAttached("FontSize", typeof(string), typeof(CustomFontEffect), "", propertyChanged: OnFileNameChanged);
        public static string GetFontFileName(BindableObject view)
        {
            try
            {
                return (string)view.GetValue(FontFileNameProperty);
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
                return "";
            }
        }

         public string FontFamily
        {
            get { return (string)GetValue(FontFileNameProperty); }
            set { SetValue(FontFileNameProperty, value); }
        }

        public static void SetFontFileName(BindableObject view, string value)
        {
            view.SetValue(FontFileNameProperty, value);
        }

        public static float GetFontSize(BindableObject view)
        {
            return (float)view.GetValue(FontSizeProperty);
        }

        public static void SetFontSize(BindableObject view, float value)
        {
            view.SetValue(FontSizeProperty, value);
        }
        static void OnFileNameChanged(BindableObject bindable, object oldValue, object newValue)
        {
            try
            {
                var view = bindable as View;
                if (view == null)
                {
                    return;
                }
                view.Effects.Add(new FontEffect());
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }
        class FontEffect : RoutingEffect
        {
            public FontEffect() : base("Verizon.FontEffect")
            {
            }
        }

    }
}
