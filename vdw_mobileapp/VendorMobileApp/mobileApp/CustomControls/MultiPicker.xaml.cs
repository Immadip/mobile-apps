﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace mobileApp.CustomControls
{
    /// <summary>
    /// This generic class which use for any list for Multi select picker and user can select all and deselect all     
    /// </summary>
    public partial class MultiPicker : ContentPage
    {
        List<PickerItem> PickerItems;

        /// <summary>
        /// This constuctor of Multi select
        /// </summary>
        /// <param name="pickerItems">Picker items as List of Picker Item</param>
        /// <returns>void</returns>
        public MultiPicker(List<PickerItem> pickerItems)
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            #region Soft Back Button Handling
            var backIconTap = new TapGestureRecognizer();
            backIconTap.Tapped += (s, e) =>
            {
                Navigation.PopAsync();
            };
            imgBackIcon.GestureRecognizers.Add(backIconTap);
            lblBackIcon.GestureRecognizers.Add(backIconTap);
            #endregion Soft Back Button Handling

            PickerItems = pickerItems;
            lvMultiSelectOptions.ItemsSource = pickerItems;
        }

        /// <summary>
        /// This is abstract method when view appearing
        /// </summary>        
        /// <returns>void</returns>
        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Refresh the Toolbar, in case any change in the Notification count, etc
            toolbar.Refresh();
            App.CurrentToolbar = toolbar;
        }

        /// <summary>
        /// This function return selected items
        /// </summary>
        /// <param name="void">void</param>
        /// <returns>pickerItems</returns>
        public List<PickerItem> GetSelection()
        {
            return PickerItems.Where(item => item.IsSelected).ToList();
        }

        /// <summary>
        /// This function bind the control to view and model values
        /// </summary>
        /// <param name="void">void</param>
        /// <returns>void</returns>
        private void BindControls()
        {
            lvMultiSelectOptions.ItemsSource = PickerItems;
        }

        /// <summary>
        /// This function execute when list view item selected
        /// </summary>
        /// <param name="sender">object</param>
        /// /// <param name="SelectedItemChangedEventArgs">e as selected item argument</param>
        /// <returns>void</returns>
        protected void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null) return;
            var o = (PickerItem)e.SelectedItem;
            o.IsSelected = !o.IsSelected;
            ((ListView)sender).SelectedItem = null; //de-select
        }

        /// <summary>
        /// This function execute when user click on "Select All" button
        /// </summary>
        /// <param name="sender">object</param>
        /// /// <param name="SelectedItemChangedEventArgs">e as selected item argument</param>
        /// <returns>void</returns>
        protected void OnSelectAllClicked(object sender, EventArgs e)
        {
            foreach (var item in PickerItems)
            {
                item.IsSelected = true;
            }
        }

        /// <summary>
        /// This function execute when user click on "None" button
        /// </summary>
        /// <param name="sender">object</param>
        /// /// <param name="SelectedItemChangedEventArgs">e as selected item argument</param>
        /// <returns>void</returns>
        protected void OnSelectNoneClicked(object sender, EventArgs e)
        {
            foreach (var item in PickerItems)
            {
                item.IsSelected = false;
            }
        }
    }
    /// <summary>
    /// This class use for Picker Item entity  which consist Name and isSelected property    
    /// </summary>
    public class PickerItem : INotifyPropertyChanged
    {
        public string Name { get; set; }
        private bool isSelected = false;
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                if (isSelected != value)
                {
                    isSelected = value;
                    PropertyChanged(this, new PropertyChangedEventArgs(nameof(IsSelected)));
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
    }
}
