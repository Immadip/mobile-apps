﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services.Media;
using XLabs.Platform.Services.Geolocation;

namespace mobileApp.Helpers
{
    class DeviceHardware
    {
        static IMediaPicker _mediaPicker = Resolver.Resolve<IDevice>().MediaPicker;
        static IGeolocator locator = Resolver.Resolve<IGeolocator>();
        static private readonly TaskScheduler _scheduler = TaskScheduler.FromCurrentSynchronizationContext();

        public static bool LocationAvailable() {
            return locator.IsGeolocationAvailable && locator.IsGeolocationEnabled;
        }
        public static async Task<MediaFile> TakePicture()
        {
            if (_mediaPicker == null) { return null; }
            return await _mediaPicker.TakePhotoAsync(new CameraMediaStorageOptions
            {
                DefaultCamera = CameraDevice.Rear,
                MaxPixelDimension = 400
            }).ContinueWith(t => {
                if (t.IsFaulted)
                {
                }
                else if (t.IsCanceled)
                {
                }
                else
                {
                    var mediaFile = t.Result;
                    return mediaFile;
                }

                return null;
            }, _scheduler);
        }
        public static async Task<Position> GetLocation() {
            return await locator.GetPositionAsync(10000);
        }
    }
}
