﻿using System;
using Xamarin.Forms;
namespace mobileApp
{
    public class EmptyPage : Xamarin.Forms.ContentPage
    {
        public EmptyPage()
        {

            var label = new Label();
            label.Text = "Hello";
            var layout = new StackLayout { Padding = new Thickness(5, 10) };
            layout.Children.Add(label);
            this.Content = layout;
        }
        public EmptyPage(String page)
        {
            var label = new Label();
            label.Text = page;
            var layout = new StackLayout { Padding = new Thickness(5, 10) };
            layout.Children.Add(label);
            this.Content = layout;
        }
    }
}

