﻿using System.Collections.Generic;
using System.Linq;
using SQLite;
using Xamarin.Forms;
using System;
using BusinessEntities.Models;
using mobileApp.DependencyServices;

namespace mobileApp.SqlLiteDataAccess
{
    /// <summary>
    /// This class is to store notification detail in sqllite notification table
    /// </summary>  
    public class NotificationLocalDataAccess
    {
        static readonly object Locker = new object();

        readonly SQLiteConnection _sqLiteConnection;

        #region NotificationLocalDataAccess
        /// <summary>
        /// This constuctor of Notification Local Data Access class which intialize connection
        /// and create table NotificationEntity
        /// </summary>       
        public NotificationLocalDataAccess()
        {
            _sqLiteConnection = DependencyService.Get<ISQLiteDependencyService>().GetConnection();
            // create the table if it does not exists
            _sqLiteConnection.CreateTable<NotificationEntity>();
        }
        #endregion NotificationLocalDataAccess

        #region GetAllNotifications
        /// <summary>
        /// This function return list of notifications
        /// </summary>
        /// <param name="void">void</param>
        /// <returns>list of notification entity</returns>
        public IEnumerable<NotificationEntity> GetAllNotifications()
        {
            lock (Locker)
            {
                return _sqLiteConnection.Table<NotificationEntity>().ToList();
            }
        }
        #endregion GetAllNotifications

        #region GetNotificationsByUserId
        /// <summary>
        /// This function return list of  notifications by user id
        /// </summary>
        /// <param name="void">void</param>
        /// <returns>list of notification entity</returns>
        public IEnumerable<NotificationEntity> GetNotificationsByUserId(string userId)
        {
            lock (Locker)
            {
                return GetAllNotifications().Where(n => n.UserId == userId).ToList();
            }
        }
        #endregion GetNotificationsByUserId

        #region GetUnreadNotifications
        /// <summary>
        /// This function returns list of notifications which is unread
        /// </summary>
        /// <param name="void">void</param>
        /// <returns>list of notification entity</returns>
        public IEnumerable<NotificationEntity> GetUnreadNotifications()
        {
            lock (Locker)
            {
                return
                    _sqLiteConnection.Table<NotificationEntity>()
                        .Where(notification => notification.HasRead == false)
                        .ToList();
            }
        }
        #endregion GetUnreadNotifications


        #region GetNotification
        /// <summary>
        /// This function returns notification entity based on notification id
        /// </summary>
        /// <param name="notificationId">notificationId</param>
        /// <returns>list of notification entity</returns>
        public NotificationEntity GetNotification(int notificationId)
        {
            lock (Locker)
            {
                return _sqLiteConnection.Table<NotificationEntity>().FirstOrDefault(x => x.NotificationId == notificationId);
            }
        }
        #endregion GetNotification

        #region SaveNotification
        /// <summary>
        /// This function save notification entiy in sqllite table
        /// </summary>
        /// <param name="NotificationEntity">notification</param>
        /// <returns>int</returns>
        public int SaveNotification(NotificationEntity notification)
        {
            var existingNotificationIds = GetAllNotifications().Select(notif => notif.NotificationId);

            lock (Locker)
            {
                return (existingNotificationIds.ToLookup(notifId => notifId).Contains(notification.NotificationId))
                    ? _sqLiteConnection.Update(notification)
                    : _sqLiteConnection.Insert(notification);
            }
        }
        #endregion SaveNotification

        #region MarkAllNotificationsAsRead
        /// <summary>
        /// This function make as read once user read the message from tray
        /// </summary>
        /// <param name="void">void</param>
        /// <returns>void</returns>
        public void MarkAllNotificationsAsRead()
        {
            var unreadNotifications = GetUnreadNotifications();
            foreach (var notification in unreadNotifications)
            {
                notification.HasRead = true;
                SaveNotification(notification);
            }
        }
        #endregion MarkAllNotificationsAsRead
    }
}
