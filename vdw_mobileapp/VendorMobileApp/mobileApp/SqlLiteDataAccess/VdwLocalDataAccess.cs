﻿using System;
using SQLite;
using Xamarin.Forms;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.VDWUILogic;
using System.Globalization;
using Newtonsoft.Json;
using mobileApp.ViewModels;
using Connectivity.Plugin;
using System.Text;
using System.Diagnostics;
using Connectivity.Plugin.Abstractions;
using BusinessEntities.Models;
using mobileApp.DependencyServices;
using BusinessEntities;
using BusinessLogic;

namespace mobileApp
{
    /// <summary>
    /// This class is to get connection and create tables in sqllite databse
    /// </summary>  
    public class VdwLocalDataAccess
    {
        static object locker = new object();

        static SQLiteConnection database;

        event ConnectivityChangedEventHandler ConnectivityChanged;

        #region VdwLocalDataAccess
        /// <summary>
        /// Initializes a new instance of the <see cref="VdwLocalDataAccess"/> VdwLocalDataAccess. 
        /// if the database doesn't exist, it will create the database and all the tables.
        /// </summary>        
        public VdwLocalDataAccess()
        {

            try
            {

                database = DependencyService.Get<ISQLiteDependencyService>().GetConnection();

                // create the tables
                database.CreateTable<SyncEntity>();
                database.CreateTable<DropTypeEntity>();
                database.CreateTable<ImageCategoryEntity>();
                database.CreateTable<JeopardyCodeEntity>();
                database.CreateTable<NotificationEntity>();
                database.CreateTable<JobEntity>();
                database.CreateTable<JobCompletionEntity>();
                database.CreateTable<ImagesEntity>();
                database.CreateTable<LoggingEntity>();
                InsertSyncTable();
                CrossConnectivity.Current.ConnectivityChanged += new ConnectivityChangedEventHandler(ConnectivityChange);

            }
            catch (Exception)
            {
                //need to stored in logger
            }

        }
        #endregion VdwLocalDataAccess

        #region RefreshNotificationList
        /// <summary>
        /// This function refresh list of notification
        /// </summary>
        /// <param name="userId">userId as string</param>
        /// <returns>void</returns>
        public static async void RefreshNotificationList(string userId)
        {
            var result = await NotificationBL.GetNotificationList(userId);
            int result1 = database.DeleteAll<NotificationEntity>();
            VdwLocalDataAccess.SaveNotificationEntity(result);

        }
        #endregion RefreshNotificationList


        #region ConnectivityChange
        /// <summary>
        /// This function calls when user connectiviy change
        /// </summary>
        /// <param name="Object">s as Object</param>
        /// <param name="ConnectivityChangedEventArgs">a as Connectivity Changed Event argument</param>
        /// <returns>void</returns>
        protected async void ConnectivityChange(Object s, ConnectivityChangedEventArgs a)
        {
            bool result = await VdwLocalDataAccess.CheckInternet(App.LoggedInUserId);
        }
        #endregion ConnectivityChange


        #region InsertSyncTable
        /// <summary>
        /// This function insert in sync table for track data is sync or not
        /// </summary>
        /// <param name="void">void</param>        
        /// <returns>void</returns>
        public static void InsertSyncTable()
        {
            lock (locker)
            {
                DateTime today = DateTime.Now.AddDays(-365);

                var count = database.Table<SyncEntity>().Count();
                if (count < 1)
                {
                    SyncEntity objSyncTable = new SyncEntity();
                    objSyncTable.ID = 1;
                    objSyncTable.Name = "JobSummaryTable";
                    objSyncTable.IsSyncRequired = true;
                    objSyncTable.LastSyncDatetime = today.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                    database.Insert(objSyncTable);
                    SyncEntity objSTCompleteJob = new SyncEntity();
                    objSTCompleteJob.ID = 2;
                    objSTCompleteJob.Name = "CompleteJobEntityTable";
                    objSTCompleteJob.IsSyncRequired = true;
                    objSTCompleteJob.LastSyncDatetime = today.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                    database.Insert(objSTCompleteJob);
                    SyncEntity objSTLogging = new SyncEntity();
                    objSTLogging.ID = 3;
                    objSTLogging.Name = "LoggingEntityTable";
                    objSTLogging.IsSyncRequired = true;
                    objSTLogging.LastSyncDatetime = today.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                    database.Insert(objSTLogging);
                }
            }
        }
        #endregion InsertSyncTable

        #region GetItems
        /// <summary>
        /// This function retuns list of  image category 
        /// </summary>
        /// <param name="void">void</param>        
        /// <returns>list of ImageCategoryEntity</returns>
        public IEnumerable<ImageCategoryEntity> GetItems()
        {
            lock (locker)
            {
                return (from i in database.Table<ImageCategoryEntity>() select i).ToList();
            }
        }
        #endregion GetItems

        #region SaveImageCategory
        /// <summary>
        /// This function save image category in image category table
        /// </summary>
        /// <param name="List<ImageCategoryEntity>">items as List<ImageCategoryEntity></param>        
        /// <returns>int</returns>
        public static int SaveImageCategory(List<ImageCategoryEntity> items)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    database.Insert(item);
                }
                return 1;
            }
        }
        #endregion SaveImageCategory

        #region SaveDropType
        /// <summary>
        /// This function save drop type in  drop type table
        /// </summary>
        /// <param name="List<DropTypeEntity>">items as List<DropTypeEntity></param>        
        /// <returns>int</returns>
        public static int SaveDropType(List<DropTypeEntity> items)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    database.Insert(item);
                }
                return 1;
            }
        }
        #endregion SaveDropType

        #region SaveGeopardyCode
        /// <summary>
        /// This function save Jeopardy Code  in  Jeopardy Code table
        /// </summary>
        /// <param name="List<JeopardyCodeEntity>">items as List<JeopardyCodeEntity></param>        
        /// <returns>int</returns>
        public static int SaveGeopardyCode(List<JeopardyCodeEntity> items)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    database.Insert(item);
                }
                return 1;
            }
        }

        public static int SaveNotifications(List<NotificationEntity> items)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    database.Insert(item);
                }
                return 1;
            }
        }
        #endregion SaveGeopardyCode

        #region SaveLoggingEntityTable
        /// <summary>
        /// This function save Logging information in  Logging table for track crash and other issues
        /// </summary>
        /// <param name="List<LoggingEntity>">items as List<LoggingEntity></param>        
        /// <returns>int</returns>
        public static int SaveLoggingEntityTable(List<LoggingEntity> items)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    database.Insert(item);
                }
                return 1;
            }
        }
        #endregion SaveLoggingEntityTable

        #region SaveNotificationEntity
        /// <summary>
        /// This function save Notification information in  Notification table for track crash and other issues
        /// </summary>
        /// <param name="List<NotificationEntity>">items as List<NotificationEntity></param>        
        /// <returns>int</returns>
        public static int SaveNotificationEntity(List<NotificationEntity> items)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    database.Insert(item);
                }
                return 1;
            }
        }
        #endregion SaveNotificationEntity


        #region GetLastSyncDate
        /// <summary>
        /// This function return sync detail for specifitable
        /// </summary>
        /// <param name="TableName">TableName as Enumrable</param>        
        /// <returns>SyncEntity</returns>
        public static SyncEntity GetLastSyncDate(TableName tablename)
        {
            try
            {
                if (tablename.ToString() == "JobSummaryTable")
                    return database.Table<SyncEntity>().FirstOrDefault(x => x.Name == "JobSummaryTable");
                else if (tablename.ToString() == "CompleteJobEntityTable")
                    return database.Table<SyncEntity>().FirstOrDefault(x => x.Name == "CompleteJobEntityTable");
                else if (tablename.ToString() == "LoggingEntityTable")
                    return database.Table<SyncEntity>().FirstOrDefault(x => x.Name == "LoggingEntityTable");
                else
                    return null;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
                return null;
            }
        }
        #endregion GetLastSyncDate

        #region UpdateJobStatus
        /// <summary>
        /// This function update job status in job summary table
        /// </summary>
        /// <param name="JobEntity">JobEntity as Josummary entity detail</param>        
        /// <returns>int</returns>
        public static int UpdateJobStatus(JobEntity jobSummaryTable)
        {
            lock (locker)
            {
                database.Update(jobSummaryTable);
                return jobSummaryTable.ID;
            }
        }
        #endregion UpdateJobStatus

        #region UpdateLastSyncDate
        /// <summary>
        /// This function update sync information in sync table
        /// </summary>
        /// <param name="SyncEntity">SyncEntity as sync entity detail</param>        
        /// <returns>int</returns>
        public static int UpdateLastSyncDate(SyncEntity syncTable)
        {
            lock (locker)
            {
                database.Update(syncTable);
                return syncTable.ID;
            }
        }
        #endregion UpdateLastSyncDate

        #region UpdateCompleteTheJob
        /// <summary>
        /// This function update job completion information in complete job table
        /// </summary>
        /// <param name="JobCompletionEntity">JobCompletionEntity as job completion information</param>        
        /// <returns>int</returns>
        public static int UpdateCompleteTheJob(JobCompletionEntity completeJobEntityTable)
        {
            lock (locker)
            {
                database.Update(completeJobEntityTable);
                return completeJobEntityTable.ID;
            }
        }
        #endregion UpdateCompleteTheJob

        #region UpdateLoggingEntityTable
        /// <summary>
        /// This function update log  information in log table
        /// </summary>
        /// <param name="LoggingEntity">loggingEntityTable as log information</param>        
        /// <returns>int</returns>
        public static int UpdateLoggingEntityTable(LoggingEntity loggingEntityTable)
        {
            lock (locker)
            {
                database.Update(loggingEntityTable);
                return loggingEntityTable.ID;
            }
        }
        #endregion UpdateLoggingEntityTable

        #region InsertLoggingEntityTable
        /// <summary>
        /// This function insert log  information in log table
        /// </summary>
        /// <param name="LoggingEntity">loggingEntityTable as log information</param>        
        /// <returns>int</returns>
        public static int InsertLoggingEntityTable(LoggingEntity loggingEntityTable)
        {
            lock (locker)
            {
                database.Insert(loggingEntityTable);
                return loggingEntityTable.ID;
            }
        }
        #endregion InsertLoggingEntityTable

        #region GetDropTypesDB
        /// <summary>
        /// This function return list of log  information from log table
        /// </summary>
        /// <param name="void"></param>        
        /// <returns>list of DropTypeEntity</returns>
        public static List<DropTypeEntity> GetDropTypesDB()
        {
            lock (locker)
            {
                return (from i in database.Table<DropTypeEntity>() select i).ToList();
            }
        }
        #endregion GetDropTypesDB

        #region GetJeopardyCodesDB
        /// <summary>
        /// This function list of JeopardyCode from JeopardyCode table
        /// </summary>
        /// <param name="void"></param>        
        /// <returns>list of JeopardyCodeEntity</returns>
        public static List<JeopardyCodeEntity> GetGeopardyCodesDB()
        {
            lock (locker)
            {
                return (from i in database.Table<JeopardyCodeEntity>() select i).ToList();
            }
        }
        #endregion GetJeopardyCodesDB

        #region GetCategoriesDB
        /// <summary>
        /// This function list of image category  from  image category table
        /// </summary>
        /// <param name="void"></param>        
        /// <returns>list of ImageCategoryEntity</returns>
        public static List<ImageCategoryEntity> GetCategoriesDB()
        {
            lock (locker)
            {

                return (from i in database.Table<ImageCategoryEntity>() select i).ToList();
            }
        }
        #endregion GetCategoriesDB

        #region getJobListDB
        /// <summary>
        /// This function list of job  from  job table
        /// </summary>
        /// <param name="void"></param>        
        /// <returns>list of JobEntity</returns>
        public static List<JobEntity> getJobListDB()
        {
            lock (locker)
            {
                return (from i in database.Table<JobEntity>() select i).ToList();
            }
        }
        #endregion getJobListDB

        #region getImageListDB
        /// <summary>
        /// This function list of images  from  images table
        /// </summary>
        /// <param name="void"></param>        
        /// <returns>list of ImagesEntity</returns>
        public static List<ImagesEntity> getImageListDB(string jobID)
        {
            lock (locker)
            {
                return (from i in database.Table<ImagesEntity>() select i).Where(i => i.JobId == jobID).ToList();
            };

        }
        #endregion getImageListDB

        #region getJobSummary
        /// <summary>
        /// This function get job summary detail from job id
        /// </summary>
        /// <param name="string"></param>        
        /// <returns>JobEntity</returns>
        public static JobEntity getJobSummary(string jobID)
        {
            lock (locker)
            {
                return (from i in database.Table<JobEntity>() select i).Where(i => i.JobId == jobID).FirstOrDefault();
            }
        }
        #endregion getJobSummary

        #region getJobComplectionDB
        /// <summary>
        /// This function get job completion detail  which is sync in table
        /// </summary>
        /// <param name="void">void</param>        
        /// <returns>list of JobCompletionEntity</returns>
        public static List<JobCompletionEntity> getJobComplectionDB()
        {
            lock (locker)
            {
                return (from i in database.Table<JobCompletionEntity>() select i).Where(i => i.SyncRequired == true).ToList();
            }
        }
        #endregion getJobComplectionDB

        #region GetJobCompletionInfoForJob
        /// <summary>
        /// This function get job completion detail  based on job id
        /// </summary>
        /// <param name="jobId">jobId as string</param>        
        /// <returns>JobCompletionEntity</returns>
        public static JobCompletionEntity GetJobCompletionInfoForJob(string jobId)
        {
            lock (locker)
            {
                return (from i in database.Table<JobCompletionEntity>() select i).FirstOrDefault(i => i.JobId == jobId);
            }
        }
        #endregion GetJobCompletionInfoForJob

        #region getLoggingEntityDB
        /// <summary>
        /// This function list of log detail  
        /// </summary>
        /// <param name="void">void</param>        
        /// <returns>list of JobCompletionEntity</returns>
        public static List<LoggingEntity> getLoggingEntityDB()
        {
            lock (locker)
            {
                return (from i in database.Table<LoggingEntity>() select i).Where(i => i.SyncRequired == true).ToList();
            }
        }
        #endregion getLoggingEntityDB

        #region GetJobSummaryRecords
        /// <summary>
        /// This function get job summary record to save in sqllite databse 
        /// </summary>
        /// <param name="vendorId">vendorId as string </param>        
        /// <param name="regionId">regionId as string</param>        
        /// <param name="lastSyncDate">lastSyncDate as DateTime</param>        
        /// <returns>int</returns>
        public static async Task<int> GetJobSummaryRecords(string vendorId, string regionId, DateTime lastSyncDate)
        {
            int result = database.DeleteAll<JobEntity>();

            List<JobEntity> _lstJobSummary = await JobSummaryBL.GetJobSummaryList(vendorId, regionId, lastSyncDate);
            if (_lstJobSummary == null) _lstJobSummary = new List<JobEntity>();

            foreach (var jobSummary in _lstJobSummary)
            {
                DateTime today = DateTime.Today;
                jobSummary.LastSyncedDate = today.ToString("dd-MM-yyyy");
                jobSummary.LastModifiedDate = today.ToString("dd-MM-yyyy");
                jobSummary.SyncRequired = false;

            }
            result = SaveJobSummary(_lstJobSummary);
            return result;
        }
        #endregion GetJobSummaryRecords

        #region GetJobCompletionRecords
        /// <summary>
        /// This function get job completion record to save in sqllite databse 
        /// </summary>
        /// <param name="userId">userId as string </param>        
        /// <param name="userRole">userRole as string</param>        
        /// <param name="lastSyncDate">lastSyncDate as DateTime</param>        
        /// <returns>int</returns>
        public static async Task<int> GetJobCompletionRecords(string userId, UserRole userRole, string regionId, DateTime lastSyncDate)
        {
            int result = database.DeleteAll<JobCompletionEntity>();
            result = database.DeleteAll<ImagesEntity>();

            List<JobCompletionEntity> jobsCompletionInfo = await JobSummaryBL.GetJobsCompletionInfo(userId, userRole, regionId, lastSyncDate);
            if (jobsCompletionInfo == null) jobsCompletionInfo = new List<JobCompletionEntity>();

            foreach (var job in jobsCompletionInfo)
            {
                DateTime today = DateTime.Today;
                job.LastSyncedDate = today.ToString("dd-MM-yyyy");
                job.LastModifiedDate = today.ToString("dd-MM-yyyy");
                job.SyncRequired = false;

            }
            result = SaveCompleteJobs(jobsCompletionInfo);
            foreach (var jobCompInfo in jobsCompletionInfo)
            {
                if (jobCompInfo.Images != null)
                {
                    result = SaveImagesTable(jobCompInfo.Images);
                }
            }

            return result;
        }
        #endregion GetJobCompletionRecords

        #region GetJobCompletionRecords
        /// <summary>
        /// This function get job completion record to save in sqllite databse 
        /// </summary>
        /// <param name="userId">userId as string </param>        
        /// <param name="userRole">userRole as string</param>        
        /// <param name="lastSyncDate">lastSyncDate as DateTime</param>        
        /// <returns>int</returns>
        public static async Task<int> CompleteTheJob(JobCompletionEntity completeJobInfo, List<ImagesEntity> imagesToUpload, MapsViewModel mapsViewModel)
        {
            try
            {
                UploadImage(completeJobInfo);

                DateTime today = DateTime.Now;
                completeJobInfo.LastModifiedDate = today.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                completeJobInfo.SyncRequired = true;

                int objJobCompleted = VdwLocalDataAccess.SaveCompleteJob(completeJobInfo);

                if (objJobCompleted > 0)
                {
                    JobEntity objJobSummaryTable = getJobSummary(completeJobInfo.JobId);
                    objJobSummaryTable.JobStatus = VdwConstants.JobCompletedStatus;
                    UpdateJobStatus(objJobSummaryTable);

                }
                bool result = await VdwLocalDataAccess.CheckInternet(App.LoggedInUserId);

                return objJobCompleted;

            }

            catch (Exception)
            {
                return 0;//return false;
            }

        }
        #endregion GetJobCompletionRecords

        #region UploadImage
        /// <summary>
        /// This function save image detail in image tabel for upload image
        /// </summary>
        /// <param name="completejobinfo">completejobinfo as JobCompletionEntity </param>                
        /// <returns>void</returns>
        public static void UploadImage(JobCompletionEntity completejobinfo)
        {
            foreach (var image in completejobinfo.Images)
            {
                DateTime todayDate = DateTime.Today;
                image.LastSyncedDate = todayDate.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                image.LastModifiedDate = todayDate.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                image.CreatedDate = todayDate.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                image.SyncRequired = false;
            }
            if (completejobinfo.Images.Any())
            {
                int objImagesTable = VdwLocalDataAccess.SaveImagesTable(completejobinfo.Images);
            }

        }
        #endregion UploadImage

        #region CheckJobIsCompleted
        /// <summary>
        /// This function check wheather job is completed or not
        /// </summary>
        /// <param name="orderID">orderID as String </param>                
        /// <returns>bool</returns>
        public static async Task<bool> CheckJobIsCompleted(String orderID)
        {
            string jobStatus = await JobActionsBL.GetJobStatus(orderID);

            if (jobStatus == VdwConstants.JobCompletedStatus)
                return true;
            else
                return false;
        }
        #endregion CheckJobIsCompleted

        #region SaveJobSummary
        /// <summary>
        /// This function save job summary in job table
        /// </summary>
        /// <param name="items>">items as List<JobEntity>  </param>                
        /// <returns>int</returns>
        public static int SaveJobSummary(List<JobEntity> items)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    database.Insert(item);
                }
                return 1;
            }
        }
        #endregion SaveJobSummary

        #region SaveCompleteJob
        /// <summary>
        /// This function save complted job  in job completion table
        /// </summary>
        /// <param name="items>">items as JobCompletionEntity  </param>                
        /// <returns>int</returns>
        public static int SaveCompleteJob(JobCompletionEntity item)
        {
            lock (locker)
            {
                return database.Insert(item);
            }
        }
        #endregion SaveCompleteJob

        #region SaveCompleteJobs
        /// <summary>
        /// This function save list of job completion in table
        /// </summary>
        /// <param name="items>">items as List<JobCompletionEntity>  </param>                
        /// <returns>int</returns>
        public static int SaveCompleteJobs(List<JobCompletionEntity> items)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    database.Insert(item);
                }
                return 1;
            }
        }
        #endregion SaveCompleteJobs

        #region SaveImagesTable
        /// <summary>
        /// This function save image detail in image table
        /// </summary>
        /// <param name="items>">items as List<ImagesEntity>  </param>                
        /// <returns>int</returns>
        public static int SaveImagesTable(List<ImagesEntity> items)
        {
            lock (locker)
            {
                foreach (var item in items)
                {
                    database.Insert(item);
                }
                return 1;
            }
        }
        #endregion SaveImagesTable

        #region SyncMasterData
        /// <summary>
        /// This function sync all master tabel like categories,droptype
        /// </summary>
        /// <param name="vendorID>">vendorID as string  </param>                
        /// <returns>int</returns>
        public static async Task<int> SyncMasterData(string vendorID)
        {
            int result = 0;
            result = await GetCategories();
            result = await GetDropType();
            result = await GetJeopardyCode();
            result = await GetNotificationByUser(App.LoggedInUserId);
            DateTime currentDatetime = DateTime.Now;
            SyncEntity objSyncTable = GetLastSyncDate(TableName.JobSummaryTable);
            result = await GetJobSummaryRecords(vendorID, "CP", DateTime.ParseExact(objSyncTable.LastSyncDatetime, "yyyy-MM-ddTHH:mm:ss.fff", CultureInfo.InvariantCulture));
            objSyncTable.LastSyncDatetime = currentDatetime.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            objSyncTable.IsSyncRequired = false;
            if (result > 0)
            {
                UpdateLastSyncDate(objSyncTable);
            }

            objSyncTable = GetLastSyncDate(TableName.CompleteJobEntityTable);
            result = await GetJobCompletionRecords(vendorID, App.IsVendor ? UserRole.Vendor : UserRole.Technician, App.LoggedInUserRegion, DateTime.ParseExact(objSyncTable.LastSyncDatetime, "yyyy-MM-ddTHH:mm:ss.fff", CultureInfo.InvariantCulture));
            objSyncTable.LastSyncDatetime = currentDatetime.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            objSyncTable.IsSyncRequired = false;
            if (result > 0)
            {
                UpdateLastSyncDate(objSyncTable);
            }

            return result;
        }
        #endregion SyncMasterData

        #region SyncTransactionData
        /// <summary>
        /// This function sync all transaction tabel like job comletion,job summary
        /// </summary>
        /// <param name="void>">void  </param>                
        /// <returns>int</returns>
        public static async Task<int> SyncTransactionData()
        {
            int result = 0;
            bool isConnected = CrossConnectivity.Current.IsConnected;
            if (isConnected)//check for interenet 
            {
                bool isRechable = true; // await CrossConnectivity.Current.IsRemoteReachable(ServiceLayer.Constants.RestServiceHostName);//service url
                if (isRechable)//check  for internet & host reachability
                {
                    result = await SyncJobComplection();
                }
            }

            return result;

        }
        #endregion SyncTransactionData

        #region CheckInternet
        /// <summary>
        /// This function checks whether internet is available or not
        /// </summary>
        /// <param name="userId>">userId as string  </param>                
        /// <returns>bool</returns>
        public static async Task<bool> CheckInternet(string userId)
        {
            bool isConnected = CrossConnectivity.Current.IsConnected;
            if (isConnected)//check for interenet
            {
                //bool isRechable = await CrossConnectivity.Current.IsRemoteReachable(ServiceLayer.Constants.RestServiceHostName);//service url
                bool isRechable = await AuthenticationBL.CheckInternet();//service url
                if (isRechable)//check  for internet & host reachability
                {
                    App.IsInternet = true;
                    int result = await VdwLocalDataAccess.SyncTransactionData();
                    result = await VdwLocalDataAccess.SyncMasterData(userId);// await DisplayAlert("Alert", "Server  is available", "OK");
                    result = await VdwLocalDataAccess.SyncLoggingTable();
                }
                else
                {
                    App.IsInternet = false;
                }
            }
            else
            {
                App.IsInternet = false;
            }

            //To indicate the internet availability on toolbar.
            App.CurrentToolbar.SetToolbarStyleBasedOnInternetAvailability();

            return App.IsInternet;
        }
        #endregion CheckInternet

        #region SyncJobComplection
        /// <summary>
        /// This function sync job completion table 
        /// </summary>
        /// <param name="void>">void </param>                
        /// <returns>int</returns>
        public static async Task<int> SyncJobComplection()
        {
            try
            {
                List<JobCompletionEntity> _lstJobSummary = getJobComplectionDB();//remove await
                                                                                 //List<JobSummary> _lstJobSummary = await GetJobSummary.GetJobSummaryList(vendorId, regionId);
                List<JobCompletionEntity> completeJobEntityTableList = _lstJobSummary;
                if (completeJobEntityTableList == null) completeJobEntityTableList = new List<JobCompletionEntity>();

                foreach (var completeJobEntityTable in completeJobEntityTableList)
                {
                    //Upload Image
                    List<ImagesEntity> _objImagesTable = getImageListDB(completeJobEntityTable.JobId);
                    List<ImagesEntity> ImagesTableList = _objImagesTable;
                    if (ImagesTableList == null) ImagesTableList = new List<ImagesEntity>();

                    bool result = await CheckJobIsCompleted(completeJobEntityTable.DoOrderId);
                    if (!result)
                    {
                        completeJobEntityTable.Images = ImagesTableList;
                        bool objJobCompleted = await JobActionsBL.CompleteTheJob(completeJobEntityTable);

                        if (objJobCompleted)
                        {
                            database.Delete(_objImagesTable);
                            completeJobEntityTable.SyncRequired = false;
                            UpdateCompleteTheJob(completeJobEntityTable);
                            DateTime currentDatetime = DateTime.Now;
                            SyncEntity objSyncTable = GetLastSyncDate(TableName.CompleteJobEntityTable);
                            objSyncTable.LastSyncDatetime = currentDatetime.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                            objSyncTable.IsSyncRequired = false;
                            UpdateLastSyncDate(objSyncTable);
                        }
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
                Debug.WriteLine(ex.StackTrace);
                return 0;
            }
        }
        #endregion SyncJobComplection

        #region SyncLoggingTable
        /// <summary>
        /// This function sync log table 
        /// </summary>
        /// <param name="void>">void </param>                
        /// <returns>int</returns>
        public static async Task<int> SyncLoggingTable()
        {
            try
            {
                List<LoggingEntity> _lstLoggingEntity = getLoggingEntityDB();
                List<LoggingEntity> loggingEntityTableList = _lstLoggingEntity;
                if (loggingEntityTableList == null) loggingEntityTableList = new List<LoggingEntity>();

                foreach (var loggingEntityTable in loggingEntityTableList)
                {
                    bool objLoggingEntity = await LoggingInfoBL.InsertLoggingInfo(loggingEntityTable);
                    if (objLoggingEntity)
                    {
                        loggingEntityTable.SyncRequired = false;
                        UpdateLoggingEntityTable(loggingEntityTable);
                        DateTime currentDatetime = DateTime.Now;
                        SyncEntity objSyncTable = GetLastSyncDate(TableName.LoggingEntityTable);
                        objSyncTable.LastSyncDatetime = currentDatetime.ToString("yyyy-MM-ddTHH:mm:ss.fff");
                        objSyncTable.IsSyncRequired = false;
                        UpdateLastSyncDate(objSyncTable);
                    }
                }

                return 1;
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
                Debug.WriteLine(ex.StackTrace);
                return 0;
            }
        }
        #endregion SyncLoggingTable

        #region GetCategories
        /// <summary>
        /// This function first delete old entries.get categories and then save into  image category table
        /// </summary>
        /// <param name="void>">void </param>                
        /// <returns>int</returns>
        public static async Task<int> GetCategories()
        {
            int result = database.DeleteAll<ImageCategoryEntity>();
            List<ImageCategoryEntity> _lstcategories = new List<ImageCategoryEntity>();
            _lstcategories = await JobActionsBL.GetCategories();
            List<ImageCategoryEntity> categoryList = _lstcategories;
            if (categoryList == null) categoryList = new List<ImageCategoryEntity>();
            foreach (var category in categoryList)
            {
                category.ActiveStatus = true;
            }
            return SaveImageCategory(_lstcategories);
        }
        #endregion GetCategories

        #region GetDropType
        /// <summary>
        /// This function first delete old entries.get DropType from server and then save into  DropType table
        /// </summary>
        /// <param name="void>">void </param>                
        /// <returns>int</returns>
        public static async Task<int> GetDropType()
        {
            int result = database.DeleteAll<DropTypeEntity>();
            List<DropTypeEntity> _lstDropTypes = await JobActionsBL.GetDropTypes();
            List<DropTypeEntity> dropList = _lstDropTypes;
            if (_lstDropTypes == null) dropList = new List<DropTypeEntity>();
            foreach (var drop in dropList)
            {
                drop.ActiveStatus = true;
            }
            return SaveDropType(_lstDropTypes);
        }
        #endregion GetDropType

        #region GetJeopardyCode
        /// <summary>
        /// This function first delete old entries.get JeopardyCode from server and then save into  JeopardyCode table
        /// </summary>
        /// <param name="void>">void </param>                
        /// <returns>int</returns>
        public static async Task<int> GetJeopardyCode()
        {
            int result = database.DeleteAll<JeopardyCodeEntity>();
            List<JeopardyCodeEntity> _lstGeopardyCodeTable = new List<JeopardyCodeEntity>();
            List<JeopardyCodeEntity> _lstgeoppardycodes = await JobActionsBL.GetJeopardyCodes();
            foreach (var item in _lstgeoppardycodes)
            {
                JeopardyCodeEntity objDropType = new JeopardyCodeEntity();
                objDropType.JwmCode = item.JwmCode;
                objDropType.ActiveStatus = true;
                _lstGeopardyCodeTable.Add(objDropType);
            }
            return SaveGeopardyCode(_lstGeopardyCodeTable);
        }

        public static async Task<int> GetNotificationByUser(string UserID)
        {
            int result = database.DeleteAll<NotificationEntity>();
            List<NotificationEntity> _lstGeopardyCodeTable = new List<NotificationEntity>();
            List<NotificationEntity> _lstgeoppardycodes = await NotificationBL.GetNotificationList(UserID);
            foreach (var item in _lstgeoppardycodes)
            {
                NotificationEntity objDropType = new NotificationEntity();
                objDropType.NotificationId = item.NotificationId;
                objDropType.NotificationText = item.NotificationText;
                objDropType.NotificationTitle = item.NotificationTitle;
                objDropType.NotificationType = item.NotificationType;
                objDropType.NotificationReceivedTime = item.NotificationReceivedTime;
                objDropType.UserId = item.UserId;
                _lstGeopardyCodeTable.Add(objDropType);
            }
            return SaveNotifications(_lstGeopardyCodeTable);
        }
        #endregion GetJeopardyCode

        #region TableName
        /// <summary>
        /// This enum of table names
        /// </summary>

        public enum TableName { ImageCategoryTable, MissCodeTable, NotificationTable, JobSummaryTable, CompleteJobEntityTable, ImagesTable, LoggingEntityTable };
        #endregion TableName
    }

}