﻿using SQLite;

namespace mobileApp.DependencyServices
{
    public interface ISQLiteDependencyService
    {
        SQLiteConnection GetConnection();
    }
}
