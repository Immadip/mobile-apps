﻿namespace mobileApp.DependencyServices
{
    public interface INotificationDependencyService
    {
        void RegisterForPushNotifications();
        void SetLauncherIconBadgeCount(int notificationCount);
        void IncrementLauncherIconBadgeCount(int incrementByValue);
    }
}
