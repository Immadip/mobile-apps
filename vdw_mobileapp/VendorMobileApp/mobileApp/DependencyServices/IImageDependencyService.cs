﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mobileApp.DependencyServices
{
    public interface IImageDependencyService
    {
        byte[] ResizeImage(byte[] sourceImageBytes, string fileFormat, float maxWidth, float maxHeight);
    }
}
