﻿using Newtonsoft.Json;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using TK.CustomMap;
using TK.CustomMap.Interfaces;
using TK.CustomMap.Overlays;
using Xamarin.Forms;
using Xamarin.Forms.Maps;



namespace mobileApp.ViewModels
{
    public class MapsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public IRendererFunctions MapFunctions { get; set; }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private ObservableCollection<TKCustomMapPin> pins;
        private ObservableCollection<TKPolyline> lines;
        private TKCustomMapPin _selectedPin;
        private MapSpan mapRegion;
        private Command cmd;
        private bool _inactive = true;
        Geocoder geoCoder = new Geocoder();

        public MapsViewModel()
        {
            this.pins = new ObservableCollection<TKCustomMapPin>();
            this.lines = new ObservableCollection<TKPolyline>();

            cmd = new Command(RecenterMapView);

        }

        public bool Inactive
        {
            get
            {
                return _inactive;
            }
            set
            {
                if (_inactive != value)
                {
                    _inactive = value;
                    cmd.ChangeCanExecute();
                    this.OnPropertyChanged("Inactive");
                }
            }
        }

        /// <summary>
        /// Map region bound to <see cref="TKCustomMap"/>
        /// </summary>
        public MapSpan MapRegion
        {
            get { return this.mapRegion; }
            set
            {
                if (this.mapRegion != value)
                {
                    this.mapRegion = value;
                    this.OnPropertyChanged("MapRegion");
                }
            }
        }

        /// <summary>
        /// Selected pin bound to the <see cref="TKCustomMap"/>
        /// </summary>
        public TKCustomMapPin SelectedPin
        {
            get { return this._selectedPin; }
            set
            {
                if (this._selectedPin != value)
                {
                    this._selectedPin = value;
                    this.OnPropertyChanged("SelectedPin");
                }
            }
        }

        public async void RecenterMapView()
        {
            try
            {
                if (pins.Any())
                {
                    this.MapFunctions.FitMapRegionToPositions(pins.Select(p => p.Position), true);
                }
                else
                {
                    double lat = 0, lon = 0;
                    var address = (string)Application.Current.Resources["address"];
                    Debug.WriteLine(address);
                    var p = await geoCoder.GetPositionsForAddressAsync(address);
                    foreach (var x in p)
                    {
                        lat = x.Latitude;
                        lon = x.Longitude;
                    }
                    var position = new Position(lat, lon);
                    if (Application.Current.Resources.ContainsKey("position"))
                    {
                        Application.Current.Resources["position"] = position;
                    }
                    else
                    {
                        Application.Current.Resources.Add("position", position);
                    }
                    Debug.WriteLine("VendorMobileApp: RecenterMapView: Position Latitude: {0}", lat);
                    Debug.WriteLine("VendorMobileApp: RecenterMapView: Position Longitude: {0}", lon);

                    this.MapFunctions.MoveToMapRegion(MapSpan.FromCenterAndRadius(position, Distance.FromMeters(100)), true);
                }
            }
            catch (Exception ex)
            {
                App.InsertInLogTable(ex);
            }
        }

        /// <summary>
        /// Pins bound to the <see cref="TKCustomMap"/>
        /// </summary>
        public ObservableCollection<TKCustomMapPin> Pins
        {
            get { return this.pins; }
            set
            {
                if (this.pins != value)
                {
                    this.pins = value;
                    this.OnPropertyChanged("Pins");
                }
            }
        }


        /// <summary>
        /// Lines bound to the <see cref="TKCustomMap"/>
        /// </summary>
        public ObservableCollection<TKPolyline> Lines
        {
            get { return this.lines; }
            set
            {
                if (this.lines != value)
                {
                    this.lines = value;
                    this.OnPropertyChanged("Lines");
                }
            }
        }

        public Command ClearMapCommand
        {
            get
            {
                return new Command(() =>
                {
                    this.pins.Clear();
                    this.lines.Clear();
                });
            }
        }

        /// <summary>
        /// Map Long Press bound to the <see cref="TKCustomMap"/>
        /// </summary>
        public Command<Position> MapLongPressCommand
        {
            get
            {
                return new Command<Position>(async position =>
                {
                    var action = await Application.Current.MainPage.DisplayActionSheet(
                        "",
                        "Cancel",
                        null,
                        "Add Fiber", "Add Hub", "Add Home");

                    switch (action)
                    {
                        case "Add Hub":
                            addPin("hub", position);
                            break;
                        case "Add Fiber":
                            addPin("fiber", position);
                            break;
                        case "Add Home":
                            addPin("home", position);
                            break;
                        default:
                            break;
                    }
                });
            }
        }


        /// <summary>
        /// Pin Selected bound to the <see cref="TKCustomMap"/>
        /// </summary>
        /// 
        public Command<TKCustomMapPin> PinSelectedCommand
        {
            get
            {
                return new Command<TKCustomMapPin>(async (TKCustomMapPin mp) =>
                {
                    var p = this.SelectedPin.Position;
                    // Chose one
                    // 1. First possibility
                    //this.MapCenter = this.SelectedPin.Position;
                    // 2. Possibility
                    //this.MapRegion = MapSpan.FromCenterAndRadius(p, this.MapRegion.Radius);
                    // 3. Possibility
                    this.MapFunctions.MoveToMapRegion(
                        MapSpan.FromCenterAndRadius(p, Distance.FromMeters(this.MapRegion.Radius.Meters)),
                        true);
                    var title = this.SelectedPin.Title;
                    string action = "";
                    switch (title)
                    {
                        case "fiber":
                            action = await Application.Current.MainPage.DisplayActionSheet(
                                                "Select an Option",
                                                "Cancel", null, "Change to Hub", "Change to Home", "Delete");
                            break;
                        case "hub":
                            action = await Application.Current.MainPage.DisplayActionSheet(
                                                "Select an Option",
                                                "Cancel", null, "Change to Fiber", "Change to Home", "Delete");
                            break;
                        case "home":
                            action = await Application.Current.MainPage.DisplayActionSheet(
                                                "Select an Option",
                                                "Cancel", null, "Change to Fiber", "Change to Hub", "Delete");
                            break;
                        default:
                            break;
                    }


                    switch (action)
                    {
                        case "Delete":
                            pins.Remove(this.SelectedPin);
                            redrawLines();
                            break;
                        case "Change to Fiber":
                            this.SelectedPin.Title = "fiber";
                            this.SelectedPin.Image = ImageSource.FromFile("fiber.png");
                            redrawLines();
                            break;
                        case "Change to Hub":
                            this.SelectedPin.Title = "hub";
                            this.SelectedPin.Image = ImageSource.FromFile("hub.png");
                            redrawLines();
                            break;
                        case "Change to Home":
                            this.SelectedPin.Title = "home";
                            this.SelectedPin.Image = ImageSource.FromFile("house.png");
                            redrawLines();
                            break;
                        default:
                            break;
                    }
                    this.SelectedPin = null;
                    redrawLines();
                });
            }
        }


        /// <summary>
        /// Drag End bound to the <see cref="TKCustomMap"/>
        /// </summary>
        public Command<TKCustomMapPin> DragEndCommand
        {
            get
            {
                return new Command<TKCustomMapPin>(pin =>
                {
                    redrawLines();
                });
            }
        }

        public void drawLine(ObservableCollection<TKCustomMapPin> pinP)
        {
            var lat1 = pinP[pinP.Count - 1].Position.Latitude;
            var long1 = pinP[pinP.Count - 1].Position.Longitude;

            var lat2 = pinP[pinP.Count - 2].Position.Latitude;
            var long2 = pinP[pinP.Count - 2].Position.Longitude;
            var material = pinP[pinP.Count - 2].Title;


            var line = new TKPolyline
            {
                Color = Color.Aqua,
                LineWidth = 5f,
                LineCoordinates = new List<Position>(new Position[]
                    {
                            new Position(lat1, long1),
                            new Position(lat2, long2)
                    })
            };
            this.lines.Add(line);
        }




        public Command Recenter
        {
            get
            {
                return cmd;
            }
        }


        public void addPin(String arg, Position position)
        {
            string source = "";
            switch (arg)
            {

                case "home":
                    source = "house.png";
                    break;
                case "fiber":
                    source = "fiber.png";
                    break;
                case "hub":
                    source = "hub.png";
                    break;
                default:
                    break;
            }
            var pin = new TKCustomMapPin
            {
                Position = position,
                Title = string.Format(arg),
                Subtitle = string.Format("{0}, {1}", position.Latitude, position.Longitude),
                Anchor = new Point(0.5f, 0.5f),
                Image = ImageSource.FromFile(source),
                ShowCallout = false,
                IsDraggable = true
            };
            this.pins.Add(pin);

            if (pins.Count > 1)
            {
                drawLine(pins);
            }
        }


        public void redrawLines()
        {
            lines.Clear();
            if (pins.Count > 1)
            {
                for (int i = 1; i < pins.Count; i++)
                {
                    var lat1 = pins[i - 1].Position.Latitude;
                    var long1 = pins[i - 1].Position.Longitude;
                    var material = pins[i - 1].Title;

                    var lat2 = pins[i].Position.Latitude;
                    var long2 = pins[i].Position.Longitude;

                    var line = new TKPolyline
                    {
                        Color = Color.Aqua,
                        LineWidth = 5f,
                        LineCoordinates = new List<Position>(new Position[]
                            {
                                        new Position(lat1, long1),
                                        new Position(lat2, long2)
                            })
                    };

                    this.lines.Add(line);
                }
            }
        }
    }
}
