﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using BusinessEntities.Models;
using ServiceLayer.Utils;
using System.Net;

namespace ServiceLayer
{
    public class ServiceProxy
    {
        public static async Task<bool> AssignJob(JobEntity assignJob)
        {

            try
            {
                string relativeUrl = "api/VDWRest/AssignJob";
                var completionJobDetails = JsonConvert.SerializeObject(assignJob);

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);

                    var stringContent = new StringContent(completionJobDetails, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(Constants.RestBaseAddress + relativeUrl, stringContent);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();

                        bool objJobCompleted = JsonConvert.DeserializeObject<bool>(carJsonString);
                        return objJobCompleted;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static async Task<List<JobEntity>> GetJobAssignList(string VendorId, string RegionId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetAssignJobList?";
                relativeUrl = relativeUrl + "vendorId=" + VendorId + "&regionId=" + RegionId;

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);
                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        List<JobEntity> objJobSummary = JsonConvert.DeserializeObject<List<JobEntity>>(carJsonString);
                        return objJobSummary;
                    }
                    return new List<JobEntity>();
                }
            }
            catch (Exception ex)
            {
                return new List<JobEntity>();
            }
        }

        public static async Task<HttpResponseMessage> GetCookiesOld(string relativeUrl)
        {
            try
            {
                string strURL = "https://cofeetechsda-c2.ebiz.verizon.com/cofeetechsdasit/VendorMobileServices/";

                HttpClientHandler handler = new HttpClientHandler();
                var count = ServiceLayer.Utils.AppConstants.cookieCollections.Count;
                foreach (Cookie cookie in ServiceLayer.Utils.AppConstants.cookieCollections)
                {
                    handler.CookieContainer.Add(new Uri(strURL + relativeUrl), new Cookie(cookie.Name, cookie.Value, cookie.Path));
                }
                var cookiescon = new CookieCollection();
                using (var client = new HttpClient(handler))
                {
                    client.BaseAddress = new Uri(strURL);//Constants.RestBaseAddress);

                    var response = await client.GetAsync(strURL + relativeUrl);

                    if (response.IsSuccessStatusCode && ServiceLayer.Utils.AppConstants.cookieCollections == null)
                    {
                        //string headerCode = response.Headers.GetValues("Set-Cookie").FirstOrDefault();
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                        {
                            foreach (var c in cookies)
                            {
                                var cookiePairs = c.Split(';');

                                var cookievalue = cookiePairs[0].Split('=')[1];
                                var cookipath = cookiePairs[1].Split('=')[1];

                                cookiescon.Add(new Cookie { Name = "SMSESSION", Value = cookievalue, Path = cookipath });
                            }
                        }

                        var cookiCount = cookiescon.Count;
                        ServiceLayer.Utils.AppConstants.cookieCollections = cookiescon;
                        return response;
                    }

                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void SetCookiesOld(HttpResponseMessage response)
        {
            var cookies = new CookieCollection();
            string headerCode = response.Headers.GetValues("Set-Cookie").FirstOrDefault();

            var cookiePairs = headerCode.Split(';');

            var cookievalue = cookiePairs[0].Split('=')[1];
            var cookipath = cookiePairs[1].Split('=')[1];

            cookies.Add(new Cookie { Name = "SMSESSION", Value = cookievalue, Path = cookipath });
            var cookiCount = cookies.Count;
            ServiceLayer.Utils.AppConstants.cookieCollections = cookies;
        }

        public static async Task<List<JobEntity>> GetJobSummaryList(string VendorId, string RegionId, DateTime LastSyncDate)
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetJobSummary?";
                relativeUrl = relativeUrl + "vendorId=" + VendorId + "&regionId=" + RegionId;

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string contentJsonString = await response.Content.ReadAsStringAsync();
                        List<JobEntity> objJobSummary = JsonConvert.DeserializeObject<List<JobEntity>>(contentJsonString);
                        return objJobSummary;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<int> GetNotificationCount(string deviceId, string userId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetNotificationCount?";

                relativeUrl = relativeUrl + "deviceID=" + deviceId + "&userID=" + userId;

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            IEnumerable<string> cookies;
                            if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                                SetCookies(cookies);

                            string carJsonString = await response.Content.ReadAsStringAsync();

                            int objJobCompleted = JsonConvert.DeserializeObject<int>(carJsonString);
                            return objJobCompleted;
                        }
                        return 0;
                    }

                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static async Task<bool> SetNotificationCount(string deviceId, string userId, int notificationCount)
        {
            try
            {
                NotificationCount objNotificationCount = new NotificationCount();
                objNotificationCount.DeviceToken = deviceId;
                objNotificationCount.UserId = userId;
                objNotificationCount.NoofNotification = notificationCount;
                string relativeUrl = "api/VDWRest/SetNotificationCount";

                var completionJobDetails = JsonConvert.SerializeObject(objNotificationCount);

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);

                    var stringContent = new StringContent(completionJobDetails, Encoding.UTF8, "application/json");
                    using (var response = await client.PostAsync(relativeUrl, stringContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            IEnumerable<string> cookies;
                            if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                                SetCookies(cookies);
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            bool objResult = JsonConvert.DeserializeObject<bool>(carJsonString);
                            return objResult;
                        }
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static async Task<List<JobEntity>> GetJobSummaryList(string VendorId, string RegionId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetJobSummary?";
                relativeUrl = relativeUrl + "vendorId=" + VendorId + "&regionId=" + RegionId;

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);

                    using (var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            IEnumerable<string> cookies;
                            if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                                SetCookies(cookies);

                            string contentJsonString = await response.Content.ReadAsStringAsync();
                            List<JobEntity> objJobSummary = JsonConvert.DeserializeObject<List<JobEntity>>(contentJsonString);
                            return objJobSummary;
                        }
                        return new List<JobEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<JobEntity>();
            }
        }

        public static async Task<List<JobCompletionEntity>> GetJobsCompletionInfo(string userId, UserRole userRole, string regionId, DateTime lastSyncDate)
        {
            //var json = "[{\"JobId\":null,\"Mon\":null,\"DoOrderId\":null,\"TechId\":null,\"VdwDispatchDateTime\":null,\"CommitmentDate\":null,\"DispatchTime\":null,\"CableLocateNum\":\"2816454\",\"CusstatRemarks\":null,\"CompletionRemarks\":\"Finished DWO:10:50 11/26/2016  **COMPLETE CMP:TRIP CHARGE:N BY USER: CMP:PULL THROUGH LENGTH:115 BY USER: CMP:PRE CUT LENGTH:250 BY USER: T-  CUSTOMER CONTACT: #ARRIVAL DATE 12/01/2016# #ARRIVAL TIME 06:00# DROP LENGTH:141; NUM BORES:1; TOTAL BORE LENGTH:25\",\"ArrivalDate\":\"12/01/2016\",\"ArrivalTime\":\"06:00\",\"ClearDate\":\"12/01/2016\",\"ClearTime\":\"12:00\",\"EnggRemarks\":\"\",\"ContactName\":\"\",\"Contact\":\"\",\"NoofBores\":\"1\",\"BoreTypeNumber\":\"1\",\"BoreTypeLength\":\"25\",\"BoreType\":\"Road/Driveway\",\"TotBoreLength\":\"25\",\"DirectBuriedLength\":null,\"PullthroughLength\":\"115\",\"DropConduitLength\":\"141\",\"PreCutLength\":\"250\",\"DropType\":\"BURIED\",\"JeoPardyCode\":\"\",\"JeopardyDate\":\"\",\"JeopardyTime\":\"\",\"DidCompletejob\":null,\"DidTerminateDrop\":\"N\",\"ReferToEngg\":\"N\",\"TripCharge\":\"N\",\"DidplaceanONT\":\"N\",\"ONTLocationRemarks\":\"\",\"ONTLocInOut\":\"\",\"MapPolygonJson\":\"[]\",\"Images\":null}]";
            //List<JobCompletionEntity> dummy = JsonConvert.DeserializeObject<List<JobCompletionEntity>>(json);
            //return dummy;

            try
            {
                string relativeUrl = "api/VDWRest/GetJobsCompletionInfo?";
                relativeUrl = relativeUrl + string.Format("userId={0}&userRole={1}&regionId={2}", userId, userRole, regionId);

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();

                        List<JobCompletionEntity> jobsCompletionInfo = JsonConvert.DeserializeObject<List<JobCompletionEntity>>(carJsonString);

                        return jobsCompletionInfo;

                    }

                }
                return new List<JobCompletionEntity>();
            }
            catch (Exception ex)
            {
                return new List<JobCompletionEntity>();
            }
        }

        public static async Task<string> GetJobStatus(string OrderId)
        {

            try
            {
                string relativeUrl = "api/VDWRest/GetJobStatus?";
                relativeUrl = relativeUrl + "doOrderId=" + OrderId;

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        return carJsonString.Replace("\"", "");
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static HttpClientHandler GetCookies(string relativeUrl)
        {
            try
            {
                HttpClientHandler handler = new HttpClientHandler();
                var count = ServiceLayer.Utils.AppConstants.cookieCollections.Count;
                foreach (Cookie cookie in ServiceLayer.Utils.AppConstants.cookieCollections)
                {
                    handler.CookieContainer.Add(new Uri(Constants.RestBaseAddress + relativeUrl), new Cookie(cookie.Name, cookie.Value, cookie.Path));
                }

                return handler;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void SetCookies(IEnumerable<string> cookieString)
        {
            try
            {
                var cookiesSet = new CookieCollection();
                foreach (var c in cookieString)
                {
                    var cookiePairs = c.Split(';');
                    var cookiename = cookiePairs[0].Split('=')[0];
                    var cookievalue = cookiePairs[0].Split('=')[1];

                    if (cookiename == AppConstants.SessionName && cookievalue != AppConstants.SessionLogOff)
                    {
                        var cookipath = cookiePairs[1].Split('=')[1];
                        cookiesSet.Add(new Cookie { Name = cookiename, Value = cookievalue, Path = cookipath });
                    }
                }
                if (cookiesSet.Count > 0)
                    ServiceLayer.Utils.AppConstants.cookieCollections = cookiesSet;
            }
            catch (Exception ex)
            {

            }
        }

        public static async Task<List<DropTypeEntity>> GetDropTypes()
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetDropTypes";

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);
                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        List<DropTypeEntity> objDropType = JsonConvert.DeserializeObject<List<DropTypeEntity>>(carJsonString);
                        return objDropType.OrderBy(d => d.FiberType).ToList();
                    }
                }
                return new List<DropTypeEntity>();
            }
            catch (Exception ex)
            {
                return new List<DropTypeEntity>();
            }
        }

        public static async Task<List<JeopardyCodeEntity>> GetJeopardyCodes()
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetJeopardyCodes";

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();

                        List<JeopardyCodeEntity> objJeocodes = JsonConvert.DeserializeObject<List<JeopardyCodeEntity>>(carJsonString);
                        return objJeocodes.OrderBy(j => j.JwmCode).ToList();
                    }
                    return new List<JeopardyCodeEntity>();

                }
            }
            catch (Exception ex)
            {
                return new List<JeopardyCodeEntity>();
            }
        }

        public static async Task<List<ImageCategoryEntity>> GetCategories()
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetCategories";

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        List<ImageCategoryEntity> objcategories = JsonConvert.DeserializeObject<List<ImageCategoryEntity>>(carJsonString);
                        return objcategories.OrderBy(c => c.CategoryDescription).ToList();
                    }
                    return new List<ImageCategoryEntity>();

                }
            }
            catch (Exception ex)
            {
                return new List<ImageCategoryEntity>();
            }
            //$$$$
            //try
            //{
            //    string relativeUrl = "api/VDWRest/GetCategories";

            //    var response = await GetCookies(relativeUrl);
            //    if (response.IsSuccessStatusCode)
            //    {
            //        string carJsonString = await response.Content.ReadAsStringAsync();
            //        List<ImageCategoryEntity> objcategories = JsonConvert.DeserializeObject<List<ImageCategoryEntity>>(carJsonString);

            //        return objcategories.OrderBy(c => c.CategoryDescription).ToList();

            //    }
            //    return new List<ImageCategoryEntity>();
            //}
            //catch (Exception ex)
            //{
            //    return new List<ImageCategoryEntity>();
            //}

        }

        public static async Task<List<GlobalFlagEntity>> GetGlobalFlags()
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetGlobalFlags";

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        List<GlobalFlagEntity> globalFlags = JsonConvert.DeserializeObject<List<GlobalFlagEntity>>(carJsonString);
                        return globalFlags;

                    }
                    return new List<GlobalFlagEntity>();

                }
            }
            catch (Exception ex)
            {
                return new List<GlobalFlagEntity>();
            }
        }

        public static async Task<bool> UpdateDeviceInfo(DeviceEntity deviceInfo)
        {

            try
            {
                string relativeUrl = "api/VDWRest/DeviceInfo";
                var completionJobDetails = JsonConvert.SerializeObject(deviceInfo);

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);

                    var stringContent = new StringContent(completionJobDetails, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(Constants.RestBaseAddress + relativeUrl, stringContent);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        bool objJobCompleted = JsonConvert.DeserializeObject<bool>(carJsonString);
                        return objJobCompleted;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> CheckConnection()
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetCheckConnection";

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string contentJsonString = await response.Content.ReadAsStringAsync();
                        var result = JsonConvert.DeserializeObject<bool>(contentJsonString);
                        return result;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> InsertLoggingInfo(LoggingEntity loggingEntity)
        {
            try
            {
                string relativeUrl = "api/VDWRest/InsertLoggingInfo";
                var saveLoggingInfoDetails = JsonConvert.SerializeObject(loggingEntity);

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);

                    var stringContent = new StringContent(saveLoggingInfoDetails, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(Constants.RestBaseAddress + relativeUrl, stringContent);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        bool objLoggingInfo = JsonConvert.DeserializeObject<bool>(carJsonString);
                        return objLoggingInfo;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> CompleteJob(JobCompletionEntity completejobinfo)
        {
            try
            {
                string relativeUrl = "api/VDWRest/CompleteJob";

                var completionJobDetails = JsonConvert.SerializeObject(completejobinfo);

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);

                    var stringContent = new StringContent(completionJobDetails, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(Constants.RestBaseAddress + relativeUrl, stringContent);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        bool objJobCompleted = JsonConvert.DeserializeObject<bool>(carJsonString);
                        return objJobCompleted;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> ValidateUser(string VendorId, string Password)
        {
            try
            {
                string relativeUrl = "api/VDWRest/ValidateUser?";
                relativeUrl = relativeUrl + "vendorId=" + VendorId + "&password=" + Password;

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        bool objValidateUser = JsonConvert.DeserializeObject<bool>(carJsonString);
                        return objValidateUser;

                    }
                    return false;

                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static async Task<List<NotificationEntity>> GetTechNotifications(string userId)
        {

            try
            {
                string relativeUrl = "api/VDWRest/GetNotificationsForUser?";
                relativeUrl = relativeUrl + "userId=" + userId;

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();
                        var notifications = JsonConvert.DeserializeObject<List<NotificationEntity>>(carJsonString);
                        return notifications;

                    }
                    return new List<NotificationEntity>();

                }
            }
            catch (Exception ex)
            {
                return new List<NotificationEntity>();
            }

        }

        public static async Task<bool> MarkTechNotificationsAsRead(string userId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/MarkTechNotificationsAsRead?";
                relativeUrl = relativeUrl + "userId=" + userId;

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string carJsonString = await response.Content.ReadAsStringAsync();

                        var result = JsonConvert.DeserializeObject<bool>(carJsonString);
                        return result;
                    }
                    return false;

                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static async Task<bool> AddVdwRemarks(RemarksEntity remarksEntity)
        {
            try
            {
                string relativeUrl = "api/VDWRest/AddVdwRemarks";

                var remarksEntityJson = JsonConvert.SerializeObject(remarksEntity);

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);//Constants.RestBaseAddress);


                    var stringContent = new StringContent(remarksEntityJson, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(Constants.RestBaseAddress + relativeUrl, stringContent);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string responseString = await response.Content.ReadAsStringAsync();

                        bool isVdwRemarksAdded = JsonConvert.DeserializeObject<bool>(responseString);
                        return isVdwRemarksAdded;

                    }
                    return false;

                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static async Task<List<NotificationEntity>> GetNotificationsForUser(string userId)
        {

            try
            {
                string relativeUrl = "api/VDWRest/GetNotificationsForUser?";
                relativeUrl = relativeUrl + "userId=" + userId;

                using (var client = new HttpClient(GetCookies(relativeUrl)))
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddress);

                    var response = await client.GetAsync(Constants.RestBaseAddress + relativeUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        IEnumerable<string> cookies;
                        if (response.Headers.TryGetValues("Set-Cookie", out cookies))
                            SetCookies(cookies);

                        string jsonResponse = await response.Content.ReadAsStringAsync();

                        List<NotificationEntity> objJobSummary = JsonConvert.DeserializeObject<List<NotificationEntity>>(jsonResponse);

                        return objJobSummary;

                    }
                    return new List<NotificationEntity>();

                }
            }
            catch (Exception ex)
            {
                return new List<NotificationEntity>();
            }

        }
    }
}