﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using BusinessEntities.Models;
using ServiceLayer.Utils;
using System.Net;

namespace ServiceLayer
{
    public class ServiceProxyDummy
    {
        public static async Task<bool> AssignJob(JobEntity assignJob)
        {
            try
            {
                string relativeUrl = "api/VDWRest/AssignJob";

                var completionJobDetails = JsonConvert.SerializeObject(assignJob);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);

                    var stringContent = new StringContent(completionJobDetails, Encoding.UTF8, "application/json");
                    using (var response = await client.PostAsync(relativeUrl, stringContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            bool objJobCompleted = JsonConvert.DeserializeObject<bool>(carJsonString);
                            return objJobCompleted;
                        }
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static async Task<List<JobEntity>> GetJobAssignList(string VendorId, string RegionId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetAssignJobList?";
                relativeUrl = relativeUrl + "vendorId=" + VendorId + "&regionId=" + RegionId;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            List<JobEntity> objJobSummary = JsonConvert.DeserializeObject<List<JobEntity>>(carJsonString);

                            return objJobSummary;
                        }
                        return new List<JobEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<JobEntity>();
            }
        }

        public static async Task<List<JobEntity>> GetJobSummaryList(string VendorId, string RegionId, DateTime LastSyncDate)
        {
            //var json = "[{\"CustomerName\":\"Paul Allen\",\"JobID\":\"XBNJE1KX00\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"2013070590\",\"CommitmentDate\":\"10/07/2016 15:42\",\"DOORDERID\":\"1119301\",\"MON\":\"MA76EBMA09023\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"13115 Sanctuary Cove Drive, Temple Terrace, FL 33637\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"201573\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-05T15:42:28\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Allen Turing\",\"JobID\":\"XBPAZUKX00\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"2156969025\",\"CommitmentDate\":\"10/08/2016 15:42\",\"DOORDERID\":\"1118996\",\"MON\":\"MA76EBMA07359\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"16327 Compton palms dr, Tampa, FL 33647\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"215221\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-06T15:42:40\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Vishal Sikka\",\"JobID\":\"XBPA0VKX00\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"2678156560\",\"CommitmentDate\":\"10/08/2016 15:42\",\"DOORDERID\":\"1118997\",\"MON\":\"MA76EBMA09038\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"701 E Telecom Parkway, Temple Terrace, FL 33637\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"215330\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-06T15:42:40\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Steve Jobs\",\"JobID\":\"EBMA630200\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"3145783624\",\"CommitmentDate\":\"10/05/2016 15:42\",\"DOORDERID\":\"1130461\",\"MON\":\"MA76XBPA09075\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"Balch Hall, Ithaca, NY 14850\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"222326\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-03T15:42:15\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Bill Gates\",\"JobID\":\"XBVASRI110\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"9990909090\",\"CommitmentDate\":\"10/06/2016 15:42\",\"DOORDERID\":\"1121787\",\"MON\":\"MA76XBPA09878\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"220 Triphammer Rd, Ithaca, NY 14850\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"804360\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-04T15:42:01\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Steve Balmer\",\"JobID\":\"EBMA530200\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"3145783624\",\"CommitmentDate\":\"10/05/2016 15:42\",\"DOORDERID\":\"1130462\",\"MON\":\"MA76XBNJ0929\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"407 Eddy St, Ithaca, NY 14850\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"222326\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-03T15:42:15\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Mark Zuckerberg\",\"JobID\":\"EBMA5M1200\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"3145783624\",\"CommitmentDate\":\"10/07/2016 15:42\",\"DOORDERID\":\"1130961\",\"MON\":\"MA76XBDC08775\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"Duffield Hall, Ithaca, NY 14850\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"222326\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-05T15:42:28\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Graham Bell\",\"JobID\":\"XBDC8E1510\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"2025826126\",\"CommitmentDate\":\"10/08/2016 15:42\",\"DOORDERID\":\"1122608\",\"MON\":\"MA76EBMA06535\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"6125 Coral Bay Road, Tampa, FL 33647\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"202581\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-06T15:42:40\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null}]";
            //List<JobEntity> dummy = JsonConvert.DeserializeObject<List<JobEntity>>(json);
            //return dummy;

            try
            {
                string relativeUrl = "api/VDWRest/GetJobSummary?";
                relativeUrl = relativeUrl + "vendorId=" + VendorId + "&regionId=" + RegionId;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            List<JobEntity> objJobSummary = JsonConvert.DeserializeObject<List<JobEntity>>(carJsonString);

                            return objJobSummary;
                        }
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<List<JobEntity>> GetJobSummaryList(string VendorId, string RegionId)
        {
            //var json = "[{\"CustomerName\":\"Paul Allen\",\"JobID\":\"XBNJE1KX00\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"2013070590\",\"CommitmentDate\":\"10/07/2016 15:42\",\"DOORDERID\":\"1119301\",\"MON\":\"MA76EBMA09023\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"13115 Sanctuary Cove Drive, Temple Terrace, FL 33637\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"201573\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-05T15:42:28\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Allen Turing\",\"JobID\":\"XBPAZUKX00\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"2156969025\",\"CommitmentDate\":\"10/08/2016 15:42\",\"DOORDERID\":\"1118996\",\"MON\":\"MA76EBMA07359\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"16327 Compton palms dr, Tampa, FL 33647\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"215221\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-06T15:42:40\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Vishal Sikka\",\"JobID\":\"XBPA0VKX00\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"2678156560\",\"CommitmentDate\":\"10/08/2016 15:42\",\"DOORDERID\":\"1118997\",\"MON\":\"MA76EBMA09038\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"701 E Telecom Parkway, Temple Terrace, FL 33637\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"215330\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-06T15:42:40\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Steve Jobs\",\"JobID\":\"EBMA630200\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"3145783624\",\"CommitmentDate\":\"10/05/2016 15:42\",\"DOORDERID\":\"1130461\",\"MON\":\"MA76XBPA09075\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"Balch Hall, Ithaca, NY 14850\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"222326\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-03T15:42:15\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Bill Gates\",\"JobID\":\"XBVASRI110\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"9990909090\",\"CommitmentDate\":\"10/06/2016 15:42\",\"DOORDERID\":\"1121787\",\"MON\":\"MA76XBPA09878\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"220 Triphammer Rd, Ithaca, NY 14850\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"804360\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-04T15:42:01\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Steve Balmer\",\"JobID\":\"EBMA530200\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"3145783624\",\"CommitmentDate\":\"10/05/2016 15:42\",\"DOORDERID\":\"1130462\",\"MON\":\"MA76XBNJ0929\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"407 Eddy St, Ithaca, NY 14850\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"222326\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-03T15:42:15\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Mark Zuckerberg\",\"JobID\":\"EBMA5M1200\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"3145783624\",\"CommitmentDate\":\"10/07/2016 15:42\",\"DOORDERID\":\"1130961\",\"MON\":\"MA76XBDC08775\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"Duffield Hall, Ithaca, NY 14850\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"222326\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-05T15:42:28\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null},{\"CustomerName\":\"Graham Bell\",\"JobID\":\"XBDC8E1510\",\"JobType\":\"IRXJO\",\"JobStatus\":\"PRE\",\"AltReachNumber\":\"2025826126\",\"CommitmentDate\":\"10/08/2016 15:42\",\"DOORDERID\":\"1122608\",\"MON\":\"MA76EBMA06535\",\"Updates\":null,\"WorkCenter\":null,\"DAC\":null,\"HandlingCode\":null,\"JobPricing\":null,\"PlantTestDate\":null,\"CircuitId\":null,\"CKL\":null,\"TechCode\":\"VTE01\",\"Dates\":null,\"DAA\":null,\"CustomerAddress\":\"6125 Coral Bay Road, Tampa, FL 33647\",\"CustomerRemarks\":null,\"CustomerCBR\":null,\"ApptWindow\":null,\"BusinessHrs\":null,\"DacTime\":null,\"EstArrivalTm\":null,\"ONTSerialNumber\":null,\"Speed\":null,\"OntWiringType\":null,\"InstlGuarantee\":null,\"OfSTB\":null,\"Hub\":null,\"AwasRegion\":\"\",\"OriginalDueDate\":null,\"CategoryDescription\":null,\"SubCategoryDescription\":null,\"Latitude\":null,\"Longitude\":null,\"IM_Flag\":null,\"Geocoded\":null,\"GroupId\":null,\"GarageId\":null,\"GarageName\":null,\"ReachNumber\":null,\"CustomerContactStatus\":null,\"LastContactDate\":null,\"WireCenter\":\"202581\",\"WireCenterGroup\":null,\"DropType\":null,\"MTN\":null,\"Region\":null,\"ISTechUpsell\":null,\"JobTags\":null,\"TroubleCategory\":null,\"PolygonName\":null,\"PolygonId\":0,\"Center\":null,\"ProductPriority\":null,\"TSP\":null,\"TTR\":null,\"IONT\":null,\"AWAS_ALERT_BITMAP\":null,\"DACIndicator\":null,\"TroubleCreatedDateTime\":null,\"CustContacted\":null,\"City\":null,\"WorkGroup\":null,\"NextStatus\":null,\"HoldStatus\":null,\"DacStatus\":null,\"DACStartTm\":null,\"REPORT_CATEGORY\":null,\"TechName\":null,\"CentralOffice\":null,\"JobStartDate\":null,\"JobEndDate\":null,\"VdwDispatchDateTime\":\"2016-10-06T15:42:40\",\"Price\":null,\"RPR_RCVD\":null,\"RPR_DUR\":null,\"FTTP_CORE_TYPE\":\"FIOS\",\"Is_Premium\":null,\"VREPAIR_STATE_CODE\":null,\"REPORTING_ID\":null,\"Care\":null,\"Chronic\":null,\"Vulnerable\":null,\"JobDispatch\":null,\"TroubleTypeCode\":null,\"ParallelProvisioning\":null,\"SingleDispatch\":null,\"VendorId\":\"NEPVC\",\"VdwWireCenterGroup\":null,\"Cancellation\":null,\"ChangeHost\":null,\"TntCount\":null,\"CompanyMissAlert\":null,\"PrevTech\":null,\"T2Score\":null,\"T2ScoreRange\":null}]";
            //List<JobEntity> dummy = JsonConvert.DeserializeObject<List<JobEntity>>(json);
            //return dummy;

            try
            {
                string relativeUrl = "api/VDWRest/GetJobSummary?";
                relativeUrl = relativeUrl + "vendorId=" + VendorId + "&regionId=" + RegionId;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            List<JobEntity> objJobSummary = JsonConvert.DeserializeObject<List<JobEntity>>(carJsonString);

                            return objJobSummary;
                        }
                        return new List<JobEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<JobEntity>();
            }
        }

        public static async Task<List<JobCompletionEntity>> GetJobsCompletionInfo(string userId, UserRole userRole, string regionId, DateTime lastSyncDate)
        {
            //var json = "[{\"JobId\":null,\"Mon\":null,\"DoOrderId\":null,\"TechId\":null,\"VdwDispatchDateTime\":null,\"CommitmentDate\":null,\"DispatchTime\":null,\"CableLocateNum\":\"2816454\",\"CusstatRemarks\":null,\"CompletionRemarks\":\"Finished DWO:10:50 11/26/2016  **COMPLETE CMP:TRIP CHARGE:N BY USER: CMP:PULL THROUGH LENGTH:115 BY USER: CMP:PRE CUT LENGTH:250 BY USER: T-  CUSTOMER CONTACT: #ARRIVAL DATE 12/01/2016# #ARRIVAL TIME 06:00# DROP LENGTH:141; NUM BORES:1; TOTAL BORE LENGTH:25\",\"ArrivalDate\":\"12/01/2016\",\"ArrivalTime\":\"06:00\",\"ClearDate\":\"12/01/2016\",\"ClearTime\":\"12:00\",\"EnggRemarks\":\"\",\"ContactName\":\"\",\"Contact\":\"\",\"NoofBores\":\"1\",\"BoreTypeNumber\":\"1\",\"BoreTypeLength\":\"25\",\"BoreType\":\"Road/Driveway\",\"TotBoreLength\":\"25\",\"DirectBuriedLength\":null,\"PullthroughLength\":\"115\",\"DropConduitLength\":\"141\",\"PreCutLength\":\"250\",\"DropType\":\"BURIED\",\"JeoPardyCode\":\"\",\"JeopardyDate\":\"\",\"JeopardyTime\":\"\",\"DidCompletejob\":null,\"DidTerminateDrop\":\"N\",\"ReferToEngg\":\"N\",\"TripCharge\":\"N\",\"DidplaceanONT\":\"N\",\"ONTLocationRemarks\":\"\",\"ONTLocInOut\":\"\",\"MapPolygonJson\":\"[]\",\"Images\":null}]";
            //List<JobCompletionEntity> dummy = JsonConvert.DeserializeObject<List<JobCompletionEntity>>(json);
            //return dummy;

            try
            {
                string relativeUrl = "api/VDWRest/GetJobsCompletionInfo?";
                relativeUrl = relativeUrl + string.Format("userId={0}&userRole={1}&regionId={2}", userId, userRole, regionId);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            List<JobCompletionEntity> jobsCompletionInfo = JsonConvert.DeserializeObject<List<JobCompletionEntity>>(carJsonString);

                            return jobsCompletionInfo;
                        }
                        return new List<JobCompletionEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<JobCompletionEntity>();
            }
        }

        public static async Task<string> GetJobStatus(string OrderId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetJobStatus?";
                relativeUrl = relativeUrl + "doOrderId=" + OrderId;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();
                            return carJsonString.Replace("\"", "");

                        }
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static HttpClientHandler GetCookies(string relativeUrl)
        {
            try
            {
                HttpClientHandler handler = new HttpClientHandler();
                var count = ServiceLayer.Utils.AppConstants.cookieCollections.Count;
                foreach (Cookie cookie in ServiceLayer.Utils.AppConstants.cookieCollections)
                {
                    handler.CookieContainer.Add(new Uri(Constants.RestBaseAddress + relativeUrl), new Cookie(cookie.Name, cookie.Value, cookie.Path));
                }

                return handler;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void SetCookies(IEnumerable<string> cookieString)
        {
            try
            {
                var cookiesSet = new CookieCollection();
                foreach (var c in cookieString)
                {
                    var cookiePairs = c.Split(';');
                    var cookiename = cookiePairs[0].Split('=')[0];
                    var cookievalue = cookiePairs[0].Split('=')[1];

                    if (cookiename == AppConstants.SessionName && cookievalue != AppConstants.SessionLogOff)
                    {
                        var cookipath = cookiePairs[1].Split('=')[1];
                        cookiesSet.Add(new Cookie { Name = cookiename, Value = cookievalue, Path = cookipath });
                    }
                }
                if (cookiesSet.Count > 0)
                    ServiceLayer.Utils.AppConstants.cookieCollections = cookiesSet;
            }
            catch (Exception ex)
            {

            }
        }

        public static async Task<List<DropTypeEntity>> GetDropTypes()
        {
            //var json = "[{\"FiberType\":\"\"},{\"FiberType\":\"CONDUIT\"},{\"FiberType\":\"TEMP\"},{\"FiberType\":\"MDU\"},{\"FiberType\":\"UNDERGROUND\"},{\"FiberType\":\"BURIED\"},{\"FiberType\":\"AERIAL\"},{\"FiberType\":\"MDU_INSIDE\"}]";
            //List<DropTypeEntity> dummy = JsonConvert.DeserializeObject<List<DropTypeEntity>>(json);
            //return dummy.OrderBy(d => d.FiberType).ToList();

            try
            {
                string relativeUrl = "api/VDWRest/GetDropTypes";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();
                            List<DropTypeEntity> objDropType = JsonConvert.DeserializeObject<List<DropTypeEntity>>(carJsonString);

                            return objDropType.OrderBy(d => d.FiberType).ToList();

                        }
                        return new List<DropTypeEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<DropTypeEntity>();
            }
        }

        public static async Task<List<JeopardyCodeEntity>> GetJeopardyCodes()
        {
            //var json = "[{\"JwmCode\":\"BD01\"},{\"JwmCode\":\"BD06\"},{\"JwmCode\":\"BD81\"},{\"JwmCode\":\"BD82\"},{\"JwmCode\":\"BD83\"},{\"JwmCode\":\"AP02\"},{\"JwmCode\":\"VB65\"},{\"JwmCode\":\"VB66\"},{\"JwmCode\":\"VB67\"},{\"JwmCode\":\"VB68\"},{\"JwmCode\":\"VB69\"},{\"JwmCode\":\"VB07\"},{\"JwmCode\":\"XN53\"},{\"JwmCode\":\"XN54\"},{\"JwmCode\":\"VB50\"},{\"JwmCode\":\"VB51\"},{\"JwmCode\":\"VB52\"},{\"JwmCode\":\"VB53\"},{\"JwmCode\":\"VB54\"},{\"JwmCode\":\"VB55\"},{\"JwmCode\":\"VB56\"},{\"JwmCode\":\"VB57\"},{\"JwmCode\":\"VB58\"},{\"JwmCode\":\"VB44\"},{\"JwmCode\":\"VB41\"},{\"JwmCode\":\"VB59\"},{\"JwmCode\":\"VB60\"},{\"JwmCode\":\"VB61\"},{\"JwmCode\":\"VB62\"},{\"JwmCode\":\"VB63\"},{\"JwmCode\":\"VB64\"},{\"JwmCode\":\"VB70\"},{\"JwmCode\":\"VB71\"},{\"JwmCode\":\"VB72\"},{\"JwmCode\":\"VB73\"},{\"JwmCode\":\"VB74\"},{\"JwmCode\":\"XN17\"},{\"JwmCode\":\"VB75\"},{\"JwmCode\":\"MN71\"},{\"JwmCode\":\"FM08\"},{\"JwmCode\":\"FM58\"}]";
            //List<JeopardyCodeEntity> dummy = JsonConvert.DeserializeObject<List<JeopardyCodeEntity>>(json);
            //return dummy.OrderBy(j => j.JwmCode).ToList();

            try
            {
                string relativeUrl = "api/VDWRest/GetJeopardyCodes";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();
                            List<JeopardyCodeEntity> objJeocodes = JsonConvert.DeserializeObject<List<JeopardyCodeEntity>>(carJsonString);

                            return objJeocodes.OrderBy(j => j.JwmCode).ToList();

                        }
                        return new List<JeopardyCodeEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<JeopardyCodeEntity>();
            }
        }

        public static async Task<List<ImageCategoryEntity>> GetCategories()
        {
            //var json = "[{\"CategoryID\":1,\"CategoryDescription\":\"Fiber\"},{\"CategoryID\":2,\"CategoryDescription\":\"Hub\"},{\"CategoryID\":3,\"CategoryDescription\":\"Home\"},{\"CategoryID\":4,\"CategoryDescription\":\"Others\"}]";
            //List<ImageCategoryEntity> dummy = JsonConvert.DeserializeObject<List<ImageCategoryEntity>>(json);
            //return dummy.OrderBy(c => c.CategoryDescription).ToList();

            try
            {
                string relativeUrl = "api/VDWRest/GetCategories";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();
                            List<ImageCategoryEntity> objcategories = JsonConvert.DeserializeObject<List<ImageCategoryEntity>>(carJsonString);

                            return objcategories.OrderBy(c => c.CategoryDescription).ToList();

                        }
                        return new List<ImageCategoryEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<ImageCategoryEntity>();
            }
        }

        public static async Task<List<GlobalFlagEntity>> GetGlobalFlags()
        {
            //var json = "[{\"Key\":\"VDW_MOBILE_IMG_CATEGORYVALIDATION\",\"Value\":\"Y\"}]";
            //List<GlobalFlagEntity> dummy = JsonConvert.DeserializeObject<List<GlobalFlagEntity>>(json);
            //return dummy;

            try
            {
                string relativeUrl = "api/VDWRest/GetGlobalFlags";

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();
                            List<GlobalFlagEntity> globalFlags = JsonConvert.DeserializeObject<List<GlobalFlagEntity>>(carJsonString);
                            return globalFlags;

                        }
                        return new List<GlobalFlagEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<GlobalFlagEntity>();
            }
        }

        public static async Task<bool> UpdateDeviceInfo(DeviceEntity deviceInfo)
        {
            try
            {
                string relativeUrl = "api/VDWRest/DeviceInfo";

                var completionJobDetails = JsonConvert.SerializeObject(deviceInfo);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);

                    var stringContent = new StringContent(completionJobDetails, Encoding.UTF8, "application/json");
                    using (var response = await client.PostAsync(relativeUrl, stringContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            bool objJobCompleted = JsonConvert.DeserializeObject<bool>(carJsonString);
                            return objJobCompleted;
                        }
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static async Task<int> GetNotificationCount(string deviceId, string userId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetNotificationCount?";

                relativeUrl = relativeUrl + "deviceID=" + deviceId + "&userID=" + userId;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            int objJobCompleted = JsonConvert.DeserializeObject<int>(carJsonString);
                            return objJobCompleted;
                        }
                        return 0;
                    }

                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static async Task<bool> SetNotificationCount(string deviceId, string userId, int notificationCount)
        {
            try
            {
                NotificationCount objNotificationCount = new NotificationCount();
                objNotificationCount.DeviceToken = deviceId;
                objNotificationCount.UserId = userId;
                objNotificationCount.NoofNotification = notificationCount;
                string relativeUrl = "api/VDWRest/SetNotificationCount";

                var completionJobDetails = JsonConvert.SerializeObject(objNotificationCount);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);

                    var stringContent = new StringContent(completionJobDetails, Encoding.UTF8, "application/json");
                    using (var response = await client.PostAsync(relativeUrl, stringContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            bool objJobCompleted = JsonConvert.DeserializeObject<bool>(carJsonString);
                            return objJobCompleted;
                        }
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> CheckConnection()
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetCheckConnection";
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            var result = JsonConvert.DeserializeObject<bool>(carJsonString);
                            return result;
                        }
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> InsertLoggingInfo(LoggingEntity loggingEntity)
        {
            try
            {
                string relativeUrl = "api/VDWRest/InsertLoggingInfo";

                var saveLoggingInfoDetails = JsonConvert.SerializeObject(loggingEntity);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);

                    var stringContent = new StringContent(saveLoggingInfoDetails, Encoding.UTF8, "application/json");
                    using (var response = await client.PostAsync(relativeUrl, stringContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            bool objLoggingInfo = JsonConvert.DeserializeObject<bool>(carJsonString);
                            return objLoggingInfo;
                        }
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> CompleteJob(JobCompletionEntity completejobinfo)
        {
            try
            {
                string relativeUrl = "api/VDWRest/CompleteJob";

                var completionJobDetails = JsonConvert.SerializeObject(completejobinfo);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);

                    var stringContent = new StringContent(completionJobDetails, Encoding.UTF8, "application/json");
                    using (var response = await client.PostAsync(relativeUrl, stringContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            bool objJobCompleted = JsonConvert.DeserializeObject<bool>(carJsonString);
                            return objJobCompleted;
                        }
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> ValidateUser(string VendorId, string Password)
        {
            try
            {
                string relativeUrl = "api/VDWRest/ValidateUser?";
                relativeUrl = relativeUrl + "vendorId=" + VendorId + "&password=" + Password;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            bool objValidateUser = JsonConvert.DeserializeObject<bool>(carJsonString);
                            return objValidateUser;
                        }
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<List<NotificationEntity>> GetTechNotifications(string userId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetTechNotifications?";
                relativeUrl = relativeUrl + "userId=" + userId;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            var notifications = JsonConvert.DeserializeObject<List<NotificationEntity>>(carJsonString);
                            return notifications;
                        }
                        return new List<NotificationEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<NotificationEntity>();
            }
        }

        public static async Task<bool> MarkTechNotificationsAsRead(string userId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/MarkTechNotificationsAsRead?";
                relativeUrl = relativeUrl + "userId=" + userId;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);
                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string carJsonString = await response.Content.ReadAsStringAsync();

                            var result = JsonConvert.DeserializeObject<bool>(carJsonString);
                            return result;
                        }
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> AddVdwRemarks(RemarksEntity remarksEntity)
        {
            try
            {
                string relativeUrl = "api/VDWRest/AddVdwRemarks";

                var remarksEntityJson = JsonConvert.SerializeObject(remarksEntity);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);

                    var stringContent = new StringContent(remarksEntityJson, Encoding.UTF8, "application/json");
                    using (var response = await client.PostAsync(relativeUrl, stringContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string responseString = await response.Content.ReadAsStringAsync();

                            bool isVdwRemarksAdded = JsonConvert.DeserializeObject<bool>(responseString);
                            return isVdwRemarksAdded;
                        }
                        return false;
                    }

                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static async Task<List<NotificationEntity>> GetNotificationsForUser(string userId)
        {
            try
            {
                string relativeUrl = "api/VDWRest/GetNotificationsForUser?";
                relativeUrl = relativeUrl + "userId=" + userId;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Constants.RestBaseAddressForOffShore);

                    using (var response = await client.GetAsync(relativeUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {

                            string jsonResponse = await response.Content.ReadAsStringAsync();

                            List<NotificationEntity> objJobSummary = JsonConvert.DeserializeObject<List<NotificationEntity>>(jsonResponse);

                            return objJobSummary;

                        }
                        return new List<NotificationEntity>();
                    }
                }
            }
            catch (Exception ex)
            {
                return new List<NotificationEntity>();
            }
        }
    }
}
