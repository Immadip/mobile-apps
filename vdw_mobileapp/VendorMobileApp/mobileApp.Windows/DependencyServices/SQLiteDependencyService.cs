﻿using mobileApp.DependencyServices;
using mobileApp.Windows.DependencyServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteDependencyService))]
namespace mobileApp.Windows.DependencyServices
{
    public class SQLiteDependencyService : ISQLiteDependencyService
    {
        public SQLite.SQLiteConnection GetConnection()
        {
            var sqliteFilename = "com.verizon.vdwmobile.db3";
            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);
            var conn = new SQLite.SQLiteConnection(path);
            return conn;
        }
    }
}
