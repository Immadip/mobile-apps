﻿using BusinessEntities.Models;
using mobileApp.Views;
using mobileApp.Windows.CustomRenderers;
using System.Collections.Generic;
using Windows.Devices.Geolocation;
using Windows.Storage.Streams;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.WinRT;
using Xamarin.Forms.Platform.WinRT;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(JobSummaryMapPage), typeof(CustomMapRenderer))]
namespace mobileApp.Windows.CustomRenderers
{
    public class CustomMapRenderer : MapRenderer
    {
        Bing.Maps.Map bingMap;
        List<MapPinEntity> customPins;
        //JobMapOverlay mapOverlay;
        bool xamarinOverlayShown = false;
        bool isDrawn = false;

        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                //bingMap.MapElementClick -= OnMapElementClick;
                bingMap.Children.Clear();
                //mapOverlay = null;
                bingMap = null;
            }

            if (e.NewElement != null)
            {
                var formsMap = (JobSummaryMapPage)e.NewElement;
                bingMap = Control as Bing.Maps.Map;
                customPins = formsMap.CustomPins;

                bingMap.Children.Clear();
                //bingMap.MapElementClick += OnMapElementClick;

                
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if(customPins != null && customPins.Count > 0 && !isDrawn)
            {
                //bingMap.Children.Clear();
                isDrawn = true;
            }

            foreach (var pin in customPins)
            {
                //var snPosition = new BasicGeoposition { Latitude = pin.Pin.Position.Latitude, Longitude = pin.Pin.Position.Longitude };
                //var snPoint = new Geopoint(snPosition);


                var pushPin = new Bing.Maps.Pushpin();
                pushPin.DataContext = new Bing.Maps.Location { Latitude = pin.Pin.Position.Latitude, Longitude = pin.Pin.Position.Longitude };
                //mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///pin.png"));
                //pushPin.CollisionBehaviorDesired = MapElementCollisionBehavior.RemainVisible;
                pushPin.Text = "Hello";
                //mapIcon.NormalizedAnchorPoint = new Windows.Foundation.Point(0.5, 1.0);

                bingMap.Children.Add(pushPin);
            }
        }

        //private void OnMapElementClick(Bing.Maps.Map sender, MapElementClickEventArgs args)
        //{
        //    var mapIcon = args.MapElements.FirstOrDefault(x => x is MapIcon) as MapIcon;
        //    if (mapIcon != null)
        //    {
        //        if (!xamarinOverlayShown)
        //        {
        //            var customPin = GetCustomPin(mapIcon.Location.Position);
        //            if (customPin == null)
        //            {
        //                throw new Exception("Custom pin not found");
        //            }

        //            if (customPin.Id != null)
        //            {
        //                if (mapOverlay == null)
        //                {
        //                    mapOverlay = new JobMapOverlay(customPin);
        //                }

        //                var snPosition = new BasicGeoposition { Latitude = customPin.Pin.Position.Latitude, Longitude = customPin.Pin.Position.Longitude };
        //                var snPoint = new Geopoint(snPosition);

        //                bingMap.Children.Add(mapOverlay);
        //                MapControl.SetLocation(mapOverlay, snPoint);
        //                MapControl.SetNormalizedAnchorPoint(mapOverlay, new Windows.Foundation.Point(0.5, 1.0));
        //                xamarinOverlayShown = true;
        //            }
        //        }
        //        else
        //        {
        //            bingMap.Children.Remove(mapOverlay);
        //            xamarinOverlayShown = false;
        //        }
        //    }
        //}

        MapPinEntity GetCustomPin(BasicGeoposition position)
        {
            var pos = new Position(position.Latitude, position.Longitude);
            foreach (var pin in customPins)
            {
                if (pin.Pin.Position == pos)
                {
                    return pin;
                }
            }
            return null;
        }
    }
}
