﻿using mobileApp.Windows.CustomRenderers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinRT;

[assembly: ResolutionGroupName("Verizon")]
[assembly: ExportEffect(typeof(CustomFontEffect), "FontEffect")]
namespace mobileApp.Windows.CustomRenderers
{
    public class CustomFontEffect : PlatformEffect
    {
        Control control;
        protected override void OnAttached()
        {
            try
            {
                control = Control as Control;
                control.FontFamily = new FontFamily(CustomControls.CustomFontEffect.GetFontFileName(Element));
                control.FontSize = CustomControls.CustomFontEffect.GetFontSize(Element);
            }
            catch(Exception ex)
            {
            }
        }

        protected override void OnDetached()
        {
        }

        protected override void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnElementPropertyChanged(args);

            if (args.PropertyName == CustomControls.CustomFontEffect.FontFileNameProperty.PropertyName)
            {
                control.FontFamily = new FontFamily(CustomControls.CustomFontEffect.GetFontFileName(Element));
                control.FontSize = CustomControls.CustomFontEffect.GetFontSize(Element);
            }
        }
    }
}
