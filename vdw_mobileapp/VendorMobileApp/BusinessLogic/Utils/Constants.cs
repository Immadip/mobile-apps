﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Constants
    {
        public static string ImageRequiredValidationFlagKey = "VDW_MOBILE_IMG_CATEGORYVALIDATION";
        public static string Yes = "Y";
        public static string No = "N";

        public static string Input = "INSIDE";
        public static string Output = "OUTSIDE";
    }
}
