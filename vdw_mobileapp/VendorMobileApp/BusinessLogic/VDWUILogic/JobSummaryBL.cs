﻿using System;
using System.Collections.Generic;
using ServiceLayer;
using System.Threading.Tasks;
using BusinessEntities.Models;
using BusinessEntities;

namespace BusinessLogic.VDWUILogic
{
    public class JobSummaryBL
    {
        public static async Task<List<JobEntity>> GetJobSummaryList(string regionId, string vendorId)
        {
            try
            {
                return await ServiceProxy.GetJobSummaryList(regionId, vendorId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<List<JobEntity>> GetJobAssignList(string vendorId, string regionId)
        {
            try
            {
                return await ServiceProxy.GetJobAssignList(vendorId, regionId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<bool> PutAssignJob(JobEntity assignJob)
        {
           try
            {
                return await ServiceProxy.AssignJob(assignJob);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<List<JobEntity>> GetJobSummaryList(string vendorId, string regionId,DateTime lastSyncDate)
        {
            try
            {
                return await ServiceProxy.GetJobSummaryList(vendorId, regionId,  lastSyncDate);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<List<JobCompletionEntity>> GetJobsCompletionInfo(string userId, UserRole userRole, string regionId, DateTime lastSyncDate)
        {
            try
            {
                return await ServiceProxy.GetJobsCompletionInfo(userId, userRole, regionId, lastSyncDate);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
