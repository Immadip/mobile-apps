﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceLayer;
using System.Threading.Tasks;

namespace BusinessLogic.VDWUILogic
{
    public class AuthenticationBL
    {
        public static async Task<bool> ValidateUserCredentials(string UserName, string Password)
        {
            try
            {
                bool objValidateUser = await ServiceProxy.ValidateUser(UserName, Password);
                return objValidateUser;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> CheckInternet()
        {
            try
            {
                bool objValidateUser = await ServiceProxy.CheckConnection();
                return objValidateUser;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


    }
}
