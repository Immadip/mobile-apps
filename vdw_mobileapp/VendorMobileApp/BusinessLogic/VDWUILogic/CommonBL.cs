﻿using BusinessEntities.Models;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.VDWUILogic
{
    public class CommonBL
    {
        public static async Task<List<GlobalFlagEntity>> GetGlobalFlags()
        {
            try
            {
                return await ServiceProxy.GetGlobalFlags();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
