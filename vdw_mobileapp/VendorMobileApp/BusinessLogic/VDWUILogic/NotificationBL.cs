﻿using System;
using BusinessEntities;
using ServiceLayer;
using System.Threading.Tasks;
using System.Collections.Generic;
using BusinessEntities.Models;

namespace BusinessLogic
{
	public class NotificationBL
	{
		
		public static async Task<List<NotificationEntity>> GetNotificationList(string userId)
		{
			try
			{
				return await ServiceProxy.GetNotificationsForUser(userId);
			}
			catch (Exception ex)
			{
				return null;
			}
		}

        public static async Task<bool> SetNotificationCount(string deviceId, string userId, int notificationCount)
        {
            try
            {
                return await ServiceProxy.SetNotificationCount(deviceId, userId, notificationCount);
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static async Task<int> GetNotificationCount(string deviceId, string userId)
        {
            try
            {
                return await ServiceProxy.GetNotificationCount(deviceId, userId);
            }
            catch (Exception ex)
            {
                return 0;
            }

        }
    }
}
