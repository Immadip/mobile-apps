﻿using System;
using System.Collections.Generic;
using ServiceLayer;
using System.Threading.Tasks;
using BusinessEntities.Models;

namespace BusinessLogic.VDWUILogic
{
    public class LoggingInfoBL
    {
        public static async Task<bool> InsertLoggingInfo(LoggingEntity loggingEntity)
        {
            try
            {
                return await ServiceProxy.InsertLoggingInfo(loggingEntity);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
