﻿using BusinessEntities.Models;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.VDWUILogic
{
   public class JobActionsBL
    {
        public static async Task<bool> AddVdwRemarks(RemarksEntity remarksEntity)
        {
            return await ServiceProxy.AddVdwRemarks(remarksEntity);
        }

        public static async Task<bool> CompleteTheJob(JobCompletionEntity completejobinfo)
        {
            try
            {
                return await ServiceProxy.CompleteJob(completejobinfo);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<List<DropTypeEntity>> GetDropTypes()
        {
            try
            {
                return await ServiceProxy.GetDropTypes();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<List<JeopardyCodeEntity>> GetJeopardyCodes()
        {
            try
            {
                return await ServiceProxy.GetJeopardyCodes();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<List<ImageCategoryEntity>> GetCategories()
        {
            try
            {
                return await ServiceProxy.GetCategories();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<string> GetJobStatus(string orderID)
        {
            try
            {
                return await ServiceProxy.GetJobStatus(orderID);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
