﻿using BusinessEntities.Models;
using ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.VDWUILogic
{
    public class DeviceInfoBL
    {
        public static async Task<bool> UpdateDeviceInfo(DeviceEntity deviceInfo)
        {
            try
            {
                return await ServiceProxy.UpdateDeviceInfo(deviceInfo); 
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
