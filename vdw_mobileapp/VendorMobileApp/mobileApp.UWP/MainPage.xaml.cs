﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace mobileApp.UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            var windowSize = Window.Current.Bounds;
            mobileApp.App.ScreenWidth = (int)windowSize.Width;
            mobileApp.App.ScreenHeight = (int)windowSize.Height;

            // Initialize the Windows Bing Maps with the API Key.
            Xamarin.FormsMaps.Init("AlKLSC7aKKH0DjJ-8a-InbpQfPFCYFB00BtSAZUcGYkkeAGjM1D-XAcmr8jZPExx");
            

            LoadApplication(new mobileApp.App());
        }
    }
}
