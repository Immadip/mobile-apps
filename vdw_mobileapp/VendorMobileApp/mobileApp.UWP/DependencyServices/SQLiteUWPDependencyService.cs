﻿using mobileApp.DependencyServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Xamarin.Forms;
using mobileApp.UWP.DependencyServices;

[assembly: Dependency(typeof(SQLiteUWPDependencyService))]
namespace mobileApp.UWP.DependencyServices
{
    public class SQLiteUWPDependencyService : ISQLiteDependencyService
    {
        public SQLite.SQLiteConnection GetConnection()
        {
            var sqliteFilename = "com.verizon.vdwmobile.db3";
            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);
            var conn = new SQLite.SQLiteConnection(path);
            return conn;
        }
    }
}
