﻿using mobileApp.UWP.CustomRenderers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ResolutionGroupName("Verizon")]
[assembly: ExportEffect(typeof(CustomFontEffect), "FontEffect")]
namespace mobileApp.UWP.CustomRenderers
{
    public class CustomFontEffect : PlatformEffect
    {
        Control control;
        protected override void OnAttached()
        {
            try
            {
                control = Control as Control;
                control.FontFamily = new FontFamily(CustomControls.CustomFontEffect.GetFontFileName(Element));
                control.FontSize = CustomControls.CustomFontEffect.GetFontSize(Element);
            }
            catch(Exception ex)
            {
                Console.WriteLine("UWP.CustomRenderers.CustomFontEffect:OnAttached(): Cannot set property on attached control. Error: ", ex.Message);
            }
        }

        protected override void OnDetached()
        {
        }

        protected override void OnElementPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnElementPropertyChanged(args);

            if (args.PropertyName == CustomControls.CustomFontEffect.FontFileNameProperty.PropertyName)
            {
                control.FontFamily = new FontFamily(CustomControls.CustomFontEffect.GetFontFileName(Element));
                control.FontSize = CustomControls.CustomFontEffect.GetFontSize(Element);
            }
        }
    }
}
