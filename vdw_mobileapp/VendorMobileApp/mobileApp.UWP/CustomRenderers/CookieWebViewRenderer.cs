﻿using System.Net;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;
using mobileApp.CustomControls;
using mobileApp.UWP.CustomRenderers;
using System;
using System.Diagnostics;
using Windows.UI.Xaml.Navigation;
//using Cookies;
//using Cookies.WinPhone;
//using Microsoft.Win32.Controls;
using Windows.Web.Http;
using Windows.Web.Http.Filters;

[assembly: ExportRenderer(typeof(CookieWebView), typeof(CookieWebViewRenderer))]
namespace mobileApp.UWP.CustomRenderers
{
    public class CookieWebViewRenderer : Xamarin.Forms.Platform.UWP.WebViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                Control.NavigationStarting += ControlOnNavigating;
                Control.NavigationCompleted += ControlOnNavigated;
            }
        }

        protected void ControlOnNavigated(object sender, Windows.UI.Xaml.Controls.WebViewNavigationCompletedEventArgs navigationEventArgs)
        {


            var filter = new HttpBaseProtocolFilter();
            HttpClient client = new HttpClient(filter);
            HttpCookieCollection httpCookieCollection = filter.CookieManager.GetCookies(navigationEventArgs.Uri);
            //httpCookieCollection.
            // Use this, while it comes from an instance, its shared across everything.
            var cookieCollection = new CookieCollection();
            foreach (var cookie in httpCookieCollection)
            {
                cookieCollection.Add(new Cookie
                {
                    Domain = cookie.Domain,
                    Name = cookie.Name,                    
                    Value = cookie.Value,                    
                    
                });
            }
            CookieWebView.OnNavigated(new CookieNavigatedEventArgs()
            {

                Cookies = cookieCollection,
                Url = navigationEventArgs.Uri.ToString()
            });
        }

        protected void ControlOnNavigating(object sender, Windows.UI.Xaml.Controls.WebViewNavigationStartingEventArgs navigatingEventArgs)
        {
            CookieWebView.OnNavigating(new CookieNavigationEventArgs()
            {
                Url = navigatingEventArgs.Uri.ToString()
            });
        }

        public CookieWebView CookieWebView
        {
            get { return Element as CookieWebView; }
        }
    }
}
