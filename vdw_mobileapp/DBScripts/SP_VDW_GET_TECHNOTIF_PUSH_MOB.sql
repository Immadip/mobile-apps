create or replace PROCEDURE       WBNCP.SP_VDW_GET_PUSHNOTIF_MOB(
	Records_ref OUT SYS_REFCURSOR)
AS
v_ErrMsg VARCHAR2(1500);
BEGIN

  OPEN Records_ref FOR
  SELECT *
  FROM V_VDW_DeviceInfo_Notification
  WHERE Is_Pushed_Flag = 'N';

EXCEPTION 
WHEN OTHERS THEN
v_ErrMsg := ' Exception ' || SUBSTR(SQLERRM, 1,500);
WBNEXCEPTIONLOGGING.EXCEPTIONLOGGING(SQLCODE, v_ErrMsg, 'SP_VDW_GET_PUSHNOTIF_MOB');
DBMS_OUTPUT.PUT_LINE('v_ErrMsg :: ' || v_ErrMsg);
OPEN Records_ref FOR SELECT 'Exception occurred in stored procedure SP_VDW_GET_PUSHNOTIF_MOB: ' || v_ErrMsg AS Error FROM DUAL;

END SP_VDW_GET_PUSHNOTIF_MOB;

/