create or replace PROCEDURE                   WBNCP.SP_VDW_SAVE_IMGDETAILS_MOBILE(
  p_Tech_Id VDW_Tech_UploadedImgDetails.Tech_Id%TYPE,
  p_Job_Id do_order.Job_Id%TYPE,
  p_Comments VDW_Tech_UploadedImgDetails.Comments%TYPE,
  p_Image_Time_Stamp VDW_Tech_UploadedImgDetails.Image_Time_Stamp%TYPE,
  p_Latitude VDW_Tech_UploadedImgDetails.Latitude%TYPE,
  p_Longitude VDW_Tech_UploadedImgDetails.Longitude%TYPE,
  p_Relative_File_Path VDW_Tech_UploadedImgDetails.Relative_File_Path%TYPE,
  
  p_Result OUT INT
  )
  
AS
v_ErrMsg VARCHAR2(1500);
v_do_order_id VDW_Tech_UploadedImgDetails.Do_Order_Id%TYPE;
BEGIN

  p_Result := -1;
  
  SELECT do_order_id INTO v_do_order_id
  FROM do_order do
  WHERE do.job_id = p_Job_Id;

  INSERT INTO VDW_Tech_UploadedImgDetails
  (Image_Id, Tech_Id, Do_Order_Id, Comments, Latitude, Longitude, Image_Time_Stamp, relative_file_path, Last_Updated_Date_Time, Created_Date_Time) VALUES
  (VDW_UploadedImgDetails_Seq.NEXTVAL, p_Tech_Id, v_do_order_id, p_Comments, p_Latitude, p_Longitude, p_Image_Time_Stamp, p_Relative_File_Path, SYSDATE, SYSDATE);
  
  COMMIT;
 
  p_Result := 1;

EXCEPTION 
WHEN OTHERS THEN
	v_ErrMsg := ' Exception ' || SUBSTR(SQLERRM, 1,500);
	WBNEXCEPTIONLOGGING.EXCEPTIONLOGGING(SQLCODE, v_ErrMsg, 'SP_VDW_SAVE_IMGDETAILS_MOBILE');
	DBMS_OUTPUT.PUT_LINE('v_ErrMsg :: ' || v_ErrMsg);
	p_Result := -2;

END SP_VDW_SAVE_IMGDETAILS_MOBILE;