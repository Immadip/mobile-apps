create or replace PROCEDURE             WBNCP.SP_VDW_GET_COMPLETION( 
    p_userId  VARCHAR2,
    p_vendorOrTech VARCHAR2, -- Vendor Or Tech
    Records_ref OUT sys_refcursor )
AS
  v_Errmsg varchar2(1500);
BEGIN
  OPEN Records_ref FOR 
  SELECT comp.DO_ORDER_ID ,
    do.JOB_ID ,
    do.MON ,
    comp.CLEAR_DATE ,
    comp.CLEAR_TIME ,
    comp.CMPREMARKS ,
    comp.CUSTOMER_CONTACT_INDR ,
    comp.CUSTOMER_CONTACT_NAME ,
    comp.DOORTAGLEFT_INDR ,
    comp.REASON_NODOORTAG ,
    comp.ARRIVAL_DATE ,
    comp.ARRIVAL_TIME ,
    comp.JWM_DATE ,
    comp.JWM_TIME ,
    comp.JWM_CODE ,
    comp.JWM_INDR ,
    comp.JWM_CAUSE ,
    comp.REFER_TO_ENGINEERING ,
    comp.ENGINEERING_REMARKS ,
    comp.CONTACT_NAME ,
    comp.CONTACT_NUMBER ,
    comp.TEMP_PLACED ,
    comp.PRECUT_DROP_LENGTH ,
    comp.TEMP_DROP_REMARKS ,
    comp.ONT_PLACED ,
    comp.DROP_TERMINATED ,
    comp.DROP_TYPE ,
    comp.DROP_LENGTH ,
    comp.NO_DROP_REMARKS ,
    comp.NUM_BORES ,
    comp.TOTAL_BORE_LENGTH ,
    comp.ONT_LOCATION ,
    comp.ONT_LOCATION_REMARKS ,
    comp.CABLE_LOCATE ,
    comp.BORE_TYPE ,
    comp.BORE_TYPE_NUMBER ,
    comp.BORE_TYPE_LENGTH ,
    comp.TRIP_CHARGE ,
    comp.PULL_THROUGH_LENGTH ,
    comp.MAP_POLYGON_JSON,
    imagesInfo.TECH_ID ,
    imagesInfo.IMAGE_CATEGORY_ID ,
    imagesInfo.COMMENTS ,
    imagesInfo.LATITUDE ,
    imagesInfo.LONGITUDE ,
    imagesInfo.IMAGE_TIME_STAMP ,
    images.IMAGE_BINARY ,
    images.IMAGE_THUMBNAIL_BINARY
    
  FROM vdw_completion comp
  INNER JOIN do_order do ON do.do_order_id = comp.do_order_id
  LEFT JOIN vdw_images_info imagesInfo ON imagesInfo.do_order_id = comp.do_order_id
  LEFT JOIN vdw_images images ON images.image_id = imagesInfo.image_id
  WHERE (
    (
      p_vendorOrTech = 'VENDOR' AND comp.do_order_id IN
      (
        SELECT do_order_id
        FROM do_order_extn doe
        WHERE doe.vdw_vendor_id = p_userId
      )
    )
    OR
    (
      p_vendorOrTech = 'TECH' AND comp.do_order_id IN
      (
        SELECT do_order_id
        FROM do_order do
        WHERE do.tech_ec = p_userId
      )
    )
  );

EXCEPTION 
WHEN OTHERS THEN
v_Errmsg := ' Exception ' || SUBSTR(SQLERRM, 1,500);
WBNEXCEPTIONLOGGING.EXCEPTIONLOGGING(SQLCODE, v_Errmsg, 'SP_VDW_GET_COMPLETION');
DBMS_OUTPUT.PUT_LINE('v_Errmsg :: ' || v_Errmsg);
OPEN Records_ref FOR select 'Exception occured in stored proc SP_VDW_GET_COMPLETION' FROM DUAL;  
  
END SP_VDW_GET_COMPLETION;

/