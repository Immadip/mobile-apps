CREATE TABLE WBNCP.VDW_Images_Info (
  Image_Id NUMBER(15,0),
  Tech_Id VARCHAR2(5) NOT NULL,
  Do_Order_Id NUMBER(15,0) NOT NULL,
  Image_Category_Id NUMBER(10,0) NOT NULL,
  Comments VARCHAR2(500),
  Latitude VARCHAR2(40),
  Longitude VARCHAR2(40),
  Image_Time_Stamp TIMESTAMP(6),
  Last_Updated_Date_Time TIMESTAMP(6),
  Created_Date_Time TIMESTAMP(6),
  CONSTRAINT Pk_VDW_Images_Info PRIMARY KEY(Image_Id),
  CONSTRAINT Fk_VDW_Images_Info_Do_Order FOREIGN KEY(Do_Order_Id) REFERENCES do_order(do_order_id),
  CONSTRAINT Fk_VDW_Images_Info_Tech_Id FOREIGN KEY(Tech_Id) REFERENCES VDW_Profile_Technician(Tech_Id),
  CONSTRAINT Fk_VDW_Images_Info_Categ_Id FOREIGN KEY(Image_Category_Id) REFERENCES VDW_Images_Categories(CATOGORY_ID)
);

CREATE SEQUENCE Vdw_Images_Info_Seq
START WITH     1
INCREMENT BY   1
NOCACHE
NOCYCLE;

