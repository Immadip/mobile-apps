CREATE TABLE WBNCP.VDW_Images (
  Image_Id NUMBER(15,0),
  Image_Binary BLOB,
  Image_Thumbnail_Binary BLOB,
  Last_Updated_Date_Time TIMESTAMP(6),
  CONSTRAINT Pk_VDW_Images PRIMARY KEY(Image_Id),
  CONSTRAINT Fk_VDW_Images_ImgId FOREIGN KEY(Image_Id) REFERENCES VDW_Images_Info(Image_Id) ON DELETE CASCADE
);
