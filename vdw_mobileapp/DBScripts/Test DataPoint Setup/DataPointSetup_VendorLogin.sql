-- These are the 10 DataPoints:
-- XBPAZUKX00	1118996	Allen Turing
-- XBPA0VKX00	1118997	Vishal Sikka
-- XBNJE1KX00	1119301	Paul Allen
-- XBVASRI110	1121787	Bill Gates
-- XBDC8E1510	1122608	Graham Bell
-- EBMA630200	1130461	Steve Jobs
-- EBMA530200	1130462	Steve Balmer
-- EBMA5M1200	1130961	Mark Zuckerberg
-- EBMAPTH200	1139974	Marrisa Mayer
-- EBMAS4K200	1140656	Sundar Pichai

UPDATE do_order_extn
SET VDW_VENDOR_ID = '', vdw_dispatch_datetime = ''
WHERE do_order_id IN (1121787, 1130461, 1130462, 1139974, 1130961, 1140656, 1119301, 1122608, 1118996, 1118997);
COMMIT;

--Run this Anonymous block before the Demo
BEGIN
  --Set Up Customer Addresses and Names
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '2310 North Harold Avenue, Tampa, FL 33607', Customer_Name = 'Bill Gates' where djd.DO_ORDER_ID = 1121787;
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '11500 Summit West Boulevard, Tampa, FL 33617', Customer_Name = 'Steve Jobs' where djd.DO_ORDER_ID = 1130461;
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '5109 East Fowler Avenue, Tampa, FL 33617', Customer_Name = 'Steve Balmer' where djd.DO_ORDER_ID = 1130462;
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '10620 Davis Road Tampa, FL 33637', Customer_Name = 'Marrisa Mayer' where djd.DO_ORDER_ID = 1139974;
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '10613 Raulerson Ranch Road, Tampa, FL 33637', Customer_Name = 'Mark Zuckerberg' where djd.DO_ORDER_ID = 1130961;
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '5659-5679 East Sligh Avenue, Tampa, FL 33617', Customer_Name = 'Sundar Pichai' where djd.DO_ORDER_ID = 1140656;
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '13115 Sanctuary Cove Drive, Temple Terrace, FL 33637', Customer_Name = 'Paul Allen' where djd.DO_ORDER_ID = 1119301;
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '4209 East Sewaha Street, Tampa, FL 33617', Customer_Name = 'Graham Bell' where djd.DO_ORDER_ID = 1122608;
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '8609 North 10th Street, Tampa, FL 33604', Customer_Name = 'Allen Turing' where djd.DO_ORDER_ID = 1118996;
  UPDATE do_job_details djd set djd.CUSTOMER_ADDRESS = '9019 Copeland Road, Tampa, FL 33637', Customer_Name = 'Vishal Sikka' where djd.DO_ORDER_ID = 1118997;
  
  UPDATE do_order do SET do.Customer_Name = 'Bill Gates', do.MON='MA65XBVA09878', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+2) || ' 08:00') WHERE do.DO_ORDER_ID = 1121787;
  UPDATE do_order do SET do.Customer_Name = 'Steve Jobs', do.MON='MA76EBMA09075', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+2) || ' 08:30') WHERE do.DO_ORDER_ID = 1130461;
  UPDATE do_order do SET do.Customer_Name = 'Steve Balmer', do.MON='MA56EBMA0929', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+2) || ' 08:10') WHERE do.DO_ORDER_ID = 1130462;
  UPDATE do_order do SET do.Customer_Name = 'Marrisa Mayer', do.MON='MA09EBMA09001', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+3) || ' 09:45') WHERE do.DO_ORDER_ID = 1139974;
  UPDATE do_order do SET do.Customer_Name = 'Mark Zuckerberg', do.MON='MA76EBMA08775', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+3) || ' 09:55') WHERE do.DO_ORDER_ID = 1130961;
  UPDATE do_order do SET do.Customer_Name = 'Sundar Pichai', do.MON='MA76EBMA05434', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+1) || ' 09:10') WHERE do.DO_ORDER_ID = 1140656;
  UPDATE do_order do SET do.Customer_Name = 'Paul Allen', do.MON='MA06XBNJ09023', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+2) || ' 10:50') WHERE do.DO_ORDER_ID = 1119301;
  UPDATE do_order do SET do.Customer_Name = 'Graham Bell', do.MON='MA78XBDC06535', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+1) || ' 11:00') WHERE do.DO_ORDER_ID = 1122608;
  UPDATE do_order do SET do.Customer_Name = 'Allen Turing', do.MON='MA98XBPA07359', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+2) || ' 11:00') WHERE do.DO_ORDER_ID = 1118996;
  UPDATE do_order do SET do.Customer_Name = 'Vishal Sikka', do.MON='MA72XBPA09038', do.commit_date_time=TO_TIMESTAMP(TO_CHAR(SYSDATE+3) || ' 12:30') WHERE do.DO_ORDER_ID = 1118997;
  
  --Deleting all the Notification table records
  DELETE FROM Vdw_Notifications;
  
  --Blank out the tech_ec Field (Assigning to the Technician is done from the Mobile UI)
  UPDATE do_order
  SET tech_ec = ''
  WHERE do_order_id IN (1121787, 1130461, 1130462, 1139974, 1130961, 1140656, 1119301, 1122608, 1118996, 1118997);
   
  --Assigning 8 Jobs to the Vendor, Pushing Data to the Notifications Table
	UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1118996') WHERE do_order_id = '1118996';
  INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
  values (110,'NEPVC','You have a new pending job XBPAZUKX00 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '08.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
  UPDATE do_order SET job_status ='PRE' WHERE job_id ='XBPAZUKX00';
  COMMIT;
  
	UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1118997') WHERE do_order_id = '1118997';
  INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
  values (111,'NEPVC','You have a new pending job XBPA0VKX00 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '08.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
  UPDATE do_order SET job_status ='PRE' WHERE job_id ='XBPA0VKX00';
  COMMIT;
  
	UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1119301') WHERE do_order_id = '1119301';
  INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
  values (112,'NEPVC','You have a new pending job XBNJE1KX00 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '08.30.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
  UPDATE do_order SET job_status ='PRE' WHERE job_id ='XBNJE1KX00';
  COMMIT;
  
	UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1121787') WHERE do_order_id = '1121787';
  INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
  values (113,'NEPVC','You have a new pending job XBVASRI110 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '08.30.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
  UPDATE do_order SET job_status ='PRE' WHERE job_id ='XBVASRI110';
  COMMIT;
  
	UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1122608') WHERE do_order_id = '1122608';
  INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
  values (114,'NEPVC','You have a new pending job XBDC8E1510 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '08.30.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
  UPDATE do_order SET job_status ='PRE' WHERE job_id ='XBDC8E1510';
  COMMIT;
  
	UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1130461') WHERE do_order_id = '1130461';
  INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
  values (115,'NEPVC','You have a new pending job EBMA630200 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '09.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
  UPDATE do_order SET job_status ='PRE' WHERE job_id ='EBMA630200';
  COMMIT;
  
	UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1130462') WHERE do_order_id = '1130462';
  INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
  values (116,'NEPVC','You have a new pending job EBMA530200 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '09.30.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
  UPDATE do_order SET job_status ='PRE' WHERE job_id ='EBMA530200';
  COMMIT;
  
	UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1130961') WHERE do_order_id = '1130961';
  INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
  values (117,'NEPVC','You have a new pending job EBMA5M1200 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '09.30.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
  UPDATE do_order SET job_status ='PRE' WHERE job_id ='EBMA5M1200';
  COMMIT;
  
  --Clean up for the Demo Data Points
	UPDATE do_order_extn SET vdw_vendor_id = '', vdw_dispatch_datetime = '' WHERE do_order_id = '1139974';
	UPDATE do_order_extn SET vdw_vendor_id = '', vdw_dispatch_datetime = '' WHERE do_order_id = '1140656';
  UPDATE do_order SET job_status='PLD' WHERE do_order_id = '1139974';
  UPDATE do_order SET job_status='PLD' WHERE do_order_id = '1140656';	  
  
  DELETE FROM vdw_completion WHERE do_order_id IN (1121787, 1130461, 1130462, 1139974, 1130961, 1140656, 1119301, 1122608, 1118996, 1118997);
  DELETE FROM vdw_images WHERE image_id IN (SELECT image_id FROM vdw_images_info WHERE do_order_id IN (1121787, 1130461, 1130462, 1139974, 1130961, 1140656, 1119301, 1122608, 1118996, 1118997));
  DELETE FROM vdw_images_info WHERE do_order_id IN (1121787, 1130461, 1130462, 1139974, 1130961, 1140656, 1119301, 1122608, 1118996, 1118997);
  
  COMMIT;
END;

--Run this each time After Pushing a set of Notifications to the Mobile (To Avoid sending the same notification again and again)
BEGIN
  UPDATE Vdw_Notifications SET IS_PUSHED_FLAG = 'Y';
  COMMIT;
END;

--To Be Assigned during Demo
UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1139974') WHERE do_order_id = '1139974';
DELETE FROM Vdw_Notifications WHERE NOTIFICATION_ID = 118;
INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
values (118,'NEPVC','You have a new pending job EBMAPTH200 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '10.00.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
UPDATE do_order set job_status ='PRE' where job_id ='EBMAPTH200';
COMMIT;

--To Be Assigned during Demo (Second Data Point, Just as a Backup)
UPDATE do_order_extn SET vdw_vendor_id = 'NEPVC', vdw_dispatch_datetime = (SELECT (do.commit_date_time - 4) FROM do_order do WHERE do.do_order_id = '1140656') WHERE do_order_id = '1140656';
DELETE FROM Vdw_Notifications WHERE NOTIFICATION_ID = 119;
INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
values (119,'NEPVC','You have a new pending job EBMAS4K200 in your list.','New Job in pending list','JOB_ASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '10.30.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
UPDATE do_order set job_status ='PRE' where job_id ='EBMAS4K200';
COMMIT;

--To Be UnAssigned during Demo
UPDATE do_order_extn SET vdw_vendor_id = '', vdw_dispatch_datetime = '' WHERE do_order_id = '1130961';
DELETE FROM Vdw_Notifications WHERE NOTIFICATION_ID = 120;
INSERT INTO Vdw_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TEXT,NOTIFICATION_TITLE,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG)
values (120,'NEPVC','A job EBMA5M1200 has been removed from your Pending list','Job removed from pending list','JOB_UNASSIGNED',to_timestamp(TO_CHAR(SYSDATE) || '10.40.00.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),SYSTIMESTAMP,'N', 'N');
UPDATE do_order set job_status ='PLD' WHERE do_order_id = '1130961';
COMMIT;