create or replace PROCEDURE WBNCP.SP_VDW_INSERT_ASSIGN_TECHN(
  p_TechId VARCHAR2,
  p_JobId VARCHAR2
) 
AS 
 v_Errmsg varchar2(1500);
 v_TechId varchar2(20);
 v_CustomerName varchar(50);
BEGIN

SELECT TECH_EC INTO v_TechId FROM DO_ORDER WHERE JOB_ID = p_JobId;
SELECT CUSTOMER_NAME INTO v_CustomerName FROM DO_ORDER WHERE JOB_ID = p_JobId;


IF p_TechId IS Not NULL then
  update do_order set TECH_EC = p_TechId where job_id = p_JobId;
  Insert into VDW_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TITLE,NOTIFICATION_TEXT,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG) 
  values (VDW_NOTIFICATION_ID_SEQ.nextval,p_TechId,'New Job assigned','You have been assigned a New Job ' ||p_JobId || '. Customer Name: ' || v_CustomerName,'JOB_ASSIGNED',SYSTIMESTAMP,SYSTIMESTAMP,'N', 'N');
  commit;

ELSE 
  update do_order set TECH_EC = p_TechId where job_id = p_JobId;
  Insert into VDW_Notifications (NOTIFICATION_ID,USER_ID,NOTIFICATION_TITLE,NOTIFICATION_TEXT,NOTIFICATION_TYPE,NOTIFICATION_RECEIVED_TIME,LAST_UPDATED_DATE_TIME,HAS_READ_FLAG, IS_PUSHED_FLAG) 
  values (VDW_NOTIFICATION_ID_SEQ.nextval,v_TechId,'A Job was Un-assigned','You have been Un-assigned from the Job ' ||p_JobId ,'JOB_UNASSIGNED',SYSTIMESTAMP,SYSTIMESTAMP,'N', 'N');
  commit;
END IF;

EXCEPTION 
WHEN OTHERS THEN
v_Errmsg := ' Exception ' || SUBSTR(SQLERRM, 1,500);
WBNEXCEPTIONLOGGING.EXCEPTIONLOGGING(SQLCODE, v_Errmsg, 'SP_VDW_INSERT_ASSIGN_TECHN');
DBMS_OUTPUT.PUT_LINE('v_Errmsg :: ' || v_Errmsg);

END SP_VDW_INSERT_ASSIGN_TECHN;

/