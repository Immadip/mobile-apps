create or replace PROCEDURE                    SP_VDW_INSERT_DEVICEINFO
(
  pDevice_Id                    VDW_DeviceInfo.UNIQUE_DEVICE_ID%type,
  pUser_Id                      VDW_DeviceInfo.USER_ID%type,
  pMobile_OS                    VDW_DeviceInfo.DEVICE_OPERATING_SYSTEM%type,
  pDevice_Token                 VDW_DeviceInfo.DEVICE_TOKEN%type
)
AS
  v_Errmsg varchar2(1500);
  BEGIN
  
    MERGE INTO VDW_DeviceInfo devinfo
        USING (SELECT pDevice_Id AS Device_Id, pUser_Id AS User_Id, pMobile_OS AS Mobile_Os, pDevice_Token AS Device_Token FROM DUAL) params
        ON (params.Device_Id = devinfo.Unique_Device_Id AND params.User_Id = devinfo.User_Id)
      WHEN MATCHED THEN
        UPDATE SET devinfo.Device_Token = params.Device_Token, devinfo.LAST_UPDATED_DATE_TIME = SYSTIMESTAMP
      WHEN NOT MATCHED THEN
        INSERT(UNIQUE_DEVICE_ID, USER_ID, DEVICE_OPERATING_SYSTEM, LAST_UPDATED_DATE_TIME, CREATED_DATE_TIME, DEVICE_TOKEN)
        VALUES(params.Device_Id, params.User_Id, params.Mobile_Os, SYSTIMESTAMP, SYSTIMESTAMP, params.Device_Token);
   
    COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    v_Errmsg := ' Exception ' || pUser_Id || '-' ||  SUBSTR(SQLERRM, 1,500);
    WBNEXCEPTIONLOGGING.EXCEPTIONLOGGING(SQLCODE, v_Errmsg, 'SP_VDW_INSERT_DEVICEINFO');
    DBMS_OUTPUT.PUT_LINE('v_Errmsg :: ' || v_Errmsg);
    
END SP_VDW_INSERT_DEVICEINFO;