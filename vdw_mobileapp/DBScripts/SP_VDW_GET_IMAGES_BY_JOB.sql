create or replace PROCEDURE                           WBNCP.SP_VDW_GET_IMAGES_BY_JOB(
	p_job_id do_order.job_id%TYPE,
	Records_ref OUT SYS_REFCURSOR)
AS
v_ErrMsg VARCHAR2(1500);
BEGIN

  OPEN Records_ref FOR
  
SELECT 	
			doOrder.job_id AS Job_Id,
			doOrder.mon AS Mon,
			doOrder.customer_name AS Customer_Name,
			imageInfo.*,
      image.*,
      Imagecategory.Catogory_Discription AS Image_Category_Description
			
  FROM Vdw_Images_Info imageInfo
  INNER JOIN Vdw_Images image ON image.Image_Id = imageInfo.Image_Id
  INNER JOIN Vdw_Images_Categories imageCategory ON imageCategory.Catogory_Id = imageInfo.Image_Category_Id
  INNER JOIN do_order doOrder ON doOrder.do_order_id = imageInfo.do_order_id
  WHERE doOrder.job_id = p_job_id;
			
EXCEPTION 
WHEN OTHERS THEN
v_ErrMsg := ' Exception ' ||  SUBSTR(SQLERRM, 1,500);
WBNEXCEPTIONLOGGING.EXCEPTIONLOGGING(SQLCODE, v_ErrMsg, 'SP_VDW_GET_IMAGES_BY_JOB');
DBMS_OUTPUT.PUT_LINE('v_ErrMsg  ' ||  v_ErrMsg);
OPEN Records_ref FOR SELECT 'Exception occurred in stored procedure SP_VDW_GET_IMAGES_BY_JOB ' ||  v_ErrMsg AS Error FROM DUAL;

END SP_VDW_GET_IMAGES_BY_JOB;

/