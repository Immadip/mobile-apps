-- Insert the Default mandatory category Map Screenshot Category. It has been used in Code as CategoryId = 5
INSERT INTO WBNCP.VDW_IMAGES_CATEGORIES
(CATOGORY_ID, CATOGORY_DISCRIPTION) VALUES
(5, 'Map Screenshot');

-- Insert the Default Global Parameters
INSERT INTO WBNCP.DO_GLOBAL_PARAMETERS
(NAME,REGION,VALUE,DESCRIPTION) VALUES
('VDW_MOBILE_IMG_CATEGORYVALIDATION','ALL','N','Determines value of Y is for Category and Image is mandatory while completing the job');