CREATE TABLE WBNCP.VDW_Notifications (
  Notification_Id NUMBER(15,0),
  User_Id VARCHAR2(10) NOT NULL,
  Notification_Title VARCHAR2(100),
  Notification_Text VARCHAR2(100),
  Notification_Type VARCHAR2(20),
  Notification_Received_Time TIMESTAMP(6),
  Last_Updated_Date_Time TIMESTAMP(6),
  Has_Read_Flag VARCHAR2(1),
  Is_Pushed_Flag VARCHAR2(1),
  CONSTRAINT Pk_Vdw_Notifications PRIMARY KEY(Notification_Id)
);

CREATE SEQUENCE  WBNCP.VDW_NOTIFICATION_ID_SEQ
MINVALUE 1
MAXVALUE 999999999999999999999999999
INCREMENT BY 1
START WITH 502
CACHE 20
NOORDER
NOCYCLE;

CREATE TABLE WBNCP.VDW_DeviceInfo (
  Unique_Device_Id VARCHAR2(100),
  User_Id VARCHAR2(10) NOT NULL,
  Device_Operating_System VARCHAR2(20),
  Device_Token VARCHAR2(200 BYTE),
  Last_Updated_Date_Time TIMESTAMP(6),
  Created_Date_Time TIMESTAMP(6),
  CONSTRAINT Pk_Vdw_DeviceInfo PRIMARY KEY(Unique_Device_Id, User_Id)
);

CREATE TABLE WBNCP.VDW_Profile_Technician (
  Tech_Id VARCHAR2(5),
  Tech_First_Name VARCHAR2(50) NOT NULL,
  Tech_Last_Name VARCHAR2(50),
  Tech_Mobile_Number VARCHAR2(20),
  Tech_Alternate_Mobile_Number VARCHAR2(20),
  Vendor_Id VARCHAR2(5) NOT NULL,
  Last_Updated_Date_Time TIMESTAMP(6),
  Created_Date_Time TIMESTAMP(6),
  CONSTRAINT Pk_Profile_VendTech PRIMARY KEY(Tech_Id)
);

CREATE OR REPLACE VIEW V_VDW_DeviceInfo_Notification AS
SELECT  notif.USER_ID USER_ID, 
        notif.NOTIFICATION_ID NOTIFICATION_ID,
        notif.NOTIFICATION_TITLE NOTIFICATION_TITLE,
        notif.NOTIFICATION_TEXT NOTIFICATION_TEXT,
        notif.NOTIFICATION_TYPE NOTIFICATION_TYPE,
        notif.NOTIFICATION_RECEIVED_TIME NOTIFICATION_RECEIVED_TIME,
        notif.HAS_READ_FLAG,
        notif.IS_PUSHED_FLAG,
        
        devInfo.UNIQUE_DEVICE_ID UNIQUE_DEVICE_ID,
        devInfo.DEVICE_OPERATING_SYSTEM DEVICE_OPERATING_SYSTEM,
        devInfo.DEVICE_TOKEN DEVICE_TOKEN,
        devInfo.LAST_UPDATED_DATE_TIME LAST_UPDATED_DATE_TIME_DEVINFO
        
FROM VDW_Notifications notif
INNER JOIN VDW_DeviceInfo devInfo ON devInfo.User_Id = notif.User_Id;