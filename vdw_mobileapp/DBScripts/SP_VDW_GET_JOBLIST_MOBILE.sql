create or replace PROCEDURE       WBNCP.SP_VDW_GET_JOBLIST_MOBILE( 
    p_vendorId  do_order_extn.VDW_VENDOR_ID%type, 
    Records_ref OUT sys_refcursor )
AS
  v_Errmsg varchar2(1500);
BEGIN
  OPEN Records_ref FOR 
  SELECT do_order.JOB_ID,
  do_order.DO_ORDER_ID,
  do_order.mon, 
  do_order.job_type, 
  do_order.JOB_STATUS,
  do_order.COMMIT_DATE_TIME,
  do_order.CUSTOMER_NAME,
  DO_ORDER.Wire_center,
  DO_ORDER.FTTP_CORE_TYPE,
  do_order.tech_ec, 
  do_order_extn.ALT_CBR_NUMBER,
  do_order_extn.vdw_dispatch_datetime, 
  do_order_extn.vdw_vendor_id, 
  do_job_details.CUSTOMER_ADDRESS,
  vdw_order_extn.VDW_REMARKS,
  vdw_order_extn.CUSTOMER_STATUS_REMARKS

FROM do_order_extn 
INNER JOIN do_order ON do_order.DO_ORDER_ID = do_order_extn.DO_ORDER_ID 
INNER JOIN do_job_details ON do_job_details.DO_ORDER_ID = do_order_extn.DO_ORDER_ID 
INNER JOIN vdw_order_extn ON vdw_order_extn.DO_ORDER_ID = do_order_extn.DO_ORDER_ID 
WHERE do_order.Tech_EC= p_vendorId
AND job_status IS NOT NULL;

EXCEPTION 
WHEN OTHERS THEN
v_Errmsg := ' Exception ' || SUBSTR(SQLERRM, 1,500);
WBNEXCEPTIONLOGGING.EXCEPTIONLOGGING(SQLCODE, v_Errmsg, 'SP_VDW_GET_JOBLIST_MOBILE');
DBMS_OUTPUT.PUT_LINE('v_Errmsg :: ' || v_Errmsg);
OPEN Records_ref FOR select 'Exception occured in stored proc SP_VDW_GET_JOBLIST_MOBILE' FROM DUAL;  
  
END SP_VDW_GET_JOBLIST_MOBILE;

/